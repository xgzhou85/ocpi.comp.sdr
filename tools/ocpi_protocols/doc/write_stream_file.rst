.. User guide for ocpi_protocols.WriteStreamFile

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _write_stream_file:

Write stream file (``ocpi_protocols.WriteStreamFile``)
======================================================
``WriteStreamFile`` is for reading files in the same format used by OpenCPI's file write component when ``messagesinfile`` is set to false (i.e. a stream data file). If a messages file is to be written then the :doc:`messages writer <write_messages_file>` should be used instead.

``WriteStreamFile`` is expected to be used as a context manager using the ``with`` keyword in a similar way to the ``open()`` function. For example:

.. code-block:: python

   import ocpi_protocols

   with ocpi_protocols.WriteStreamFile("~/data.file", "short") as protocol_file:
       protocol_file.write([1, 2, 3, 4, 5])


Structure
---------
.. autoclass:: ocpi_protocols.WriteStreamFile
   :members:
