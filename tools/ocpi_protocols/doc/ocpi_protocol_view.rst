.. User guide for ocpi_protocol_view command line instruction

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _ocpi_protocol_view:


Timed samples protocols viewer (``ocpi_protocol_view``)
=======================================================
To use ``ocpi_protocol_view`` on the command line, commands have the form:

.. code-block:: bash

   ocpi_protocol_view [OPTIONS] PROTOCOL FILE [FILES]

Where:

 * ``PROTOCOL`` is the timed samples protocol type of the file(s) being viewed.

 * ``FILE`` is the file path to the file being viewed.

 * ``[FILES]`` optional argument(s) to list additional files that wish to be viewed side-by-side.

The available options are:

+-------------------------------------------------------+---------------------+---------------+
| Option                                                | Long format         | Short format  |
+=======================================================+=====================+===============+
| Show message(s)                                       | ``--message INDEX`` | ``-m INDEX``  |
+-------------------------------------------------------+---------------------+---------------+
| Number of messages to show                            | ``--number NUMBER`` | ``-n NUMBER`` |
| (can only be used when ``--message`` / ``-m`` is set) |                     |               |
+-------------------------------------------------------+---------------------+---------------+
| Show from sample                                      | ``--sample INDEX``  | ``-s INDEX``  |
| (can only be used when ``--message`` / ``-m`` is set) |                     |               |
+-------------------------------------------------------+---------------------+---------------+
| Number of samples to show                             | ``--points NUMBER`` | ``-p NUMBER`` |
| (can only be used when ``--sample`` / ``-s`` is set)  |                     |               |
+-------------------------------------------------------+---------------------+---------------+
| Set custom sample print format                        | ``--format FORMAT`` | ``-f FORMAT`` |
| (can only be used when ``--message`` / ``-m`` is set) |                     |               |
+-------------------------------------------------------+---------------------+---------------+
| Display help                                          | ``--help``          | ``-h``        |
+-------------------------------------------------------+---------------------+---------------+


Typical use cases
-----------------

 * To see what messages are in a file by inspecting the headers: ``ocpi_protocol_view PROTOCOL FILE``.

 * To view the messages in two (or more) files by viewing their headers side-by-side: ``ocpi_protocol_view PROTOCOL FILE_1 FILE2``.

 * To inspect a single message, here message 2 (zero indexed), in a file: ``ocpi_protocol_view --message 2 PROTOCOL FILE``. When a stream message this will show all samples in the requested message.

 * To compare a particular indexed message, here message 2 (zero indexed), from two (or more) files: ``ocpi_protocol_view --message 2 PROTOCOL FILE_1 FILE_2``.

 * To view several messages, here 3 messages from message index 1, of a file (or files): ``ocpi_protocol_view --message 1 --number 3 PROTOCOL FILE_PATH``.

 * To view set samples of a message, here 15 samples from sample index 100 (zero indexed) of message 4 (zero indexed), from a file (or files): ``ocpi_protocol_view --message 4 --sample 100 --points 15 PROTOCOL FILE``.

 * To change the formatting used for displaying samples: ``ocpi_protocol_view --message 3 --format {sample:027} PROTOCOL FILE_1 FILE_2``.

 * To display the help message: ``ocpi_protocol_view --help``.
