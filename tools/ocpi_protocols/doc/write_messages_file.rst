.. User guide for ocpi_protocols.WriteMessagesFile

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Write message file (``ocpi_protocols.WriteMessagesFile``)
=========================================================
``WriteMessageFile`` is for writing files in the same format used by OpenCPI's file write component when ``messagesinfile`` is set to true. If a stream file is to be written then the :doc:`stream writer <write_stream_file>` should be used instead.

``WriteMessagesFile`` is expected to be used as a context manager using the ``with`` keyword in a similar way to the ``open()`` function. The attribute ``headers`` and ``messages_data`` can be iterated through and indexed to read the messages file. For example:

.. code-block:: python

   import ocpi_protocols

   with ocpi_protocols.WriteMessagesFile("~/data.file", "short") as protocol_file:
       protocol_file.write_message("time", 1.25)

       protocol_file.write_dict_messages([
           {"opcode": "flush", "data": None},
           {"opcode": "stream", "data": [1, 2, 3, 4, 5]])


Structure
---------
.. autoclass:: ocpi_protocols.WriteMessagesFile
   :members:
