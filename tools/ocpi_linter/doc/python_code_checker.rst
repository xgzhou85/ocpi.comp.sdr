.. Python code checker outline

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Python code checker (``ocpi_linter.PythonCodeChecker``)
=======================================================
Formatter and code checker for Python source code files.

Checking of a file will normally be done by using the command line instruction ``ocpilint``, when a Python file is detected this code checker will be used.

Tests
-----

.. autoclass:: ocpi_linter.python_code_checker.PythonCodeChecker
   :members:
