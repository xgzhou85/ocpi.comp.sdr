#!/usr/bin/env python3

# Test code in python_code_checker.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import pathlib
import unittest

from ocpi_linter.python_code_checker import PythonCodeChecker


class TestPythonCodeChecker(unittest.TestCase):
    def setUp(self):
        self.test_file_path = pathlib.Path("test_file.py")
        self.test_file_path.touch()

    def tearDown(self):
        if self.test_file_path.is_file():
            os.remove(self.test_file_path)

    # Linter test 000 and, 001 are not tested here since they use external code
    # checkers and formatters and it is assumed those are tested separately.

    def test_py_002_pass(self):
        code_sample = (
            "#!/usr/bin/env python3\n" +
            "\n" +
            "# Brief description\n" +
            "#\n" +
            "# This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "# distributed with this source distribution.\n" +
            "#\n" +
            "# This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "#\n" +
            "# OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "# terms of the GNU Lesser General Public License as " +
            "published by the Free\n" +
            "# Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "# later version.\n" +
            "#\n" +
            "# OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "# WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "# A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "# more details.\n" +
            "#\n" +
            "# You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "# along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "\n" +
            "This would be the start of the main file.")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = PythonCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_py_002()

        self.assertEqual([], issues)

    def test_py_002_fail_no_header(self):
        code_sample = "A single line of text"
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = PythonCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_py_002()

        self.assertEqual(1, len(issues))

    def test_py_002_fail_license_notice_typo(self):
        code_sample = (
            "#!/usr/bin/env python3\n" +
            "\n" +
            "# Brief description\n" +
            "#\n" +
            "# This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "# distributed with this source distribution.\n" +
            "#\n" +
            "# This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "#\n" +
            "# OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "# terms of the GNU Lesser General Public License as " +
            "published by the Free typo\n" +
            "# Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "# later version.\n" +
            "#\n" +
            "# OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "# WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "# A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "# more details.\n" +
            "#\n" +
            "# You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "# along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "\n" +
            "This would be the start of the main file.")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = PythonCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_py_002()

        self.assertEqual(1, len(issues))
        self.assertEqual(11, issues[0]["line"])

    def test_py_002_fail_no_blank_line_after_header(self):
        code_sample = (
            "#!/usr/bin/env python3\n" +
            "\n" +
            "# Brief description\n" +
            "#\n" +
            "# This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "# distributed with this source distribution.\n" +
            "#\n" +
            "# This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "#\n" +
            "# OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "# terms of the GNU Lesser General Public License as " +
            "published by the Free\n" +
            "# Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "# later version.\n" +
            "#\n" +
            "# OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "# WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "# A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "# more details.\n" +
            "#\n" +
            "# You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "# along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "This would be the start of the main file.")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = PythonCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_py_002()

        self.assertEqual(1, len(issues))
        self.assertEqual(22, issues[0]["line"])

    def test_py_003_pass(self):
        code_sample = ("This is a line of text\n" +
                       "\n" +
                       "A line with a \"double quote\".\n" +
                       "print(\"A single quote ' in a string\")\n" +
                       "\"\"\" A single quote ' in a docsrting \"\"\"\n" +
                       "print(\"Many\", \"strings\")")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = PythonCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_py_003()

        self.assertEqual([], issues)

    def test_py_003_fail(self):
        code_sample = ("This is a line of text\n" +
                       "\n" +
                       "A line with a 'single quote'.\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = PythonCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_py_003()

        self.assertEqual(1, len(issues))
        self.assertEqual(3, issues[0]["line"])

    def test_py_004_pass(self):
        code_sample = (
            "This is a line of text\n" +
            "\n" +
            "# Comment with complex literal pattern 0j\n" +
            "# Next line includes valid use of letter j\n" +
            "jump()\n" +
            "# Next lines have complex literal pattern in string\n" +
            "print(\"1 + 1.2j\")\n" +
            "print('1 + 5j')\n" +
            "\"\"\" Now a docstring 1.6j\n" +
            "   This is still docstring 1 + 2j\n" +
            "\"\"\"\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = PythonCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_py_004()

        self.assertEqual([], issues)

    def test_py_004_fail(self):
        code_sample = ("# This is a line of text\n" +
                       "\n" +
                       "value = 1 + 2j\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = PythonCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_py_004()

        self.assertEqual(1, len(issues))
        self.assertEqual(3, issues[0]["line"])

    def test_exception(self):
        code_sample = (
            "#!/usr/bin/env python3\n" +
            "\n" +
            "# Brief description\n" +
            "#\n" +
            "# This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "# distributed with this source distribution.\n" +
            "#\n" +
            "# This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "#\n" +
            "# OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "# terms of the GNU Lesser General Public License as " +
            "published by the Free\n" +
            "# Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "# later version.\n" +
            "#\n" +
            "# OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "# WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "# A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "# more details.\n" +
            "#\n" +
            "# You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "# along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "\n" +
            "if __name__ == \"__main__\":\n" +
            "    # LINT EXCEPTION: py_004: 2: Allow complex literal.\n" +
            "    # Another comment between lint exception and code\n" +
            "    complex_ = 5j\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = PythonCodeChecker(self.test_file_path)
        completed_tests = code_checker.lint(quick_check=True)

        self.assertEqual(0, completed_tests["py_004"]["issue_count"])

    def test_remove_comments_and_strings(self):
        test_text = (
            "This line should stay\n" +
            "  This line should stay\n" +
            "  This part of this line should stay    # This part should go\n" +
            "This part of this line should stay \"This part should go\"\n" +
            "This part of this line should stay 'This part should go'\n" +
            "\"This part should go\" This part of this line should stay\n" +
            "'This part should go' This part of this line should stay\n" +
            "  Stay \"Go\" Stay \"Go\" Stay\n" +
            "  Stay 'Go' Stay 'Go' Stay\n" +
            "This part should stay \"\"\"This part should go\"\"\"\n" +
            "This part should stay\n" +
            "\"\"\"This part should go\n" +
            "This line should go\n" +
            "This line should go \"\"\"\n" +
            "This line should stay\n" +
            "  \"\\n\" This should stay \n" +
            "\"String with # symbol\" This should stay\n")

        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(test_text)

        code_checker = PythonCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        reduced_code = code_checker._remove_comments_and_strings()

        expected_text = [
            "This line should stay",
            "  This line should stay",
            "  This part of this line should stay    ",
            "This part of this line should stay \"\"",
            "This part of this line should stay ''",
            "\"\" This part of this line should stay",
            "'' This part of this line should stay",
            "  Stay \"\" Stay \"\" Stay",
            "  Stay '' Stay '' Stay",
            "This part should stay ",
            "This part should stay",
            "",
            "",
            "",
            "This line should stay",
            "  \"\" This should stay ",
            "\"\" This should stay",
            ""]

        self.assertEqual(expected_text, reduced_code)
