#!/usr/bin/env python3

# Test code in lint_log_handler.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import json
import pathlib
import shutil
import unittest
import uuid

from ocpi_linter.utilities import LintLogHandler


class TestLintLogHandler(unittest.TestCase):
    def setUp(self):
        # Create a temporary test file structure
        self._test_directory_base = pathlib.Path("/tmp").joinpath(
            f"ocpi_lint_test-{uuid.uuid4()}")
        self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath("a_component.rcc").mkdir(
            parents=True)
        self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath("a_component.hdl").mkdir()
        self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath("a_component.test").mkdir()
        self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.comp").mkdir(parents=True)
        self._test_directory_base.joinpath("proj").joinpath("hdl").joinpath(
            "primitives").joinpath("proj_dsp").joinpath("a_primitive").joinpath(
            "src").mkdir(parents=True)
        self._test_directory_base.joinpath("proj").joinpath(".project").touch()

    def tearDown(self):
        # Delete the test directory structure
        shutil.rmtree(self._test_directory_base)

    def test_cpp_worker_file(self):
        test_file = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath("a_component.rcc").joinpath(
            "a_component.cc")

        LintLogHandler(test_file)

        # Confirm a blank linting log has been made
        expected_log_path = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.comp").joinpath("lint_log.json")

        self.assertTrue(expected_log_path.exists())
        with open(expected_log_path, "r") as log_file:
            self.assertEqual(
                {"components/dsp/a_component.rcc/a_component.cc": {}},
                json.load(log_file))

    def test_hdl_worker_file(self):
        test_file = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath("a_component.hdl").joinpath(
            "a_component.vhd")

        LintLogHandler(str(test_file))

        # Confirm a blank linting log has been made
        expected_log_path = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.comp").joinpath("lint_log.json")

        self.assertTrue(expected_log_path.exists())
        with open(expected_log_path, "r") as log_file:
            self.assertEqual(
                {"components/dsp/a_component.hdl/a_component.vhd": {}},
                json.load(log_file))

    def test_component_test_file(self):
        test_file = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.test").joinpath("generate.py")

        LintLogHandler(str(test_file))

        # Confirm a blank linting log has been made
        expected_log_path = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.comp").joinpath("lint_log.json")

        self.assertTrue(expected_log_path.exists())
        with open(expected_log_path, "r") as log_file:
            self.assertEqual(
                {"components/dsp/a_component.test/generate.py": {}},
                json.load(log_file))

    def test_component_documentation_file(self):
        test_file = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.comp").joinpath("a_component-index.rst")

        LintLogHandler(str(test_file))

        # Confirm a blank linting log has been made
        expected_log_path = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.comp").joinpath("lint_log.json")

        self.assertTrue(expected_log_path.exists())
        with open(expected_log_path, "r") as log_file:
            self.assertEqual(
                {"components/dsp/a_component.comp/a_component-index.rst": {}},
                json.load(log_file))

    def test_component_specification_file(self):
        test_file = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath("specs").joinpath(
            "a_component.xml")

        LintLogHandler(str(test_file))

        # Confirm a blank linting log has been made
        expected_log_path = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.comp").joinpath("lint_log.json")

        self.assertTrue(expected_log_path.exists())
        with open(expected_log_path, "r") as log_file:
            self.assertEqual(
                {"components/dsp/specs/a_component.xml": {}},
                json.load(log_file))

    def test_primitive_vhdl_file(self):
        test_file = self._test_directory_base.joinpath("proj").joinpath(
            "hdl").joinpath("primitives").joinpath("proj_dsp").joinpath(
            "a_primitive").joinpath("src").joinpath("a_primitive.vhd")

        LintLogHandler(str(test_file))

        # Confirm a blank linting log has been made
        expected_log_path = self._test_directory_base.joinpath("proj").joinpath(
            "hdl").joinpath("primitives").joinpath("proj_dsp").joinpath(
            "a_primitive").joinpath("lint_log.json")

        self.assertTrue(expected_log_path.exists())
        with open(expected_log_path, "r") as log_file:
            self.assertEqual(
                {"hdl/primitives/proj_dsp/a_primitive/src/a_primitive.vhd": {}},
                json.load(log_file))

    def test_primitive_documentation_file(self):
        test_file = self._test_directory_base.joinpath("proj").joinpath(
            "hdl").joinpath("primitives").joinpath("proj_dsp").joinpath(
            "a_primitive").joinpath("a_primitive-index.rst")

        LintLogHandler(str(test_file))

        # Confirm a blank linting log has been made
        expected_log_path = self._test_directory_base.joinpath("proj").joinpath(
            "hdl").joinpath("primitives").joinpath("proj_dsp").joinpath(
            "a_primitive").joinpath("lint_log.json")

        self.assertTrue(expected_log_path.exists())
        with open(expected_log_path, "r") as log_file:
            self.assertEqual(
                {"hdl/primitives/proj_dsp/a_primitive/a_primitive-index.rst":
                 {}},
                json.load(log_file))

    def test_invalid_file(self):
        lint_log = LintLogHandler(
            self._test_directory_base.joinpath("random_file.txt"))
        self.assertIsNone(lint_log._log_path)

    def test_update_test_result(self):
        test_file = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath("a_component.rcc").joinpath(
            "a_component.cc")

        lint_log = LintLogHandler(test_file)
        lint_log.record("000", "test", 0)

        # Testing updating the same record
        another_lint_log = LintLogHandler(test_file)
        another_lint_log.record("000", "test", 2)

        expected_log_path = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.comp").joinpath("lint_log.json")

        self.assertTrue(expected_log_path.exists())
        with open(expected_log_path, "r") as log_file:
            test_log = json.load(log_file)
        relative_path = "components/dsp/a_component.rcc/a_component.cc"
        # As some elements in the test log (e.g. time) change for each case
        # test items in the test log individually rather than checking whole
        # log is the same as some expected case.
        self.assertEqual(test_log[relative_path]["000"]["result"], "FAILED")
        self.assertIn("date", test_log[relative_path]["000"])
        self.assertIn("time", test_log[relative_path]["000"])
        self.assertIn("commit_id", test_log[relative_path]["000"])

    def test_store_multiple_results(self):
        test_file = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath("a_component.rcc").joinpath(
            "a_component.cc")

        lint_log = LintLogHandler(test_file)
        lint_log.record("000", "test", 0)
        lint_log.record("001", "test", 1)

        expected_log_path = self._test_directory_base.joinpath("proj").joinpath(
            "components").joinpath("dsp").joinpath(
            "a_component.comp").joinpath("lint_log.json")

        self.assertTrue(expected_log_path.exists())
        with open(expected_log_path, "r") as log_file:
            test_log = json.load(log_file)
        relative_path = "components/dsp/a_component.rcc/a_component.cc"
        # As some elements in the test log (e.g. time) change for each case
        # test items in the test log individually rather than checking whole
        # log is the same as some expected case.
        self.assertEqual(test_log[relative_path]["000"]["result"], "PASSED")
        self.assertIn("date", test_log[relative_path]["000"])
        self.assertIn("time", test_log[relative_path]["000"])
        self.assertIn("commit_id", test_log[relative_path]["000"])
        self.assertEqual(test_log[relative_path]["001"]["result"], "FAILED")
        self.assertIn("date", test_log[relative_path]["001"])
        self.assertIn("time", test_log[relative_path]["001"])
        self.assertIn("commit_id", test_log[relative_path]["001"])

    def test_get_file_commit_id(self):
        lint_log = LintLogHandler(__file__)
        self.assertEqual(40, len(lint_log._get_file_commit_id()))
