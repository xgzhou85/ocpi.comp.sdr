#!/usr/bin/env python3

# Test code in project_lint_log_handler.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import unittest

from ocpi_linter.utilities import ProjectLintLogHandler


class TestProjectLintLogHandler(unittest.TestCase):
    def test_write(self):
        log_file = "/tmp/TestProjectLintLogHandler.log"

        log_handler = ProjectLintLogHandler(log_file)
        log_handler.write("/home/user/file.log", "004", "A test", 2)

        with open(log_file, "r") as file_handler:
            log_text = file_handler.read()

        self.assertEqual("/home/user/file.log: 004, A test, 2 issues\n",
                         log_text)

        os.remove(log_file)
