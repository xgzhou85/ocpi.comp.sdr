#!/usr/bin/env python3

# Test code in cpp_code_checker.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import pathlib
import unittest

from ocpi_linter.cpp_code_checker import CppCodeChecker


class TestCppCodeChecker(unittest.TestCase):
    def setUp(self):
        self.test_file_path = pathlib.Path("test_file.cc")
        self.test_file_path.touch()

    def tearDown(self):
        if self.test_file_path.is_file():
            os.remove(self.test_file_path)

    # Linter test 000, 001 and 002 are not tested here since they use external
    # code checkers and formatters and it is assumed those are tested
    # separately.

    def test_cpp_003_pass(self):
        code_sample = (
            "// Brief description\n" +
            "//\n" +
            "// This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "// distributed with this source distribution.\n" +
            "//\n" +
            "// This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "//\n" +
            "// OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "// terms of the GNU Lesser General Public License as published " +
            "by the Free\n" +
            "// Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "// later version.\n" +
            "//\n" +
            "// OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "// WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "// A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "// more details.\n" +
            "//\n" +
            "// You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "// along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "\n" +
            "This would be the start of the main file.")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_003()

        self.assertEqual([], issues)

    def test_cpp_003_fail_no_header(self):
        code_sample = "A single line of text"
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_003()

        self.assertEqual(1, len(issues))

    def test_cpp_003_fail_license_notice_typo(self):
        code_sample = (
            "// Brief description\n" +
            "//\n" +
            "// This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "// distributed with this source distribution.\n" +
            "//\n" +
            "// This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "//\n" +
            "// OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "// terms of the GNU Lesser General Public License as published " +
            "by the Free typo\n" +
            "// Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "// later version.\n" +
            "//\n" +
            "// OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "// WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "// A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "// more details.\n" +
            "//\n" +
            "// You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "// along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "\n" +
            "This would be the start of the main file.")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_003()

        self.assertEqual(1, len(issues))
        self.assertEqual(9, issues[0]["line"])

    def test_cpp_003_fail_no_blank_line_after_header(self):
        code_sample = (
            "// Brief description\n" +
            "//\n" +
            "// This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "// distributed with this source distribution.\n" +
            "//\n" +
            "// This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "//\n" +
            "// OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "// terms of the GNU Lesser General Public License as published " +
            "by the Free\n" +
            "// Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "// later version.\n" +
            "//\n" +
            "// OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "// WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "// A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "// more details.\n" +
            "//\n" +
            "// You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "// along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "This would be the start of the main file.\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_003()

        self.assertEqual(1, len(issues))
        self.assertEqual(20, issues[0]["line"])

    def test_cpp_004_pass(self):
        code_sample = (
            "// A single line comment which should be allowed.\n" +
            "Using the symbols that are in block comments like * and /.")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_004()

        self.assertEqual([], issues)

    def test_cpp_004_fail(self):
        code_sample = (
            "// A single line comment which should be allowed.\n" +
            "Using the symbols that are in block comments like * and /.\n" +
            "/* Some block comment which should be detected.*/\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_004()

        self.assertEqual(1, len(issues))
        self.assertEqual(3, issues[0]["line"])

    def test_cpp_005_pass(self):
        code_sample = ("This is a line of text\n" +
                       "\n" +
                       "Another line.\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_005()

        self.assertEqual([], issues)

    def test_cpp_005_fail(self):
        code_sample = (
            "// A single line comment\n" +
            "\n" +
            "#define SOME_VARIABLE 5\n" +
            "\n" +
            "// Some comment.")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_005()

        self.assertEqual(1, len(issues))
        self.assertEqual(3, issues[0]["line"])

    def test_cpp_006_pass(self):
        code_sample = ("#include <iostream>\n" +
                       "#include \"some_user_file\"\n" +
                       "\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_006()

        self.assertEqual([], issues)

    def test_cpp_006_fail(self):
        code_sample = (
            "#include <stdio.h>\n" +
            "\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_006()

        self.assertEqual(1, len(issues))
        self.assertEqual(1, issues[0]["line"])

    def test_cpp_007_pass(self):
        code_sample = ("#include <cmath>\n" +
                       "#include \"some_user_file\"\n" +
                       "\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_007()

        self.assertEqual([], issues)

    def test_cpp_007_fail(self):
        code_sample = (
            "#include <iostream>\n" +
            "\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_007()

        self.assertEqual(1, len(issues))
        self.assertEqual(1, issues[0]["line"])

    def test_cpp_008_pass(self):
        code_sample = (
            "// Brief description\n" +
            "//\n" +
            "// This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "// distributed with this source distribution.\n" +
            "//\n" +
            "// This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "//\n" +
            "// OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "// terms of the GNU Lesser General Public License as published " +
            "by the Free\n" +
            "// Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "// later version.\n" +
            "//\n" +
            "// OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "// WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "// A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "// more details.\n" +
            "//\n" +
            "// You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "// along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "\n" +
            "#include <cmath>\n" +
            "\n" +
            "int main() {\n" +
            "  if (cos(0.3) < 1) {\n" +
            "    return 1;\n" +
            "  } else {\n" +
            "    return 0;\n" +
            "  }\n" +
            "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_008()

        self.assertEqual([], issues)

    def test_cpp_008_fail_no_header(self):
        code_sample = (
            "#include <cmath>\n" +
            "\n" +
            "int main() {\n" +
            "  if (cos(0.3) < 1) {\n" +
            "    return 1;\n" +
            "  } else {\n" +
            "    return 0;\n" +
            "  }\n" +
            "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_008()

        self.assertEqual(1, len(issues))
        self.assertEqual(1, issues[0]["line"])

    def test_cpp_008_fail_no_blank_lines_after_header(self):
        code_sample = (
            "// Brief description\n" +
            "//\n" +
            "// This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "// distributed with this source distribution.\n" +
            "//\n" +
            "// This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "//\n" +
            "// OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "// terms of the GNU Lesser General Public License as published " +
            "by the Free\n" +
            "// Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "// later version.\n" +
            "//\n" +
            "// OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "// WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "// A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "// more details.\n" +
            "//\n" +
            "// You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "// along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "#include <cmath>\n" +
            "\n" +
            "int main() {\n" +
            "  if (cos(0.3) < 1) {\n" +
            "    return 1;\n" +
            "  } else {\n" +
            "    return 0;\n" +
            "  }\n" +
            "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_008()

        self.assertEqual(1, len(issues))
        self.assertEqual(20, issues[0]["line"])

    def test_cpp_008_fail_include_late_in_code(self):
        code_sample = (
            "// Brief description\n" +
            "//\n" +
            "// This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "// distributed with this source distribution.\n" +
            "//\n" +
            "// This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "//\n" +
            "// OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "// terms of the GNU Lesser General Public License as published " +
            "by the Free\n" +
            "// Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "// later version.\n" +
            "//\n" +
            "// OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "// WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "// A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "// more details.\n" +
            "//\n" +
            "// You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "// along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "\n" +
            "int main() {\n" +
            "  #include <cmath>\n" +
            "  if (cos(0.3) < 1) {\n" +
            "    return 1;\n" +
            "  } else {\n" +
            "    return 0;\n" +
            "  }\n" +
            "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_008()

        self.assertEqual(1, len(issues))
        self.assertEqual(22, issues[0]["line"])

    def test_cpp_009_pass(self):
        code_sample = ("int main(void) {\n" +
                       "  return RCC_FATAL;\n" +
                       "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_009()

        self.assertEqual([], issues)

    def test_cpp_009_fail(self):
        code_sample = ("int main(void) {\n" +
                       "  return RCC_ERROR;\n" +
                       "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_009()

        self.assertEqual(1, len(issues))
        self.assertEqual(2, issues[0]["line"])

    def test_cpp_010_pass(self):
        code_sample = ("int main(void) {\n" +
                       "  setError();"
                       "  return RCC_FATAL;\n" +
                       "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_010()

        self.assertEqual([], issues)

    def test_cpp_010_fail(self):
        code_sample = ("int main(void) {\n" +
                       "  return RCC_FATAL;\n" +
                       "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_010()

        self.assertEqual(1, len(issues))

    def test_cpp_011_pass(self):
        code_sample = ("class Some_exampleWorker\n" +
                       ": public Some_exampleWorkerase {\n" +
                       "}\n" +
                       "\n" +
                       "int main(void) {\n" +
                       "  return 0;\n" +
                       "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_011()

        self.assertEqual([], issues)

    def test_cpp_011_fail_non_worker_class(self):
        code_sample = ("class some_class {\n" +
                       "  public:\n" +
                       "    some_value;\n" +
                       "}\n" +
                       "\n" +
                       "int main(void) {\n" +
                       "  return RCC_ERROR;\n" +
                       "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_011()

        self.assertEqual(1, len(issues))
        self.assertEqual(1, issues[0]["line"])

    def test_cpp_011_fail_two_worker_classes(self):
        code_sample = ("class Some_exampleWorker\n" +
                       ": public Some_exampleWorkerase {\n" +
                       "}\n" +
                       "\n" +
                       "class Some_other_exampleWorker\n" +
                       ": public Some_other_exampleWorkerase {\n" +
                       "}\n" +
                       "\n" +
                       "int main(void) {\n" +
                       "  return 0;\n" +
                       "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_011()

        self.assertEqual(1, len(issues))
        self.assertEqual(5, issues[0]["line"])

    def test_cpp_012_pass(self):
        code_sample = ("int main(void) {\n" +
                       "  uint8_t *some_memory = new uint8_t[10];\n" +
                       "  delete[] some_memory;\n" +
                       "  some_memory = NULL;"
                       "  return 0;\n" +
                       "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_012()

        self.assertEqual([], issues)

    def test_cpp_012_fail(self):

        code_sample = ("int main(void) {\n" +
                       "  uint8_t *some_memory = malloc(10);\n" +
                       "  free(some_memory);\n" +
                       "  some_memory = NULL;"
                       "  return 0;\n" +
                       "}\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_cpp_012()

        self.assertEqual(2, len(issues))
        self.assertEqual(2, issues[0]["line"])
        self.assertEqual(3, issues[1]["line"])

    def test_exception(self):
        code_sample = (
            "// Brief description\n" +
            "//\n" +
            "// This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "// distributed with this source distribution.\n" +
            "//\n" +
            "// This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "//\n" +
            "// OpenCPI is free software: you can redistribute it and/or " +
            "modify it under the\n" +
            "// terms of the GNU Lesser General Public License as published " +
            "by the Free\n" +
            "// Software Foundation, either version 3 of the License, or " +
            "(at your option) any\n" +
            "// later version.\n" +
            "//\n" +
            "// OpenCPI is distributed in the hope that it will be useful, " +
            "but WITHOUT ANY\n" +
            "// WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS FOR\n" +
            "// A PARTICULAR PURPOSE. See the GNU Lesser General Public " +
            "License for\n" +
            "// more details.\n" +
            "//\n" +
            "// You should have received a copy of the GNU Lesser General " +
            "Public License\n" +
            "// along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>.\n" +
            "\n" +
            "// A single line comment\n" +
            "\n" +
            "// LINT EXCEPTION: cpp_005: 1: Allow this define.\n"
            "#define SOME_VARIABLE 5\n" +
            "\n" +
            "// Some comment.\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = CppCodeChecker(self.test_file_path)
        completed_tests = code_checker.lint(quick_check=True)

        self.assertEqual(0, completed_tests["cpp_005"]["issue_count"])
