#!/usr/bin/env python3

# Parent class for language specific code checkers
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import datetime
import pathlib
import re
import shutil

from . import utilities


class BaseCodeChecker:
    """ Parent class for the language specific code checkers
    """

    def __init__(self, path):
        """ Initialise a BaseCodeChecker instance

        Args:
            path (str): The file to be linted.

        Returns:
            Initialised BaseCodeChecker instance.
        """
        self.path = pathlib.Path(path).expanduser().resolve()

    def lint(self, quick_check=False):
        """ Run all tests on file

        Tests are defined in child classes which inherit from this class. All
        tests should be methods which have a name that starts ``test`` or
        ``user_test``.

        Args:
            quick_check (bool, optiona): When set to True user prompted
                questions are not done. Default is False which means user
                prompted questions are implemented.

        Returns:
            A dictionary of the result of the tests that have been run.
        """
        self._read_in_code()

        completed_tests = {}

        for attribute in sorted(dir(self)):
            if attribute[0:4] == "test" and callable(getattr(self, attribute)):
                test_number = attribute[5:]
                a_test = getattr(self, attribute)
                test_name, issues = a_test()

                # Remove any issues that are identified as allowed exceptions
                if test_number in self._allowed_exceptions:
                    issues = [issue for issue in issues if issue["line"]
                              not in self._allowed_exceptions[test_number]]

                if len(issues) > 0:
                    self._print_issues(test_number, test_name, issues)

                completed_tests[test_number] = {
                    "name": test_name,
                    "issue_count": len(issues)}

        if not quick_check:
            for attribute in sorted(dir(self)):
                if attribute[0:9] == "user_test" and \
                        callable(getattr(self, attribute)):
                    test_number = int(attribute[-3:])
                    a_test = getattr(self, attribute)
                    test_name, test_passed = a_test()

                    if test_passed:
                        completed_tests[test_number] = {"name": test_name,
                                                        "issue_count": 0}
                    else:
                        completed_tests[test_number] = {"name": test_name,
                                                        "issue_count": 1}

        return completed_tests

    def _read_in_code(self):
        """ Read source file into local variable

        Each line in the source code file is an element in the list stored in
        ``self._code``.
        """
        self._code = []
        with open(self.path, "r") as source_file:
            # Add new line so all blank line at end of file is read in
            self._code = (source_file.read() + "\n").splitlines()

        self._allowed_exceptions = {}
        for line_number, line in enumerate(self._code):
            line = line.lstrip()
            # Regular expression pattern to match any line starting with
            # comment symbol (// for C++, # for Python, .. for Restructured
            # Text, -- for VHDL and <!-- for XML), then single space, then the
            # string "LINT EXCEPTION: "
            if re.match(r"^((\/\/)|#|(\.\.)|(--)|(<!--)) LINT EXCEPTION: ",
                        line):
                try:
                    test_number, line_offset = line.split(":")[1:3]
                except ValueError:
                    self._print_issues(
                        "N/A", "LINT EXCEPTION FORMAT",
                        f"Line {line_number + 1}: Linter exception format " +
                        "incorrect.")
                    continue

                test_number = test_number.strip().lower()
                exception_line = line_number + 1 + int(line_offset.strip())
                if test_number in self._allowed_exceptions:
                    self._allowed_exceptions[test_number].append(
                        exception_line)
                else:
                    self._allowed_exceptions[test_number] = [exception_line]

    def _check_installed(self, program_name):
        """ Check if a program is installed

        Args:
            program_name (str): The name, used at the command line, of the
                program to be checked whether installed or not.

        Returns:
            Boolean of True if the program is installed, False otherwise.
        """
        if shutil.which(program_name) is None:
            print(utilities.PrintStyle.BOLD + utilities.PrintStyle.YELLOW +
                  f"WARNING: {program_name} not installed and so cannot be " +
                  "used as part of the linter.\n" +
                  utilities.PrintStyle.NORMAL +
                  f"  It is recommend you install {program_name} and re-run " +
                  "the linter.")
            return False
        else:
            return True

    def _print_issues(self, test_number, test_name, issues):
        """ Print the issues found to the terminal.

        Args:
            test_number (str): The test number that found these issues.
            test_name (str): The name of the tests that found these issues.
            issues (list): The list of issues to be printed.
        """
        if len(issues) > 0:
            print(
                utilities.PrintStyle.BOLD + utilities.PrintStyle.UNDERLINE +
                utilities.PrintStyle.RED +
                f"Test {test_number} ({test_name}) output:" +
                utilities.PrintStyle.NORMAL)
            for issue in issues:
                if issue["line"] is not None:
                    print(f"{self.path}:{issue['line']}: "
                          + issue["message"].strip())
                else:
                    print(f"{self.path}: {issue['message'].strip()}")
