.. Outlines testing verifier class

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Verifier (``ocpi_testing.Verifier``)
====================================
Coordinates the verification tests applied to an implementation-under-test's output, to determine if the implementation-under-test passes or fails the test. Specifically it:

 * Uses a (Python) reference implementation to generate an output to compare to the output from the implementation-under-test.

 * Reads in the implementation-under-test's output from file.

 * Calls the relevant comparison method class and initiates the comparison class' checks on the data.

 * Logs the test result to the documentation directory for that component (if the documentation directory is present).

 * Returns ``True`` or ``False`` to indicate if the test passed or failed, respectively.

An example of using ``ocpi_testing.Verifier`` is :doc:`here <example_verify>`.


Reference output
----------------
The output of the (Python) reference implementation is saved as part of the verifier. This is saved to in the platform directory under ``run`` of the test directory, with the file extension ``.reference``.

The reference output is a messages-in-file format file.


Comparison methods
------------------
Comparison methods are the way the (Python) reference output is compared to the implementation-under-test output to determined if the messages from each are the same and if the test passes. For multiple output port components different comparison methods can be used on each output port.

The provided comparison methods are:

.. toctree::
   :maxdepth: 1

   equal_comparison_method
   bounded_comparison_method
   bounded_with_exception_comparison_method
   relative_comparison_method
   statistical_comparison_method

A special comparison method called ``basic_compare`` is provided which is inherited by all specific comparison methods. The checks included in ``basic_compare`` are:

 * That the message opcodes are the same between the reference and implementation-under-test outputs.

 * That the message lengths are the same between the reference and implementation-under-test outputs.

However ``basic_compare`` but does not check the data in messages is valid, as this is done by the specific comparison methods which inherit ``basic_compare``.

Implementing a custom comparison method
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A custom comparison method can be used. This should inherit from ``ocpi_testing._comparison_methods.BasicComparison`` as this will provide checking that the same number and type of messages have been returned by both the (Python) reference and the implementation-under-test.

The actual comparison method should override and define its own ``same()`` method. This should take two arguments ``reference_values`` and ``implementation_values``. ``reference_values`` is the output values from the (Python) reference. ``implementation_values`` is the output values from the implementation-under-test.

Class structure
---------------
.. autoclass:: ocpi_testing.Verifier
   :members:
