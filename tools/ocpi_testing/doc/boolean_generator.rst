.. Outlines testing boolean generator

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Boolean generator (``ocpi_testing.generator.BooleanGenerator``)
===============================================================
Generate input data for testing of components with a boolean protocol input port.

Since there is no equivalent to a sinusoidal wave for a boolean input, the typical test case generates a random stream of true and false samples.

Generator parameters
--------------------
The following variables, with their default values, are inherited into ``BooleanGenerator`` and set the properties of the messages generated.

These variables have the same variable name in the class instance, once declared, as defined below. Therefore to modify one of these variables would require a code pattern such as:

.. code-block:: python

   import ocpi_testing

   generator = ocpi_testing.generator.BooleanGenerator()
   generator.SAMPLE_DATA_LENGTH = 512

The following variables are set as part of the initialisation of a ``BooleanGenerator`` instance.

.. autoattribute:: ocpi_testing.generator.boolean_generator.BooleanGeneratorDefaults.MESSAGE_SIZE_LONGEST
   :noindex:

The following variables are inherited into ``BooleanGenerator``.

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SAMPLE_DATA_LENGTH
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.MESSAGE_SIZE_SHORTEST
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.MESSAGE_SIZE_NUMBER_OF_MESSAGES
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.MESSAGE_SIZE_MAX_NUMBER_OF_MESSAGES
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.TIME_MIN
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.TIME_MAX
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SAMPLE_INTERVAL_MIN
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SAMPLE_INTERVAL_MAX
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.METADATA_ID_MAX
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.METADATA_VALUE_MAX
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SOAK_ALL_OPCODE_AVERAGE_NUMBER_OF_MESSAGES
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SOAK_ALL_OPCODE_STANDARD_DEVIATION_NUMBER_OF_MESSAGES
   :noindex:
