#!/usr/bin/env python3

# Example verify.py file
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from some_implementation import SomeImplementation


some_name_property = int(os.environ["OCPI_TEST_some_name"])

input_file = str(sys.argv[-1])
output_file = str(sys.argv[-2])

some_implementation = SomeImplementation(some_name_property)

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(some_implementation)
verifier.set_port_types(["short"], ["short"], ["equal"])
if verifier.verify(test_id, [input_file], [output_file]) is True:
    sys.exit(0)
else:
    sys.exit(1)
