.. Outlines testing unsigned short generator

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Unsigned short generator (``ocpi_testing.generator.UnsignedShortGenerator``)
============================================================================
Generate input data for testing of components with an unsigned short protocol input port.

Generator parameters
--------------------
The following variables, with their default values, are set when an instance of ``UnsignedShortGenerator`` is declared. These variables set the properties of the messages generated and so modifying them will change the properties of the messages generated.

These variables have the same variable name in the class instance, once declared, as defined below. Therefore to modify one of these variables would require a code pattern such as:

.. code-block:: python

   import ocpi_testing

   generator = ocpi_testing.generator.UnsignedShortGenerator()
   generator.SAMPLE_DATA_LENGTH = 512

The following variables are set as part of the initialisation of an ``UnsignedShortGenerator`` instance.

.. autoattribute:: ocpi_testing.generator.unsigned_short_generator.UnsignedShortGeneratorDefaults.UNSIGNED_SHORT_MINIMUM
   :noindex:

.. autoattribute:: ocpi_testing.generator.unsigned_short_generator.UnsignedShortGeneratorDefaults.UNSIGNED_SHORT_MAXIMUM
   :noindex:

.. autoattribute:: ocpi_testing.generator.unsigned_short_generator.UnsignedShortGeneratorDefaults.UNSIGNED_SHORT_MID
   :noindex:

.. autoattribute:: ocpi_testing.generator.unsigned_short_generator.UnsignedShortGeneratorDefaults.TYPICAL_AMPLITUDE_MEAN
   :noindex:

.. autoattribute:: ocpi_testing.generator.unsigned_short_generator.UnsignedShortGeneratorDefaults.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH
   :noindex:

.. autoattribute:: ocpi_testing.generator.unsigned_short_generator.UnsignedShortGeneratorDefaults.TYPICAL_MAXIMUM_AMPLITUDE
   :noindex:

.. autoattribute:: ocpi_testing.generator.unsigned_short_generator.UnsignedShortGeneratorDefaults.MESSAGE_SIZE_LONGEST
   :noindex:

The following variables are inherited into ``UnsignedShortGenerator``.

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SAMPLE_DATA_LENGTH
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.MESSAGE_SIZE_SHORTEST
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.MESSAGE_SIZE_NUMBER_OF_MESSAGES
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.MESSAGE_SIZE_MAX_NUMBER_OF_MESSAGES
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.TIME_MIN
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.TIME_MAX
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SAMPLE_INTERVAL_MIN
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SAMPLE_INTERVAL_MAX
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.METADATA_ID_MAX
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.METADATA_VALUE_MAX
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SOAK_ALL_OPCODE_AVERAGE_NUMBER_OF_MESSAGES
   :noindex:

.. autoattribute:: ocpi_testing.generator.base_generator.GeneratorDefaults.SOAK_ALL_OPCODE_STANDARD_DEVIATION_NUMBER_OF_MESSAGES
   :noindex:
