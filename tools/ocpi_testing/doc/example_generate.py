#!/usr/bin/env python3

# Example generate.py file
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os

import ocpi_protocols
import ocpi_testing


arguments = ocpi_testing.get_generate_arguments()

some_name_property = int(os.environ["OCPI_TEST_some_name"])
subcase = os.environ["OCPI_TEST_subcase"]

seed = ocpi_testing.get_test_seed(arguments.case, subcase, some_name_property,
                                  arguments.seed)

generator = ocpi_testing.generator.ShortGenerator()

messages = generator.generate(seed, arguments.case, subcase)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "short") as file_id:
    file_id.write_dict_messages(messages)
