.. Outlines OpenCPI testing helper library

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


OpenCPI testing (``ocpi_testing``)
==================================
``ocpi_testing`` is a Python module to aid testing of OpenCPI components.

.. toctree::
   :maxdepth: 1

   generator
   implementation
   verifier
   example_generate
   example_implementation
   example_verify
   helper_functions

Installing
----------
``ocpi_testing`` is written for and tested with Python 3.6 - newer versions may work, older versions are not expected to. Python must be installed before attempting to install ``ocpi_testing``.

To install ``ocpi_testing`` if you have ``pip3`` installed, from the ``ocpi_testing`` directory (which contains ``setup.py``) use the command:

.. code-block:: bash

   pip3 install --user ./

Alternatively if you do not have ``pip3`` installed Python can be used directly:

.. code-block:: bash

   python3 setup.py install

Depending on your set-up you may need to run the above command as ``sudo``.

Use
---
``ocpi_testing`` is used as a library, so accessed using ``import ocpi_testing`` in Python testing files. Each of the pages linked in the contents above outline more detail about how to use the functionality contained within ``ocpi_testing``.

Requirements
------------
``ocpi_testing`` requires:

 * Python 3.6.

 * ``ocpi_protocols`` for file read and write actions.

 * ``numpy`` for mathematical analysis.

Library testing
---------------
``ocpi_testing`` has been tested using the Python's ``unittest`` module. Tests are most easily run by using the following command in the top most ``ocpi_testing`` directory:

.. code-block:: python

   python3 -m unittest
