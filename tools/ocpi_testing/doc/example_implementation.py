#!/usr/bin/env python3

# Example Python reference implementation for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class test_component(ocpi_testing.Implementation):
    def __init__(self):
        super().__init__(self)

        # Change from default of one input port to two
        self.input_ports = ["input_1", "input_2"]

    def reset(self):
        pass

    def select_input(self, input_1, input_2):
        # Pull through message if not a stream, so that both ports get a stream
        # message being the next on each port.
        if input_1 != "stream" and input_1 is not None:
            return 0
        if input_2 != "stream" and input_2 is not None:
            return 1

        # Otherwise only stream messages or no messages on port.
        if input_1 is not None:
            return 0
        if input_2 is not None:
            return 1

    def stream(self, input_1, input_2):
        output_messages = []

        # Combine input data stream to the single output
        if input_1 is not None:
            output_messages.append({"opcode": "stream", "data": input_1})
        if input_2 is not None:
            output_messages.append({"opcode": "stream", "data": input_2})

        return self.output_formatter(output_messages)

    def sample_interval(self, input_1, input_2):
        # Ignore any sample interval messages on input_2
        if input_1 is not None:
            # Half the received sample interval (as data streams combined)
            return self.output_formatter(
                [{"opcode": "sample_interval", "data": input_1 / 2}])
