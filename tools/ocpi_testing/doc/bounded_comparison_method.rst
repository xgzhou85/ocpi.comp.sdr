.. Description of bounded comparison method

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _bounded_comparison:

Bounded comparison method
=========================
The bounded comparison method checks all data values in all messages are within some set bound error of a reference.

Bounded comparison method is intended for testing with integer output data, as there is a fixed resolution between adjacent values in the integer number system. Bounded comparison is generally not intended for testing with floating point output data, as the difference between adjacent values changes if the numbers have a large or small magnitude, in such cases `statistical comparison <statistical_comparison>`_ should be used; however it is noted some floating point output components may still use bounded comparison.

.. _bounded_comparison-diagram:

.. figure:: bounded_comparison.svg
   :alt: Outline of the range of allowed values for bounded comparison.
   :align: center

   Range of values allowed in relation to the reference for test to pass.

In :numref:`bounded_comparison-diagram` :math:`\delta` is the set bound value; the red crosses are the reference values and any value within the range of :math:`\delta` will pass the comparison test.

If testing floating point numbers and a positive infinity, negative infinity or not-a-number sample is encountered, this will pass if both the reference and implementation-under-test value are the same.


Parameters
----------

Bound
~~~~~
Used to control the width of the bound for comparison testing.

.. autoattribute:: ocpi_testing._comparison_methods.bounded.BoundedDefaults.BOUND
   :noindex:

Once initialised as a comparison method, as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].BOUND = 4

The above example is for the first output port, index ``0``.

Wrap round
~~~~~~~~~~

.. autoattribute:: ocpi_testing._comparison_methods.bounded.BoundedDefaults.WRAP_ROUND_VALUES
   :noindex:

.. _bounded_comparison_with_wrap-diagram:

.. figure:: bounded_comparison_with_wrap.svg
   :alt: Outline of the range of allowed values for bounded comparison with wrap.
   :align: center

   Range of values allowed, in relation to the reference, for test to pass.

When ``WRAP_ROUND_VALUES`` are set :numref:`bounded_comparison_with_wrap-diagram` shows how the bounds allow values near the limit of the ``WRAP_ROUND_VALUES``.

Once initialised, as a comparison method as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].WRAP_ROUND_VALUES = [-128, 127]

The above example is for the first output port, index ``0``.
