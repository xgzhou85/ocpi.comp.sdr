.. Description of statistical comparison method

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _statistical_comparison:

Statistical comparison method
=============================
Statistical comparison method checks all differences between all respective reference and implementation-under-test meet a set range of statistical rules.

Bounded comparison method is intended for testing with floating point output data. Statistical comparison is not intended for testing with integer output data in such cases `bounded comparison <bounded_comparison>`_ or `bounded comparison with exception <bounded_with_exception_comparison>`_ should be used.

.. _statistical_comparison-diagram:

.. figure:: statistical_comparison.svg
   :alt: Outline of how the differences between reference and implementation-under-test values are found.
   :align: center

In :numref:`statistical_comparison-diagram` the red crosses are the reference values and the blue crosses are the implementation-under-test values; :math:`\delta_i` are the difference values which the statistical analysis is completed on.

For the comparison method to pass the following conditions must be met:

 * The mean difference of all difference values must be less than ``MEAN_DIFFERENCE_LIMIT``.

 * The standard deviation of all difference values must be less than ``STANDARD_DEVIATION_LIMIT``.

 * No difference can be more than ``STANDARD_DEVIATION_MULTIPLE`` standard deviations from the mean difference.

    * If the bound of allowed differences does not extend over zero difference, the allowed difference bound will be extended to include zero difference (i.e. zero difference between reference and implementation should always pass).

 * ``MEAN_DIFFERENCE_LIMIT`` and ``STANDARD_DEVIATION_LIMIT`` must not be set to values that does not provide assurance in relation to the data size (e.g. cannot be so large in proportion to all the data values being considered).


Parameters
----------

Mean difference limit
~~~~~~~~~~~~~~~~~~~~~
.. autoattribute:: ocpi_testing._comparison_methods.statistical.StatisticalDefaults.MEAN_DIFFERENCE_LIMIT
   :noindex:

Once initialised as a comparison method, as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].MEAN_DIFFERENCE_LIMIT = 0.2

Above example is for the first output port, index ``0``.

Standard deviation limit
~~~~~~~~~~~~~~~~~~~~~~~~
.. autoattribute:: ocpi_testing._comparison_methods.statistical.StatisticalDefaults.STANDARD_DEVIATION_LIMIT
   :noindex:

Once initialised, as a comparison method as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].STANDARD_DEVIATION_LIMIT = 0.1

Above example is for the first output port, index ``0``.

Standard deviation multiple
~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. autoattribute:: ocpi_testing._comparison_methods.statistical.StatisticalDefaults.STANDARD_DEVIATION_MULTIPLE
   :noindex:

Once initialised as a comparison method, as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].STANDARD_DEVIATION_MULTIPLE = 3.5

Above example is for the first output port, index ``0``.
