.. Description of relative comparison method

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _relative_comparison:

Relative comparison method
==========================
The relative comparison method checks all data values in all messages are within some relative tolerance error of a reference, and that tolerance limit is at least some absolute minimum tolerance to allow values near zero to pass.

Relative comparison method uses Python's ``math.isclose`` function as the primary check that data is close.

If testing floating point numbers and a positive infinity, negative infinity or not-a-number sample is encountered, this will pass if both the reference and implementation-under-test value are the same.


Parameters
----------

Relative tolerance
~~~~~~~~~~~~~~~~~~
Used to control the allowed relative error between values.

.. autoattribute:: ocpi_testing._comparison_methods.relative.RelativeDefaults.RELATIVE_TOLERANCE
   :noindex:

Once initialised as a comparison method, as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].RELATIVE_TOLERANCE = 1e-10

The above example is for the first output port, index ``0``.

Absolute tolerance
~~~~~~~~~~~~~~~~~~
Used to control the minimum absolute allowed error between values.

.. autoattribute:: ocpi_testing._comparison_methods.relative.RelativeDefaults.ABSOLUTE_TOLERANCE
   :noindex:

Once initialised as a comparison method, as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].ABSOLUTE_TOLERANCE = 1e-10

The above example is for the first output port, index ``0``.
