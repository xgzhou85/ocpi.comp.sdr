.. Outlines testing implementation class

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Implementation (``ocpi_testing.Implementation``)
================================================
A possible testing approach is to use a reference (Python) implementation of the same functionality as the implementation-under-test to compare their outputs. This Python reference should use built-in Python functions wherever possible or recognised stable non-standard libraries (e.g. NumPy) - so the Python reference implementation is written in as "higher-level" language as possible.

The Python implementation of a component's function is expected to be a class. With the Python implementation class having methods for each possible opcode the input protocol supports. To aid writing these implementation classes, a base parent class for the specific Python implementation to inherit from is provided. This base class is ``ocpi_testing.Implementation``.

``ocpi_testing.Implementation`` has:

 * Methods for processing input messages, which the ``ocpi_testing.Verifier`` class expects and uses when verifying test output.

 * Methods for handling the most common ways non-sample opcode messages are processed by components. However, in more complex cases these may need to be overloaded by a specific implementation.

``ocpi_testing.Implementation`` has no method to handle sample opcode messages, as how sample data is handled is specific to each component and so must be defined by each component specific implementation. Additionally a ``reset()`` method must be written for each specific implementation; this method must put the reference implementation into the same state as first use. The ``reset()`` method is a testing artefact it does not need to replicate how a worker resets.

When defining the ``sample()`` method or overloading one of the other opcode message handling methods, some rules must be followed:

 * The input arguments represent the input ports to the component. For example, if the component has two input ports the specific method being written must accept two input arguments.

 * The order of positional arguments matches the order set in ``self.input_ports``. So for example if, ``self.input_ports = ["input_1", "input_2"]`` then the first argument to this specific implementation will be ``input_1`` and the second argument will be ``input_2``.

 * The returned output must be a named tuple. Where the names are as defined in ``self.output_ports`` and the order is the same order as defined in ``self.output_ports``. To assist with this, ``self.output_formatter()`` method is part of the ``Implementation`` class and will generate a correctly formatted named tuple.

 * Within the returned named tuple, each element which relates to each output port, must be a list of messages that are being outputted on that port. All messages are dictionaries with the keys ``opcode`` and ``data``. ``opcode``'s value must be a string of the opcode name, ``data``'s value must be the data related to the opcode - this should be ``None`` for opcodes which do not have any data fields.

An example of using ``ocpi_testing.Implementation`` is :doc:`here <example_implementation>`.

Managing ports
--------------
To change the number of input and output ports then the variables ``input_ports`` and ``output_ports`` must be changed. These are lists of strings, where the strings are the names of the input or output ports. The default values for these are:

.. code-block:: python

   ocpi_testing.Implementation.input_ports = ["input"]
   ocpi_testing.Implementation.output_ports = ["output"]

There must always be at least one ``output_port``.

No input ports
~~~~~~~~~~~~~~
When there are no input ports the ``sample()`` method must still be overloaded, but this can be set to ``pass``. On testing the method ``generate()`` will be called to generate the test output. If any arguments are needed when this is called (e.g. output data length) these can be set in the dictionary ``ocpi_testing.Implementation.no_input_settings``, and will be passed as keyword arguments when ``generate()`` is called.

Multiple input ports
~~~~~~~~~~~~~~~~~~~~
For multiple input port components the ``select_input()`` method must be overloaded to manage which port's data is next processed.

For multiple input port components, before each call of a method relating to an opcode, ``select_input()`` will be called. ``select_input()`` will get an input argument for each input port, which will be a string of the next opcode on that port. The implementation must then return the input port number which has the opcode of the message it would next like called. This allows, for example, non-sample opcode messages to be pulled through on ports until a sample opcode message is present on all input ports.

Structure
---------
Some details of selected ``ocpi_testing.Implementation`` members are given below. There are more members to ``ocpi_testing.Implementation``, however:

 #. Some are focused on interaction with ``ocpi_testing.Verifier`` and so not of interest when using ``ocpi_testing.Implementation``.

 #. All the non-sample opcodes are handled in similar ways and so not presented here, as doing so would be repetitive.

.. autoclass:: ocpi_testing.Implementation
   :members: sample, reset, generate, select_input, output_formatter
   :noindex:

As an example of how non-sample opcode messages are handled by default, the methods for time are shown below, all other non-sample sample opcode messages are handled in a similar way. If a different behaviour is needed this can be overloaded in the specific implementation.

.. autoclass:: ocpi_testing.Implementation
   :members: time, _time_default
   :noindex:
