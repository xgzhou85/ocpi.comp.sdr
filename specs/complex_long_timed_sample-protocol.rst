.. complex_long_timed_sample documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _complex_long_timed_sample-protocol:


Complex long timed sample protocol (``complex_long_timed_sample``)
==================================================================
Protocol for streamed complex long data samples, with time and metadata opcodes which are used to provide additional information about the sample data while maintaining alignment with samples in the streamed data.

For the abbreviation used in the sample type an input or output port supports, in component names, this protocol has the abbreviation ``xl``.

Protocol
--------
.. literalinclude:: complex_long_timed_sample-prot.xml
   :language: xml
   :lines: 1,19-

The included ``timed_sample_metadata-prot.xml`` protocol structure is:

.. literalinclude:: timed_sample_metadata-prot.xml
   :language: xml
   :lines: 1,19-
