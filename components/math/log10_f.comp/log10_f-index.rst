.. log10_f documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: log


.. _log10_f:


Log10 (``log10_f``)
===================
Calculates the logarithm to the base 10 of all input sample values.

Design
------
The mathematical representation of the implementation is given in :eq:`log10_f-equation`.

.. math::
   :label: log10_f-equation

   y[n] = \log10{x[n]}

In :eq:`log10_f-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

A block diagram representation of the implementation is given in :numref:`log10_f-diagram`.

.. _log10_f-diagram:

.. figure:: log10_f.svg
   :alt: Block diagram outlining log10 implementation.
   :align: center

   Log10 implementation.

Interface
---------
.. literalinclude:: ../specs/log10_f-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The logarithm operation is only applied to values in a sample opcode message. All other opcodes messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../log10_f.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``cmath``

Limitations
-----------
Limitations of ``log10_f`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
