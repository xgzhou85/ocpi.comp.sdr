#!/usr/bin/env python3

# Python implementation of square_scaled block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import ocpi_testing


class Square(ocpi_testing.Implementation):
    BITMASK_INT16 = 0xffff  # the bits valid for an int16_t type

    def __init__(self, scale_output):
        super().__init__(scale_output=scale_output)
        self._scale_shift = scale_output

    def reset(self):
        # It is a requirement for the verification classes that all
        # implementations have a reset function, however for square this
        # doesn't make much sense so just pass.
        pass

    def sample(self, values):
        input_values = np.array(values, dtype=np.int16)
        output_values = np.square(input_values, dtype=np.int32)
        scaled_output_values = np.right_shift(output_values, self._scale_shift)
        return self.output_formatter([{
            "opcode": "sample",
            "data":
            np.bitwise_and(
                scaled_output_values, self.BITMASK_INT16, dtype=np.int16)}
        ])
