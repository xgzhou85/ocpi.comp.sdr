.. abs_s documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _abs_s:


Absolute (``abs_s``)
====================
Finds magnitude of sample values.

Design
------
The mathematical representation of the implementation is given in :eq:`abs_s-equation`.

.. math::
   :label: abs_s-equation

   y[n] = |x[n]|


In :eq:`abs_s-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

A block diagram representation of the implementation is given in :numref:`abs_s-diagram`.

.. _abs_s-diagram:

.. figure:: abs_s.svg
   :alt: Block diagram of absolute component implementation.
   :align: center

   Absolute implementation.

Interface
---------
.. literalinclude:: ../specs/abs_s-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Absolute value is only calculated on data in sample opcode messages. All other opcode messages are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../abs_s.hdl ../abs_s.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``abs_s`` are:

 * For an input of the minimum value supported by a short (-32768) this component will return zero due to an overflow.

Testing
-------
.. ocpi_documentation_test_result_summary::
