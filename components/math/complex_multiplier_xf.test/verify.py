#!/usr/bin/env python3

# Runs checks for complex_multiplier_xf testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from complex_multiplier import ComplexMultiplier


input_1_file = str(sys.argv[-2])
input_2_file = str(sys.argv[-1])
output_file = str(sys.argv[-3])

complex_multiplier_implementation = ComplexMultiplier()

test_id = ocpi_testing.get_test_case()

# Check the result
verifier = ocpi_testing.Verifier(complex_multiplier_implementation)
verifier.set_port_types(
    ["complex_float_timed_sample", "complex_float_timed_sample"], ["complex_float_timed_sample"], ["relative"])
if verifier.verify(test_id, [input_1_file, input_2_file], [output_file]
                   ) is True:
    sys.exit(0)
else:
    sys.exit(1)
