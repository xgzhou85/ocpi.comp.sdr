#!/usr/bin/env python3

# Python implementation of complex multiplier for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import numpy

import ocpi_testing


class ComplexMultiplier(ocpi_testing.Implementation):
    def __init__(self):
        super().__init__()

        self.input_ports = ["input_1", "input_2"]

        self._input_1_data = []
        self._input_2_data = []

        self._output_message_lengths = []

    def reset(self):
        self._input_1_data = []
        self._input_2_data = []
        self._output_message_lengths = []

    def select_input(self, input_1, input_2):
        # Pull through message if not a stream, so that both ports get a stream
        # message being the next on each port.
        if input_1 != "sample" and input_1 is not None:
            return 0
        if input_2 != "sample" and input_2 is not None:
            return 1

        # Otherwise only stream messages or no messages on port.
        if input_1 is not None:
            return 0
        if input_2 is not None:
            return 1

    def sample(self, input_1, input_2):
        if input_1 is not None:
            self._input_1_data = self._input_1_data + list(input_1)
            self._output_message_lengths.append(len(input_1))
        if input_2 is not None:
            self._input_2_data = self._input_2_data + list(input_2)

        messages = []
        while len(self._output_message_lengths) > 0:
            # Check there is enough data on both inputs for output
            if len(self._input_1_data) >= self._output_message_lengths[0] and \
                    len(self._input_2_data) >= self._output_message_lengths[0]:
                output_length = self._output_message_lengths.pop(0)

                stream_data = self._get_stream_data(output_length)
                messages.append({"opcode": "sample", "data": stream_data})

                # Remove the data from the input port buffers that has been
                # processed
                self._input_1_data = self._input_1_data[output_length:]
                self._input_2_data = self._input_2_data[output_length:]

            else:
                break

        return self.output_formatter(messages)

    def _get_stream_data(self, output_length):
        output = [complex(0, 0)] * output_length

        for index, (input_1_value, input_2_value) in enumerate(zip(
                self._input_1_data[0:output_length],
                self._input_2_data[0:output_length])):

            # Use numpy float 32 to match single precision of RCC
            # implementation
            complex_real = numpy.float32(
                numpy.float32(input_1_value.real * input_2_value.real) -
                numpy.float32(input_1_value.imag * input_2_value.imag))
            complex_imaginary = numpy.float32(
                numpy.float32(input_1_value.imag * input_2_value.real) +
                numpy.float32(input_1_value.real * input_2_value.imag))
            output[index] = complex(complex_real, complex_imaginary)

        return output
