-- complex_combiner_s_xs HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of complex_combiner_s_xs_worker is

  constant delay_c : integer := 1;

  signal enable               : std_logic;
  signal input_real_take      : std_logic;
  signal input_imaginary_take : std_logic;
  signal combiner_out         : std_logic_vector(output_out.data'length - 1 downto 0);
  signal output               : worker_output_out_t;
begin

  enable <= output_in.ready and ctl_in.is_operating;

  input_real_out.take      <= input_real_take;
  input_imaginary_out.take <= input_imaginary_take;

  -- Always take non stream opcodes from both inputs.
  -- Only take stream opcodes if both ports have a stream opcode.
  input_take_logic_p : process (enable, input_real_in, input_imaginary_in)
  begin
    -- INPUT 1 (REAL)
    -- If input and output are ready
    if (enable = '1' and input_real_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid
      if input_real_in.opcode /= short_timed_sample_sample_op_e or input_real_in.valid = '0' then
        input_real_take <= '1';
      -- Only take a stream opcode if input imaginary is ready, valid,
      -- and also a stream opcode
      elsif input_imaginary_in.ready = '1' and input_imaginary_in.valid = '1' and
        input_imaginary_in.opcode = short_timed_sample_sample_op_e then
        input_real_take <= '1';
      else
        input_real_take <= '0';
      end if;
    else
      input_real_take <= '0';
    end if;
    -- INPUT 2 (IMAGINARY)
    -- If input and output are ready
    if (enable = '1' and input_imaginary_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid
      if input_imaginary_in.opcode /= short_timed_sample_sample_op_e or input_imaginary_in.valid = '0' then
        input_imaginary_take <= '1';
      -- Only take a stream opcode if input real is ready, valid, and also a
      -- stream opcode.
      elsif input_real_in.ready = '1' and input_real_in.valid = '1' and
        input_real_in.opcode = short_timed_sample_sample_op_e then
        input_imaginary_take <= '1';
      else
        input_imaginary_take <= '0';
      end if;
    else
      input_imaginary_take <= '0';
    end if;
  end process;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the combiner module.
  imaginary_opcode_passthrough_gen : if input_real_opcode_passthrough = '0' generate
    -- Passthrough all non stream opcodes signals from the imaginary port
    -- Discard all non-stream opcodes from the real port
    imag_interface_delay_i : entity work.imaginary_short_to_complex_short_protocol_delay
      generic map (
        DELAY_G          => delay_c,
        DATA_IN_WIDTH_G  => input_imaginary_in.data'length,
        DATA_OUT_WIDTH_G => output.data'length
        )
      port map (
        clk                 => ctl_in.clk,
        reset               => ctl_in.reset,
        enable              => enable,
        take_in             => input_imaginary_take,
        input_in            => input_imaginary_in,
        processed_stream_in => combiner_out,
        output_out          => output
        );
  end generate;

  real_opcode_passthrough_gen : if input_real_opcode_passthrough = '1' generate
    -- Passthrough all non stream opcodes signals from the real port
    -- Discard all non-stream opcodes from the imaginary port
    real_interface_delay_i : entity work.real_short_to_complex_short_protocol_delay
      generic map (
        DELAY_G          => delay_c,
        DATA_IN_WIDTH_G  => input_real_in.data'length,
        DATA_OUT_WIDTH_G => output.data'length
        )
      port map (
        clk                 => ctl_in.clk,
        reset               => ctl_in.reset,
        enable              => enable,
        take_in             => input_real_take,
        input_in            => input_real_in,
        processed_stream_in => combiner_out,
        output_out          => output
        );
  end generate;

  -- Performs combination operation
  complex_combiner_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if enable = '1' then
        combiner_out <= (std_logic_vector(input_imaginary_in.data) &
                         std_logic_vector(input_real_in.data));
      end if;
    end if;
  end process;

  -- Split stream messages over OCPI_MAX_BYTES_OUTPUT / 4 words, as this is the
  -- maximum size specified by the protocol.
  limit_message_size_i : entity work.complex_short_protocol_message_limiter
    generic map(
      DATA_OUT_WIDTH_G     => output_out.data'length,
      MAX_MESSAGE_LENGTH_G => to_integer(OCPI_MAX_BYTES_OUTPUT) / 4
      )
    port map(
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      enable             => enable,
      output_out         => output,
      output_out_limited => output_out
      );

end rtl;
