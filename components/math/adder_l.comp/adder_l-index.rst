.. adder_l documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. Difference, subtract, etc keywords included as when negator options are set
   can be used for find difference / subtraction of values

.. meta::
   :keywords: addition sum difference subtract subtraction


.. _adder_l:


Adder (``adder_l``)
===================
Adds together, or finds the difference between, two input values.

Design
------
Adds together two input values, with the option to negate either input before the addition.

The mathematical representation of the implementation is given in :eq:`adder_l-equation`.

.. math::
   :label: adder_l-equation

   z[n] = (x[n] * N_x) + (y[n] * N_y)

In :eq:`adder_l-equation`:

 * :math:`x[n]` and :math:`y[n]` are the input values.

 * :math:`N_x` and :math:`N_y` are the ``negate_input_1`` and ``negate_input_2`` properties, which have the value 1 when their respective property is ``False`` and -1 when their respective property is ``True``.

 * :math:`z[n]` is the output values.

A block diagram representation of the implementation is given in :numref:`adder_l-diagram`.

.. _adder_l-diagram:

.. figure:: adder_l.svg
   :alt: Block diagram outlining adder implementation.
   :align: center

   Adder implementation.

Interface
---------
.. literalinclude:: ../specs/adder_l-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input_1: Input samples port.
   input_2: Input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Addition is only applied to data in sample opcode messages. All other opcode messages received on ``input_1`` are passed through this component without any effect. All messages with a non-sample opcode received on ``input_2`` are discarded.

Sample message lengths received on ``input_1`` are persevered on the output. Sample message lengths received on ``input_2`` are ignored.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../adder_l.hdl ../adder_l.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``adder_l`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
