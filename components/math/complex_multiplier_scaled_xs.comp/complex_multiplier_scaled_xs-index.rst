.. complex_multiplier_scaled_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _complex_multiplier_scaled_xs:


Complex multiplier (``complex_multiplier_scaled_xs``)
=====================================================
Multiplies two complex values together.

Design
------
Multiplies two complex short values together and then right shifts the output value to generate an output value in the range of a complex short.

The mathematical representation of the implementation is given in :eq:`complex_multiplier_scaled_xs-equation`.

.. math::
   :label: complex_multiplier_scaled_xs-equation

   z[n] = \left \lfloor \frac{x[n] * y[n]}{2^N} \right \rfloor

In :eq:`complex_multiplier_scaled_xs-equation`:

 * :math:`x[n]` and :math:`y[n]` is the input values.

 * :math:`z[n]` is the output values.

 * :math:`N` is the output scaling.

In :eq:`complex_multiplier_scaled_xs-equation`, all outputs are converted to integer representations.

A block diagram representation of the implementation is given in :numref:`complex_multiplier_scaled_xs-diagram`.

.. _complex_multiplier_scaled_xs-diagram:

.. figure:: complex_multiplier_scaled_xs.svg
   :alt: Block diagram outlining complex multiplier implementation.
   :align: center

   Complex multiplier implementation.

Interface
---------
.. literalinclude:: ../specs/complex_multiplier_scaled_xs-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input_1: Input samples port.
   input_2: Input samples port.
   output: Primary output samples port.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

   scale_output: Setting to the default value of 16 ensures the output never overflows for full scale 16-bit input values.

Implementations
---------------
.. ocpi_documentation_implementations:: ../complex_multiplier_scaled_xs.hdl ../complex_multiplier_scaled_xs.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Complex multiplier primitive <complex_multiplier-primitive>`

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``complex_multiplier_scaled_xs`` are:

 * The output of the multiplier has only been tested with a scale factor of 2 to the power of 16, 15, 14, and 13.

Testing
-------
.. ocpi_documentation_test_result_summary::
