.. complex_combiner_s_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: add


.. _complex_combiner_s_xs:


Complex combiner (``complex_combiner_s_xs``)
============================================
Combines two real inputs to create a complex output.

Design
------
The mathematical representation of the implementation is given in :eq:`complex_combiner_s_xs-equation`.

.. math::
   :label: complex_combiner_s_xs-equation

   z[n] = x[n] + j*y[n]

In :eq:`complex_combiner_s_xs-equation`:

 * :math:`x[n]` is the input values which are mapped to the real axis.

 * :math:`y[n]` is the input values which are mapped to the imaginary axis.

 * :math:`z[n]` is the output values.

.. _complex_combiner_s_xs-diagram:

.. figure:: complex_combiner_s_xs.svg
   :alt: Block diagram outlining complex combiner implementation.
   :align: center

   Complex combiner implementation.

Interface
---------
.. literalinclude:: ../specs/complex_combiner_s_xs-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input_real: Input samples port for values mapped to the real axis.
   input_imaginary: Input samples port for values mapped to the imaginary axis.
   output: Primary output samples port.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../complex_combiner_s_xs.hdl ../complex_combiner_s_xs.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Narrow to wide protocol interface delay primitive <narrow_to_wide_protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``complex_combiner_s_cs``'s use are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
