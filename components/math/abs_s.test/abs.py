#!/usr/bin/env python3

# Python implementation of abs block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class Abs(ocpi_testing.Implementation):
    def __init__(self):
        super().__init__()

    def reset(self):
        # It is a requirement for the verification classes that all
        # implementations have a reset function, however for gain this doesn't
        # make much sense so just pass.
        pass

    def sample(self, values):
        output_values = []
        for value in values:
            if value == -32768:
                output_values.append(0)
            else:
                output_values.append(abs(value))

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
