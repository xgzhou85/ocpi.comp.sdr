#!/usr/bin/env python3

# Python implementation of exponentiation block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import math

import ocpi_testing


class Exponentiation(ocpi_testing.Implementation):
    def __init__(self, base, scale_input):
        super().__init__(base=base, scale_input=scale_input)

    def reset(self):
        pass

    def sample(self, values):
        output_values = []
        for value in values:
            scaled_value = value / 2**self.scale_input
            output_values.append(round(self.base**scaled_value))
        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
