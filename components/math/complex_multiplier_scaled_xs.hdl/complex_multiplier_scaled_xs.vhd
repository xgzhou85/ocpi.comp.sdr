-- complex_multiplier_scaled_xs HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_math;
use sdr_math.sdr_math.complex_multiplier;

architecture rtl of complex_multiplier_scaled_xs_worker is
  -- Delay to align data after complex multiplier
  constant delay_c        : integer := 2;
  -- I & Q data input widths
  constant input_size_c   : integer := 16;
  -- Scales output as a power of two
  constant scale_output_c : integer := to_integer(scale_output);

  signal enable            : std_logic;  -- true when output is ready
  signal input_1_take      : std_logic;  -- true when taking input_1 data
  signal input_2_take      : std_logic;  -- true when taking input_2 data
  signal data_out_i        : std_logic_vector(input_size_c * 2 downto 0);
  signal data_out_q        : std_logic_vector(input_size_c * 2 downto 0);
  signal scaled_data_out_i : signed(15 downto 0);
  signal scaled_data_out_q : signed(15 downto 0);
  signal data_out          : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  enable <= output_in.ready and ctl_in.is_operating;

  input_1_out.take <= input_1_take;
  input_2_out.take <= input_2_take;

  -- Always take non stream opcodes from both inputs.
  -- Only take stream opcodes if both ports have a stream opcode.
  input_take_logic_p : process (enable, input_1_in, input_2_in)
  begin
    -- INPUT 1
    -- If input and output are ready
    if (enable = '1' and input_1_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid.
      if input_1_in.opcode /= complex_short_timed_sample_sample_op_e or input_1_in.valid = '0' then
        input_1_take <= '1';
      -- Only take a stream opcode if input 2 is ready, valid, and also a
      -- stream opcode.
      elsif input_2_in.ready = '1' and input_2_in.valid = '1' and
        input_2_in.opcode = complex_short_timed_sample_sample_op_e then
        input_1_take <= '1';
      else
        input_1_take <= '0';
      end if;
    else
      input_1_take <= '0';
    end if;
    -- INPUT 2
    -- If input and output are ready
    if (enable = '1' and input_2_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid.
      if input_2_in.opcode /= complex_short_timed_sample_sample_op_e or input_2_in.valid = '0' then
        input_2_take <= '1';
      -- Only take a stream opcode if input 1 is ready, valid and also a
      -- stream opcode.
      elsif input_1_in.ready = '1' and input_1_in.valid = '1' and
        input_1_in.opcode = complex_short_timed_sample_sample_op_e then
        input_2_take <= '1';
      else
        input_2_take <= '0';
      end if;
    else
      input_2_take <= '0';
    end if;
  end process;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the complex multiplier module.
  interface_delay_i : entity work.complex_short_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_1_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => input_1_take,
      input_in            => input_1_in,
      processed_stream_in => data_out,
      output_out          => output_out
      );

  -- Complex multiplier instantiation.
  complex_multiplier_inst : complex_multiplier
    generic map (
      input_size_g  => input_size_c,
      output_size_g => (input_size_c * 2) + 1,
      bit_drop_g    => 0
      )
    port map (
      clk    => ctl_in.clk,
      rst    => ctl_in.reset,
      enable => enable,
      a_real => input_1_in.data(15 downto 16 - input_size_c),
      a_imag => input_1_in.data(31 downto 32 - input_size_c),
      b_real => input_2_in.data(15 downto 16 - input_size_c),
      b_imag => input_2_in.data(31 downto 32 - input_size_c),
      p_real => data_out_i,
      p_imag => data_out_q
      );

  -- Scale data by dividing by 2^scale_factor.
  scaled_data_out_i <= signed(data_out_i(scale_output_c + 15 downto scale_output_c));
  scaled_data_out_q <= signed(data_out_q(scale_output_c + 15 downto scale_output_c));

  data_out <= (std_logic_vector(resize(scaled_data_out_q, output_out.data'length/2)) &
               std_logic_vector(resize(scaled_data_out_i, output_out.data'length/2)));

end rtl;
