.. shift_right_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: division divide scale


.. _shift_right_xs:


Shift right (``shift_right_xs``)
================================
Right shift all input sample values.

Design
------
Separately right shifts the bit values of the real and imaginary values of a complex value. Upper bit positions are padded with the most significant bit to preserve the sign of the number. Equivalent to a division by a power of 2.

The mathematical representation of the implementation is given in :eq:`shift_right_xs-equation`.

.. math::
   :label: shift_right_xs-equation

   y[n] = \lfloor x[n]/2^N \rfloor

In :eq:`shift_right_xs-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`N` is the number of bits the input is right shifted by.

A block diagram representation of the implementation is given in :numref:`shift_right_xs-diagram`.

.. _shift_right_xs-diagram:

.. figure:: shift_right_xs.svg
   :alt: Block diagram outlining shift right implementation.
   :align: center

   Shift right implementation.

The real and imaginary inputs are treated as separate value streams (i.e. :eq:`shift_right_xs-equation` / :numref:`Fig. %s <shift_right_xs-diagram>` will be implemented twice within the component, once for the real values and a second time for the imaginary values).

Interface
---------
.. literalinclude:: ../specs/shift_right_xs-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Only values in a sample opcode message are right shifted. All other opcodes messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../shift_right_xs.hdl ../shift_right_xs.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``shift_right_xs`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
