-- exp_us HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
library sdr_math;
use sdr_math.sdr_math.exponentiation;
library sdr_dsp;
use sdr_dsp.sdr_dsp.rounding_halfup;

architecture rtl of exp_us_worker is

  constant base_c        : real    := from_float(base);  -- exponentiation base
  constant input_size_c  : integer := 16;  -- data input width
  constant output_size_c : integer := 16;  -- data output width
  constant exp_size_c    : integer := output_size_c + 16;  -- exp output width
  constant delay_c       : integer := 36;  -- delay for flow control signals

  signal take         : std_logic;      -- true when taking input data
  signal valid_data   : std_logic;      -- true when input data is valid
  signal enable       : std_logic;      -- clock enable
  signal resized_data : signed(input_size_c + 16 downto 0);  -- resized data
  signal scaled_data  : signed(input_size_c + 15 downto 0);  -- scaled data
  signal scaled_valid : std_logic;      -- scaled data valid signal
  signal exp_valid    : std_logic;      -- exp output data valid signal
  signal exp_data     : unsigned(exp_size_c - 1 downto 0);   -- exp output data
  signal data_out     : signed(output_size_c - 1 downto 0);  -- exp result
  signal data_out_slv : std_logic_vector(output_size_c - 1 downto 0);

begin

  -- Take data when input and output are ready
  take           <= ctl_in.is_operating and input_in.ready and output_in.ready;
  input_out.take <= take;

  -- Signal for valid input data
  valid_data <= '1' when take = '1' and input_in.valid = '1' and
                input_in.opcode = ushort_timed_sample_sample_op_e else '0';

  -- Clock enable signal
  enable <= ctl_in.is_operating and output_in.ready;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the exponentiation module.
  interface_delay_i : entity work.unsigned_short_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => take,
      input_in            => input_in,
      processed_stream_in => data_out_slv,
      output_out          => output_out
      );

  -- Resize input data for 16 fractional bits (plus one for signed values)
  resized_data <= '0' & signed(input_in.data) & X"0000";

  -- Scale input data by dividing by 2^scale_factor
  input_halfup_rounder_inst : rounding_halfup
    generic map (
      input_width_g  => input_size_c + 17,
      output_width_g => input_size_c + 16
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => enable,
      data_in        => resized_data,
      data_valid_in  => valid_data,
      binary_point   => to_integer(props_in.scale_input),
      data_out       => scaled_data,
      data_valid_out => scaled_valid
      );

  -- Exp instantiation
  exp_inst : exponentiation
    generic map (
      input_size_g  => input_size_c + 16,
      output_size_g => exp_size_c,
      base_g        => base_c
      )
    port map (
      clk            => ctl_in.clk,
      rst            => ctl_in.reset,
      clk_en         => enable,
      data_valid_in  => scaled_valid,
      data_in        => unsigned(scaled_data),
      data_valid_out => exp_valid,
      data_out       => exp_data
      );

  -- Scale output data to 16-bits
  output_halfup_rounder_inst : rounding_halfup
    generic map (
      input_width_g  => exp_size_c,
      output_width_g => output_size_c
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => enable,
      data_in        => signed(exp_data),
      data_valid_in  => exp_valid,
      binary_point   => 16,
      data_out       => data_out,
      data_valid_out => open
      );

  data_out_slv <= std_logic_vector(data_out);

end rtl;
