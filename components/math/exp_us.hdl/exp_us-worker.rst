.. exp_us HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _exp_us-HDL-worker:


``exp_us`` HDL worker
=====================
Pipelined HDL implementation of exponentiation, allowing one input and one output every FPGA clock cycle, with a delay of 36 clock cycles through the pipeline.

Detail
------
The input scaling factor :math:`N` can be set anywhere from 0 to 16 without any loss being introduced by the HDL implementation. As the exponentiation primitive is designed for UQ16.16 fixed point input values and UQ16.16 fixed point output values. With the output of the exponentiation primitive passed to a rounding half-up primitive instance where the output value is rounded to the nearest integer (0.5 rounded up) and the 16 fractional bits thrown away.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
