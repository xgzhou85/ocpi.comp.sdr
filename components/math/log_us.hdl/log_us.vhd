-- log_us HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
library sdr_math;
use sdr_math.sdr_math.logarithm;
library sdr_dsp;
use sdr_dsp.sdr_dsp.rounding_halfup;

architecture rtl of log_us_worker is

  constant base_c        : real    := from_float(base);  -- Logarithm base
  constant input_size_c  : integer := 16;  -- data input width
  constant output_size_c : integer := 16;  -- data output width
  constant max_log_val_c : real    := log2(2.0**input_size_c - 1.0) / log2(base_c);
  constant log_size_c    : integer := integer(ceil(max_log_val_c)) + 17;
  constant delay_c       : integer := 24;  -- delay for flow control signals

  signal take         : std_logic;      -- true when taking input data
  signal valid_data   : std_logic;      -- true when input data is valid
  signal enable       : std_logic;      -- clock enable
  signal resized_data : unsigned(input_size_c + 15 downto 0);  -- resized data
  signal log_valid    : std_logic;      -- log output data valid signal
  signal log_data     : signed(log_size_c - 1 downto 0);  -- log output data
  signal binary_point : integer range 0 to log_size_c - 1;  -- o/p binary point
  signal log_out      : signed(log_size_c - 1 downto 0);  -- scaled log result
  signal data_out     : unsigned(output_size_c - 1 downto 0);  -- data output
  signal data_out_slv : std_logic_vector(output_size_c - 1 downto 0);

begin

  -- Take data when input and output are ready
  take           <= ctl_in.is_operating and input_in.ready and output_in.ready;
  input_out.take <= take;

  -- Signal for valid input data
  valid_data <= '1' when take = '1' and input_in.valid = '1' and
                input_in.opcode = ushort_timed_sample_sample_op_e else '0';

  -- Clock enable signal
  enable <= ctl_in.is_operating and output_in.ready;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the logarithm module.
  interface_delay_i : entity work.unsigned_short_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => take,
      input_in            => input_in,
      processed_stream_in => data_out_slv,
      output_out          => output_out
      );

  -- Resize input data for 16 fractional bits and convert any zero to a one
  resized_data <= X"00010000" when valid_data = '1' and input_in.data = X"0000"
                  else unsigned(input_in.data) & X"0000";

  -- Log instantiation
  log_inst : logarithm
    generic map (
      input_size_g  => input_size_c + 16,
      output_size_g => log_size_c,
      base_g        => base_c
      )
    port map (
      clk            => ctl_in.clk,
      rst            => ctl_in.reset,
      clk_en         => enable,
      data_valid_in  => valid_data,
      data_in        => resized_data,
      data_valid_out => log_valid,
      data_out       => log_data
      );

  -- Set output binary point
  binary_point <= 16 - to_integer(props_in.scale_output);

  -- Halfup Rounder instantiation
  halfup_rounder_inst : rounding_halfup
    generic map (
      input_width_g  => log_size_c,
      output_width_g => log_size_c
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => enable,
      data_in        => log_data,
      data_valid_in  => log_valid,
      binary_point   => binary_point,
      data_out       => log_out,
      data_valid_out => open
      );

  -- Ensure log values aren't less than 0 and rail if greater than 65535
  data_out <= X"0000" when log_out < 0 else
              X"FFFF" when log_out > 65535 else
              unsigned(log_out(output_size_c - 1 downto 0));

  data_out_slv <= std_logic_vector(data_out);

end rtl;
