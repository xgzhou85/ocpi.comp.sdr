-- Shift left implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
architecture rtl of shift_left_xs_worker is

  constant input_size_c : integer := 16;
  constant shift_c      : integer := to_integer(shift);

  -- Interface signals/registers
  signal input_data_valid  : std_logic := '0';
  signal output_data_valid : std_logic := '0';

  signal data_in_i  : std_logic_vector(input_size_c - 1 downto 0) := (others => '0');
  signal data_in_q  : std_logic_vector(input_size_c - 1 downto 0) := (others => '0');
  constant zero_pad : std_logic_vector(shift_c - 1 downto 0)      := (others => '0');

begin

  -- Split I and Q signals to allow shift to be applied to each one separately
  data_in_i <= input_in.data(input_size_c - 1 downto 0);
  data_in_q <= input_in.data(input_in.data'length -1 downto input_size_c);

  -- Valid only when the input is valid and contains the "stream" opcode
  input_data_valid <= '1' when input_in.valid = '1' and input_in.opcode = complex_short_timed_sample_sample_op_e else '0';

  -- Take input data whenever both the input and the output are ready
  input_out.take <= input_in.ready and output_in.ready and ctl_in.is_operating;

  shift_left_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.is_operating = '1' and output_in.ready = '1') then
        if input_data_valid = '1' then
          -- If input data is stream data then left shift data
          output_out.data(input_size_c - 1 downto 0) <=
            data_in_i(input_size_c - 1 - shift_c downto 0) & zero_pad;
          output_out.data(input_in.data'length - 1 downto input_size_c) <=
            data_in_q(input_size_c - 1 - shift_c downto 0) & zero_pad;
        else
          -- Pass all other inputs through directly
          output_out.data <= input_in.data;
        end if;
      end if;
    end if;
  end process;

  -- Register the input interface signals and present them on the output
  output_interface_register_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        output_data_valid <= '0';
      elsif (ctl_in.is_operating = '1' and output_in.ready = '1') then
        output_data_valid      <= input_in.ready;
        output_out.som         <= input_in.som;
        output_out.eom         <= input_in.eom;
        output_out.valid       <= input_in.valid;
        output_out.byte_enable <= input_in.byte_enable;
        output_out.opcode      <= input_in.opcode;
      end if;
    end if;
  end process;

  -- Give an output for every input taken
  output_out.give <= output_data_valid and output_in.ready and ctl_in.is_operating;

end rtl;
