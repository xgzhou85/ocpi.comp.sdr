// Shift left implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "shift_left_xs-worker.hh"

using namespace OCPI::RCC;
using namespace Shift_left_xsWorkerTypes;

class Shift_left_xsWorker : public Shift_left_xsWorkerBase {
  RCCResult run(bool) {
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const Complex_short_timed_sampleSampleData* inData =
          input.sample().data().data();
      Complex_short_timed_sampleSampleData* outData =
          output.sample().data().data();

      output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);

      for (size_t i = 0; i < length; i++) {
        outData->real = inData->real << SHIFT_LEFT_XS_SHIFT;
        outData->imaginary = inData->imaginary << SHIFT_LEFT_XS_SHIFT;

        inData++;
        outData++;
      }

      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

SHIFT_LEFT_XS_START_INFO

SHIFT_LEFT_XS_END_INFO
