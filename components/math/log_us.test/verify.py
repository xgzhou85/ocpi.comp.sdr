#!/usr/bin/env python3

# Runs on correct output for logarithm_us implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from logarithm import Logarithm


input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

# Initiate logarithm object
base = float(os.environ.get("OCPI_TEST_base"))
scale_output = int(os.environ.get("OCPI_TEST_scale_output"))
logarithm_implementation = Logarithm(base, scale_output)

test_id = ocpi_testing.get_test_case()

# Check the result
verifier = ocpi_testing.Verifier(logarithm_implementation)
verifier.set_port_types(["ushort_timed_sample"], ["ushort_timed_sample"],
                        ["bounded_with_exception"])
# HDL implementation uses iterative algorithm to approximate the log function.
# Bounds empirically set to account for error in approximation.
verifier.comparison[0].BOUND = 125
verifier.comparison[0].EXCEPTION_BOUND = 145
# Set exception rate to 0 to allow for only a single exception per message
verifier.comparison[0].ALLOWED_EXCEPTION_RATE = 0
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
