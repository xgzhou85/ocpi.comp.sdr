.. imaginary_xs_s documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _imaginary_xs_s:


Imaginary (``imaginary_xs_s``)
==============================
Returns the imaginary part of complex numbers.

Design
------
The mathematical representation of the implementation is given in :eq:`imaginary_xs_s-equation`.

.. math::
   :label: imaginary_xs_s-equation

   y[n] = \operatorname{Im}(x[n])

In :eq:`imaginary_xs_s-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

A block diagram representation of the implementation is given in :numref:`imaginary_xs_s-diagram`.

.. _imaginary_xs_s-diagram:

.. figure:: imaginary_xs_s.svg
   :alt: Block diagram outlining imaginary implementation.
   :align: center

   Imaginary implementation.

Interface
---------
.. literalinclude:: ../specs/imaginary_xs_s-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The imaginary value is only found for values in a sample opcode message. All other opcodes messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../imaginary_xs_s.hdl ../imaginary_xs_s.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface delay (wide to narrow) primitive <wide_to_narrow_protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``imaginary_xs_s`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
