.. adder_s HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _adder_s-HDL-worker:


``adder_s`` HDL worker
======================

Detail
------
The adder is fully pipelined and can take a new input on every clock edge. If sample opcode message data is only available on a single input port, backpressure is applied to the input port with sample opcode message data until the other input port has sample data available.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
