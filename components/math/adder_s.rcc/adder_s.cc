// adder_s RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "adder_s-worker.hh"

// define bits used within negate variable to signal negate_input_1
// and negate_input_2 props
const uint8_t NEGATE_1 = 1;
const uint8_t NEGATE_2 = 2;
const uint8_t NOT_NEGATE_1 = 2;
const uint8_t NOT_NEGATE_2 = 1;

// define meaning of negate bits within negate variable
const uint8_t INPUT_1_ADD_2 = 0x00;
const uint8_t INPUT_NEGATE_1_ADD_2 = 0x01;
const uint8_t INPUT_1_ADD_NEGATE_2 = 0x02;
const uint8_t INPUT_NEGATE_1_ADD_NEGATE_2 = 0x03;

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Adder_sWorkerTypes;

class Adder_sWorker : public Adder_sWorkerBase {
  uint8_t negate = 0;  // keep a single variable holding negate_x status
  size_t input_1_unprocessed_data = 0;
  size_t input_2_unprocessed_data = 0;

  int16_t *output_data;
  const int16_t *input_1_data;
  const int16_t *input_2_data;

  RCCResult negate_input_1_written() {
    // bit 0 of negate represents input_1
    if (properties().negate_input_1 == true) {
      negate |= NEGATE_1;
    } else {
      negate &= NOT_NEGATE_1;
    }
    return RCC_OK;
  }

  RCCResult negate_input_2_written() {
    // bit 1 of negate represents input_2
    if (properties().negate_input_2 == true) {
      negate |= NEGATE_2;
    } else {
      negate &= NOT_NEGATE_2;
    }
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input_1.opCode() == Short_timed_sampleSample_OPERATION &&
        input_2.opCode() == Short_timed_sampleSample_OPERATION) {
      // As output messages must be the same length as messages on input_1
      // port this controls the size of the output
      if (input_1_unprocessed_data == 0) {
        input_1_data = input_1.sample().data().data();
        input_1_unprocessed_data = input_1.sample().data().size();
        output_data = output.sample().data().data();
        output.setOpCode(Short_timed_sampleSample_OPERATION);
        output.sample().data().resize(input_1_unprocessed_data);
      }

      if (input_2_unprocessed_data == 0) {
        input_2_data = input_2.sample().data().data();
        input_2_unprocessed_data = input_2.sample().data().size();
      }

      size_t samples_available = 0;
      if (input_1_unprocessed_data < input_2_unprocessed_data) {
        samples_available = input_1_unprocessed_data;
      } else {
        samples_available = input_2_unprocessed_data;
      }

      switch (negate) {      // make negate decision outside of loop
        case INPUT_1_ADD_2:  // negate neither input
          for (size_t i = 0; i < samples_available; i++) {
            *output_data++ = *input_1_data++ + *input_2_data++;
          }
          break;
        case INPUT_NEGATE_1_ADD_2:  // negate input_1 only
          for (size_t i = 0; i < samples_available; i++) {
            *output_data++ = *input_2_data++ - *input_1_data++;
          }
          break;
        case INPUT_1_ADD_NEGATE_2:  // negate input_2 only
          for (size_t i = 0; i < samples_available; i++) {
            *output_data++ = *input_1_data++ - *input_2_data++;
          }
          break;
        case INPUT_NEGATE_1_ADD_NEGATE_2:  // negate input_1 and input_2
        default:
          for (size_t i = 0; i < samples_available; i++) {
            *output_data++ = -*input_1_data++ - *input_2_data++;
          }
          break;
      }

      input_1_unprocessed_data = input_1_unprocessed_data - samples_available;
      input_2_unprocessed_data = input_2_unprocessed_data - samples_available;

      // Input 1 controls the size of the output messages, so when input_1 needs
      // advancing so will the output
      if (input_1_unprocessed_data == 0) {
        input_1.advance();
        output.advance();
      }
      if (input_2_unprocessed_data == 0) {
        input_2.advance();
      }
    } else {
      // Passthrough any non stream opcodes of input_1
      if (input_1.opCode() != Short_timed_sampleSample_OPERATION) {
        if (input_1.opCode() == Short_timed_sampleTime_OPERATION) {
          // Pass through time opcode and time data
          output.setOpCode(Short_timed_sampleTime_OPERATION);
          output.time().fraction() = input_1.time().fraction();
          output.time().seconds() = input_1.time().seconds();
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() ==
                   Short_timed_sampleSample_interval_OPERATION) {
          // Pass through sample interval opcode and sample interval data
          output.setOpCode(Short_timed_sampleSample_interval_OPERATION);
          output.sample_interval().fraction() =
              input_1.sample_interval().fraction();
          output.sample_interval().seconds() =
              input_1.sample_interval().seconds();
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() == Short_timed_sampleFlush_OPERATION) {
          // Pass through flush opcode
          output.setOpCode(Short_timed_sampleFlush_OPERATION);
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() ==
                   Short_timed_sampleDiscontinuity_OPERATION) {
          // Pass through discontinuity opcode
          output.setOpCode(Short_timed_sampleDiscontinuity_OPERATION);
          input_1.advance();
          output.advance();
        } else if (input_1.opCode() == Short_timed_sampleMetadata_OPERATION) {
          // Pass through metadata opcode, id, and data
          output.setOpCode(Short_timed_sampleMetadata_OPERATION);
          output.metadata().id() = input_1.metadata().id();
          output.metadata().value() = input_1.metadata().value();
          input_1.advance();
          output.advance();
        } else {
          setError("Unknown OpCode received");
          return RCC_FATAL;
        }
      }
      // Discard any non stream opcodes on input_2
      if (input_2.opCode() != Short_timed_sampleSample_OPERATION) {
        input_2.advance();
      }
    }
    return RCC_OK;
  }
};

ADDER_S_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
ADDER_S_END_INFO
