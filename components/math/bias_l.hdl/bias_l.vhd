-- bias_l HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of bias_l_worker is

  constant delay_c   : integer := 1;
  signal add_out     : signed(31 downto 0);
  signal add_out_slv : std_logic_vector(31 downto 0);
  signal give        : std_logic;
  signal take        : std_logic;
  signal enable      : std_logic;

begin

  -- Take input data whenever both the input and the output are ready
  take           <= input_in.ready and output_in.ready and ctl_in.is_operating;
  input_out.take <= take;
  enable         <= ctl_in.is_operating and output_in.ready;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the processing module.
  interface_delay_i : entity work.long_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => take,
      input_in            => input_in,
      processed_stream_in => add_out_slv,
      output_out          => output_out
      );

  -- Perform addition operation
  bias_l_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if enable = '1' then
        add_out <= signed(input_in.data) + props_in.bias;
      end if;
    end if;
  end process;
  add_out_slv <= std_logic_vector(add_out);

end rtl;
