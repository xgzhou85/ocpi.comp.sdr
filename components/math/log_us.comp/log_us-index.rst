.. log_us documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: log


.. _log_us:


Logarithm (``log_us``)
======================
Calculates the logarithm to a base of all input sample values.

Design
------
The logarithm of the input value is taken to a base value, this is then scaled by a factor of two before the output is rounded to the nearest integer.

The mathematical representation of the implementation is given in :eq:`log_us-equation`.

.. math::
   :label: log_us-equation

   y[n] = (2^N) log_b(x[n])

In :eq:`log_us-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`b` is the base value, this is set by the ``base`` property.

 * :math:`N` is the output scale power, this is set by the ``scale_output`` property.

A block diagram representation of the implementation is given in :numref:`log_us-diagram`.

.. _log_us-diagram:

.. figure:: log_us.svg
   :alt: Block diagram outlining logarithm implementation.
   :align: center

   Logarithm implementation.

Interface
---------
.. literalinclude:: ../specs/log_us-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The logarithm operation is only applied to values in a sample opcode message. All other opcodes messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../log_us.hdl ../log_us.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Logarithm primitive <logarithm-primitive>`

 * :ref:`Rounding halfup primitive <rounding_halfup-primitive>`

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``log_us`` are:

 * Base values less than or equal to one are not supported.

Testing
-------
.. ocpi_documentation_test_result_summary::
