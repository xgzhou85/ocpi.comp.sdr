#!/usr/bin/env python3

# Python implementation of complex_combiner_s_xs for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import numpy as np

import ocpi_protocols
import ocpi_testing


class ComplexCombiner(ocpi_testing.Implementation):
    def __init__(self, input_real_opcode_passthrough):
        super().__init__(
            input_real_opcode_passthrough=input_real_opcode_passthrough)

        self.input_ports = ["input_real", "input_imaginary"]
        if input_real_opcode_passthrough is False:
            self.non_sample_opcode_port_select = 1

        self._input_real_data = []
        self._input_imaginary_data = []
        self._output_message_lengths = []

        self._max_output_length = ocpi_protocols.PROTOCOLS[
            "complex_short_timed_sample"].max_sample_length

    def reset(self):
        self._input_real_data = []
        self._input_imaginary_data = []
        self._output_message_lengths = []

    def select_input(self, input_real, input_imaginary):
        # Since the only one port is used to pass through non sample opcode and
        # set message lengths, read all of the data to come in on the other
        # port first so when sample data is needed it is available
        if self.input_real_opcode_passthrough:
            if input_imaginary is not None:
                return 1
            else:
                return 0
        else:
            if input_real is not None:
                return 0
            else:
                return 1

    def sample(self, input_real, input_imaginary):

        if input_real is not None:
            self._input_real_data = self._input_real_data + list(input_real)
            if self.input_real_opcode_passthrough is True:
                self._output_message_lengths.append(len(input_real))

        if input_imaginary is not None:
            self._input_imaginary_data = self._input_imaginary_data + \
                list(input_imaginary)
            if self.input_real_opcode_passthrough is False:
                self._output_message_lengths.append(len(input_imaginary))

        messages = []
        while len(self._output_message_lengths) > 0:
            # Check there is enough data on both inputs for output
            if len(self._input_real_data) >= self._output_message_lengths[0] \
                and len(self._input_imaginary_data) >= \
                    self._output_message_lengths[0]:
                output_length = self._output_message_lengths.pop(0)

                while output_length > self._max_output_length:
                    samples = self._get_samples(self._max_output_length)
                    messages.append({"opcode": "sample", "data": samples})
                    output_length = output_length - self._max_output_length
                    # Remove the data from the input port buffers that has been
                    # processed
                    self._input_real_data = self._input_real_data[
                        self._max_output_length:]
                    self._input_imaginary_data = \
                        self._input_imaginary_data[self._max_output_length:]

                samples = self._get_samples(output_length)
                messages.append({"opcode": "sample", "data": samples})
                # Remove the data from the input port buffers that has been
                # processed
                self._input_real_data = self._input_real_data[output_length:]
                self._input_imaginary_data = \
                    self._input_imaginary_data[output_length:]

            else:
                break

        return self.output_formatter(messages)

    def _get_samples(self, output_length):

        input_real_data = np.array(
            self._input_real_data[0:output_length], dtype=np.int16)
        input_imaginary_data = np.array(
            self._input_imaginary_data[0:output_length], dtype=np.int16)

        output = np.vectorize(complex)(input_real_data, input_imaginary_data)

        return output
