#!/usr/bin/env python3

# Runs checks for complex_combiner_s_xs testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from complex_combiner import ComplexCombiner


input_real_opcode_passthrough_property = \
    os.environ["OCPI_TEST_input_real_opcode_passthrough"].upper() == "TRUE"

input_real_file = str(sys.argv[-2])
input_imaginary_file = str(sys.argv[-1])
output_file = str(sys.argv[-3])

complex_combiner_implementation = ComplexCombiner(
    input_real_opcode_passthrough_property)

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(complex_combiner_implementation)
verifier.set_port_types(
    ["short_timed_sample", "short_timed_sample"],
    ["complex_short_timed_sample"], ["equal"])
if verifier.verify(test_id, [
        input_real_file, input_imaginary_file], [output_file]) is True:
    sys.exit(0)
else:
    sys.exit(1)
