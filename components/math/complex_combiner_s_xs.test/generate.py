#!/usr/bin/env python3

# Generates the input binary file for complex_combiner_s_xs testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os

import ocpi_protocols
import ocpi_testing


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
input_real_opcode_passthrough = os.environ[
    "OCPI_TEST_input_real_opcode_passthrough"].upper() == "TRUE"

# As both input ports use the same generate file a seed needs to be passed from
# the test XML to ensure different random numbers are used for each input port.
seed = ocpi_testing.get_test_seed(
    arguments.case, subcase, input_real_opcode_passthrough, arguments.seed)

generator = ocpi_testing.generator.ShortGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
