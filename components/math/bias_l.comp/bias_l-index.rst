.. bias_l documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: offset


.. _bias_l:


Bias (``bias_l``)
=================
Adds a fixed bias to all input sample values.

Design
------
The mathematical representation of the implementation is given in :eq:`bias_l-equation`.

.. math::
   :label: bias_l-equation

   y[n] = B + x[n]

In :eq:`bias_l-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`B` is the bias value.

A block diagram representation of the implementation is given in :numref:`bias_l-diagram`.

.. _bias_l-diagram:

.. figure:: bias_l.svg
   :alt: Block diagram outlining bias implementation.
   :align: center

   Bias implementation.

Interface
---------
.. literalinclude:: ../specs/bias_l-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
A bias is only added to values in sample opcode messages. All other opcode messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../bias_l.hdl ../bias_l.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``bias_l`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
