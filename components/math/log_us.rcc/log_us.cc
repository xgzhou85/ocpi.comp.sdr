// Logarithm implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include "log_us-worker.hh"

using namespace OCPI::RCC;
using namespace Log_usWorkerTypes;

class Log_usWorker : public Log_usWorkerBase {
  uint8_t scale_output = 13;

  RCCResult scale_output_written() {
    scale_output = static_cast<uint8_t>(properties().scale_output);
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Ushort_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const uint16_t *inData =
          reinterpret_cast<const uint16_t *>(input.sample().data().data());
      uint16_t *outData =
          reinterpret_cast<uint16_t *>(output.sample().data().data());
      output.setOpCode(Ushort_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);

      for (size_t i = 0; i < length; i++) {
        uint16_t input_data = *inData++;
        if (!input_data) input_data = 1;  // Map 0 to 1 on the input
        double_t data = static_cast<double_t>(input_data);
        double_t result = std::log2(data) / std::log2(LOG_US_BASE);
        result *= std::pow(2, scale_output);
        if (result > 65535) result = 65535;
        *outData++ = static_cast<uint16_t>(result + 0.5);
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Ushort_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Ushort_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Ushort_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Ushort_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Ushort_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Ushort_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Ushort_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Ushort_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Ushort_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Ushort_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

LOG_US_START_INFO

LOG_US_END_INFO
