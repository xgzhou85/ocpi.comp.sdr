#!/usr/bin/env python3

# Python implementation of log10 for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import numpy

import ocpi_testing


class Log10(ocpi_testing.Implementation):
    def reset(self):
        pass

    def sample(self, input_):
        output = [0.0] * len(input_)
        for index, value in enumerate(input_):
            output[index] = numpy.log10(value)

        return self.output_formatter([{"opcode": "sample", "data": output}])
