#!/usr/bin/env python3

# Python implementation of multiplier block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import numpy as np

import ocpi_protocols
import ocpi_testing


class Multiplier(ocpi_testing.Implementation):
    def __init__(self):
        super().__init__()

        self.input_ports = ["input_1", "input_2"]

        self._input_1_data = []
        self._input_2_data = []

        self._output_message_lengths = []

        self._max_output_length = ocpi_protocols.PROTOCOLS["long_timed_sample"].max_sample_length

    def reset(self):
        self._input_1_data = []
        self._input_2_data = []
        self._output_message_lengths = []

    def select_input(self, input_1, input_2):
        # Since only input 1 is used to pass through non sample opcode and set
        # message lengths, read all of the data to come in on input 2 first so
        # when sample data is needed it is available
        if input_2 is not None:
            return 1
        else:
            return 0

        # Pull through message if not a sample, so that both ports get a sample
        # message being the next on each port.
        if input_1 != "sample" and input_1 is not None:
            return 0
        if input_2 != "sample" and input_2 is not None:
            return 1

        # Otherwise only sample messages or no messages on port.
        if input_1 is not None:
            return 0
        if input_2 is not None:
            return 1

    def sample(self, input_1, input_2):

        if input_1 is not None:
            self._input_1_data = self._input_1_data + list(input_1)
            self._output_message_lengths.append(len(input_1))
        if input_2 is not None:
            self._input_2_data = self._input_2_data + list(input_2)

        messages = []
        while len(self._output_message_lengths) > 0:
            # Check there is enough data on both inputs for output
            if len(self._input_1_data) >= self._output_message_lengths[0] and \
                    len(self._input_2_data) >= self._output_message_lengths[0]:
                output_length = self._output_message_lengths.pop(0)

                while output_length > self._max_output_length:
                    samples = self._get_samples(self._max_output_length)
                    messages.append({"opcode": "sample", "data": samples})
                    output_length = output_length - self._max_output_length
                    # Remove the data from the input port buffers that has been
                    # processed
                    self._input_1_data = self._input_1_data[self._max_output_length:]
                    self._input_2_data = self._input_2_data[self._max_output_length:]

                stream_data = self._get_samples(output_length)
                messages.append({"opcode": "sample", "data": stream_data})
                # Remove the data from the input port buffers that has been
                # processed
                self._input_1_data = self._input_1_data[output_length:]
                self._input_2_data = self._input_2_data[output_length:]

            else:
                break

        return self.output_formatter(messages)

    def _get_samples(self, output_length):

        input_1_data = np.array(self._input_1_data[0:output_length])
        input_2_data = np.array(self._input_2_data[0:output_length])

        output = np.array(np.int32(input_1_data * input_2_data))

        return output
