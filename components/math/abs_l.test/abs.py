#!/usr/bin/env python3

# Python implementation of abs_l for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import ocpi_testing
import numpy as np


class Abs(ocpi_testing.Implementation):
    def __init__(self):
        super().__init__()

    def reset(self):
        # It is a requirement for the verification classes that all
        # implementations have a reset function, however for abs this doesn't
        # make much sense so just pass.
        pass

    def sample(self, values):
        output_values = []

        input_data = np.array(values, dtype=np.int32)
        output_values = np.absolute(input_data)

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
