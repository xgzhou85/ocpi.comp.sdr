// RCC implementation of crc_generator_c worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "crc_generator_c-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Crc_generator_cWorkerTypes;

class Crc_generator_cWorker : public Crc_generator_cWorkerBase {
  const uint8_t *polynomial_taps = CRC_GENERATOR_C_POLYNOMIAL_TAPS;
  const RCCBoolean input_reflect = CRC_GENERATOR_C_INPUT_REFLECT;
  const RCCBoolean output_reflect = CRC_GENERATOR_C_OUTPUT_REFLECT;

  uint64_t polynomial = 0;
  uint8_t polynomial_bit_size;

  uint64_t seed = 0;
  uint64_t final_xor = 0;

  uint8_t output_buffer[CRC_GENERATOR_C_OCPI_MAX_BYTES_OUTPUT];
  uint16_t output_buffer_length = 0;

  uint64_t crc = 0;

  // Ensures that a crc is sent on every flush or discontinuity opcode even if
  // no data is present in crc_array. In this case, the default crc is sent.
  bool crc_sent = false;

  RCCResult start() {
    // The first value in polynomial taps is the highest power, and therefore
    // gives the polynomial size in bits
    this->polynomial_bit_size = *this->polynomial_taps;

    // Extract coefficients from polynomial taps to be used as divisor
    for (uint8_t i = 1; i < this->polynomial_bit_size; i++) {
      this->polynomial |= ((uint64_t)1 << *(this->polynomial_taps + i));
      if (*(this->polynomial_taps + i) == 0) {
        break;
      }
    }
    return RCC_OK;
  }

  // Notification that seed property has been written
  RCCResult seed_written() {
    this->seed = properties().seed;
    this->crc = this->seed;
    return RCC_OK;
  }

  // Notification that final_xor property has been written
  RCCResult final_xor_written() {
    this->final_xor = properties().final_xor;
    return RCC_OK;
  }

  // Generates an n-bit CRC from the data within the saved output buffer, where
  // n is taken from the number of polynomial taps
  void generate_crc() {
    for (uint16_t byte = 0; byte < this->output_buffer_length; byte++) {
      uint8_t output_byte = 0;
      if (this->input_reflect == false) {
        output_byte = this->output_buffer[byte];
      } else {
        for (uint8_t i = 0; i < 8; i++) {
          output_byte |= (((this->output_buffer[byte] >> i) & 0x01) << (7 - i));
        }
      }

      // Align message byte to left
      this->crc ^= (uint64_t)output_byte << (this->polynomial_bit_size - 8);

      for (uint8_t bit = 0; bit < 8; bit++) {
        // If left most bit is 1
        if (this->crc & ((uint64_t)1 << (this->polynomial_bit_size - 1))) {
          // Perform the division and discard left most bit to get remainder
          if (this->polynomial_bit_size > 63) {
            // Split crc in two to avoid overflow
            uint32_t mask_32_bit = 0xFFFFFFFF;
            uint64_t crc_left = ((this->crc >> 32) & mask_32_bit);
            uint64_t crc_right = this->crc & mask_32_bit;
            crc_left = (crc_left << 1);
            crc_right = (crc_right << 1);
            crc_left |= (crc_right >> 32);  // Set overlapping bit
            crc_left = crc_left ^ ((this->polynomial >> 32) & mask_32_bit);
            crc_right = crc_right ^ (this->polynomial & mask_32_bit);
            crc_left &= mask_32_bit;
            crc_right &= mask_32_bit;
            // Merge result into final crc
            this->crc = (crc_left << 32) | crc_right;
          } else {
            this->crc = ((this->crc << 1) ^ this->polynomial);
            this->crc &= (((uint64_t)1 << this->polynomial_bit_size) - 1);
          }
        } else {
          // Shift to next bit without dividing
          this->crc = this->crc << 1;
        }
      }
    }

    // Empty output buffer from processed data
    this->output_buffer_length = 0;
  }

  // Reverses the bit endianness of each individual byte within the member CRC
  void reflect_crc() {
    uint64_t reflected_crc = 0;
    for (uint8_t i = 0; i < this->polynomial_bit_size; i++) {
      reflected_crc |=
          (((this->crc >> i) & 0x01) << ((this->polynomial_bit_size - 1) - i));
    }
    this->crc = reflected_crc;
  }

  // Exclusive ORs the member CRC with the settable XOR parameter.
  void xor_crc() { this->crc ^= this->final_xor; }

  // Splits the member crc into 8 bit chunks and saves into the given array
  // Returns the length of the array in bytes
  uint8_t crc_to_array(uint8_t *crc_array) {
    // Save final crc to character array
    uint8_t crc_size = (this->polynomial_bit_size / 8);
    uint8_t crc_shift_bits = (this->polynomial_bit_size - 8);
    uint8_t crc_array_length = 0;
    for (uint8_t i = 0; i < crc_size; i++) {
      uint8_t crc_byte = (this->crc >> crc_shift_bits) & 0xFF;
      if (crc_byte < 127) {
        crc_array[crc_array_length++] = crc_byte;
      } else {
        // Handle negative values
        crc_array[crc_array_length++] = -1 * (256 - crc_byte);
      }
      crc_shift_bits -= 8;
    }
    return crc_array_length;
  }

  RCCResult run(bool) {
    if (input.opCode() == Char_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const uint8_t *inData =
          reinterpret_cast<const uint8_t *>(input.sample().data().data());
      uint8_t *outData =
          reinterpret_cast<uint8_t *>(output.sample().data().data());

      output.setOpCode(Char_timed_sampleSample_OPERATION);

      for (size_t i = 0; i < length; i++) {
        // Forward all input data
        *outData = *inData;
        // Save copy of sent data for future CRC calculation
        this->output_buffer[this->output_buffer_length++] = *outData;
        outData++;
        inData++;
      }

      if (length) {
        generate_crc();
      }

      output.sample().data().resize(length);
      return RCC_ADVANCE;
    } else if (input.opCode() == Char_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Char_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Char_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Char_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Char_timed_sampleFlush_OPERATION) {
      if (!this->crc_sent) {
        // Pass through crc for data received since last flush or discontinuity
        // opcode
        uint8_t *outData =
            reinterpret_cast<uint8_t *>(output.sample().data().data());

        if (this->output_reflect) {
          reflect_crc();
        }
        if (this->final_xor) {
          xor_crc();
        }

        uint8_t crc_array[8];
        uint8_t crc_array_length = crc_to_array(crc_array);
        for (uint8_t i = 0; i < crc_array_length; i++) {
          *outData = crc_array[i];
          outData++;
        }

        output.sample().data().resize(crc_array_length);
        output.setOpCode(Char_timed_sampleSample_OPERATION);
        output.advance();
        this->crc_sent = true;
        // Reset crc to default value
        this->crc = this->seed;
        return RCC_OK;
      }
      // Pass through flush opcode
      output.setOpCode(Char_timed_sampleFlush_OPERATION);
      this->crc_sent = false;
      return RCC_ADVANCE;
    } else if (input.opCode() == Char_timed_sampleDiscontinuity_OPERATION) {
      if (!this->crc_sent) {
        // Pass through crc for data received since last flush or discontinuity
        // opcode
        uint8_t *outData =
            reinterpret_cast<uint8_t *>(output.sample().data().data());

        if (this->output_reflect) {
          reflect_crc();
        }

        if (this->final_xor) {
          xor_crc();
        }

        uint8_t crc_array[8];
        uint8_t crc_array_length = crc_to_array(crc_array);

        for (uint8_t i = 0; i < crc_array_length; i++) {
          *outData = crc_array[i];
          outData++;
        }

        output.sample().data().resize(crc_array_length);
        output.setOpCode(Char_timed_sampleSample_OPERATION);
        output.advance();
        this->crc_sent = true;
        // Reset crc to default value
        this->crc = this->seed;
        return RCC_OK;
      }
      // Pass through discontinuity opcode
      output.setOpCode(Char_timed_sampleDiscontinuity_OPERATION);
      this->crc_sent = false;
      return RCC_ADVANCE;
    } else if (input.opCode() == Char_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Char_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

CRC_GENERATOR_C_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
CRC_GENERATOR_C_END_INFO
