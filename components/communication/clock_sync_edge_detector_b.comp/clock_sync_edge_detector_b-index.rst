.. clock_sync_edge_detector_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _clock_sync_edge_detector_b:


Edge detecting clock synchroniser (``clock_sync_edge_detector_b``)
==================================================================
Recovers binary symbols from an oversampled stream of binary data.

Design
------
The edge detecting clock synchroniser is designed to recover binary samples from an oversampled binary data stream, even in the presence of minor variances in symbol rate.

The component works by slewing a local clock (counter) to align with symbol transition edges of the input binary data stream. A counter is incremented by one for every sample that is input to the module. The counter is reset to 0 whenever the counter reaches value of ``samples_per_symbol - 1``. The counter is also reset to zero whenever a transition of the input from a zero to a one or a one to a zero occurs. This indicates the edge of the oversampled input symbol. When the counter is equal to ``sample_point`` the current input sample is passed to the output. Input samples when the counter is not equal to ``sample_point`` are discarded.

A diagram showing operation of the component is shown in :numref:`clock_sync_edge_detector_b_ideal-diagram`.

.. _clock_sync_edge_detector_b_ideal-diagram:

.. figure:: clock_sync_edge_detector_b_ideal.svg
   :alt: Operation of edge detecting clock synchroniser comparing input and output.
   :align: center

   Edge detecting clock synchroniser operation, with ideal input.``samples_per_symbol`` set to ``7`` and ``sample_point`` set to ``3``.

The system is able to cope with minor variances in the input sample rate from the ideal value. The exact tolerance to sample rate offset depends on the frequency that symbol transition edges occur within the input data stream, as well as how many times the input data symbols are oversampled. Sustained errors in sample rate will eventually lead to errors during long streams of repeated zeros or ones. More symbol edge transitions give the system a greater chance of staying synchronised. Since most binary streams, used in communications systems, are whitened (even distribution between ones and zeros) this is rarely an issue.

A diagram showing operation of the component when presented with a non-ideal binary input stream is shown in :numref:`clock_sync_edge_detector_b_non_ideal-diagram`. In this example some symbols are shorter than expected by one sample and some symbols are longer by one sample. Despite these errors the correct symbols are still extracted.

.. _clock_sync_edge_detector_b_non_ideal-diagram:

.. figure:: clock_sync_edge_detector_b_non_ideal.svg
   :alt: Operation of edge detecting clock synchroniser comparing input and output.
   :align: center

   Edge detecting clock synchroniser operation, with non-ideal input.``samples_per_symbol`` set to ``7`` and ``sample_point`` set to ``3``.

The edge detecting clock synchronisation method is not tolerant to noise in the input signal. Single bit glitches in the middle of a symbol will cause the system to realign timing to the point that the glitch occurred. An averaging filter such as ``moving_average_filter_b`` should be applied to the binary data before passing it through this component.

Edge detecting clock synchronisation is a basic and compact clock recovery technique that often produces acceptable results, but rarely provides an optimal solution. It works best when the input data is oversampled by at least 5 times. Input streams with smaller oversample factors are better suited to other clock recovery methods.

Interface
---------
.. literalinclude:: ../specs/clock_sync_edge_detector_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port. The input data is expected to be a stream of binary symbols oversampled by a factor equal to the value of the property ``samples_per_symbol``.
   output: Primary output samples port. The output data is a stream of binary symbols with one sample per symbol.

Opcode handling
~~~~~~~~~~~~~~~
Messages with the sample opcode are down sampled based on the edge detecting clock synchronisation algorithm. The message boundaries of the input sample message are preserved on the output sample message. This means that output sample messages are typically smaller than the input sample messages by a factor of ``samples_per_symbol``. Input sample messages where all the samples are discarded due to the clock synchronisation algorithm are output as zero length messages.

Timestamp opcode messages contains the timestamp of the next valid message with a sample opcode. As this component is designed to down sample the input data stream, the sample that the timestamp relates to may not be output from the component. For this reason ``timestamp`` opcode messages are not directly passed through but instead stored and incremented by the sample interval for each valid sample opcode message that is input into the component. The corrected timestamp is then inserted into the output stream directly before the next valid output message with the sample opcode. The result of this is that the timestamp opcode message is passed through the component as close as possible to its position in the input stream, and the timestamp is corrected so that it always contains the time of the next valid sample opcode message.

Sample interval opcode messages are passed through the component from input to output. The sample interval is stored in the component so that it can be used to increment the timestamp.

Messages with the flush opcode are passed through the component from input to output. A flush opcode message causes the edge detecting clock synchronisation algorithm to be reset to its initial state. No additional samples are output as a result of the flush opcode message as the component has no history other than the algorithm state.

Messages with the discontinuity opcode are passed through the component from input to output. A discontinuity opcode message causes the edge detecting clock synchronisation algorithm to be reset to its initial state.

Messages with a metadata opcodes are passed through the component unmodified and in the same position in the output stream as in the input stream.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../clock_sync_edge_detector_b.hdl ../clock_sync_edge_detector_b.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Down-sample protocol interface delay primitive <downsample_protocol_interface_delay-primitive>`

 * :ref:`Timestamp recovery primitive <timestamp_recovery-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``clock_sync_edge_detector_b`` are:

 * The clock synchronising edge detector will typically provide poor results when ``samples_per_symbol`` is less than 5.

 * The clock synchronising edge detector must be used in conjunction with a binary averaging filter unless it is known that the input data is free from glitches.

 * Alternative symbol recovery methods will typically provide better results in low signal to noise ratio applications.

 * After a timestamp message opcode is received any zero length messages with the sample opcode are discarded until the next valid message with a sample opcode is received. Under all other conditions zero length stream messages are passed directly through.

Testing
-------
.. ocpi_documentation_test_result_summary::
