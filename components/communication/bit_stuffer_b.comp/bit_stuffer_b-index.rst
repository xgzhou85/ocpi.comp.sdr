.. bit_stuffer_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: padding padder


.. _bit_stuffer_b:


Bit stuffer (``bit_stuffer_b``)
===============================
Zero insertion bit stuffer.

Design
------
Bit stuffing is the insertion of non-information bits into binary data. It has various purposes, such as to alter the bit rate of a bit stream, or to fill up a frame or to aid receiver synchronisation. Typically it is used to limit the number of consecutive bits of the same value in the data to be transmitted. This allows for certain sequences of ones and zeros to be reserved for frame delimiting by ensuring the data itself can never be the same as the beginning or end flags.

The component stores the values of the last ``consecutive_ones - 1`` samples. If the current sample and the last ``consecutive_ones - 1`` samples are all ``1``, then a single ``0`` symbol is inserted into the sample stream.

Interface
---------
.. literalinclude:: ../specs/bit_stuffer_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.


Opcode handling
~~~~~~~~~~~~~~~
When a message with a discontinuity opcode is received this acts to reset the component, and clears the internal input buffer of previously received data to all zeros. Once this is completed the discontinuity opcode message is forwarded on.

When a message with a flush opcode is received ``flush_length`` zeros  are injected into the :ref:`bit_stuffer <bit_stuffer-primitive>`'s input whilst applying backpressure. Once this is completed the flush opcode message is forwarded on.

All other opcode messages are passed through the component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../bit_stuffer_b.hdl ../bit_stuffer_b.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Bit stuffer primitive <bit_stuffer-primitive>`

 * :ref:`Flush inserter primitive <flush_inserter-primitive>`

 * :ref:`Message length limiter primitive <message_length_limiter-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``bit_stuffer_b`` are:

 * ``consecutive_ones`` cannot be set to zero.

Testing
-------
.. ocpi_documentation_test_result_summary::
