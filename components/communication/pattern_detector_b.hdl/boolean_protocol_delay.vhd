-- Delay module for Boolean Protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes stream data.
-- Delay_G should be set to the delay in clock cycles of the module that
-- processes stream data.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.pattern_detector_b_worker_defs.all;

entity boolean_protocol_delay is
  generic (
    DELAY_G : integer := 1
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;  -- High when output is ready
    take_in             : in  std_logic;  -- High when data is taken from input
    input_in            : in  worker_input_in_t;   -- Input streaming interface
    processed_stream_in : in  std_logic;  -- Connect output from data
                                          -- processing module.
    output_out          : out worker_output_out_t  -- Output streaming interface
    );
end boolean_protocol_delay;

architecture rtl of boolean_protocol_delay is

  type input_in_array_t is array (DELAY_G - 1 downto 0) of worker_input_in_t;

  -- Interface delay registers
  signal take_store     : std_logic_vector(DELAY_G - 1 downto 0);
  signal input_register : input_in_array_t;

begin

  -- Add delay to align data with respective flow control signals
  delay_pipeline_1_gen : if DELAY_G = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          take_store <= (others => '0');
        -- other registers don't need to be reset as gated by take_store
        elsif(enable = '1') then
          take_store(0)     <= take_in;
          input_register(0) <= input_in;
        end if;
      end if;
    end process;
  end generate;

  delay_pipeline_2_plus_gen : if DELAY_G > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          take_store <= (others => '0');
        -- other registers don't need to be reset as gated by take_store
        elsif(enable = '1') then
          take_store     <= take_store(take_store'length - 2 downto 0) & take_in;
          input_register <= input_register(input_register'length - 2 downto 0) & input_in;
        end if;
      end if;
    end process;
  end generate;

  -- When the delayed interface opcode is the stream opcode and the data is
  -- valid, output the processed stream data rather than the delayed data.
  output_out.data <=
    "0000000" & processed_stream_in when
    input_register(input_register'high).valid = '1' and
    input_register(input_register'high).opcode = bool_timed_sample_sample_op_e
    else input_register(input_register'high).data;

  -- Streaming interface output
  output_out.give        <= take_store(take_store'high) and enable;
  output_out.valid       <= input_register(input_register'high).valid;
  output_out.som         <= input_register(input_register'high).som;
  output_out.eom         <= input_register(input_register'high).eom;
  output_out.byte_enable <= input_register(input_register'high).byte_enable;
  output_out.opcode      <= input_register(input_register'high).opcode;

end rtl;
