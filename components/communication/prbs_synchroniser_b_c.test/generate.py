#!/usr/bin/env python3

# Generates input binary file for prbs_synchroniser testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import random

import scipy.signal

import ocpi_protocols
import ocpi_testing


class PrbsGenerator(ocpi_testing.generator.BooleanGenerator):
    def __init__(self, invert_output, polynomial_taps):
        self.invert_output = invert_output

        self.shift_register_length = polynomial_taps[0]

        # Convert taps from Fibonacci LFSR to Galois LFSR
        self.polynomial_taps = [(self.shift_register_length - tap) for
                                tap in polynomial_taps[1:]]

        super().__init__()
        # Custom test case is to run with a range of bit error rate cases.
        self.CASES["custom"] = self._bit_error_rate

    def generate_binary_sequence(self, initial_seed, sequence_length):
        # Prepare the initial seed value
        # Limit initial seed to values supported by polynomial taps.
        initial_seed_limit = initial_seed & (
            2**(self.shift_register_length) - 1)
        # Convert initial seed to binary string and extend to width of linear
        # feedback shift register.
        format_string = "{:0" + str(self.shift_register_length) + "b}"
        initial_seed_binary = format_string.format(initial_seed_limit)
        # Convert to integer list
        if self.invert_output:
            initial_seed = [1 - int(bit) for bit in initial_seed_binary]
        else:
            initial_seed = [int(bit) for bit in initial_seed_binary]

        # Determine if the output needs to be blanked.
        if (not self.invert_output and initial_seed_limit == 0) or \
                (self.invert_output and
                 initial_seed_limit == (
                     2**(self.shift_register_length) - 1)):
            blank_output = True
        else:
            blank_output = False

        if blank_output:
            if self.invert_output:
                sequence = [True] * sequence_length
            else:
                sequence = [False] * sequence_length
        else:
            # Get PRBS sequence
            binary_sequence = scipy.signal.max_len_seq(
                nbits=self.shift_register_length, state=initial_seed,
                length=sequence_length, taps=self.polynomial_taps)[0]

            # Convert to boolean list
            if self.invert_output:
                sequence = [not bool(bit) for bit in binary_sequence]
            else:
                sequence = [bool(bit) for bit in binary_sequence]

        return sequence

    def typical(self, seed, subcase):
        random.seed(seed)
        initial_seed = random.randint(0, 2**31 - 1)

        data = self.generate_binary_sequence(initial_seed,
                                             self.SAMPLE_DATA_LENGTH)

        return [{"opcode": "sample", "data": data}]

    def _bit_error_rate(self, seed, subcase):
        random.seed(seed)
        initial_seed = random.randint(0, 2**31 - 1)

        data = self.generate_binary_sequence(initial_seed,
                                             self.SAMPLE_DATA_LENGTH)

        # Add an amount of bit error to the data. Allow up to 25% of the data
        # to be incorrect.
        number_of_bit_errors = random.randint(0, self.SAMPLE_DATA_LENGTH // 4)
        for invert_index in [random.randint(0, self.SAMPLE_DATA_LENGTH - 1)
                             for _ in range(number_of_bit_errors)]:
            data[invert_index] = not data[invert_index]

        return [{"opcode": "sample", "data": data}]


enable_property = os.environ.get("OCPI_TEST_enable").upper() == "TRUE"
invert_input_property = os.environ.get(
    "OCPI_TEST_invert_input").upper() == "TRUE"
polynomial_taps_property = [
    int(tap) for tap in os.environ.get("OCPI_TEST_polynomial_taps").split(",")]
check_period_property = int(os.environ.get("OCPI_TEST_check_period"))
error_threshold_property = int(os.environ.get("OCPI_TEST_error_threshold"))
subcase = os.environ["OCPI_TEST_subcase"]

# Remove any zero taps
polynomial_taps_property = list(
    filter(lambda x: x != 0, polynomial_taps_property))

arguments = ocpi_testing.get_generate_arguments()

seed = ocpi_testing.get_test_seed(
    arguments.case, subcase, enable_property, invert_input_property,
    polynomial_taps_property, check_period_property, error_threshold_property)

generator = PrbsGenerator(invert_input_property, polynomial_taps_property)

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "bool_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
