#!/usr/bin/env python3

# Runs checks for prbs_synchroniser_b_c testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import pathlib
import sys

import ocpi_testing

from prbs_synchroniser import PrbsSynchroniser


input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

enable_property = os.environ.get("OCPI_TEST_enable").upper() == "TRUE"
invert_input_property = os.environ.get(
    "OCPI_TEST_invert_input").upper() == "TRUE"
polynomial_taps_property = [
    int(tap) for tap in os.environ.get("OCPI_TEST_polynomial_taps").split(",")]
check_period_property = int(os.environ.get("OCPI_TEST_check_period"))
error_threshold_property = int(os.environ.get("OCPI_TEST_error_threshold"))

# Remove any zero taps
polynomial_taps_property = list(
    filter(lambda x: x != 0, polynomial_taps_property))

# Initiate PRBS synchroniser
prbs_synchroniser_implementation = PrbsSynchroniser(
    enable=enable_property, invert_input=invert_input_property,
    polynomial_taps=polynomial_taps_property,
    check_period=check_period_property,
    error_threshold=error_threshold_property)

test_id = ocpi_testing.get_test_case()
test_case, test_subcase = ocpi_testing.id_to_case(test_id)

# Check the output port
verifier = ocpi_testing.Verifier(prbs_synchroniser_implementation)
verifier.set_port_types(
    ["bool_timed_sample"], ["char_timed_sample"], ["equal"])

# No runtime variables has the worker under test so use the output data file
# name
case_worker_port = pathlib.Path(output_file_path).stem.split(".")
worker = f"{case_worker_port[-3]}.{case_worker_port[-2]}"

if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    # Check output property end of test values
    if enable_property:
        bit_checked_counter = int(
            os.environ.get("OCPI_TEST_bits_checked_counter"))
        bit_error_counter = int(os.environ.get("OCPI_TEST_error_counter"))
        bits_received_counter = int(os.environ.get(
            "OCPI_TEST_bits_received_counter"))
        sync_loss_counter = int(os.environ.get("OCPI_TEST_sync_loss_counter"))

        if bit_checked_counter != \
                prbs_synchroniser_implementation.bit_checked_counter:
            fail_message = (
                "bit_checked_counter does not match expected value\n" +
                "  Python count                   : " +
                f"{prbs_synchroniser_implementation.bit_checked_counter}\n" +
                f"  Implementation-under-test count: {bit_checked_counter}")
            verifier.test_failed(worker, "bit_checked_counter", test_case,
                                 test_subcase, fail_message)
            sys.exit(1)

        if (bit_error_counter !=
                prbs_synchroniser_implementation.bit_error_counter):
            fail_message = (
                "bit_error_counter does not match expected value\n" +
                "  Python count                   : " +
                f"{prbs_synchroniser_implementation.bit_error_counter}\n" +
                f"  Implementation-under-test count: {bit_error_counter}")
            verifier.test_failed(worker, "bit_error_counter", test_case,
                                 test_subcase, fail_message)
            sys.exit(1)

        if (bits_received_counter !=
                prbs_synchroniser_implementation.total_bit_counter):
            fail_message = (
                "bits_received_counter does not match expected value\n" +
                "  Python count                   : " +
                f"{prbs_synchroniser_implementation.total_bit_counter}\n" +
                f"  Implementation-under-test count: {bits_received_counter}")
            verifier.test_failed(worker, "bits_received_counter", test_case,
                                 test_subcase, fail_message)
            sys.exit(1)

        if (sync_loss_counter !=
                prbs_synchroniser_implementation.sync_loss_counter):
            fail_message = (
                "sync_loss_counter does not match expected value\n" +
                "  Python count                   : " +
                f"{prbs_synchroniser_implementation.sync_loss_counter}\n" +
                f"  Implementation-under-test count: {sync_loss_counter}")
            verifier.test_failed(worker, "sync_loss_counter", test_case,
                                 test_subcase, fail_message)
            sys.exit(1)
    sys.exit(0)
else:
    sys.exit(1)
