#!/usr/bin/env python3

# Python implementation of pseudo-random binary sequence synchroniser
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class PrbsSynchroniser(ocpi_testing.Implementation):
    def __init__(self, enable, invert_input, polynomial_taps, check_period,
                 error_threshold):

        super().__init__(enable=enable, invert_input=invert_input,
                         polynomial_taps=polynomial_taps,
                         check_period=check_period,
                         error_threshold=error_threshold)

        self.lfsr_length = polynomial_taps[0]

        self.reset()

    def reset(self):
        self.lfsr = 0
        self.delay = 0
        self.sync_counter = 0
        self.bit_counter = 0
        self.error_counter = 0
        self.total_bit_counter = 0
        self.state = "SYNC_LOST"
        self.bit_error_counter = 0
        self.bit_checked_counter = 0
        self.sync_loss_counter = 0

    def discontinuity(self, *inputs):
        self.reset()
        return self.output_formatter(
            [{"opcode": "discontinuity", "data": None}])

    def sample(self, input_):
        if self.enable:
            output = [self._new_bit(int(bit)) for bit in input_]
            return self.output_formatter([
                {"opcode": "sample", "data": output}])

        else:
            return self.output_formatter([])

    def _new_bit(self, bit):
        if self.state == "SYNC_LOST":
            self.lfsr = self.delay & ((2**self.lfsr_length) - 1)
            self.sync_counter = self.sync_counter + 1

            if self.sync_counter >= (self.lfsr_length + 1):
                self.state = "CHECK_SYNC"
                self.sync_counter = 0

            return_value = 0

        elif self.state == "CHECK_SYNC":
            self.bit_counter = self.bit_counter + 1
            local_prbs = self.advance_prbs()

            if local_prbs != ((self.delay >> self.lfsr_length) & 1):
                self.error_counter = self.error_counter + 1

            if self.bit_counter >= self.check_period:
                if self.error_counter > self.error_threshold:
                    self.state = "SYNC_LOST"
                else:
                    self.state = "SYNCED"
                    self.bit_error_counter = (
                        self.bit_error_counter + self.error_counter)
                    self.bit_checked_counter = (
                        self.bit_checked_counter + self.bit_counter)

                self.error_counter = 0
                self.bit_counter = 0

            return_value = 4

        elif self.state == "SYNCED":
            self.bit_counter = self.bit_counter + 1
            local_prbs = self.advance_prbs()
            if local_prbs != ((self.delay >> self.lfsr_length) & 1):
                self.error_counter = self.error_counter + 1
                # Bit error but still locked
                return_value = 3
            else:
                # No errors and synced
                return_value = 2

            if self.bit_counter >= self.check_period:
                if self.error_counter > self.error_threshold:
                    self.state = "SYNC_LOST"
                    self.sync_loss_counter = self.sync_loss_counter + 1

                self.bit_error_counter = (
                    self.bit_error_counter + self.error_counter)
                self.bit_checked_counter = (
                    self.bit_checked_counter + self.bit_counter)
                self.error_counter = 0
                self.bit_counter = 0

        self.delay = ((self.delay << 1) | bit) & (
            (2**(self.lfsr_length + 1)) - 1)
        self.total_bit_counter = self.total_bit_counter + 1

        return return_value

    def advance_prbs(self):
        next_bit = (self.lfsr >> (self.lfsr_length - 1)) & 1

        feedback = (self.lfsr >> (self.lfsr_length - 1)) & 1
        for tap in self.polynomial_taps[1:]:
            if tap != 0:
                feedback = (feedback ^ (self.lfsr >> (tap - 1))) & 1

        feedback = feedback ^ self.invert_input
        self.lfsr = ((self.lfsr << 1) | feedback) & (
            (2**self.lfsr_length) - 1)

        return next_bit
