.. data_pack_b_uc documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _data_pack_b_uc:


Data packer (``data_pack_b_uc``)
================================
Packs eight boolean values into a byte.

Design
------
This component takes in a stream of boolean inputs, packs the data into bytes, and outputs a stream of unsigned char values. The component can pack data into a byte starting with either the most significant bit (MSB) or the least significant bit (LSB).

Interface
---------
.. literalinclude:: ../specs/data_pack_b_uc-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Messages with a sample opcode are packed into bytes and output as sample opcode messages with a fixed length of one.

If a time opcode arrives message is received after a multiple of eight sample messages data values have been received it is passed directly through the component. If the time opcode message arrives after a non-multiple of eight sample message data values have been received, then the time value received will relate to a sample value in the middle of an output byte. As such, the timestamp is advanced by the sample interval for each new sample value received until the number of input sample values is a multiple of eight, the advanced timestamp is then output. If another non-stream message is received before a multiple of eight sample values have been received, the time opcode message is discarded and not sent.

If a flush opcode messages is received after a multiple of eight sample values have been received it is passed directly through the component. If a flush opcode message arrives after a non-multiple of eight sample values have been received, then the required number of zeros are input into the data packer so that a full byte can be output. The flush opcode message is then passed on to the output of the component.

If a discontinuity opcode message is received after a multiple of eight sample values have been received it is passed directly through the component. If a discontinuity opcode message is received after a non-multiple of eight sample values have been received, the data bits that have been received but not output yet (because the all bits to make a full byte have not yet been received) are discarded and the discontinuity opcode message is passed onto the output of the component.

For all other opcodes messages, if they are received after a multiple of eight input sample values have been received, they are passed directly through the component. If a non-multiple of eight input values have been received the component either flushes the residual data or discards it depending on the value of the ``flush_on_non_stream_opcode`` property. The opcode message is then passed on to the output of the component.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../data_pack_b_uc.hdl ../data_pack_b_uc.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``data_pack_b_uc`` are:

 * Input stream message boundaries are not preserved. All output stream messages are of length one.

Testing
-------
.. ocpi_documentation_test_result_summary::
