.. crc_generator_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _crc_generator_b:


CRC generator (``crc_generator_b``)
===================================
Appends a trailing cyclic redundancy check (CRC) to stream data between two flush or discontinuity messages.

Design
------
On reception of a flush or discontinuity opcode message, a CRC will be generated for any sample data received since the last flush or discontinuity message.

A CRC is an error-detecting algorithm commonly used to detect errors during the exchange of data. The CRC value is the remainder of a polynomial division algorithm on the input data stream.

An example of a common CRC type is CRC-8-WCDMA. This is used for error detection in mobile networks. It has a polynomial shown by :eq:`crc_b_8_wcdma-equation`, which can also be defined as ``0x9B``.

.. math::
   :label: crc_b_8_wcdma-equation

   x^8+x^7+x^4+x^3+x+1

Using the CRC-8-WCDMA example the with the input ``0000 0001 0000 0010 0000 0011 0000 0100``, the resulting output is ``0000 0001 0000 0010 0000 0011 0000 0100 1101 0110``. Where the left most bit is input into the CRC generator component first. Compared to the input the output message size has increased by 8 bits to 40 bits due to the addition of a trailing CRC. The appended CRC is ``1101 0110`` (``0xD6``).

The ``polynomial_taps`` property sets the divisor of the polynomial long division algorithm. It is set at build time.

The ``seed`` property defines the initial value of the CRC at the start of a message. It is settable during run time.

Depending on the application the CRC result may need to be reflected for a different endianness. Property ``output_reflect`` provides the option to enable or disable this. It is set at build time.

Some CRC's require a final XOR on the CRC result. The final XOR value is set by the property ``final_xor``. It is settable during run time.

Interface
---------
.. literalinclude:: ../specs/crc_generator_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Discontinuity and flush opcode messages mark the end of a message and cause the component to append a CRC to any sample data received since the previous discontinuity or flush message. The component applies backpressure whilst outputting the CRC. When the component has finished generating the CRC it is reset, the discontinuity or flush opcode is forwarded to the output, and backpressure is removed.

All other message opcode types are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

   polynomial_taps: An array of up to 64 unsigned characters, meaning a maximum of 64 taps can be specified. If the polynomial is :math:`x^8+x^2+x+1` then ``polynomial_taps`` should be set to "8,2,1". There is no need to specify the :math:`x^0` term, this is implied.

A table listing the default polynomials the component is built for is shown below.

+-----------------------------------------------------------------+--------------------------+--------------------+
| Polynomial                                                      | ``polynomial_taps``      | ``output_reflect`` |
+=================================================================+==========================+====================+
| :math:`x^8 + x^2 + x + 1`                                       | ``8,2,1``                | | ``true`` and     |
|                                                                 |                          | | ``false``        |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^8 + x^4 + x^3 + x^2 + 1`                               | ``8,4,3,2``              | | ``true`` and     |
|                                                                 |                          | | ``false``        |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^8 + x^5 + x^3 + x^2 + x + 1`                           | ``8,5,3,2,1``            | ``false``          |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^8 + x^5 + x^4 + 1`                                     | ``8,5,4``                | ``true``           |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^8 + x^5 + x^4 + x^3 + 1`                               | ``8,5,4,3``              | ``true``           |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^8 + x^7 + x^4 + x^3 + x + 1`                           | ``8,7,4,3,1``            | ``true``           |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^8 + x^7 + x^6 + x^4 + x^2 + 1`                         | ``8,7,6,4,2``            | ``false``          |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^{16} + x^{10} + x^8 + x^7 + x^3 + 1`                   | ``16,10,8,7,3``          | ``false``          |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^{16} + x^{12} + x^5 + 1`                               | ``16,12,5``              | | ``true`` and     |
|                                                                 |                          | | ``false``        |
+-----------------------------------------------------------------+--------------------------+--------------------+
| | :math:`x^{16} + x^{13} + x^{12} + x^{11} + x^{10} + x^8 +`    | | ``16,13,12,11,10,8,``  | | ``true`` and     |
| | :math:`x^6 + x^5 + x^2 + 1`                                   | | ``6,5,2``              | | ``false``        |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^{16} + x^{15} + x^2 + 1`                               | ``16,15,2``              | | ``true`` and     |
|                                                                 |                          | | ``false``        |
+-----------------------------------------------------------------+--------------------------+--------------------+
| | :math:`x^{16} + x^{15} + x^{11} + x^9 + x^8 + x^7 +`          | | ``16,15,11,9,8,7,``    | ``false``          |
| | :math:`x^5 + x^4 + x^2 + x + 1`                               | | ``5,4,2,1``            |                    |
+-----------------------------------------------------------------+--------------------------+--------------------+
| | :math:`x^{16} + x^{15} + x^{13} + x^7 + x^4 + x^2 +`          | | ``16,15,13,7,4,2,``    | ``false``          |
| | :math:`x + 1`                                                 | | ``1``                  |                    |
+-----------------------------------------------------------------+--------------------------+--------------------+
| | :math:`x^{16} + x^{15} + x^{14} + x^{11} + x^6 + x^5 +`       | | ``16,15,14,11,6,5,``   | false              |
| | :math:`x^2 + x + 1`                                           | | ``2,1``                |                    |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^{32} + x ^7 + x^5 + x^3 + x^2 + x + 1`                 | ``32,7,5,3,2,1``         | ``false``          |
+-----------------------------------------------------------------+--------------------------+--------------------+
| | :math:`x^{32} + x^{26} + x^{23} + x^{22} + x^{16} + x^{12} +` | | ``32,26,23,22,16,12,`` | | ``true`` and     |
| | :math:`x^{11} + x^{10} + x^8 + x^7 + x^5 + x^4 +`             | | ``11,10,8,7,5,4,``     | | ``false``        |
| | :math:`x^2 + x + 1`                                           | | ``2,1``                |                    |
+-----------------------------------------------------------------+--------------------------+--------------------+
| | :math:`x^{32} + x^{28} + x^{27} + x^{26} + x^{25} + x^{23} +` | | ``32,28,27,26,25,23,`` | ``true``           |
| | :math:`x^{22} + x^{20} + x^{19} + x^{18} + x^{14} + x^{13} +` | | ``22,20,19,18,14,13,`` |                    |
| | :math:`x^{11} + x^{10} + x^9 + x^8 + x^6 + 1`                 | | ``11,10,9,8,6``        |                    |
+-----------------------------------------------------------------+--------------------------+--------------------+
| | :math:`x^{32} + x^{31} + x^{24} + x^{22} + x^{16} + x^{14} +` | | ``32,31,24,22,16,14,`` | ``false``          |
| | :math:`+ x^8 + x^7 + x^5 + x^3 + x + 1`                       | | ``8,7,5,3,1``          |                    |
+-----------------------------------------------------------------+--------------------------+--------------------+
| | :math:`x^{32} + x^{31} + x^{29} + x^{27} + x^{21} + x^{20} +` | | ``32,31,29,27,21,20,`` | ``true``           |
| | :math:`x^{17} + x^{16} + x^{15} + x^{12} + x^{11} + x^5 +`    | | ``17,16,15,12,11,5,``  |                    |
| | :math:`x^3 + x + 1`                                           | | ``3,1``                |                    |
+-----------------------------------------------------------------+--------------------------+--------------------+
| :math:`x^{64} + x^4 + x^3 + x + 1`                              | ``64,4,3,1``             | ``true``           |
+-----------------------------------------------------------------+--------------------------+--------------------+
| | :math:`x^{64} + x^{62} + x^{57} + x^{55} + x^{54} + x^{53} +` | | ``64,62,57,55,54,53,`` | | ``true`` and     |
| | :math:`x^{52} + x^{47} + x^{46} + x^{45} + x^{40} + x^{39} +` | | ``52,47,46,45,40,39,`` | | ``false``        |
| | :math:`x^{38} + x^{37} + x^{35} + x^{33} + x^{32} + x^{31} +` | | ``38,37,35,33,32,31,`` |                    |
| | :math:`x^{29} + x^{27} + x^{24} + x^{23} + x^{22} + x^{21} +` | | ``29,27,24,23,22,21,`` |                    |
| | :math:`x^{19} + x^{17} + x^{13} + x^{12} + x^{10} + x^9 +`    | | ``19,17,13,12,10,9,``  |                    |
| | :math:`x^7 + x^4 + x + 1`                                     | | ``7,4,1``              |                    |
+-----------------------------------------------------------------+--------------------------+--------------------+

Implementations
---------------
.. ocpi_documentation_implementations:: ../crc_generator_b.hdl ../crc_generator_b.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`CRC serial primitive <crc_serial-primitive>`

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``crc_generator_b`` are:

 * CRC sizes above 64 are not supported.

Testing
-------
.. ocpi_documentation_test_result_summary::
