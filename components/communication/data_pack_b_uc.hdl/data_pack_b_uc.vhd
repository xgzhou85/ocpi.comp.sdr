-- data_pack_b_uc HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_interface;
use sdr_interface.sdr_interface.timestamp_recovery;

architecture rtl of data_pack_b_uc_worker is

  function in_to_out_opcode(inop : bool_timed_sample_OpCode_t) return uchar_timed_sample_OpCode_t is
  begin
    case inop is
      when bool_timed_sample_sample_op_e =>
        return uchar_timed_sample_sample_op_e;
      when bool_timed_sample_sample_interval_op_e =>
        return uchar_timed_sample_sample_interval_op_e;
      when bool_timed_sample_time_op_e =>
        return uchar_timed_sample_time_op_e;
      when bool_timed_sample_flush_op_e =>
        return uchar_timed_sample_flush_op_e;
      when bool_timed_sample_discontinuity_op_e =>
        return uchar_timed_sample_discontinuity_op_e;
      when others =>
        return uchar_timed_sample_metadata_op_e;
    end case;
  end function;
  ------------------------------------------------------------------------------
  -- Interface signals/registers
  ------------------------------------------------------------------------------
  signal take          : std_logic;
  signal stream_valid  : std_logic;
  signal input_ready   : std_logic;
  signal output_ready  : std_logic;
  signal input_hold    : std_logic;
  signal flush         : std_logic;
  signal discontinuity : std_logic;
  signal early_som     : std_logic;
  signal end_of_file   : std_logic;
  ------------------------------------------------------------------------------
  -- State machine signals
  ------------------------------------------------------------------------------
  type state_t is (passthrough_s, shift_data_s,
                   insert_time_s, timestamp_delay_s);
  signal current_state         : state_t;
  signal insert_time           : std_logic;
  signal flush_enable          : std_logic;
  signal give                  : std_logic;
  ------------------------------------------------------------------------------
  -- Data packer signals
  ------------------------------------------------------------------------------
  signal output_word_counter   : unsigned(3 downto 0);
  signal packing_counter       : unsigned(3 downto 0);
  signal data_bit              : std_logic;
  signal data_reg              : std_logic_vector(output_out.data'range);
  ------------------------------------------------------------------------------
  -- Timestamp signals
  ------------------------------------------------------------------------------
  constant timestamp_length_c  : integer := 96 / output_out.data'length;
  signal timestamp             : std_logic_vector(95 downto 0);
  signal timestamp_r           : std_logic_vector(95 downto 0);
  signal timestamp_units       : std_logic_vector(31 downto 0);
  signal timestamp_fraction    : std_logic_vector(63 downto 0);
  signal timestamp_taken       : std_logic;
  signal sample_interval_taken : std_logic;
  signal stream_taken          : std_logic;

begin

  -----------------------------------------------------------------------------
  -- Interface signals
  -----------------------------------------------------------------------------
  -- Take input data whenever both the input and the output are ready.
  output_ready   <= output_in.ready and ctl_in.is_operating;
  input_ready    <= input_in.ready and output_ready;
  take           <= input_ready and not input_hold;
  input_out.take <= take;

  -- Valid only when the input is valid and contains the stream opcode.
  stream_valid <= '1' when (input_in.valid = '1' and
                            input_in.opcode = bool_timed_sample_sample_op_e)
                  else '0';

  flush <= '1' when input_in.opcode = bool_timed_sample_flush_op_e
           or (props_in.flush_on_non_stream_opcode = '1'
               and input_in.opcode /= bool_timed_sample_sample_op_e
               and input_in.opcode /= bool_timed_sample_time_op_e
               and input_in.opcode /= bool_timed_sample_discontinuity_op_e)
           else '0';

  discontinuity <= '1' when input_in.opcode = bool_timed_sample_discontinuity_op_e
                   or (props_in.flush_on_non_stream_opcode = '0'
                       and input_in.opcode /= bool_timed_sample_sample_op_e
                       and input_in.opcode /= bool_timed_sample_time_op_e
                       and input_in.opcode /= bool_timed_sample_flush_op_e)
                   else '0';

  input_hold <= '1' when (current_state = shift_data_s
                          and input_in.opcode /= bool_timed_sample_sample_op_e
                          and input_in.opcode /= bool_timed_sample_time_op_e) or
                current_state = insert_time_s or
                current_state = timestamp_delay_s
                else '0';
  ------------------------------------------------------------------------------
  -- End of file detection
  ------------------------------------------------------------------------------
  early_som_reg_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        early_som <= '0';
      else
        if take = '1' and input_in.som = '1' and input_in.eom = '0' and input_in.valid = '0' and input_in.opcode = bool_timed_sample_sample_op_e then
          early_som <= '1';
        elsif take = '1' then
          early_som <= '0';
        end if;
      end if;
    end if;
  end process;

  end_of_file <= '1' when (early_som = '1'
                           and input_in.valid = '0'
                           and input_in.eom = '1') or
                 (input_in.som = '1'
                  and input_in.eom = '1'
                  and input_in.valid = '0'
                  and input_in.opcode = bool_timed_sample_sample_op_e)
                 else '0';
  ------------------------------------------------------------------------------
  -- Output data register
  ------------------------------------------------------------------------------
  data_bit <= input_in.data(0) when flush_enable = '0' else '0';

  shift_register_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if output_ready = '1' and current_state = insert_time_s then
        data_reg <= timestamp_r(output_out.data'range);
      elsif output_ready = '1' and ((input_ready = '1' and stream_valid = '1') or flush_enable = '1') then
        if (props_in.msb_first = '1') then
          data_reg <= data_reg(output_out.data'length - 2 downto 0) & data_bit;
        else
          data_reg <= data_bit & data_reg(output_out.data'length - 1 downto 1);
        end if;
      elsif output_ready = '1' and input_ready = '1' and current_state = passthrough_s then
        data_reg <= input_in.data;
      end if;
    end if;
  end process;

  output_out.data <= data_reg;

  ------------------------------------------------------------------------------
  -- Interface state machine
  ------------------------------------------------------------------------------
  shift_register_controller_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        current_state <= passthrough_s;
        give          <= '0';
      elsif output_ready = '1' then
        case current_state is
          when passthrough_s =>
            insert_time  <= '0';
            flush_enable <= '0';
            -- If a valid stream is received then start shifting data
            if input_ready = '1' then
              if stream_valid = '1' then
                current_state   <= shift_data_s;
                give            <= '0';
                packing_counter <= to_unsigned(input_in.data'length - 2, packing_counter'length);
              -- If the end of file is detected then send a stream ZLM
              elsif end_of_file = '1' then
                give                   <= '1';
                output_out.som         <= '1';
                output_out.eom         <= '1';
                output_out.valid       <= '0';
                output_out.opcode      <= uchar_timed_sample_sample_op_e;
                output_out.byte_enable <= (others => '0');
              -- Otherwise suppress any non-valid stream messages
              -- (Early SOM, late EOM)
              elsif input_in.opcode = bool_timed_sample_sample_op_e then
                -- Don't pass through invalid stream messages
                give <= '0';
              -- For all other message types passthrough directly
              else
                give                   <= take;
                output_out.som         <= input_in.som;
                output_out.eom         <= input_in.eom;
                output_out.valid       <= input_in.valid;
                output_out.byte_enable <= input_in.byte_enable;
                output_out.opcode      <= in_to_out_opcode(input_in.opcode);
              end if;
            else
              -- Clear give flag when input is not ready
              give <= '0';
            end if;
          when shift_data_s =>
            -- If valid data is received (or we are flushing), keep shifting
            -- data until end of message.
            if (input_ready = '1' and stream_valid = '1') or flush_enable = '1' then
              packing_counter <= packing_counter - 1;
              ------------------------------------------------------------------
              -- Output Packed data
              ------------------------------------------------------------------
              if packing_counter = 0 then
                ----------------------------------------------------------------
                -- Set interface signals
                ----------------------------------------------------------------
                -- Enable the output interface
                give                   <= '1';
                output_out.som         <= '1';
                output_out.eom         <= '1';
                output_out.valid       <= '1';
                output_out.opcode      <= uchar_timed_sample_sample_op_e;
                output_out.byte_enable <= (others => '1');
                ----------------------------------------------------------------
                -- Select next state
                ----------------------------------------------------------------
                -- If a timestamp was received during the message, then send
                -- that now. Otherwise go back to passthrough mode.
                if insert_time = '1' then
                  current_state       <= timestamp_delay_s;
                  output_word_counter <= (others => '0');
                else
                  current_state <= passthrough_s;
                end if;
                -- Clear the flush flag
                flush_enable <= '0';
              end if;
            -- If a discontinuity is received, backpressure is applied to
            -- hold the message until we are back in the passthrough state.
            -- Data in the packer buffer is discarded.
            elsif input_ready = '1' and discontinuity = '1' then
              current_state <= passthrough_s;
            -- If a flush is received, flush packer buffer and output data.
            -- Backpressure is applied to hold the message until we are back in
            -- the passthrough state.
            elsif input_ready = '1' and flush = '1' then
              flush_enable <= '1';
              -- Don't output a timestamp after a flush
              insert_time  <= '0';
            -- If a timestamp is received, record that it occurred and send it
            -- after the output byte.
            elsif input_ready = '1' and input_in.opcode = bool_timed_sample_time_op_e then
              insert_time <= '1';
            -- If the end of file signal is detected, pass it on to the output
            elsif input_ready = '1' and end_of_file = '1' then
              give                   <= '1';
              output_out.som         <= '1';
              output_out.eom         <= '1';
              output_out.valid       <= '0';
              output_out.opcode      <= uchar_timed_sample_sample_op_e;
              output_out.byte_enable <= (others => '0');
            end if;
          when timestamp_delay_s =>
            -- Wait one clock cycle to allow the timestamp for the next sample
            -- to be generated.
            current_state <= insert_time_s;
            timestamp_r   <= timestamp;
            give          <= '0';
          when insert_time_s =>
            --------------------------------------------------------------------
            -- Timestamp interface
            --------------------------------------------------------------------
            give <= '1';
            -- Set SOM based on position in message
            if output_word_counter = 0 then
              output_out.som <= '1';
            else
              output_out.som <= '0';
            end if;
            -- Set EOM based on position in message, or if a timestamp is
            -- to be sent next.
            if (output_word_counter = timestamp_length_c - 1) then
              output_out.eom <= '1';
            else
              output_out.eom <= '0';
            end if;
            -- Set fixed output message parameters
            output_out.valid       <= '1';
            output_out.opcode      <= uchar_timed_sample_time_op_e;
            output_out.byte_enable <= (others => '1');
            --------------------------------------------------------------------
            -- Timestamp shifter
            --------------------------------------------------------------------
            output_word_counter    <= output_word_counter + 1;
            timestamp_r            <= std_logic_vector(unsigned(timestamp_r) srl output_out.data'length);
            --------------------------------------------------------------------
            -- Select next state
            --------------------------------------------------------------------
            if output_word_counter = (timestamp_length_c - 1) then
              current_state <= passthrough_s;
            end if;
        end case;
      end if;
    end if;
  end process;

  output_out.give <= give and output_ready;

  ------------------------------------------------------------------------------
  -- Timestamp calculation
  ------------------------------------------------------------------------------
  timestamp_taken <= '1' when take = '1' and
                     input_in.opcode = bool_timed_sample_time_op_e and
                     input_in.valid = '1' else '0';
  sample_interval_taken <= '1' when take = '1' and
                           input_in.opcode = bool_timed_sample_sample_interval_op_e and
                           input_in.valid = '1' else '0';
  stream_taken <= '1' when take = '1' and
                  input_in.opcode = bool_timed_sample_sample_op_e and
                  input_in.valid = '1' else '0';

  timestamp_i : timestamp_recovery
    generic map (
      ADDER_WIDTH_G   => 32,
      DATA_IN_WIDTH_G => input_in.data'length
      )
    port map (
      clk                          => ctl_in.clk,
      reset                        => ctl_in.reset,
      enable                       => output_ready,
      timestamp_valid_in           => timestamp_taken,
      sample_interval_valid_in     => sample_interval_taken,
      data_valid_in                => stream_taken,
      data_in                      => input_in.data,
      timestamp_units_out          => timestamp_units,
      timestamp_fraction_out       => timestamp_fraction,
      sample_interval_units_out    => open,
      sample_interval_fraction_out => open
      );
  -- Create full 96 bit timestamp
  timestamp <= timestamp_units & timestamp_fraction;

end rtl;
