.. multiplicative_descrambler_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _multiplicative_descrambler_b:


Multiplicative descrambler (``multiplicative_descrambler_b``)
=============================================================
Multiplicative (self-synchronising) descrambler.

Design
------
A scrambler (also referred to as a randomiser) is a device that manipulates a data stream before transmitting. The scrambled data stream can be reversed by a descrambler at the receiver end to recover the original data stream. The purpose of scrambling is to reduce the length of consecutive zeros or ones in a transmitted data stream, which facilitates accurate timing recovery at the receiver. Their designs are usually based on linear feedback shift registers (LFSRs) due to their good statistical properties and ease of implementation in hardware. Note that there are still sequences of ones and zeros that can yield all ones or zeros, or other undesirable periodic output data.

Interface
---------
.. literalinclude:: ../specs/multiplicative_descrambler_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
When a discontinuity opcode message is received a reset like behaviour is triggered in the component and sets the LFSRs to the ``seed`` property value. Once this is done the discontinuity opcode message is forwarded on.

When a flush opcode message is received, ``flush_length`` zeros are injected into the input whilst applying backpressure. Once this is done the flush opcode message is forwarded on.

All other non-sample opcode messages pass through this component without any effect on its operation.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

   polynomial_taps: An array of up to 64 unsigned characters, meaning a maximum of 64 taps can be specified. If the desired polynomial is :math:`x^4+x^2+x+1` then ``polynomial_taps`` should be set to "``4,2,1``". There is no need to specify the :math:`x^0` term, this is implied.

A table listing the default polynomials the component is built for is shown below.

+--------------------------------------------+---------------------+
| Polynomial                                 | ``polynomial_taps`` |
+============================================+=====================+
| :math:`x^3+x^2+1`                          | ``3,2``             |
+--------------------------------------------+---------------------+
| :math:`x^4+x^2+x+1`                        | ``4,2,1``           |
+--------------------------------------------+---------------------+
| :math:`x^7+x^4+1`                          | ``7,4``             |
+--------------------------------------------+---------------------+
| :math:`x^7+x^6+1`                          | ``7,6``             |
+--------------------------------------------+---------------------+
| :math:`x^8+x^5+x+1`                        | ``8,5,1``           |
+--------------------------------------------+---------------------+
| :math:`x^8+x^6+1`                          | ``8,6``             |
+--------------------------------------------+---------------------+
| :math:`x^9+x^5+1`                          | ``9,5``             |
+--------------------------------------------+---------------------+
| :math:`x^{10}+x+1`                         | ``10,1``            |
+--------------------------------------------+---------------------+
| :math:`x^{15}+x^{14}+1`                    | ``15,14``           |
+--------------------------------------------+---------------------+
| :math:`x^{16}+x^{14}+x^{13}+x^{11}+1`      | ``16,14,13,11``     |
+--------------------------------------------+---------------------+
| :math:`x^{16}+x^{15}+x^{13}+x^4+1`         | ``16,15,13,4``      |
+--------------------------------------------+---------------------+
| :math:`x^{17}+x^{12}+1`                    | ``17,12``           |
+--------------------------------------------+---------------------+
| :math:`x^{18}+x^7+1`                       | ``18,7``            |
+--------------------------------------------+---------------------+
| :math:`x^{18}+x^{10}+x^7+x^5+1`            | ``18,10,7,5``       |
+--------------------------------------------+---------------------+
| :math:`x^{23}+x^4+1`                       | ``23,4``            |
+--------------------------------------------+---------------------+
| :math:`x^{23}+x^{18}+1`                    | ``23,18``           |
+--------------------------------------------+---------------------+
| :math:`x^{23}+x^{21}+x^{16}+x^8+x^5+x^2+1` | ``23,21,16,8,5,2``  |
+--------------------------------------------+---------------------+
| :math:`x^{43}+1`                           | ``43``              |
+--------------------------------------------+---------------------+

Implementations
---------------
.. ocpi_documentation_implementations:: ../multiplicative_descrambler_b.hdl ../multiplicative_descrambler_b.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Multiplicative descrambler primitive <multiplicative_descrambler-primitive>`

 * :ref:`Flush inserter primitive <flush_inserter-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``multiplicative_descrambler_b`` are:

 * Polynomial orders greater than 64 are not supported.

Testing
-------
.. ocpi_documentation_test_result_summary::
