-- symbol_mapper_uc_l HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;  -- remove this to avoid all ocpi name collisions
library sdr_interface;
use sdr_interface.narrow_to_wide_protocol_interface_delay;

architecture rtl of symbol_mapper_uc_l_worker is

  -- Constants
  constant delay_c             : integer := 1;
  constant opcode_width_c      : integer := 3;
  constant byte_enable_width_c : integer := 1;
  constant data_in_width_c     : integer := 8;
  constant data_out_width_c    : integer := 32;

  -- Function to convert unsigned_character opcode to standard_logic_vector
  function opcode_to_slv(inop : in uchar_timed_sample_OpCode_t)
    return std_logic_vector is
  begin
    case inop is
      when uchar_timed_sample_sample_op_e =>
        return "000";
      when uchar_timed_sample_time_op_e =>
        return "001";
      when uchar_timed_sample_sample_interval_op_e =>
        return "010";
      when uchar_timed_sample_flush_op_e =>
        return "011";
      when uchar_timed_sample_discontinuity_op_e =>
        return "100";
      when others =>
        return "101";
    end case;
  end function;

  -- Function to convert standard_logic_vector to unsigned character opcode
  function slv_to_opcode(inslv : in std_logic_vector(OPCODE_WIDTH_C - 1 downto 0))
    return long_timed_sample_OpCode_t is
  begin
    case inslv is
      when "000" =>
        return long_timed_sample_sample_op_e;
      when "001" =>
        return long_timed_sample_time_op_e;
      when "010" =>
        return long_timed_sample_sample_interval_op_e;
      when "011" =>
        return long_timed_sample_flush_op_e;
      when "100" =>
        return long_timed_sample_discontinuity_op_e;
      when others =>
        return long_timed_sample_metadata_op_e;
    end case;
  end function;

  -- Flow control signals
  signal give   : std_logic;
  signal take   : std_logic;
  signal enable : std_logic;
  signal output : worker_output_out_t;

  -- Interface signals
  signal flush         : std_logic;
  signal discontinuity : std_logic;
  signal input_opcode  : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal output_opcode : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal output_data   : std_logic_vector(DATA_OUT_WIDTH_C - 1 downto 0);

  signal mapper_out : std_logic_vector(output_out.data'length - 1 downto 0);

begin
  -- Take input data whenever both the input and the output are ready
  take           <= input_in.ready and output_in.ready and ctl_in.is_operating;
  input_out.take <= take;

  -- Reset counter
  enable <= ctl_in.is_operating and output_in.ready;

  ---------------------------------------------------------------------------
  -- Mapper Output
  -------------------------------------------------------------------------
  mux_control_mapper_out_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        mapper_out <= (others => '0');
      else
        if enable = '1' then
          mapper_out <= std_logic_vector(props_in.values(to_integer(unsigned(input_in.data))));
        end if;
      end if;
    end if;
  end process;

  input_opcode <= opcode_to_slv(input_in.opcode);

  protocol_delay_i : entity narrow_to_wide_protocol_interface_delay
    generic map (
      DELAY_G                 => delay_c,
      DATA_IN_WIDTH_G         => DATA_IN_WIDTH_C,
      DATA_OUT_WIDTH_G        => DATA_OUT_WIDTH_C,
      OPCODE_WIDTH_G          => OPCODE_WIDTH_C,
      BYTE_ENABLE_WIDTH_G     => BYTE_ENABLE_WIDTH_C,
      PROCESSED_DATA_OPCODE_G => "000"
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => take,
      processed_stream_in => mapper_out,
      input_som           => input_in.som,
      input_eom           => input_in.eom,
      input_valid         => input_in.valid,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_opcode,
      input_data          => input_in.data,
      output_som          => output.som,
      output_eom          => output.eom,
      output_valid        => output.valid,
      output_give         => output.give,
      output_byte_enable  => output.byte_enable,
      output_opcode       => output_opcode,
      output_data         => output_data
      );

  output.data   <= output_data;
  output.opcode <= slv_to_opcode(output_opcode);

  -- Split stream messages over OCPI_MAX_BYTES_OUTPUT / 4 words, as this is the
  -- maximum size specified by the protocol.
  limit_message_size_i : entity work.long_protocol_message_limiter
    generic map(
      DATA_OUT_WIDTH_G     => output_out.data'length,
      MAX_MESSAGE_LENGTH_G => to_integer(OCPI_MAX_BYTES_OUTPUT) / 4
      )
    port map(
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      enable             => enable,
      output_out         => output,
      output_out_limited => output_out
      );

end rtl;
