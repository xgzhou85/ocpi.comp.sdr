-- Message limiter module for boolean protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Limits stream messages to a fixed maximum length.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.bit_stuffer_b_worker_defs.all;
library sdr_interface;
use sdr_interface.sdr_interface.message_length_limiter;

entity boolean_protocol_message_limiter is
  generic (
    DATA_OUT_WIDTH_G     : integer := 8;
    MAX_MESSAGE_LENGTH_G : integer := 16384
    );
  port (
    clk                : in  std_logic;
    reset              : in  std_logic;
    enable             : in  std_logic;           -- High when output is ready
    output_out         : in  worker_output_out_t;  -- Unlimited interface
    output_out_limited : out worker_output_out_t  -- Limited streaming interface
    );
end boolean_protocol_message_limiter;

architecture rtl of boolean_protocol_message_limiter is

  constant OPCODE_WIDTH_C      : integer := 3;
  constant BYTE_ENABLE_WIDTH_C : integer := 1;

  function opcode_to_slv(inop : in bool_timed_sample_OpCode_t) return std_logic_vector is
  begin
    case inop is
      when bool_timed_sample_sample_op_e =>
        return "000";
      when bool_timed_sample_time_op_e =>
        return "001";
      when bool_timed_sample_sample_interval_op_e =>
        return "010";
      when bool_timed_sample_flush_op_e =>
        return "011";
      when bool_timed_sample_discontinuity_op_e =>
        return "100";
      when others =>
        return "101";
    end case;
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(OPCODE_WIDTH_C - 1 downto 0)) return bool_timed_sample_OpCode_t is
  begin
    case inslv is
      when "000" =>
        return bool_timed_sample_sample_op_e;
      when "001" =>
        return bool_timed_sample_time_op_e;
      when "010" =>
        return bool_timed_sample_sample_interval_op_e;
      when "011" =>
        return bool_timed_sample_flush_op_e;
      when "100" =>
        return bool_timed_sample_discontinuity_op_e;
      when others =>
        return bool_timed_sample_metadata_op_e;
    end case;
  end function;

  signal output_opcode         : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal output_limited_opcode : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal output_limited_data   : std_logic_vector(DATA_OUT_WIDTH_G - 1 downto 0);

begin

  output_opcode <= opcode_to_slv(output_out.opcode);

  message_length_limiter_p : message_length_limiter
    generic map (
      DATA_WIDTH_G         => DATA_OUT_WIDTH_G,
      OPCODE_WIDTH_G       => OPCODE_WIDTH_C,
      BYTE_ENABLE_WIDTH_G  => BYTE_ENABLE_WIDTH_C,
      MAX_MESSAGE_LENGTH_G => MAX_MESSAGE_LENGTH_G,
      LIMIT_OPCODE_G       => "000"
      )
    port map (
      clk                        => clk,
      reset                      => reset,
      enable                     => enable,
      output_som                 => output_out.som,
      output_eom                 => output_out.eom,
      output_valid               => output_out.valid,
      output_give                => output_out.give,
      output_byte_enable         => output_out.byte_enable,
      output_opcode              => output_opcode,
      output_data                => output_out.data,
      output_limited_som         => output_out_limited.som,
      output_limited_eom         => output_out_limited.eom,
      output_limited_valid       => output_out_limited.valid,
      output_limited_give        => output_out_limited.give,
      output_limited_byte_enable => output_out_limited.byte_enable,
      output_limited_opcode      => output_limited_opcode,
      output_limited_data        => output_limited_data
      );

  output_out_limited.data   <= output_limited_data;
  output_out_limited.opcode <= slv_to_opcode(output_limited_opcode);

end rtl;
