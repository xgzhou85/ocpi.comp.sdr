.. symbol_mapper_b_l documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _symbol_mapper_b_l:


Symbol mapper (``symbol_mapper_b_l``)
=====================================
Maps a binary symbol to an integer value.

Design
------
The symbol mapper component takes a binary input value and selects one of two configurable symbol values based on whether the input is a true or false.


A block diagram representation of the implementation is given in :numref:`symbol_mapper_b_l-diagram`.

.. _symbol_mapper_b_l-diagram:

.. figure:: symbol_mapper_b_l.svg
   :alt: Block diagram outlining symbol mapper implementation.
   :align: center

   Symbol mapper implementation.

Interface
---------
.. literalinclude:: ../specs/symbol_mapper_b_l-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The symbol mapper function is applied on data with sample opcodes only.

As the output data type is larger than the input data type, messages can be split to ensure no output messages are longer than the maximum supported message length of the protocol.

All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../symbol_mapper_b_l.hdl ../symbol_mapper_b_l.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface delay (narrow to wide) primitive <narrow_to_wide_protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``symbol_mapper_b_l`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
