// RCC implementation of data_pack_b_uc worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "data_pack_b_uc-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Data_pack_b_ucWorkerTypes;

class Data_pack_b_ucWorker : public Data_pack_b_ucWorkerBase {
  bool msb_first = false;
  bool flush_on_non_stream_opcode = false;

  // Buffer to hold processed data that has not yet been output between loops
  uint8_t input_buffer[DATA_PACK_B_UC_OCPI_MAX_BYTES_OUTPUT];
  size_t input_buffer_length = 0;

  // Position within input port data to indicate unprocessed data that has not
  // yet been saved to input buffer
  size_t input_port_position = 0;

  bool timestamp_pending = false;
  bool send_timestamp = false;
  uint64_t current_sample_time_fraction = 0;
  uint32_t current_sample_time_seconds = 0;
  uint64_t sample_interval_fraction = 0;
  uint32_t sample_interval_seconds = 0;

  RCCResult msb_first_written() {
    this->msb_first = properties().msb_first;
    return RCC_OK;
  }

  RCCResult flush_on_non_stream_opcode_written() {
    this->flush_on_non_stream_opcode = properties().flush_on_non_stream_opcode;
    return RCC_OK;
  }

  // Flush input buffer, padding any incomplete data words with zeroes.
  // Should only be called when input buffer is confirmed to contain data.
  RCCResult flush_buffer() {
    if (this->input_buffer_length) {
      // Allow incomplete data words, data will be padded to 8 bits later
      uint8_t word_length = 8;
      if (this->input_buffer_length < word_length) {
        word_length = this->input_buffer_length;
      }

      uint8_t *outData =
          reinterpret_cast<uint8_t *>(output.sample().data().data());
      // Pack input data into word len output
      *outData = 0;
      for (uint8_t i = 0; i < 8; i++) {
        uint8_t input_bit = 0;
        if (i < word_length) {
          input_bit = this->input_buffer[i];
        }
        if (this->msb_first == true) {
          *outData = (*outData << 1) | input_bit;
        } else {
          *outData = *outData | (input_bit << i);
        }
      }
      // Save remaining boolean inputs to buffer
      memcpy(this->input_buffer, this->input_buffer + word_length,
             this->input_buffer_length - word_length);
      this->input_buffer_length = this->input_buffer_length - word_length;

      // If the buffer has been flushed, any potential pending timestamps can be
      // discarded
      this->timestamp_pending = false;
      this->send_timestamp = false;
      output.setOpCode(Uchar_timed_sampleSample_OPERATION);
      output.sample().data().resize(1);
      output.advance();
      return RCC_OK;
    } else {
      setError("Tried to flush empty buffer");
      return RCC_FATAL;
    }
  }

  // Clears input buffer
  void clear_buffer() {
    this->input_buffer_length = 0;
    // If the buffer has been cleared, any potential pending timestamps can be
    // discarded
    this->timestamp_pending = false;
    this->send_timestamp = false;
  }

  void increment_timestamp() {
    // Check for fractional overflow
    if (this->current_sample_time_fraction >
        (UINT64_MAX - this->sample_interval_fraction)) {
      // Calculate the remaining fraction after the overflow
      this->current_sample_time_fraction =
          UINT64_MAX - (UINT64_MAX - this->sample_interval_fraction) -
          (UINT64_MAX - this->current_sample_time_fraction);
      this->current_sample_time_seconds++;
    } else {
      this->current_sample_time_fraction += this->sample_interval_fraction;
    }
    this->current_sample_time_seconds += this->sample_interval_seconds;
  }

  RCCResult run(bool) {
    // Check if data needs to be flushed or cleared before continuing
    if (input.opCode() != Bool_timed_sampleSample_OPERATION &&
        input.opCode() != Bool_timed_sampleTime_OPERATION &&
        input.opCode() != Bool_timed_sampleFlush_OPERATION &&
        input.opCode() != Bool_timed_sampleDiscontinuity_OPERATION) {
      if (this->input_buffer_length) {
        if (this->flush_on_non_stream_opcode == true) {
          return flush_buffer();
        } else {
          clear_buffer();
        }
      }
    }

    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const bool *inData =
          reinterpret_cast<const bool *>(input.sample().data().data());

      // Pass through a zero length message
      if (length == 0) {
        clear_buffer();
        output.setOpCode(Uchar_timed_sampleSample_OPERATION);
        output.sample().data().resize(0);
        this->input_port_position = 0;
        return RCC_ADVANCE;
      }

      // Process unread received data by saving into buffer
      for (size_t i = this->input_port_position; i < length; i++) {
        this->input_buffer[this->input_buffer_length++] = *inData;
        inData++;
        this->input_port_position++;
      }

      // If there is enough data in buffer to fit into a byte
      if (this->input_buffer_length >= 8) {
        // Pending timestamp should be sent when a complete data word is
        // present, but only after the previously incomplete data word has been
        // sent in its entirety
        if (this->timestamp_pending && this->send_timestamp) {
          this->timestamp_pending = false;
          this->send_timestamp = false;
          output.setOpCode(Bool_timed_sampleTime_OPERATION);
          output.time().fraction() = this->current_sample_time_fraction;
          output.time().seconds() = this->current_sample_time_seconds;
          output.advance();
          return RCC_OK;
        }

        uint8_t *outData =
            reinterpret_cast<uint8_t *>(output.sample().data().data());
        // Pack input data into 8 bit word output
        *outData = 0;
        for (uint8_t i = 0; i < 8; i++) {
          if (this->msb_first == true) {
            *outData = (*outData << 1) | this->input_buffer[i];
          } else {
            *outData = *outData | (this->input_buffer[i] << i);
          }
        }
        // Save remaining boolean inputs to buffer
        memcpy(this->input_buffer, this->input_buffer + 8,
               this->input_buffer_length - 8);
        this->input_buffer_length = this->input_buffer_length - 8;

        if (this->timestamp_pending) {
          // Now that the previously incomplete word has been sent, the
          // timestamp can be sent on the next iteration.
          this->send_timestamp = true;
        }

        // Output length is always 1 to account for single 8 bit word
        output.sample().data().resize(1);
        output.setOpCode(Uchar_timed_sampleSample_OPERATION);
        output.advance();
        return RCC_OK;
      } else {
        this->input_port_position = 0;
        input.advance();
        return RCC_OK;
      }
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      if (this->input_buffer_length == 0) {
        // Pass through time opcode and time data
        output.setOpCode(Uchar_timed_sampleTime_OPERATION);
        output.time().fraction() = input.time().fraction();
        output.time().seconds() = input.time().seconds();
        this->timestamp_pending = false;
        this->send_timestamp = false;
        return RCC_ADVANCE;
      } else {
        // Save time data adjusted for the number of bits required for a full 8
        // bit word.
        this->current_sample_time_fraction = input.time().fraction();
        this->current_sample_time_seconds = input.time().seconds();

        if (this->input_buffer_length > 7) {
          setError("Remaining input length exceeds 8 bit word length");
          return RCC_FATAL;
        }

        for (uint8_t i = 0; i < 8 - this->input_buffer_length; i++) {
          this->increment_timestamp();
        }
        // Time opcode is not passed through until the complete 8 bit word has
        // been output.
        this->timestamp_pending = true;
        this->send_timestamp = false;
        input.advance();
        return RCC_OK;
      }
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Save the sample interval value for potential timestamp adjustment
      this->sample_interval_fraction = input.sample_interval().fraction();
      this->sample_interval_seconds = input.sample_interval().seconds();
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Uchar_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      // Flush data before passing flush opcode
      if (this->input_buffer_length) {
        return flush_buffer();
      }
      // Pass through flush opcode
      output.setOpCode(Uchar_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      if (this->input_buffer_length) {
        clear_buffer();
      }
      // Pass through discontinuity opcode
      output.setOpCode(Uchar_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Uchar_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

DATA_PACK_B_UC_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
DATA_PACK_B_UC_END_INFO
