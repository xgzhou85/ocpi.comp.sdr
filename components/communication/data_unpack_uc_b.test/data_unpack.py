#!/usr/bin/env python3

# Python implementation of data_unpack_uc_b block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing
import numpy as np


class DataUnpack(ocpi_testing.Implementation):
    def __init__(self, msb_first):
        super().__init__(msb_first=msb_first)
        self._max_message_samples = 16384

    def reset(self):
        pass

    def sample(self, values):
        byte_inputs = bytearray(values)

        if self.msb_first:
            input_data_bits = np.unpackbits(byte_inputs)
        else:
            input_data_bits = np.unpackbits(
                byte_inputs).reshape(-1, 8)[:, ::-1].ravel()

        output = [bool(bit) for bit in input_data_bits]

        # Sample messages cannot have more than 16384 samples in each message.
        # As this component increases the data size, break long messages up
        # into messages with no more than 16384 samples.
        messages = [{
            "opcode": "sample",
            "data": output[index: index + self._max_message_samples]}
            for index in range(0, len(output),
                               self._max_message_samples)]

        return self.output_formatter(messages)
