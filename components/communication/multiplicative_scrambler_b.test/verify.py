#!/usr/bin/env python3

# Runs on correct output for multiplicative_scrambler_b implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from multiplicative_scrambler import MultiplicativeScrambler


polynomial_taps = os.environ.get("OCPI_TEST_polynomial_taps")
seed = int(os.environ.get("OCPI_TEST_seed"))
flush_length = int(os.environ.get("OCPI_TEST_flush_length"))

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

scrambler_implementation = MultiplicativeScrambler(polynomial_taps,
                                                   seed,
                                                   flush_length)

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(scrambler_implementation)
verifier.set_port_types(
    ["bool_timed_sample"], ["bool_timed_sample"], ["equal"])
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
