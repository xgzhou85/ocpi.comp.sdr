.. crc_generator_b HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _crc_generator_b-HDL-worker:


``crc_generator_b`` HDL worker
==============================
Galois linear feedback shift register (LFSR) HDL implementation.

Detail
------
In a Galois LFSR implementation each XOR gate requires only two inputs and is positioned between consecutive registers. In the alternative Fibonacci LFSR implementation the XOR gates are cascaded which results in greater propagation delay between registers and therefore negatively affects timing performance. However modern FPGA's can take advantage of their LUT architecture and implement the cascaded XOR gates as a single multi-port XOR gate.

For detail on the implementation of the HDL worker refer to the CRC serial primitive for further detail.

The size of the polynomial sets the number of shift registers required to implement the LFSR.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
