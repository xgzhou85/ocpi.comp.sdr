#!/usr/bin/env python3

# Python implementation of symbol mapper block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_protocols
import ocpi_testing


class SymbolMapper(ocpi_testing.Implementation):
    def __init__(self, values_property):
        super().__init__(values_property=values_property)

    def reset(self):
        pass

    def sample(self, values):
        output_values = [0] * len(values)
        for index, value in enumerate(values):
            output_values[index] = self.values_property[value]

        max_message_length = ocpi_protocols.PROTOCOLS[
            "long_timed_sample"].max_sample_length
        messages = []
        for index in range(0, len(values), max_message_length):
            messages.append(
                {"opcode": "sample",
                 "data": output_values[index: index + max_message_length]})

        return self.output_formatter(messages)
