// RCC implementation of clock_sync_edge_dector_b
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "clock_sync_edge_detector_b-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Clock_sync_edge_detector_bWorkerTypes;

class Clock_sync_edge_detector_bWorker
    : public Clock_sync_edge_detector_bWorkerBase {
  uint16_t samples_per_symbol = 0;
  uint16_t sample_point = 0;

  bool last_sample = false;
  uint16_t sample_counter = 0;

  bool timestamp_received = false;
  uint64_t current_sample_time_fraction = 0;
  uint32_t current_sample_time_seconds = 0;
  uint64_t sample_interval_fraction = 0;
  uint32_t sample_interval_seconds = 0;

  void increment_timestamp() {
    // Check for fractional overflow
    if (this->current_sample_time_fraction >
        (UINT64_MAX - this->sample_interval_fraction)) {
      // Calculate the remaining fraction after the overflow
      this->current_sample_time_fraction =
          UINT64_MAX - (UINT64_MAX - this->sample_interval_fraction) -
          (UINT64_MAX - this->current_sample_time_fraction);
      this->current_sample_time_seconds++;
    } else {
      this->current_sample_time_fraction += this->sample_interval_fraction;
    }
    this->current_sample_time_seconds += this->sample_interval_seconds;
  }

  RCCResult samples_per_symbol_written() {
    this->samples_per_symbol =
        static_cast<uint16_t>(properties().samples_per_symbol);

    if (this->samples_per_symbol > 1) {
      this->samples_per_symbol--;
      // Reset counter on samples_per_symbol change
      this->sample_counter = 0;
      this->last_sample = false;
      return RCC_OK;
    } else {
      setError("samples_per_symbol must be > 1");
      return RCC_FATAL;
    }
  }

  RCCResult sample_point_written() {
    sample_point = static_cast<uint16_t>(properties().sample_point);
    // Reset counter on sample point change
    this->sample_counter = 0;
    this->last_sample = false;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      bool *outData;
      // Get pointers to input and output ports
      size_t inLength = input.sample().data().size();
      const bool *inData =
          reinterpret_cast<const bool *>(input.sample().data().data());
      size_t outLength = 0;

      if (!this->timestamp_received) {
        output.setOpCode(Bool_timed_sampleSample_OPERATION);
        outData = reinterpret_cast<bool *>(output.sample().data().data());
      }

      // Pass through a zero length message
      if (inLength == 0) {
        output.setOpCode(Bool_timed_sampleSample_OPERATION);
        output.sample().data().resize(0);
        return RCC_ADVANCE;
      }
      // Get local copy of counter. We can't act directly on sample_counter
      // as when a timestamp needs to be sent we just step through the data
      // and sample_counter is not updated.
      uint16_t counter = this->sample_counter;

      // Handle first input sample
      if ((this->last_sample ^ inData[0]) ||
          (counter >= this->samples_per_symbol)) {
        counter = 0;
      } else {
        counter++;
      }
      if (counter == this->sample_point) {
        // If there is a pending timestamp
        if (this->timestamp_received) {
          this->timestamp_received = false;
          output.setOpCode(Bool_timed_sampleTime_OPERATION);
          output.time().fraction() = this->current_sample_time_fraction;
          output.time().seconds() = this->current_sample_time_seconds;
          output.advance();
          return RCC_OK;
        } else {
          *outData = inData[0];
          outData++;
          outLength++;
        }
      } else if (this->timestamp_received) {
        this->increment_timestamp();
      }

      // Handle all other samples
      for (size_t i = 1; i < inLength; i++) {
        if ((inData[i] ^ inData[i - 1]) ||
            (counter >= this->samples_per_symbol)) {
          counter = 0;
        } else {
          counter++;
        }
        if (counter == this->sample_point) {
          // If there is a pending timestamp
          if (this->timestamp_received) {
            this->timestamp_received = false;
            output.setOpCode(Bool_timed_sampleTime_OPERATION);
            output.time().fraction() = this->current_sample_time_fraction;
            output.time().seconds() = this->current_sample_time_seconds;
            output.advance();
            return RCC_OK;
          } else {
            *outData = inData[i];
            outData++;
            outLength++;
          }
        } else if (this->timestamp_received) {
          this->increment_timestamp();
        }
      }

      // Edge case where timestamp_received == true, but no samples are
      // output from the clock sync algorithm.
      if (outLength == 0) {
        output.setOpCode(Bool_timed_sampleSample_OPERATION);
      }

      // Store the last sample value for use in the next message
      this->last_sample = inData[inLength - 1];
      // Update the sample_counter so it starts from the same point when the
      // next message is received.
      this->sample_counter = counter;
      // Discard length 1 input messages that have been downsampled away
      if (inLength == 1 && outLength == 0) {
        input.advance();
        return RCC_OK;
      } else {
        // Output the data
        output.sample().data().resize(outLength);
        return RCC_ADVANCE;
      }

    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Save time data. Opcode is not passed through until an
      // output stream value is generated.
      this->current_sample_time_fraction = input.time().fraction();
      this->current_sample_time_seconds = input.time().seconds();
      this->timestamp_received = true;
      input.advance();
      return RCC_OK;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Save the sample interval value for potential timestamp adjustment
      this->sample_interval_fraction = input.sample_interval().fraction();
      this->sample_interval_seconds = input.sample_interval().seconds();
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      // Clear buffer
      this->sample_counter = 0;
      this->last_sample = false;
      // Pass through flush opcode
      output.setOpCode(Bool_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      // Clear buffer
      this->sample_counter = 0;
      this->last_sample = false;
      // Pass through discontinuity opcode
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

CLOCK_SYNC_EDGE_DETECTOR_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
CLOCK_SYNC_EDGE_DETECTOR_B_END_INFO
