// RCC implementation of multiplicative_scrambler_b worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <vector>
#include "multiplicative_scrambler_b-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Multiplicative_scrambler_bWorkerTypes;

class Multiplicative_scrambler_bWorker
    : public Multiplicative_scrambler_bWorkerBase {
  const uint8_t maximum_taps =
      sizeof(MULTIPLICATIVE_SCRAMBLER_B_POLYNOMIAL_TAPS);
  std::vector<bool> state = std::vector<bool>(maximum_taps, 0);
  uint64_t seed = 0;
  uint8_t flush_length = 0;
  uint8_t data_flushed_flag = false;  // Flags when flush data has been handled,
                                      // and flush opcode should be passed

  void reset_state(void) {
    // Reset the LFSRs to the seed property value
    for (int i = 0; i < MULTIPLICATIVE_SCRAMBLER_B_POLYNOMIAL_TAPS[0]; i++) {
      uint64_t mask =
          1ull << (MULTIPLICATIVE_SCRAMBLER_B_POLYNOMIAL_TAPS[0] - i - 1);
      state[i] = seed & mask;
    }
  }

  void scramble_data(const bool data_in[], bool *data_out, size_t length) {
    for (size_t i = 0; i < length; i++) {
      // Compute the value to be xor'd with input
      bool xor_value = 0;
      for (int n = MULTIPLICATIVE_SCRAMBLER_B_POLYNOMIAL_TAPS[0]; n-- > 0;) {
        if (MULTIPLICATIVE_SCRAMBLER_B_POLYNOMIAL_TAPS[n])
          xor_value ^= state[MULTIPLICATIVE_SCRAMBLER_B_POLYNOMIAL_TAPS[n] - 1];
      }
      // Shift the state
      for (int k = MULTIPLICATIVE_SCRAMBLER_B_POLYNOMIAL_TAPS[0]; k-- > 1;)
        state[k] = state[k - 1];
      // Compute the output
      bool output_value = xor_value ^ data_in[i];
      state[0] = output_value;
      *data_out++ = output_value;
    }
  }

  RCCResult seed_written() {
    seed = properties().seed;
    reset_state();
    return RCC_OK;
  }

  RCCResult flush_length_written() {
    flush_length = properties().flush_length;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const bool *inData =
          reinterpret_cast<const bool *>(input.sample().data().data());
      bool *outData = reinterpret_cast<bool *>(output.sample().data().data());
      output.setOpCode(Bool_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);
      scramble_data(inData, outData, length);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Bool_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      if (!data_flushed_flag && flush_length > 0) {
        // Pass through any data remaining in buffer
        output.setOpCode(Bool_timed_sampleSample_OPERATION);
        bool *outData = reinterpret_cast<bool *>(output.sample().data().data());
        size_t length = flush_length * sizeof(bool);
        output.setLength(length);
        bool zeros[1 << (sizeof(flush_length) * 8)] = {0};
        scramble_data(zeros, outData, length);
        data_flushed_flag = true;
        output.advance();  // Advance output only as no more input data is
                           // needed until after the flush opcode is sent.
        return RCC_OK;
      } else {
        data_flushed_flag = false;
        // Pass through flush opcode
        output.setOpCode(Bool_timed_sampleFlush_OPERATION);
        return RCC_ADVANCE;
      }
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      reset_state();
      // Pass through discontinuity opcode
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

MULTIPLICATIVE_SCRAMBLER_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
MULTIPLICATIVE_SCRAMBLER_B_END_INFO
