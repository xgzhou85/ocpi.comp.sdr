#!/usr/bin/env python3

# Python implementation of serial crc block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class CrcSerial(ocpi_testing.Implementation):
    def __init__(self, polynomial_taps, output_reflect, seed, final_xor):
        polynomial_taps = polynomial_taps.split(",")

        super().__init__(polynomial_taps=polynomial_taps,
                         output_reflect=output_reflect, seed=seed,
                         final_xor=final_xor)

        # Get polynomial and size from taps
        self.polynomial_size = int(self.polynomial_taps[0])
        self.polynomial = 0
        for val in self.polynomial_taps[1:]:
            self.polynomial += 1 << int(val)

        self.data = []

    def reset(self):
        self.data = []

    def sample(self, values):
        self.data.extend(values)
        return self.output_formatter(
            [{"opcode": "sample", "data": values}])

    def discontinuity(self, values):
        crc = self._gen_crc()
        self.data = []
        return self.output_formatter(
            [{"opcode": "sample", "data": crc},
             {"opcode": "discontinuity", "data": None}])

    def flush(self, values):
        crc = self._gen_crc()
        self.data = []
        return self.output_formatter(
            [{"opcode": "sample", "data": crc},
             {"opcode": "flush", "data": None}])

    def _gen_crc(self):
        crc = self.seed & (2**self.polynomial_size - 1)

        if len(self.data) > 0:
            # Pad data with zeros to handle bytes
            padding = [False] * (-len(self.data) % 8)
            data_length = len(self.data)
            self.data = self.data + padding

            # Iterate over data set
            for byte_index in range(0, len(self.data), 8):
                # Get next byte from boolean array
                byte = sum(bit << index for index, bit in enumerate(
                    self.data[byte_index: byte_index + 8][::-1]))

                # Move byte to MSB
                crc ^= (byte << (self.polynomial_size - 8)) & (
                    2**self.polynomial_size - 1)
                # Generate crc for data set only, not zero padded data
                if int(data_length / (byte_index + 8)):
                    loop = 8
                else:
                    loop = data_length % 8
                for _ in range(loop):
                    # Test for MSB
                    if crc & (2**(self.polynomial_size - 1)):
                        crc = ((crc << 1) ^ self.polynomial) & (
                            2**self.polynomial_size - 1)
                    else:
                        crc = crc << 1

        # Reflect output CRC
        if self.output_reflect:
            crc_string = format(crc, f"0{self.polynomial_size}b")
            crc = int(crc_string[::-1], 2)

        # Final XOR
        crc ^= (self.final_xor & (2**self.polynomial_size - 1))

        # Convert CRC value to boolean list
        crc_string = format(crc, f"0{self.polynomial_size}b")
        crc_binary = [False] * self.polynomial_size
        for bit_position, bit in enumerate(crc_string):
            crc_binary[bit_position] = (bit == "1")

        return crc_binary
