#!/usr/bin/env python3

# Runs on correct output for crc_generator_b implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from crc_serial import CrcSerial


polynomial_taps_property = os.environ.get("OCPI_TEST_polynomial_taps")
if os.environ.get("OCPI_TEST_output_reflect").lower() == "true":
    output_reflect_property = True
else:
    output_reflect_property = False
seed_property = int(os.environ.get("OCPI_TEST_seed"))
final_xor_property = int(os.environ.get("OCPI_TEST_final_xor"))

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

crc_serial_implementation = CrcSerial(polynomial_taps_property,
                                      output_reflect_property,
                                      seed_property, final_xor_property)

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(crc_serial_implementation)
verifier.set_port_types(
    ["bool_timed_sample"], ["bool_timed_sample"], ["equal"])
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
