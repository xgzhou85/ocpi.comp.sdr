#!/usr/bin/env python3

# Generates the input binary file for crc_generator_b testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os

import ocpi_protocols
import ocpi_testing


class CrcGenerator(ocpi_testing.generator.CharacterGenerator):
    def typical(self, seed, subcase):
        # Typical behaviour for CRC Generator is to expect a flush or
        # discontinuity after streamed data in order to trigger a CRC
        # to be generated, so override the default typical behaviour for
        # this particular case.

        messages = super().typical(seed, subcase)
        messages += ([{"opcode": "discontinuity", "data": None}])
        return messages

    def property(self, seed, subcase):
        # Typical behaviour for CRC Generator is to expect a flush or
        # discontinuity after streamed data in order to trigger a CRC
        # to be generated, so override the default typical behaviour for
        # this particular case.

        messages = super().property(seed, subcase)
        messages += ([{"opcode": "discontinuity", "data": None}])
        return messages

    def sample(self, seed, subcase):
        # Typical behaviour for CRC Generator is to expect a flush or
        # discontinuity after streamed data in order to trigger a CRC
        # to be generated, so override the default typical behaviour for
        # this particular case.

        messages = super().sample(seed, subcase)
        messages += ([{"opcode": "discontinuity", "data": None}])
        return messages

    def message_size(self, seed, subcase):
        # Typical behaviour for CRC Generator is to expect a flush or
        # discontinuity after streamed data in order to trigger a CRC
        # to be generated, so override the default typical behaviour for
        # this particular case.

        messages = super().message_size(seed, subcase)
        messages += ([{"opcode": "discontinuity", "data": None}])
        return messages


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
polynomial_taps_property = os.environ.get("OCPI_TEST_polynomial_taps")
output_reflect_property = (
    os.environ.get("OCPI_TEST_output_reflect").lower() == "true")
seed_property = int(os.environ.get("OCPI_TEST_seed"))
final_xor_property = int(os.environ.get("OCPI_TEST_final_xor"))

seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  polynomial_taps_property,
                                  output_reflect_property,
                                  seed_property, final_xor_property)

generator = CrcGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "bool_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
