#!/usr/bin/env python3

# Python implementation of edge detecting clock synchroniser for verification.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import ocpi_testing
import decimal


class ClockSyncEdgeDetector(ocpi_testing.Implementation):
    def __init__(self, samples_per_symbol, sample_point):
        assert isinstance(samples_per_symbol, int), \
            "samples_per_symbol passed to clock_sync_edge_detector " + \
            "must be an integer, not a " + str(type(samples_per_symbol)) + "."
        assert isinstance(sample_point, int), \
            "sample_point passed to clock_sync_edge_detector " + \
            "must be an integer, not a " + str(type(sample_point)) + "."
        assert samples_per_symbol > 1, \
            "samples_per_symbol passed to clock_sync_edge_detector " + \
            "must be greater than 1. Current value is " + \
            str(samples_per_symbol) + "."
        assert sample_point < samples_per_symbol, \
            "sample_point passed to clock_sync_edge_detector " + \
            "must be less than samples_per_symbol. Current value is " + \
            str(sample_point) + "."

        super().__init__(samples_per_symbol=samples_per_symbol,
                         sample_point=sample_point)

        self._last_sample = False
        self._sample_counter = 0

        self._sample_interval = decimal.Decimal(0)
        self._timestamp = decimal.Decimal(0)
        self._send_timestamp = False

    def reset(self):
        self._last_sample = False
        self._sample_counter = 0

    def sample(self, values):
        output_values = []
        output_messages = []
        for value in values:

            if value != self._last_sample:
                # Edge detected
                self._sample_counter = 0
            else:
                if self._sample_counter >= (self.samples_per_symbol - 1):
                    # Restart to beginning of symbol
                    self._sample_counter = 0
                else:
                    self._sample_counter += 1

            if self._sample_counter == self.sample_point:
                # Add output value to output list
                output_values.append(value)
                # Check if there is a timestamp to send and if so send it
                if self._send_timestamp:
                    self._send_timestamp = False
                    output_messages += [
                        {"opcode": "time", "data": self._timestamp}]
            elif self._send_timestamp:
                # If we have not reached the sample point, but there is a
                # pending timestamp, then increment the timestamp by the
                # sample interval.
                self._timestamp = (decimal.Decimal(self._timestamp) +
                                   decimal.Decimal(self._sample_interval))

            self._last_sample = value
        output_messages += [{"opcode": "sample", "data": output_values}]
        # Suppress length 1 messages that are decimated away.
        if len(values) == 1 and len(output_values) == 0:
            return self.output_formatter([])
        else:
            return self.output_formatter(output_messages)

    def sample_interval(self, value):
        self._sample_interval = value
        return self.output_formatter(
            [{"opcode": "sample_interval", "data": value}])

    def time(self, value):
        self._timestamp = value
        self._send_timestamp = True
        return self.output_formatter([])

    def flush(self, *inputs):
        self._sample_counter = 0
        self._last_sample = False
        return self.output_formatter([{"opcode": "flush", "data": None}])

    def discontinuity(self, *inputs):
        self._sample_counter = 0
        self._last_sample = False
        return self.output_formatter(
            [{"opcode": "discontinuity", "data": None}])
