.. prbs_synchroniser_b_c documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _prbs_synchroniser_b_c:


Pseudo-random binary sequence (PRBS) synchroniser (``prbs_synchroniser_b_c``)
=============================================================================
Synchronises with a stream of pseudo-random boolean symbols and detects errors compared with the ideal pseudo-random binary sequence (PRBS).

Design
------
A PRBS is a deterministic sequence that is statistically similar to a truly random sequence. A PRBS synchroniser automatically aligns a local PRBS sequence generator to the same state as an input PRBS sequence. Once aligned the input PRBS sequence can be compared against the ideal, locally generated, PRBS so that differences between the sequences can be identified. Typically a PRBS synchroniser is used to count the errors that have occurred in the input PRBS pattern due to noise and errors in a communications system.

A PRBS is defined by a polynomial that determines the length of the PRBS before it repeats. The polynomial of the PRBS synchroniser should be set to the same polynomial as the :ref:`PRBS generator <prbs_generator_b>` that is being used to generate the input sequence.

While synchronised the an internal ``bits_checked_counter`` property will increment with each new input sample. Additionally, while synchronised the ``error_counter`` property will increment for each input PRBS symbol that does not match the locally generated PRBS symbol. If ``error_counter`` exceeds ``error_threshold`` errors occur within ``check_period`` input samples, the system will assume synchronisation with the input PRBS has been lost and attempt to resynchronise. Each time the system assumes loss of synchronisation the ``sync_loss_counter`` property is incremented.

The PRBS synchroniser component can be used in two modes:

 #. Without the output port of the PRBS synchroniser connected. In this mode the four counter properties ``bits_checked_counter``, ``error_counter``, ``bits_received_counter`` and ``sync_loss_counter``, provide the output from the component.

    An application using the PRBS synchroniser can be run for a set length of time and the counter properties read at the end of execution, or using the OpenCPI Application Control Interface (ACI) the ``enable`` property can be modified during runtime to start and stop bit error rate tests as desired; the counter properties can be read during runtime via the ACI.

 #. With the output port of the PRBS synchroniser is connected and the status information provided by the output port. This mode is typically use when an understanding of the pattern of bit errors is required i.e. do the errors appear in bursts. The counter properties can still be used in this mode.

Interface
---------
.. literalinclude:: ../specs/prbs_synchroniser_b_c-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port. The meaning of the output values is shown in the table below.

+----------------------+---------------------------------------------------------------------------------------------------+
| Output value         | Meaning                                                                                           |
+======================+===================================================================================================+
| ``0x00``             | Local PRBS is not synchronised with input PRBS.                                                   |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``0x01``             | Invalid.                                                                                          |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``0x02``             | Local PRBS is synchronised with input PRBS. Last input bit received matched local PRBS.           |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``0x03``             | Local PRBS is synchronised with input PRBS. Last input bit received did not match the local PRBS. |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``0x04``             | Local PRBS is checking synchronisation with input PRBS.                                           |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``0x05`` to ``0xFF`` | Invalid.                                                                                          |
+----------------------+---------------------------------------------------------------------------------------------------+

Opcode handling
~~~~~~~~~~~~~~~
The PRBS synchronisation operation is applied on data with sample opcodes only. Input sample opcode message boundaries are preserved on output sample opcode messages.

Discontinuity opcode messages causes the PRBS synchroniser to be reset prior to the opcode being passed to the output.

All other opcodes pass through this component without any effect. When the ``enable`` property is set to ``False`` messages with the sample opcode are not passed through the component, however all other opcodes are still passed through.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   enable: When ``false`` the value of ``bits_checked_counter``, ``error_counter``, ``bits_received_counter`` and ``sync_loss_counter`` are reset to 0.

   check_period: Should be set longer than any period of expected burst errors. Must not be set to 0.

   bits_checked_counter: Does not include inputs bits received while the component is not synchronised to the input PRBS.

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

   polynomial_taps: Is an array of up to six unsigned chars, meaning a maximum of six taps can be specified. If the PRBS polynomial is :math:`x^7+x^6+1` then ``polynomial_taps`` should be set to ``7,6``. There is no need to specify the :math:`x^0` term as this is implied.

The below table gives the polynomials the component is built for.

+-----------------------------------------------+---------------------+---------------+
| Polynomial                                    | ``polynomial_taps`` | Period        |
+===============================================+=====================+===============+
| :math:`x^7 + x^6 + 1`                         |                7, 6 |           127 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^9 + x^5 + 1`                         |                9, 5 |           511 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{11} + x^9 + 1`                      |               11, 9 |         2,047 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{15} + x^{14} + 1`                   |              15, 14 |        32,767 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{16} + x^{14} + x^{13} + x^{11} + 1` |      16, 14, 13, 11 |        65,535 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{17} + x^{14} + 1`                   |              17, 14 |       131,071 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{20} + x^3 + 1`                      |               20, 3 |     1,948,575 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{23} + x^{18} + 1`                   |              23, 18 |     8,388,607 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{29} + x^{27} + 1`                   |              29, 27 |   536,870,911 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{31} + x^{28} + 1`                   |              31, 28 | 2,147,483,647 |
+-----------------------------------------------+---------------------+---------------+

Implementations
---------------
.. ocpi_documentation_implementations:: ../prbs_synchroniser_b_c.hdl

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`PRBS synchroniser primitive <prbs_synchroniser-primitive>`

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``prbs_synchroniser_b_c`` are:

 * A maximum of 6 taps can be specified i.e. :math:`x^a + x^b + x^c + x^d +x^e + x^f + 1`.

 * The ``check_period`` property must not be set to 0.

Testing
-------
.. ocpi_documentation_test_result_summary::
