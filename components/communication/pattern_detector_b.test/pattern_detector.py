#!/usr/bin/env python3

# Python implementation of pattern detector component for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing
import numpy as np


class PatternDetector(ocpi_testing.Implementation):
    def __init__(self, pattern, bitmask):
        super().__init__(pattern=pattern,
                         bitmask=bitmask)

        self.memory = np.zeros(64, dtype=np.bool_)
        self.output = []
        self.bitmask = np.asarray(
            [int(bit) for bit in np.binary_repr(bitmask, width=64)])
        self.pattern = np.asarray(
            [int(bit) for bit in np.binary_repr(pattern, width=64)])

    def reset(self):
        self.memory = np.zeros(64, dtype=np.bool_)

    def sample(self, values):
        stream_index = 0
        match_count = 0
        output_list = []
        for position, value in enumerate(values):
            # Shift and insert new value at 'lsb'
            self.memory = np.roll(self.memory, -1)
            self.memory[63] = value

            # Bitwise logic: pattern AND mask XNOR memory AND mask
            pattern_match = np.logical_not(np.logical_xor(
                np.logical_and(self.memory, self.bitmask),
                np.logical_and(self.pattern, self.bitmask)))
            # When the mask is all zero a pattern match can never occur as no
            # bit as being checked.
            if np.all(pattern_match) and np.any(self.bitmask):
                # Add previous samples to output and insert discontinuity
                output_list.append({"opcode": "sample",
                                    "data": values[stream_index:position + 1]})
                output_list.append({"opcode": "discontinuity",
                                    "data": None})
                match_count += 1
                stream_index = position + 1
        if stream_index < len(values):
            # If match does not occur at the end of the input data, or no match
            # is found, then there is still input data to be appended before
            # the method ends
            output_list.append({"opcode": "sample",
                                "data": values[stream_index:]})
        return self.output_formatter(output_list)

    def discontinuity(self, values):
        self.reset()
        return self.output_formatter([])

    def flush(self, values):
        self.reset()
        return self.output_formatter(
            [{"opcode": "flush", "data": None}])
