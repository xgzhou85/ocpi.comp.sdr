.. pattern_detector_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _pattern_detector_b:


Pattern detector (``pattern_detector_b``)
=========================================
Finds a set binary pattern in a boolean stream.

Design
------
The input boolean stream is checked to see if it contains a set pattern.

The mathematical representation of when a match with the set pattern is given in :eq:`pattern_detector_b-equation`.

.. math::
   :label: pattern_detector_b-equation

   result = \overline{ \texttt{memory} & \texttt{bitmask} | \texttt{pattern} & \texttt{bitmask} }

In :eq:`pattern_detector_b-equation` the boolean "and" (:math:`&`) and boolean "or" (:math:`|`) are bit-wise operations, with a match detected when :math:`result` takes the value ``0xFFFFFFFFFFFFFFFF``.

 * :math:`\texttt{memory}` is the most recently received bits being searched for the pattern.

 * :math:`\texttt{bitmask}` selects which bits are used in the pattern match.

 * :math:`\texttt{pattern}` is the pattern being searched for in the boolean stream.

When a pattern is detected a discontinuity opcode message is generated.

Interface
---------
.. literalinclude:: ../specs/pattern_detector_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Sample opcode messages are searched for the set pattern but otherwise passed directly from input to output without modification.

Since a discontinuity opcode message is inserted when the pattern is detected, incoming discontinuity opcode messages are not forwarded on. Incoming discontinuity opcode messages cause the memory of the last 64 samples to be reset to all zeros.

Incoming flush opcode messages are passed through the component. Incoming flush opcode messages cause the memory of the last 64 samples to be reset to all zeros.

All other opcodes are passed through the component.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   bitmask: Can be used to match patterns shorter than 64 bits, or to ignore specific bits in a pattern. A value of 0 in the N-th bit of ``bitmask`` will exclude the N-th bit of pattern and memory from being compared. A value of 1 in the N-th bit of ``bitmask`` will include the N-th bit of pattern and memory in the comparison. When set to all zero bits all bits are excluded from the check and so no match can occur.

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../pattern_detector_b.hdl ../pattern_detector_b.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``pattern_detector_b`` are:

 * Pattern must be 64 bits or less.

 * ``bitmask`` must be 64 bits or less.

 * Setting ``bitmask`` to ``0x0000000000000000`` (all zeros) will cause the component to insert a discontinuity every time an input sample is processed; this is because setting a mask of all zeros is logically equivalent to setting both memory and pattern to be all zeros, which continuously results in a match.

Testing
-------
.. ocpi_documentation_test_result_summary::
