-- HDL Implementation of PRBS Synchroniser
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_communication;
use sdr_communication.sdr_communication.prbs_synchroniser;

architecture rtl of prbs_synchroniser_b_c_worker is
  constant delay_c             : integer := 1;
  constant LFSR_POLY_ORDER_C   : integer := to_integer(polynomial_taps(0));
  signal lfsr_seed             : std_logic_vector(LFSR_POLY_ORDER_C - 1 downto 0);
  signal output_ready          : std_logic;
  signal valid                 : std_logic;
  signal discontinuity         : std_logic;
  signal reset                 : std_logic;
  signal prbs_bit              : std_logic;
  signal take                  : std_logic;
  signal take_passthrough      : std_logic;
  signal data_out              : std_logic_vector(output_out.data'length - 1 downto 0);
  signal input_interface       : worker_input_in_t;
  signal bits_checked_counter  : unsigned(props_out.bits_checked_counter'length - 1 downto 0);
  signal error_counter         : unsigned(props_out.error_counter'length - 1 downto 0);
  signal bits_received_counter : unsigned(props_out.bits_received_counter'length - 1 downto 0);
  signal sync_loss_counter     : unsigned(props_out.sync_loss_counter'length - 1 downto 0);
begin
  -- Enable output when either output is ready or not connected.
  output_ready <= '1' when (output_in.ready = '1' or output_in.reset = '1') and ctl_in.is_operating = '1' else '0';

  -- PRBS generator module held in reset when enable property is 0
  reset <= '1' when ctl_in.reset = '1' or props_in.enable = '0' or discontinuity = '1' else '0';

  -- Always take data when input port is ready, and output is ready or
  -- not connected.
  take           <= '1' when input_in.ready = '1' and output_ready = '1' else '0';
  input_out.take <= take;

  -- Only passthrough all input signals when enabled. When disabled, suppress
  -- sample opcode messages.
  take_passthrough <= take when props_in.enable = '1' or input_in.opcode /= bool_timed_sample_sample_op_e or
                      (input_in.som = '1' and input_in.eom = '1' and input_in.valid = '0') else '0';

  -- Only enable synchroniser when data is ready, valid, and of the correct
  -- opcode.
  valid <= '1' when take = '1' and input_in.valid = '1' and input_in.opcode = bool_timed_sample_sample_op_e else '0';

  -- PRBS synchroniser reset on discontinuity
  discontinuity <= '1' when take = '1' and input_in.opcode = bool_timed_sample_discontinuity_op_e else '0';

  -- Data bit is LSB of input data interface
  prbs_bit <= input_in.data(0);
  ------------------------------------------------------------------------------
  -- Interface delay module
  ------------------------------------------------------------------------------
  -- Delays streaming interface signals to align with the delay introduced by
  -- the prbs_synchroniser module.
  interface_delay_i : entity work.boolean_to_character_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_ready,
      take_in             => take_passthrough,
      input_in            => input_interface,
      processed_stream_in => data_out,
      output_out          => output_out
      );

  -- Modify valid input signal
  input_interface.data        <= input_in.data;
  input_interface.som         <= input_in.som;
  input_interface.eom         <= input_in.eom;
  input_interface.valid       <= input_in.valid when props_in.enable = '1' or input_in.opcode /= bool_timed_sample_sample_op_e else '0';
  input_interface.opcode      <= input_in.opcode;
  input_interface.byte_enable <= input_in.byte_enable;

  ------------------------------------------------------------------------------
  -- PRBS Synchroniser
  ------------------------------------------------------------------------------
  prbs_sync_i : prbs_synchroniser
    generic map (
      LFSR_WIDTH_G => LFSR_POLY_ORDER_C,
      LFSR_TAP_1_G => to_integer(polynomial_taps(1)),
      LFSR_TAP_2_G => to_integer(polynomial_taps(2)),
      LFSR_TAP_3_G => to_integer(polynomial_taps(3)),
      LFSR_TAP_4_G => to_integer(polynomial_taps(4)),
      LFSR_TAP_5_G => to_integer(polynomial_taps(5))
      )
    port map (
      clk                     => ctl_in.clk,
      reset                   => reset,
      clk_en                  => valid,
      invert_i                => props_in.invert_input,
      data_bit_i              => prbs_bit,
      check_period_i          => props_in.check_period,
      error_threshold_i       => props_in.error_threshold,
      bits_checked_counter_o  => bits_checked_counter,
      bit_error_counter_o     => error_counter,
      bits_received_counter_o => bits_received_counter,
      sync_loss_counter_o     => sync_loss_counter,
      data_o                  => data_out
      );
  ------------------------------------------------------------------------------
  -- Output counters
  ------------------------------------------------------------------------------
  -- Register the counter properties so that all counters can be captured at
  -- the same time.
  properties_reg_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        props_out.bits_checked_counter  <= (others => '0');
        props_out.error_counter         <= (others => '0');
        props_out.bits_received_counter <= (others => '0');
        props_out.sync_loss_counter     <= (others => '0');
      else
        if ctl_in.is_operating = '1' and props_in.counter_update = '1' then
          props_out.bits_checked_counter  <= bits_checked_counter;
          props_out.error_counter         <= error_counter;
          props_out.bits_received_counter <= bits_received_counter;
          props_out.sync_loss_counter     <= sync_loss_counter;
        end if;
      end if;
    end if;
  end process;

end rtl;
