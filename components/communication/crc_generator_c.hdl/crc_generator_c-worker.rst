.. crc_generator_c HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _crc_generator_c-HDL-worker:


``crc_generator_c`` HDL worker
==============================

Detail
------
An XOR logic tree generated at build time calculates the next CRC from the current CRC output and the input data. Refer to the CRC parallel primitive for further details.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
