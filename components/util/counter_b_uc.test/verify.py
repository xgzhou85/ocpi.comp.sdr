#!/usr/bin/env python3

# Runs checks for counter_b_uc testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import pathlib
import sys

import ocpi_testing

from counter_b_uc import counter_b_uc


# Work around for not being able to read initial values of counter_value.
# Corresponding values can be found in -test.xml
default_cv = 0

case_3_0_cv = default_cv
case_3_1_cv = 10
case_3_2_cv = 67
case_3_3_cv = 129
case_3_4_cv = 255

case_15_a_cv = default_cv
case_15_b_cv = 76
case_15_c_cv = 233

case = 0
subcase = 0

enable = (os.environ.get("OCPI_TEST_enable").lower() == "true")
direction = (os.environ.get("OCPI_TEST_direction").lower() == "true")

case_text = str(os.environ.get("OCPI_TESTCASE"))
case = int(case_text[-2:])

subcase_text = str(os.environ.get("OCPI_TESTSUBCASE"))
subcase = int(subcase_text)

# Commented out as verify.py reads value after hdl mod instead of initial value
# counter_value = int(os.environ.get("OCPI_TEST_counter_value"))

if case == 3:
    if subcase == 0:
        counter_value = case_3_0_cv
    elif subcase == 1:
        counter_value = case_3_1_cv
    elif subcase == 2:
        counter_value = case_3_2_cv
    elif subcase == 3:
        counter_value = case_3_3_cv
    else:
        counter_value = case_3_4_cv
elif case == 13:
    if ((subcase // 8) % 3) == 0:
        counter_value = case_15_a_cv
    elif ((subcase // 8) % 3) == 1:
        counter_value = case_15_b_cv
    elif ((subcase // 8) % 3) == 2:
        counter_value = case_15_c_cv
else:
    counter_value = default_cv

size = int(os.environ.get("OCPI_TEST_size"))

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

counter_b_uc_implementation = counter_b_uc(
    enable, direction, counter_value, size)

test_id = ocpi_testing.get_test_case()
test_case, test_subcase = ocpi_testing.id_to_case(test_id)

verifier = ocpi_testing.Verifier(counter_b_uc_implementation)
verifier.set_port_types(
    ["bool_timed_sample"], ["uchar_timed_sample"], ["equal"])

if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    if enable:
        counter_value_property = int(os.environ.get("OCPI_TEST_counter_value"))
        if counter_value_property != counter_b_uc_implementation.counter_value:
            fail_message = (
                "counter_value_property does not match expected value\n" +
                "  Python count                   : " +
                f"{counter_b_uc_implementation.counter_value}\n" +
                f"  Implementation-under-test count: {counter_value_property}")

            # No runtime variables has the worker under test so use the output
            # data file name
            case_worker_port = pathlib.Path(output_file_path).stem.split(".")
            worker = f"{case_worker_port[-3]}.{case_worker_port[-2]}"

            verifier.test_failed(worker, "counter_value", test_case,
                                 test_subcase, fail_message)
            sys.exit(1)
    sys.exit(0)
else:
    sys.exit(1)
