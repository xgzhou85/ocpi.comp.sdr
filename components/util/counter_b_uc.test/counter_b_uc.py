#!/usr/bin/env python3

# Python implementation of counter_b_uc block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class counter_b_uc(ocpi_testing.Implementation):
    def __init__(self, enable, direction, counter_value, size):
        super().__init__(enable=enable, direction=direction,
                         counter_value=counter_value, size=size)

    def reset(self):
        # Removed due to order of operations causing failure
        # __init__() -> reset() - stream()
        # Wipes out value loaded into counter
        # self.counter_value = 0
        pass

    def sample(self, values):
        # Handle rollover of 0 size value
        max_val = self.size
        if(max_val > 0):
            max_val = max_val - 1
        else:
            max_val = 0xFF
        # Empty output values array
        output_values = []
        for value in values:
            # Only modify counter value when enable and input data
            # are both true
            if self.enable and value:
                # Increment or decrement counter
                if self.direction:
                    # If equal to size or
                    # if greater than size
                    # (ie. user has loaded in a value greater that size)
                    # set to 0
                    if self.counter_value >= max_val:
                        self.counter_value = 0
                    else:
                        self.counter_value += 1
                else:
                    # If zero rollover to size
                    if not self.counter_value:
                        self.counter_value = max_val
                    # If value loaded greater than size, set to size
                    elif self.counter_value > max_val:
                        self.counter_value = max_val
                    else:
                        self.counter_value -= 1

            output_values.append(self.counter_value)

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])

    def discontinuity(self, values):
        self.counter_value = 0
        return self.output_formatter(
            [{"opcode": "discontinuity", "data": None}])

    def flush(self, values):
        self.counter_value = 0
        return self.output_formatter(
            [{"opcode": "flush", "data": None}])
