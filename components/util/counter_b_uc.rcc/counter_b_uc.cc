// Counter_b_uc implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "counter_b_uc-worker.hh"
using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Counter_b_ucWorkerTypes;

class Counter_b_ucWorker : public Counter_b_ucWorkerBase {
  bool enable = false;
  bool direction = true;
  uint8_t counter_value = 0;
  uint8_t size = 255;

  RCCResult enable_written() {
    this->enable = properties().enable;
    return RCC_OK;
  }

  RCCResult direction_written() {
    this->direction = properties().direction;
    return RCC_OK;
  }

  RCCResult counter_value_written() {
    this->counter_value = properties().counter_value;
    return RCC_OK;
  }

  RCCResult size_written() {
    this->size = (uint8_t)properties().size - 1;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      // Get pointers to input and output ports
      size_t inLength = input.sample().data().size();
      const bool *inData =
          reinterpret_cast<const bool *>(input.sample().data().data());
      uint8_t *outData =
          reinterpret_cast<uint8_t *>(output.sample().data().data());

      output.setOpCode(Uchar_timed_sampleSample_OPERATION);
      output.sample().data().resize(inLength);

      // For all values in stream
      for (size_t i = 0; i < inLength; i++) {
        // Only modify counter value when enable and input data are both true
        if (this->enable && *inData++) {
          // Increment or decrement counter
          if (this->direction) {
            if (this->counter_value >= this->size) {
              this->counter_value = 0;
            } else {
              this->counter_value += 1;
            }
          } else {
            if (!this->counter_value) {
              this->counter_value = this->size;
            } else if (this->counter_value > this->size) {
              this->counter_value = this->size;
            } else {
              this->counter_value -= 1;
            }
          }
        }
        // Output value to volatile:
        properties().counter_value = this->counter_value;
        // Move to next value
        *outData++ = this->counter_value;
      }
      // Return counter values
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      /// Pass through time opcode and time data
      output.setOpCode(Uchar_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Uchar_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      this->counter_value = 0;
      // Pass through flush opcode
      output.setOpCode(Uchar_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      this->counter_value = 0;
      // Pass through discontinuity opcode
      output.setOpCode(Uchar_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Uchar_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

COUNTER_B_UC_START_INFO
COUNTER_B_UC_END_INFO
