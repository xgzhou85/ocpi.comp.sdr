-- HDL implementation of counter_b_uc.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;  -- remove this to avoid all ocpi name collisions
library sdr_interface;
use sdr_interface.protocol_interface_delay;
library sdr_util;
use sdr_util.sdr_util.counter_updown;

architecture rtl of counter_b_uc_worker is

  -- Function to convert boolean opcode to standard_logic_vector
  function opcode_to_slv_b(inop : in bool_timed_sample_OpCode_t)
    return std_logic_vector is
  begin
    case inop is
      when bool_timed_sample_sample_op_e =>
        return "000";
      when bool_timed_sample_time_op_e =>
        return "001";
      when bool_timed_sample_sample_interval_op_e =>
        return "010";
      when bool_timed_sample_flush_op_e =>
        return "011";
      when bool_timed_sample_discontinuity_op_e =>
        return "100";
      when others =>
        return "101";
    end case;
  end function;

  -- Function to convert standard_logic_vector to unsigned character opcode
  function slv_to_opcode_uc(inslv : in std_logic_vector(2 downto 0))
    return uchar_timed_sample_OpCode_t is
  begin
    case inslv is
      when "000" =>
        return uchar_timed_sample_sample_op_e;
      when "001" =>
        return uchar_timed_sample_time_op_e;
      when "010" =>
        return uchar_timed_sample_sample_interval_op_e;
      when "011" =>
        return uchar_timed_sample_flush_op_e;
      when "100" =>
        return uchar_timed_sample_discontinuity_op_e;
      when others =>
        return uchar_timed_sample_metadata_op_e;
    end case;
  end function;

  -- Flow control signals
  constant delay_c    : integer := 1;
  signal take         : std_logic;
  signal output_ready : std_logic;

  -- Input interface signals
  signal input_in_opcode : std_logic_vector(2 downto 0);
  signal flush           : std_logic;
  signal discontinuity   : std_logic;

  -- Output interface signals
  constant opcode_width_c  : integer := 3;
  constant data_in_width_c : integer := 8;
  signal output_data       : std_logic_vector(data_in_width_c - 1 downto 0);
  signal output_opcode     : std_logic_vector(opcode_width_c - 1 downto 0);

  -- Counter signals
  constant counter_size_c : integer := 8;
  signal counter_reset    : std_logic;
  signal counter_enable   : std_logic;
  signal counter_value    : unsigned(counter_size_c - 1 downto 0);

begin

  -- Take input data whenever both the input and the output are ready
  take           <= input_in.ready and output_in.ready and ctl_in.is_operating;
  input_out.take <= take;
  output_ready   <= ctl_in.is_operating and output_in.ready;

  -- Reset counter
  flush         <= '1' when take = '1' and input_in.opcode = bool_timed_sample_flush_op_e         else '0';
  discontinuity <= '1' when take = '1' and input_in.opcode = bool_timed_sample_discontinuity_op_e else '0';
  counter_reset <= ctl_in.reset or flush or discontinuity;

  -- Valid stream data
  counter_enable <= '1' when props_in.enable = '1' and take = '1' and
                    input_in.valid = '1' and input_in.data(0) = '1' and
                    input_in.opcode = bool_timed_sample_sample_op_e else '0';

  -- Instantiate updown counter
  counter_updown_i : counter_updown
    generic map (
      counter_size_g => counter_size_c
      )
    port map (
      clk        => ctl_in.clk,
      reset      => counter_reset,
      enable     => counter_enable,
      direction  => props_in.direction,
      load_trig  => props_in.counter_value_written,
      load_value => props_in.counter_value,
      length     => props_in.size,
      count_out  => counter_value
      );

  props_out.counter_value <= counter_value;

  -- Convert input opcode
  input_in_opcode <= opcode_to_slv_b(input_in.opcode);

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the processing module.
  interface_delay_i : entity protocol_interface_delay
    generic map (
      DELAY_G                 => delay_c,
      DATA_WIDTH_G            => 8,
      OPCODE_WIDTH_G          => 3,
      BYTE_ENABLE_WIDTH_G     => 1,
      PROCESSED_DATA_MUX_G    => '1',
      PROCESSED_DATA_OPCODE_G => "000"
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_ready,
      take_in             => take,
      processed_stream_in => std_logic_vector(counter_value),
      -- Input interface signals
      input_som           => input_in.som,
      input_eom           => input_in.eom,
      input_valid         => input_in.valid,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_in_opcode,
      input_data          => input_in.data,
      -- Output interface signals
      output_som          => output_out.som,
      output_eom          => output_out.eom,
      output_valid        => output_out.valid,
      output_give         => output_out.give,
      output_byte_enable  => output_out.byte_enable,
      output_opcode       => output_opcode,
      output_data         => output_data
      );

  output_out.data   <= output_data;
  output_out.opcode <= slv_to_opcode_uc(output_opcode);


end rtl;
