#!/usr/bin/env python3

# Generates the input binary file for throttle_b testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os

import ocpi_protocols
import ocpi_testing


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
step_size_property = int(os.environ.get("OCPI_TEST_step_size"))
enable_property = os.environ.get("OCPI_TEST_enable").upper() == "TRUE"
zero_pad_when_not_ready_property = (
    os.environ.get("OCPI_TEST_zero_pad_when_not_ready").upper() == "TRUE")

seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  step_size_property,
                                  enable_property,
                                  zero_pad_when_not_ready_property)

generator = ocpi_testing.generator.BooleanGenerator()

# Throttle tests can take a long time to run. The worker completes no
# function on the data so a reduced test set has little impact on test
# coverage, but greatly reduces test time.
generator.STREAM_DATA_LENGTH = 100

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "bool_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
