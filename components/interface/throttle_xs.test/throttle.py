#!/usr/bin/env python3

# Python implementation of throttle for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class Throttle(ocpi_testing.Implementation):
    def __init__(self, step_size, enable, zero_pad_when_not_ready):
        super().__init__(step_size=step_size, enable=enable,
                         zero_pad_when_not_ready=zero_pad_when_not_ready)

    def reset(self):
        pass

    def sample(self, values):
        return self.output_formatter(
            [{"opcode": "sample", "data": values}])
