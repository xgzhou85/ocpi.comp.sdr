.. throttle_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _throttle_b:


Throttle (``throttle_b``)
=========================
Restricts maximum throughput of data on a streaming interface.

Design
------
The throttle component allows the rate that data samples are taken from its input to be limited. Data samples are passed through unmodified.

The component uses a phase accumulator to allow any data rate to be selected. The ``step_size`` property controls the amount that the phase accumulator is incremented each FPGA clock cycle. The maximum possible throughput through a component is when there is one input and output on every FPGA clock cycle.

The higher ``step_size`` is set the closer the throughput allowed through the throttle will be to the maximum possible throughput.

The value of ``step_size`` for a given throughput (in boolean samples/second) is given by :eq:`throttle_b-equation`.

.. math::
   :label: throttle_b-equation

   \texttt{step_size} = \left \lfloor \frac{2^{32} R}{T_{fpga}} \right \rfloor

In :eq:`throttle_b-equation`:

 * :math:`R` is the desired throughput.

 * :math:`T_{fpga}` is the FPGA clock rate.

The component will only take samples from the input if its output is also ready. This means the ``step_size`` actually sets the maximum allowable throughput. The actual throughput will depend on how often the components connected to the throttle's input and output ports are ready to give or take data.

If data is always ready at the component's input and and the component's output is always ready to take data then the maximum throughput set by ``step_size`` will be achieved.

If the ``zero_pad_when_not_ready`` property is set to ``True`` the component will attempt to output samples exactly at the data rate set by the ``step_size`` property. This is done by adding output data samples, with a value of zero, when then input is not ready but an output sample is required to maintain the set output data rate. This mode is typically used when interfacing with digital to analogue converters that are expecting a continuous stream of samples at a specified data rate. In this mode the actual achieved output rate is still limited by how often the output is ready to accept data as it is not possible to output a sample on to an OpenCPI interface if the interface is not ready.

Interface
---------
.. literalinclude:: ../specs/throttle_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
All opcodes pass through this component at the throttled data rate.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

 step_size: Setting this property to zero is not supported and untested - it will likely result in no data being passed by the component.

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../throttle_b.hdl

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``throttle_b`` are:

 * ``zero_pad_when_not_ready`` should only be used with a data opcode.

Testing
-------
.. ocpi_documentation_test_result_summary::
