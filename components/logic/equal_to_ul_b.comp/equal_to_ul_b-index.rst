.. equal_to_ul_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: equality comparison match same


.. _equal_to_ul_b:


Equal to (``equal_to_ul_b``)
============================
Test if the input sample is equal to a set value.

Design
------
Output a zero or one depending on if the signal is equal to the set value or not.

The mathematical representation of the implementation is given in :eq:`equal_to_ul_b-equation`.

.. math::
   :label: equal_to_ul_b-equation

   y[n] = \begin{cases}
            1-i & x[n] == T \\
            i   & \text{otherwise}
          \end{cases}

In :eq:`equal_to_ul_b-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`i` is the ``invert_output`` property here considered as :math:`1` for ``True`` and :math:`0` for ``False``.

 * :math:`T` is the value the input data samples are compared to.

When ``invert_output`` is set to ``False`` the output will be one when the input is equal to the threshold, otherwise the output will be zero. When ``invert_output`` is set to ``True`` then the output will be zero when the input is equal to the threshold, otherwise the output will be one.

Interface
---------
.. literalinclude:: ../specs/equal_to_ul_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Only values in a sample opcode message are compared to the set ``value``. All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../equal_to_ul_b.hdl ../equal_to_ul_b.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * * :ref:`Protocol interface delay (narrow to wide) primitive <narrow_to_wide_protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``equal_to_ul_b`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
