#!/usr/bin/env python3

# Runs checks for equal_to_ul_b implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from equal_to import Equal_To


value_property = int(os.environ.get("OCPI_TEST_value"))
if os.environ.get("OCPI_TEST_invert_output").upper() == "TRUE":
    invert_output_property = True
else:
    invert_output_property = False

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

equal_to_implementation = Equal_To(
    value_property, invert_output_property)

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(equal_to_implementation)
verifier.set_port_types(
    ["ulong_timed_sample"], ["bool_timed_sample"], ["equal"])

if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
