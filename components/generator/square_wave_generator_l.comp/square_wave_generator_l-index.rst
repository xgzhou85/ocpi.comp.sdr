.. square_wave_generator_l documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _square_wave_generator_l:


Square wave generator (``square_wave_generator_l``)
===================================================
Generates a square wave.

Design
------
Generates a stream of samples that forms a square wave. The duty cycle, period and amplitude of the generated square wave are user settable.

The output of the square wave generator is controlled by a counter that determines if the output should be set to the on or off amplitude. The output is set to ``on_amplitude`` for counter values less than ``on_samples`` and to ``off_amplitude`` for counter values greater than ``on_samples``. The counter is reset to zero when it reaches the sum of ``on_samples`` and ``off_samples``. How these parameters relate to the output wave is shown in :numref:`square_wave_generator_l-diagram`.

.. _square_wave_generator_l-diagram:

.. figure:: square_wave_generator_l.svg
   :alt: Graph of square wave and how available properties relate to the output wave.
   :align: center

   Square wave generator output.

The mathematical representation of the implementation is given in :eq:`square_wave_generator_l-equation`.

.. math::
   :label: square_wave_generator_l-equation

   y[n] = \begin{cases}
            A_{on}  & n \mod (s_{on} + s_{off}) < s_{on} \\
            A_{off} & \text{otherwise}
          \end{cases}

In :eq:`square_wave_generator_l-equation`:

 * :math:`y[n]` is the output values.

 * :math:`A_{on}`, :math:`A_{off}` are ``on_amplitude`` and ``off_amplitude`` respectively.

 * :math:`s_{on}` and :math:`s_{off}` are ``on_samples`` and ``off_sample`` respectively.

When ``on_samples`` or ``off_samples`` is changed the internal counter is reset meaning the output returns to the start of the square wave period.

Interface
---------
.. literalinclude:: ../specs/square_wave_generator_l-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../square_wave_generator_l.hdl ../square_wave_generator_l.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface generator primitive <protocol_interface_generator-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``square_wave_generator_l`` are:

 * Setting of ``message_size`` above 4096 is unsupported behaviour.

 * ``on_samples`` and ``off_samples`` must not be set to zero.

Testing
-------
.. ocpi_documentation_test_result_summary::
