#!/usr/bin/env python3

# Python implementation of constant_s for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import math

import ocpi_testing


class ConstantS(ocpi_testing.Implementation):
    def __init__(self, enable, message_length, value,
                 discontinuity_on_value_change):

        super().__init__(
            enable=enable,
            message_length=message_length,
            value=value,
            discontinuity_on_value_change=discontinuity_on_value_change)

        self.input_ports = []

    def reset(self):
        pass

    def sample(self, *args):
        TypeError("constant_s does not have any input ports")

    def time(self, *args):
        TypeError("constant_s does not have any input ports")

    def sample_interval(self, *args):
        TypeError("constant_s does not have any input ports")

    def flush(self, *args):
        TypeError("constant_s does not have any input ports")

    def discontinuity(self, *args):
        TypeError("constant_s does not have any input ports")

    def metadata(self, *args):
        TypeError("constant_s does not have any input ports")

    def generate(self, total_output_length):
        if self.enable:
            number_of_messages = math.ceil(
                total_output_length / self.message_length)
            messages = [{"opcode": "sample",
                         "data": [self.value] * self.message_length}
                        ] * number_of_messages
            return self.output_formatter(messages)
        else:
            return self.output_formatter([])
