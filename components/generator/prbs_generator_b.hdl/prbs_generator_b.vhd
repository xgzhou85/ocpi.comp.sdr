-- HDL Implementation of PRBS Generator
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_communication;
use sdr_communication.sdr_communication.prbs_generator;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_generator;

architecture rtl of prbs_generator_b_worker is

  constant DELAY_C           : integer := 1;
  constant LFSR_POLY_ORDER_C : integer := to_integer(polynomial_taps(0));
  constant OPCODE_WIDTH_C    : integer := 3;

  function slv_to_opcode(inslv : in std_logic_vector(OPCODE_WIDTH_C - 1 downto 0)) return bool_timed_sample_OpCode_t is
  begin
    case inslv is
      when "100" =>
        return bool_timed_sample_discontinuity_op_e;
      when others =>
        return bool_timed_sample_sample_op_e;
    end case;
  end function;

  signal lfsr_seed            : std_logic_vector(props_in.initial_seed'length - 1 downto 0);
  signal lfsr_seed_slv        : std_logic_vector(LFSR_POLY_ORDER_C - 1 downto 0);
  signal output_ready         : std_logic;
  signal prbs_reset           : std_logic;
  signal prbs_bit, prbs_bit_r : std_logic;
  signal prbs_bit_r_slv       : std_logic_vector(output_out.data'length - 1 downto 0);
  signal prbs_enable          : std_logic;
  signal output_hold          : std_logic;
  signal output_opcode        : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal output_data          : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  -- PRBS generator module held in reset when enable property is 0
  prbs_reset   <= '1' when ctl_in.reset = '1' or props_in.enable = '0' or ctl_in.is_operating = '0' else '0';
  prbs_enable  <= output_ready and not output_hold;
  output_ready <= ctl_in.is_operating and output_in.ready;

  lfsr_seed     <= std_logic_vector(props_in.initial_seed);
  lfsr_seed_slv <= lfsr_seed(LFSR_POLY_ORDER_C - 1 downto 0);

  prbs_generator_i : prbs_generator
    generic map (
      LFSR_WIDTH_G => LFSR_POLY_ORDER_C,
      LFSR_TAP_1_G => to_integer(polynomial_taps(1)),
      LFSR_TAP_2_G => to_integer(polynomial_taps(2)),
      LFSR_TAP_3_G => to_integer(polynomial_taps(3)),
      LFSR_TAP_4_G => to_integer(polynomial_taps(4)),
      LFSR_TAP_5_G => to_integer(polynomial_taps(5))
      )
    port map (
      clk        => ctl_in.clk,
      reset      => prbs_reset,
      clk_en     => prbs_enable,
      invert_i   => props_in.invert_output,
      initial_i  => lfsr_seed_slv,
      data_o     => open,
      data_msb_o => prbs_bit
      );

  output_delay_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if prbs_enable = '1' then
        prbs_bit_r <= prbs_bit;
      end if;
    end if;
  end process;

  prbs_bit_r_slv <= "0000000" & prbs_bit_r;

  interface_generator_i : protocol_interface_generator
    generic map (
      DELAY_G                 => DELAY_C,
      DATA_WIDTH_G            => output_out.data'length,
      OPCODE_WIDTH_G          => OPCODE_WIDTH_C,
      BYTE_ENABLE_WIDTH_G     => output_out.byte_enable'length,
      PROCESSED_DATA_OPCODE_G => "000",
      DISCONTINUITY_OPCODE_G  => "100"
      )
    port map (
      clk                   => ctl_in.clk,
      reset                 => ctl_in.reset,
      output_ready          => output_ready,
      discontinuity_trigger => '0',
      generator_enable      => '1',
      generator_reset       => prbs_reset,
      processed_stream_in   => prbs_bit_r_slv,
      message_length        => props_in.message_length,
      output_hold           => output_hold,
      output_som            => output_out.som,
      output_eom            => output_out.eom,
      output_valid          => output_out.valid,
      output_give           => output_out.give,
      output_byte_enable    => output_out.byte_enable,
      output_opcode         => output_opcode,
      output_data           => output_data
      );

  output_out.data   <= output_data;
  output_out.opcode <= slv_to_opcode(output_opcode);

end rtl;
