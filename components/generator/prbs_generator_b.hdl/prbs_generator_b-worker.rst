.. prbs_generator_b HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _prbs_generator_b-HDL-worker:


``prbs_generator_b`` HDL worker
===============================
A Fibonacci LFSR HDL implementation. This is sometimes referred to as a backwards counter LFSR implementation. The alternative LFSR implementation is known as a Galois LFSR. Fibonacci and Galois LFSRs will output a different sequence for the same polynomial and initial value. To achieve the same sequence in a Galois LFSR as with a Fibonacci LFSR, subtract each of the tap values from the polynomial order. For example a Fibonacci LFSR with polynomial :math:`x^7 + x^6 + 1` is equivalent to a Galois LFSR with polynomial :math:`x^7 + x^1 + 1`.

Detail
------
.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
