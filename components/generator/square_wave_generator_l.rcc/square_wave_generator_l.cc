// Square wave generator implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "square_wave_generator_l-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Square_wave_generator_lWorkerTypes;

class Square_wave_generator_lWorker : public Square_wave_generator_lWorkerBase {
  bool enable = false;
  bool discontinuity_on_amplitude_change = false;
  bool discontinuity_on_cycle_change = false;

  uint16_t message_length = 0;
  uint32_t on_samples = 0;
  uint32_t off_samples = 0;
  int32_t on_amplitude = 0;
  int32_t off_amplitude = 0;

  bool samples_changed = false;
  bool amplitude_changed = false;

  // These flags are used to surpress discontinuity opcodes during the
  // parameter initialisation stage. These must be set within the writesync
  // functions as well as the run function, as the writesync function may be
  // called multiple times before the program enters its operating state.
  bool startup_complete = false;

  uint64_t sample_counter = 0;

  RCCResult enable_written() {
    enable = properties().enable;
    return RCC_OK;
  }

  RCCResult message_length_written() {
    message_length = properties().message_length;
    if (message_length == 0) {
      setError("message_length must be greater than 0");
      return RCC_FATAL;
    } else if (message_length >
               (SQUARE_WAVE_GENERATOR_L_OCPI_MAX_BYTES_OUTPUT / 4)) {
      setError("message_length greater than maximum allowed value");
      return RCC_FATAL;
    }
    return RCC_OK;
  }

  RCCResult on_samples_written() {
    on_samples = properties().on_samples;
    // The wave should be reset to its initial state by setting sample_counter
    // to zero.
    sample_counter = 0;
    // Discontinuity opcode should be supressed until the property has been
    // initialised or the program has entered operating state.
    if (startup_complete) {
      samples_changed = true;
    }
    return RCC_OK;
  }

  RCCResult off_samples_written() {
    off_samples = properties().off_samples;
    // The wave should be reset to its initial state by setting sample_counter
    // to zero.
    sample_counter = 0;
    // Discontinuity opcode should be supressed until the property has been
    // initialised or the program has entered operating state.
    if (startup_complete) {
      samples_changed = true;
    }
    return RCC_OK;
  }

  RCCResult on_amplitude_written() {
    on_amplitude = properties().on_amplitude;
    // Discontinuity opcode should be supressed until the property has been
    // initialised or the program has entered operating state.
    if (startup_complete) {
      amplitude_changed = true;
    }
    return RCC_OK;
  }

  RCCResult off_amplitude_written() {
    off_amplitude = properties().off_amplitude;
    // Discontinuity opcode should be supressed until the property has been
    // initialised or the program has entered operating state.
    if (startup_complete) {
      amplitude_changed = true;
    }
    return RCC_OK;
  }

  RCCResult discontinuity_on_amplitude_change_written() {
    discontinuity_on_amplitude_change =
        properties().discontinuity_on_amplitude_change;
    return RCC_OK;
  }

  RCCResult discontinuity_on_cycle_change_written() {
    discontinuity_on_cycle_change = properties().discontinuity_on_cycle_change;
    return RCC_OK;
  }

  RCCResult run(bool) {
    startup_complete = true;

    if (samples_changed && discontinuity_on_cycle_change) {
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      output.advance();
      samples_changed = false;
      return RCC_OK;
    }

    if (amplitude_changed && discontinuity_on_amplitude_change) {
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      output.advance();
      amplitude_changed = false;
      return RCC_OK;
    }

    if (enable) {
      int32_t *outData = output.sample().data().data();
      output.setOpCode(Long_timed_sampleSample_OPERATION);
      output.sample().data().resize(message_length);

      for (size_t i = 0; i < message_length; i++) {
        if (sample_counter < on_samples) {
          *outData++ = on_amplitude;
          sample_counter++;
        } else if ((sample_counter >= on_samples) &&
                   (sample_counter < (on_samples + off_samples))) {
          *outData++ = off_amplitude;
          sample_counter++;
        } else {
          *outData++ = on_amplitude;
          sample_counter = 1;
        }
      }

      output.advance();
      return RCC_OK;
    } else {
      sample_counter = 0;
      return RCC_OK;
    }
  }
};

SQUARE_WAVE_GENERATOR_L_START_INFO

SQUARE_WAVE_GENERATOR_L_END_INFO
