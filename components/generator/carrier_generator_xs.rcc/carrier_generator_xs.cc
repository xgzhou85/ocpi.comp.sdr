// RCC implementation of carrier_generator_xs worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include "carrier_generator_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Carrier_generator_xsWorkerTypes;

class Carrier_generator_xsWorker : public Carrier_generator_xsWorkerBase {
  bool enable = false;
  int32_t step_size = 0;
  uint16_t amplitude = 0;
  bool discontinuity_on_step_size_change = false;
  bool discontinuity_on_carrier_amplitude_change = false;
  uint16_t message_length = 0;

  bool step_size_changed = false;
  bool carrier_amplitude_changed = false;

  // These flags are used to surpress discontinuity opcodes during the
  // parameter initialisation stage.
  bool startup_complete = false;

  uint64_t sample_counter = 0;

  // Notification that enable property has been written
  RCCResult enable_written() {
    this->enable = properties().enable;
    return RCC_OK;
  }

  // Notification that step_size property has been written
  RCCResult step_size_written() {
    this->step_size = properties().step_size;

    // Discontinuity opcode should be supressed until the program has entered
    // operating state
    if (this->startup_complete) {
      this->step_size_changed = true;
    }
    return RCC_OK;
  }

  // Notification that carrier_amplitude property has been written
  RCCResult carrier_amplitude_written() {
    uint16_t carrier_amplitude = properties().carrier_amplitude;

    if (carrier_amplitude > 19872) {
      setError("carrier amplitude must be less than 19872");
      return RCC_FATAL;
    }

    RCCFloat amplitude_scaling = 1.646760258;
    this->amplitude = carrier_amplitude * amplitude_scaling;

    // Discontinuity opcode should be supressed until the program has entered
    // operating state.
    if (this->startup_complete) {
      this->carrier_amplitude_changed = true;
    }
    return RCC_OK;
  }

  // Notification that discontinuity_on_step_size_change property has been
  // written
  RCCResult discontinuity_on_step_size_change_written() {
    this->discontinuity_on_step_size_change =
        properties().discontinuity_on_step_size_change;
    return RCC_OK;
  }

  // Notification that discontinuity_on_carrier_amplitude_change property has
  // been written
  RCCResult discontinuity_on_carrier_amplitude_change_written() {
    this->discontinuity_on_carrier_amplitude_change =
        properties().discontinuity_on_carrier_amplitude_change;
    return RCC_OK;
  }

  // Notification that message_length property has been written
  RCCResult message_length_written() {
    this->message_length = properties().message_length;

    if (this->message_length == 0) {
      setError("message length must be greater than 0");
      return RCC_FATAL;
    } else if (this->message_length >
               (CARRIER_GENERATOR_XS_OCPI_MAX_BYTES_OUTPUT / 4)) {
      setError("message length greater than maximum allowed value");
      return RCC_FATAL;
    }
    return RCC_OK;
  }

  RCCResult run(bool) {
    this->startup_complete = true;

    if (this->step_size_changed && this->discontinuity_on_step_size_change) {
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      output.advance();
      this->step_size_changed = false;
      return RCC_OK;
    }

    if (this->carrier_amplitude_changed &&
        this->discontinuity_on_carrier_amplitude_change) {
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      output.advance();
      this->carrier_amplitude_changed = false;
      return RCC_OK;
    }

    if (this->enable) {
      size_t length = this->message_length;
      Complex_short_timed_sampleSampleData *outData =
          output.sample().data().data();

      for (uint16_t i = 0; i < this->message_length; i++) {
        outData->real = this->amplitude *
                        cos((2 * M_PI * this->step_size *
                             (this->sample_counter + 1) / ((uint64_t)1 << 32)));
        outData->imaginary = this->amplitude * sin((2 * M_PI * this->step_size *
                                                    (this->sample_counter + 1) /
                                                    ((uint64_t)1 << 32)));
        this->sample_counter++;
        outData++;
      }
      output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);
      output.advance();
      return RCC_OK;
    } else {
      return RCC_OK;
    }
  }
};

CARRIER_GENERATOR_XS_START_INFO

CARRIER_GENERATOR_XS_END_INFO
