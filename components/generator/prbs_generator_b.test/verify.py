#!/usr/bin/env python3

# Runs checks prbs_generator_b testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_protocols
import ocpi_testing

from prbs_generator import PrbsGenerator


output_file_path = str(sys.argv[-1])

enable_property = os.environ.get("OCPI_TEST_enable").upper() == "TRUE"
invert_output_property = os.environ.get(
    "OCPI_TEST_invert_output").upper() == "TRUE"
initial_seed_property = int(os.environ.get("OCPI_TEST_initial_seed"))
polynomial_taps_property = [
    int(tap) for tap in os.environ.get("OCPI_TEST_polynomial_taps").split(",")]

# Remove any zero taps
polynomial_taps_property = list(
    filter(lambda x: x != 0, polynomial_taps_property))

# Initiate PRBS Generator object
prbs_generator_implementation = PrbsGenerator(
    enable=enable_property, invert_output=invert_output_property,
    initial_seed=initial_seed_property,
    polynomial_taps=polynomial_taps_property)

# Set the total output length to be generated
with ocpi_protocols.ParseMessagesFile(
        output_file_path, "bool_timed_sample") as output_file:
    output_length = output_file.get_sample_data_length()
prbs_generator_implementation.no_input_settings[
    "total_output_length"] = output_length

test_id = ocpi_testing.get_test_case()

# Check the result
verifier = ocpi_testing.Verifier(prbs_generator_implementation)
verifier.set_port_types([], ["bool_timed_sample"], ["equal"])

if verifier.verify(test_id, [], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
