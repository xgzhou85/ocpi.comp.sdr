-- HDL Implementation of constant short generator
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_generator;

architecture rtl of constant_s_worker is

  constant DELAY_C        : integer := 1;
  constant OPCODE_WIDTH_C : integer := 3;

  -- Function to convert standard_logic_vector to short opcode
  function slv_to_opcode_s(inslv : in std_logic_vector(2 downto 0))
    return short_timed_sample_OpCode_t is
  begin
    case inslv is
      when "100" =>
        return short_timed_sample_discontinuity_op_e;
      when others =>
        return short_timed_sample_sample_op_e;
    end case;
  end function;

  -- Generator signals
  signal discontinuity_trigger   : std_logic;
  signal output_hold             : std_logic;
  signal output_ready            : std_logic;
  signal output_interface_opcode : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal static_value            : std_logic_vector(props_in.value'range);
  signal output_data             : std_logic_vector(output_out.data'range);

begin

  -- Signal when output is ready
  output_ready <= ctl_in.is_operating and output_in.ready;

  discontinuity_trigger <= props_in.discontinuity_on_value_change and
                           props_in.value_written;

  -- Instantiate generator
  interface_generator_i : protocol_interface_generator
    generic map (
      DELAY_G                 => DELAY_C,
      DATA_WIDTH_G            => output_out.data'length,
      OPCODE_WIDTH_G          => OPCODE_WIDTH_C,
      BYTE_ENABLE_WIDTH_G     => output_out.byte_enable'length,
      PROCESSED_DATA_OPCODE_G => "000",
      DISCONTINUITY_OPCODE_G  => "100"
      )
    port map (
      clk                   => ctl_in.clk,
      reset                 => ctl_in.reset,
      output_ready          => output_ready,
      -- High when discontinuity event occurs
      discontinuity_trigger => discontinuity_trigger,
      generator_enable      => props_in.enable,
      generator_reset       => ctl_in.reset,
      -- Connect output from data processing module
      processed_stream_in   => static_value,
      -- Specifies the length that the output message should be
      message_length        => props_in.message_length,
      -- High when processing module should be disabled.
      output_hold           => output_hold,
      -- Output interface signals
      output_som            => output_out.som,
      output_eom            => output_out.eom,
      output_valid          => output_out.valid,
      output_give           => output_out.give,
      output_byte_enable    => output_out.byte_enable,
      output_opcode         => output_interface_opcode,
      output_data           => output_data
      );

  output_out.opcode <= slv_to_opcode_s(output_interface_opcode);
  output_out.data   <= output_data;

  constant_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        static_value <= (others => '0');
      elsif output_hold = '0' and props_in.enable = '1' then
        static_value <= std_logic_vector(props_in.value);
      end if;
    end if;
  end process;


end rtl;
