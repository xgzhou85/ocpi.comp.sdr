#!/usr/bin/env python3

# Python implementation of cic_interpolator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


from numpy import int32


import ocpi_testing
import numpy as np


# Comb stage
class Comb:
    def __init__(self, delay):
        self._delay = delay
        self.reset()

    def reset(self):
        self._delay_buffer = [0] * (self._delay)

    def call(self, value):
        self._delay_buffer.append(value)
        return value - self._delay_buffer.pop(0)


# Integrator stage
class Integrator:
    def __init__(self):
        self.reset()

    def reset(self):
        self._previous_value = 0

    def call(self, value):
        self._previous_value = value + self._previous_value
        return self._previous_value


# Interpolate stage
class Interpolate:
    def __init__(self, upsample_factor):
        self._upsample_factor = upsample_factor

    def call(self, value):
        return [value] + ([0] * (self._upsample_factor - 1))


class CicInterpolatorSingleChannel():
    def __init__(self, cic_order, delay_factor, up_sample_factor,
                 output_scale_factor, cic_reg_size):
        self.cic_order = cic_order
        self.delay_factor = delay_factor
        self.up_sample_factor = up_sample_factor
        self.output_scale_factor = output_scale_factor
        self.cic_reg_size = cic_reg_size

        self._max_internal_value = 2**(self.cic_reg_size - 1) - 1
        self._min_internal_value = -1 * (2**(self.cic_reg_size - 1))

        self._integrators = [Integrator() for n in range(self.cic_order)]
        self._combs = [Comb(self.delay_factor) for n in range(self.cic_order)]
        self._interpolator = Interpolate(self.up_sample_factor)

    def reset(self):
        for n in range(0, self.cic_order):
            self._combs[n].reset()
            self._integrators[n].reset()

    def sample(self, values):
        output_values = []
        for value in values:
            # Apply each comb stage
            for n in range(0, self.cic_order):
                value = self._combs[n].call(value)
                # Match implementation bit width
                while value < self._min_internal_value:
                    value = value + (2**self.cic_reg_size)
                while value > self._max_internal_value:
                    value = value - (2**self.cic_reg_size)

            value = self._interpolator.call(value)

            # Apply each integrator stage
            for n in range(0, len(value)):
                for m in range(0, self.cic_order):
                    value[n] = self._integrators[m].call(value[n])
                    # Match implementation bit width
                    while value[n] < self._min_internal_value:
                        value[n] = value[n] + (2**self.cic_reg_size)
                    while value[n] > self._max_internal_value:
                        value[n] = value[n] - (2**self.cic_reg_size)

            # Do the output scaling
            output_values = output_values + list(value)

        rounded_output_values = []
        for value in (output_values):
            if self.output_scale_factor == 0:
                # Don't halfup round
                value = np.int16(value)
                rounded_output_values.append(value)

            else:
                # Round halfup
                value = np.right_shift(
                    int(value), self.output_scale_factor - 1) + 1
                value = np.int16(np.right_shift(value, 1))
                rounded_output_values.append(value)

        return rounded_output_values


class CicInterpolator(ocpi_testing.Implementation):
    def __init__(self, cic_order, delay_factor, up_sample_factor,
                 output_scale_factor, flush_length, cic_reg_size):
        super().__init__(cic_order=cic_order,
                         delay_factor=delay_factor,
                         up_sample_factor=up_sample_factor,
                         output_scale_factor=output_scale_factor,
                         flush_length=flush_length,
                         cic_reg_size=cic_reg_size)

        self._max_message_samples = 4096

        self._cic_real = CicInterpolatorSingleChannel(
            self.cic_order, self.delay_factor, self.up_sample_factor,
            self.output_scale_factor, self.cic_reg_size)
        self._cic_imag = CicInterpolatorSingleChannel(
            self.cic_order, self.delay_factor, self.up_sample_factor,
            self.output_scale_factor, self.cic_reg_size)

    def reset(self):
        self._cic_real.reset()
        self._cic_imag.reset()

    def sample(self, values):
        input_data = np.array(values)
        output_real = self._cic_real.sample(np.real(input_data))
        output_imag = self._cic_imag.sample(np.imag(input_data))
        output = list(np.array(output_real) + (np.array(output_imag) *
                                               complex(0, 1)))

        # Sample messages cannot have more than 4096 samples in each message.
        # As this component increases the data size, break long messages up
        # into messages with no more than 4096 samples.
        messages = [{
            "opcode": "sample",
            "data": output[index: index + self._max_message_samples]}
            for index in range(0, len(output),
                               self._max_message_samples)]
        return self.output_formatter(messages)

    def flush(self, *inputs):
        if self.flush_length > 0:
            flush_data = [0] * self.flush_length

            # As sample() will return a named tuple, with the names the output
            # port names - and cic_interpolator only has one output port
            # called "output" get the messages from that port. Then add the
            # flush message as this will be passed through after the flushed
            # data.
            messages = self.sample(flush_data).output
            messages.append({"opcode": "flush", "data": None})
            return self.output_formatter(messages)
        else:
            return self.output_formatter([{"opcode": "flush", "data": None}])
