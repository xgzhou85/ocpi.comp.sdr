.. frequency_demodulator_xs_s HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _frequency_demodulator_xs_s-HDL-worker:


``frequency_demodulator_xs_s`` HDL worker
=========================================
HDL implementation which makes use of the CORDIC algorithm.

Detail
------
This worker implementation uses the CORDIC rectangular to polar primitive and complex multiplier primitive.

Due to the accumulating rounding errors of the CORDIC rectangular to polar primitive, very small input values (``x_in`` and ``y_in`` on the primitive) will give a low accuracy output. The figure below plots the maximum absolute deviation of the HDL implementation compared to the expected value for combinations of small input values.

In the below figure, the deviation is found for each of :math:`a + jn`, :math:`n + ja`, :math:`(n + a) + jn` and :math:`n + j(n + a)`, and for all integer values of :math:`a` in the range :math:`-3 \leq a \leq 3`. The maximum deviation found for :math:`n` for each of seen for all of these conditions is then plotted and shown below.

.. figure:: frequency_demodulator_xs_s_hdl_deviation.svg
   :alt: Maximum absolute deviation of frequency demodulator implementation.
   :align: center

   Maximum absolute deviation of frequency demodulator HDL implementation.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
