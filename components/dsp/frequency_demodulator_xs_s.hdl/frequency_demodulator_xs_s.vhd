-- HDL Implementation of a frequency demodulator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.cordic_rec_to_pol;
library sdr_math;
use sdr_math.sdr_math.complex_multiplier;

architecture rtl of frequency_demodulator_xs_s_worker is

  constant input_size_c        : integer := 16;
  constant output_size_c       : integer := 16;
  constant multiply_out_size_c : integer := input_size_c*2+1;
  constant cordic_pad_c        : integer := 4;
  constant cordic_input_size_c : integer := multiply_out_size_c + cordic_pad_c;
  -- Cordic delay is output_size_c + 1
  -- Multiplier delay is 2
  constant delay_c             : integer := output_size_c + 1 + 2;

  -- Input registers
  signal i_data      : std_logic_vector(input_size_c downto 0);
  signal q_data      : std_logic_vector(input_size_c downto 0);
  signal i_data_d    : std_logic_vector(input_size_c downto 0);
  signal q_data_d    : std_logic_vector(input_size_c downto 0);
  signal q_data_d_s  : std_logic_vector(input_size_c downto 0);
  signal valid_in_d  : std_logic;
  signal valid_in_dd : std_logic;

  -- Multiplier signals
  signal multiply_out_i : std_logic_vector(multiply_out_size_c - 1 downto 0);
  signal multiply_out_q : std_logic_vector(multiply_out_size_c - 1 downto 0);

  -- ATAN2 signals
  signal cordic_x_in      : signed(cordic_input_size_c - 1 downto 0);
  signal cordic_y_in      : signed(cordic_input_size_c - 1 downto 0);
  signal phase_difference : std_logic_vector(output_size_c - 1 downto 0);
  signal atan2_out        : signed(output_size_c - 1 downto 0);
  signal cordic_pad       : signed(cordic_pad_c - 1 downto 0) := (others => '0');

  -- Interface give and take signals
  signal take         : std_logic;
  signal output_ready : std_logic;
  signal enable       : std_logic;
  signal input_hold   : std_logic;

  -- Data valid signals
  signal frequency_demodulator_valid_in : std_logic;

begin

  ------------------------------------------------------------------------------
  -- Interface signals
  ------------------------------------------------------------------------------

  -- Take input when both input and output are ready
  take           <= ctl_in.is_operating and input_in.ready and output_in.ready and not input_hold;
  input_out.take <= take;

  output_ready <= ctl_in.is_operating and output_in.ready;
  -- Only enable processing when the output is ready and input_hold is low.
  -- Input hold is high when non-stream opcodes with valid data are being
  -- output from the component, as this takes two clock cycles per input due to
  -- the difference in input and output width.
  enable       <= output_ready and not input_hold;

  frequency_demodulator_valid_in <= '1' when take = '1' and
                                    input_in.valid = '1' and
                                    input_in.opcode = complex_short_timed_sample_sample_op_e
                                    else '0';

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the cordic_rec_to_pol and multiplier modules.
  interface_delay_i : entity work.complex_short_to_short_protocol_delay
    generic map (
      DELAY_G          => delay_c,
      DATA_IN_WIDTH_G  => input_in.data'length,
      DATA_OUT_WIDTH_G => output_out.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_ready,
      take_in             => take,
      input_in            => input_in,
      processed_stream_in => phase_difference,
      output_out          => output_out,
      input_hold_out      => input_hold
      );

  ------------------------------------------------------------------------------
  -- Registers the previous I and Q input samples.
  -- Only operates when valid_in and enable are high.
  ------------------------------------------------------------------------------
  i_data <= input_in.data(input_size_c - 1) &
            input_in.data(input_size_c - 1 downto 0);
  q_data <= input_in.data((input_size_c*2) - 1) &
            input_in.data((input_size_c*2) - 1 downto input_size_c);

  input_data_register_p : process(ctl_in.clk)
  begin

    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        i_data_d    <= (others => '0');
        q_data_d    <= (others => '0');
        valid_in_d  <= '0';
        valid_in_dd <= '0';
      else
        if (enable = '1' and frequency_demodulator_valid_in = '1') then
          -- Register previous IQ symbols so that phase difference can be
          -- calculated.
          i_data_d <= i_data;
          q_data_d <= q_data;
        end if;

        if enable = '1' then
          -- Delay valid signal by 2 cycles due to complex multiplier
          valid_in_d  <= frequency_demodulator_valid_in;
          valid_in_dd <= valid_in_d;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Calculate difference vector between current and previous IQ samples.
  -- Multiplies the current sample by the complex conjugate of the
  -- previous sample.
  ------------------------------------------------------------------------------
  -- Get conjugate of i_data_d + j q_data_d
  q_data_d_s <= std_logic_vector(-signed(q_data_d));

  complex_multiplier_i : complex_multiplier
    generic map (
      input_size_g  => input_size_c + 1,
      output_size_g => multiply_out_size_c,
      bit_drop_g    => 0
      )
    port map (
      clk    => ctl_in.clk,
      rst    => ctl_in.reset,
      enable => enable,
      a_real => i_data,
      a_imag => q_data,
      b_real => i_data_d,
      b_imag => q_data_d_s,
      p_real => multiply_out_i,
      p_imag => multiply_out_q
      );

  ------------------------------------------------------------------------------
  -- Calculate ATAN2 of difference vector.
  -- ATAN2 Calculated using pipelined CORDIC method.
  ------------------------------------------------------------------------------
  -- Left shift the input to the cordic as small signals input into
  -- the cordic method have less accuracy.
  cordic_x_in <= signed(multiply_out_i) & cordic_pad;
  cordic_y_in <= signed(multiply_out_q) & cordic_pad;
  cordic_rec_to_pol_i : cordic_rec_to_pol
    generic map (
      input_size_g  => cordic_input_size_c,
      output_size_g => output_size_c
      )
    port map (
      clk           => ctl_in.clk,
      rst           => ctl_in.reset,
      enable        => enable,
      valid_in      => valid_in_dd,
      x_in          => cordic_x_in,
      y_in          => cordic_y_in,
      valid_out     => open,
      angle_out     => atan2_out,
      magnitude_out => open
      );

  -- ATAN2 of difference vector = phase difference
  phase_difference <= std_logic_vector(atan2_out);

end rtl;
