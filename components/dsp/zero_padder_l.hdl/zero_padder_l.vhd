-- zero_padder_l HDL implementation
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of zero_padder_l_worker is

  signal take           : std_logic;    -- true when taking input value
  signal output_ready   : std_logic;    -- true when output ready
  signal valid_data     : std_logic;    -- true when input is valid
  signal get_next_value : std_logic;    -- true when ready for next input value
  signal samples        : unsigned(7 downto 0);  -- amount of zero's to pad
  signal eom_d          : std_logic;    -- registered end of message signal
  signal data_eom       : std_logic;    -- true when EOM should be inserted
  signal output         : worker_output_out_t;

begin

  -- Take input when both input and output ports are ready
  output_ready   <= output_in.ready and ctl_in.is_operating;
  take           <= input_in.ready and output_ready and get_next_value;
  input_out.take <= take;

  -- Test for valid input
  valid_data <= '1' when take = '1' and input_in.valid = '1' and
                input_in.opcode = long_timed_sample_sample_op_e else '0';

  -- Sample counter
  samples_count_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        samples <= to_unsigned(0, samples'length);
        eom_d   <= '0';
      else
        if valid_data = '1' then
          samples <= props_in.samples;
          eom_d   <= input_in.eom;
        elsif output_in.ready = '1' and
          samples > to_unsigned(0, samples'length) then
          samples <= samples - 1;
        end if;
      end if;
    end if;
  end process;

  get_next_value <= '1' when samples = to_unsigned(0, samples'length) else '0';

  -- When the last input was the EOM, hold the EOM flag until the last repeat
  -- for this input sample. Or when input is EOM and we are not repeating
  -- output EOM flag straight away.
  data_eom <= '1' when (samples = 1 and eom_d = '1') or (props_in.samples = 0 and input_in.eom) else '0';

  -- Streaming interface output
  output.give        <= input_in.ready or not get_next_value;
  output.valid       <= input_in.valid       when take = '1' else '1';
  output.som         <= input_in.som         when take = '1' else '0';
  output.data        <= input_in.data        when take = '1' else (others => '0');
  output.byte_enable <= input_in.byte_enable when take = '1' else (others => '1');
  output.opcode      <= input_in.opcode      when take = '1' else long_timed_sample_sample_op_e;

  output.eom <= input_in.eom when take = '1' and valid_data = '0' else data_eom;

  -- Split stream messages over OCPI_MAX_BYTES_OUTPUT/4 words, as this is the
  -- maximum size specified by the protocol.
  limit_message_size_i : entity work.long_protocol_message_limiter
    generic map(
      DATA_OUT_WIDTH_G     => output_out.data'length,
      MAX_MESSAGE_LENGTH_G => to_integer(OCPI_MAX_BYTES_OUTPUT)/4
      )
    port map(
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      enable             => output_ready,
      output_out         => output,
      output_out_limited => output_out
      );

end rtl;
