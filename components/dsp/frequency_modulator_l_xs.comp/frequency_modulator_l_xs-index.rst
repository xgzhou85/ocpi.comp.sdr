.. frequency_modulator_l_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _frequency_modulator_l_xs:


Frequency modulator (``frequency_modulator_l_xs``)
==================================================
Frequency modulates a real input signal to generates a complex output waveform.

Design
------
Output is a complex sinusoidal waveform whose frequency is related to the amplitude of the input signal. A positive input generates a positive frequency deviation, and a negative input generates a negative frequency deviation. The absolute value of the input determines the amount of frequency deviation, with larger values generating larger frequency deviations. A full scale positive input will generate a positive frequency deviation of half the system's sample rate. A full scale negative input will generate a negative frequency deviation of half the system's sample rate. An input of zero will generate a DC signal.

The mathematical representation of the implementation is given in :eq:`frequency_modulator_l_xs-equation`.

.. math::
   :label: frequency_modulator_l_xs-equation

   y[n] = A \exp{\left(j 2 \pi \frac{\sum_{m=0}^{n-1} x[n]}{2^{32}} \right)}

In :eq:`frequency_modulator_l_xs-equation`:

 * :math:`x[n]` is the input values. :math:`x[-1]` is assumed to be zero.

 * :math:`y[n]` is the output values, which are mapped to integer values before output from the component.

 * :math:`A` is the amplitude modulated complex short output waveform, equal to 1.646760258 * ``modulator_amplitude`` property.

A block diagram representation of the implementation is given in :numref:`frequency_modulator_l_xs-diagram`.

.. _frequency_modulator_l_xs-diagram:

.. figure:: frequency_modulator_l_xs.svg
   :alt: Block diagram outlining frequency modulator implementation.
   :align: center

   Frequency modulator implementation.

The relationship between :math:`f` (the instantaneous frequency of :math:`y[n]`) and :math:`x[n]` values is given by :eq:`frequency_modulator_l_xs_freq_output-equation`.

.. math::
   :label: frequency_modulator_l_xs_freq_output-equation

   x[n] = \frac{2^{32} f}{F_s}

In :eq:`frequency_modulator_l_xs_freq_output-equation` :math:`F_s` is the sample rate of the system.

Interface
---------
.. literalinclude:: ../specs/frequency_modulator_l_xs-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The frequency modulation is only calculated for values in a sample opcode message.

All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   modulator_amplitude: Should not be set to greater than 19,872, as this has a safety margin under the maximum of the short output values.

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../frequency_modulator_l_xs.hdl ../frequency_modulator_l_xs.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`CORDIC DDS <cordic_dds-primitive>`

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``frequency_modulator_l_xs`` are:

 * Setting of ``modulator_amplitude`` above 19,872 is untested behaviour and will result in unpredictable behaviour.

Testing
-------
.. ocpi_documentation_test_result_summary::
