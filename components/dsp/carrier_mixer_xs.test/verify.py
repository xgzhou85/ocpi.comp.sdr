#!/usr/bin/env python3

# Runs checks for carrier mixer testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from carrier_mixer import CarrierMixer


carrier_amplitude_property = int(os.environ.get("OCPI_TEST_carrier_amplitude"))
instep_opcode_passthrough_property = os.environ[
    "OCPI_TEST_instep_opcode_passthrough"].upper() == "TRUE"

input_messages_file = str(sys.argv[-2])
instep_messages_file = str(sys.argv[-1])
output_messages_file = str(sys.argv[-3])

carrier_mixer_implementation = CarrierMixer(
    carrier_amplitude_property, instep_opcode_passthrough_property)

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(carrier_mixer_implementation)
verifier.set_port_types(
    ["complex_short_timed_sample", "long_timed_sample"], ["complex_short_timed_sample"], ["bounded"])

# From empirical testing a bound of 18 is found to be suitable to cover the
# variation the approximation in the HDL implementation shows.
verifier.comparison[0].BOUND = 18
if verifier.verify(test_id, [input_messages_file, instep_messages_file],
                   [output_messages_file]) is True:
    sys.exit(0)
else:
    sys.exit(1)
