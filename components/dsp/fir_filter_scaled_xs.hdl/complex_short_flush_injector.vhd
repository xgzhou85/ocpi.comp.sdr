-- Flush injector for complex short protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- On receipt of a flush opcode, applies backpressure to the input port and
-- generates an input message of `flush_length` stream samples of value zero,
-- before releasing backpressure and sending on the flush opcode. All other
-- message types passthrough undelayed.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.fir_filter_scaled_xs_worker_defs.all;
library sdr_interface;
use sdr_interface.sdr_interface.flush_inserter;

entity complex_short_flush_injector is
  generic (
    DATA_IN_WIDTH_G      : integer := 32;
    MAX_MESSAGE_LENGTH_G : integer := 4096
    );
  port (
    clk             : in  std_logic;
    reset           : in  std_logic;
    take_in         : in  std_logic;
    input_in        : in  worker_input_in_t;
    flush_length    : in  unsigned;
    input_out       : out worker_input_out_t;
    input_interface : out worker_input_in_t
    );
end complex_short_flush_injector;

architecture rtl of complex_short_flush_injector is

  constant OPCODE_WIDTH_C      : integer := 3;
  constant BYTE_ENABLE_WIDTH_C : integer := 1;

  function opcode_to_slv(inop : in complex_short_timed_sample_OpCode_t) return std_logic_vector is
  begin
    case inop is
      when complex_short_timed_sample_sample_op_e =>
        return "000";
      when complex_short_timed_sample_time_op_e =>
        return "001";
      when complex_short_timed_sample_sample_interval_op_e =>
        return "010";
      when complex_short_timed_sample_flush_op_e =>
        return "011";
      when complex_short_timed_sample_discontinuity_op_e =>
        return "100";
      when others =>
        return "101";
    end case;
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(OPCODE_WIDTH_C - 1 downto 0)) return complex_short_timed_sample_OpCode_t is
  begin
    case inslv is
      when "000" =>
        return complex_short_timed_sample_sample_op_e;
      when "001" =>
        return complex_short_timed_sample_time_op_e;
      when "010" =>
        return complex_short_timed_sample_sample_interval_op_e;
      when "011" =>
        return complex_short_timed_sample_flush_op_e;
      when "100" =>
        return complex_short_timed_sample_discontinuity_op_e;
      when others =>
        return complex_short_timed_sample_metadata_op_e;
    end case;
  end function;

  signal input_opcode           : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal input_interface_opcode : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal input_interface_data   : std_logic_vector(DATA_IN_WIDTH_G - 1 downto 0);

begin

  input_opcode <= opcode_to_slv(input_in.opcode);

  protocol_delay_i : flush_inserter
    generic map (

      DATA_WIDTH_G         => DATA_IN_WIDTH_G,
      OPCODE_WIDTH_G       => OPCODE_WIDTH_C,
      BYTE_ENABLE_WIDTH_G  => BYTE_ENABLE_WIDTH_C,
      MAX_MESSAGE_LENGTH_G => MAX_MESSAGE_LENGTH_G,
      FLUSH_OPCODE_G       => "011",
      DATA_OPCODE_G        => "000"
      )
    port map (
      clk                => clk,
      reset              => reset,
      flush_length       => flush_length,
      take_in            => take_in,
      take_out           => input_out.take,
      input_som          => input_in.som,
      input_eom          => input_in.eom,
      input_valid        => input_in.valid,
      input_byte_enable  => input_in.byte_enable,
      input_opcode       => input_opcode,
      input_data         => input_in.data,
      input_ready        => input_in.ready,
      output_som         => input_interface.som,
      output_eom         => input_interface.eom,
      output_valid       => input_interface.valid,
      output_byte_enable => input_interface.byte_enable,
      output_opcode      => input_interface_opcode,
      output_data        => input_interface_data,
      output_ready       => input_interface.ready
      );

  input_interface.data   <= input_interface_data;
  input_interface.opcode <= slv_to_opcode(input_interface_opcode);

end rtl;
