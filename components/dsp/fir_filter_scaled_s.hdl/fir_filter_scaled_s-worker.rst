.. fir_filter_scaled_s HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _fir_filter_scaled_s-HDL-worker:


``fir_filter_scaled_s`` HDL worker
==================================
HDL implementation with adjustable number of multipliers.

Detail
------
The number of multiply accumulate (MAC) units used to implement the filter is definable using an OpenCPI parameter allowing the resource usage to be customised depending on the number of DSPs available and the required data rate. The number of clock cycles required between each input is given by the below equation. This FIR filter design is most suited for when ``number_of_multipliers`` is small compared to ``number_of_taps``, however it will operate with any combination  of ``number_of_multipliers`` and ``number_of_taps``.

.. math::

   \text{clock_cycles_between_inputs} = \left \lceil \frac{\text{number_of_taps}}{\text{number_of_multipliers}} \right \rceil

Each MAC unit is used to perform part the FIR filter operation. The results of each of those MACs then need to be added together to get the final filter output. If :math:`\text{clock_cycles_between_inputs} > 1` then part of this summation operation can be time shared over multiple clock cycles. In order to take advantage of this an array of serial adders are instantiated to perform the summation. The number of serial adders required is given by the below equation.

.. math::

   \text{number_of_serial_adders} = \left \lceil \frac{\text{number_of_multipliers}}{\left \lceil \frac{\text{number_of_taps}}{\text{number_of_multipliers}} \right \rceil} \right \rceil

If :math:`\text{number_of_serial_adders} > 1` then the results of the serial adders also need to be summed to get the final filter result. This final operation is done using a pipeline adder tree. A pipelined adder tree is typically resource intensive requiring instantiation of:math:`\text{number_of_serial_adders} - 1` adders. If :math:`\text{number_of_serial_adders} > 32`, then other HDL FIR implementations such as a systolic FIR filter may be more suitable.

.. ocpi_documentation_worker::

``number_of_multipliers`` sets the number of multiplier units (DSP blocks) used in the FIR filter.

Utilisation
-----------
.. ocpi_documentation_utilisation::
