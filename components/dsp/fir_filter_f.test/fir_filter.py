#!/usr/bin/env python3

# Python implementation of FIR filter for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import numpy

import ocpi_testing


class FirFilter(ocpi_testing.Implementation):
    def __init__(self, taps, flush_length):
        taps = [numpy.float32(tap) for tap in taps]
        super().__init__(taps=taps, flush_length=flush_length)

        self._filter_buffer = [numpy.float32(0.0)] * len(self.taps)

    def reset(self):
        self._filter_buffer = [numpy.float32(0.0)] * len(self.taps)

    def sample(self, input_):
        data = self._filter(input_)

        return self.output_formatter([{"opcode": "sample", "data": data}])

    def flush(self, input_):
        if input_ is True:
            if self.flush_length > 0:
                flush_data = self._filter(
                    [numpy.float32(0.0)] * self.flush_length)

                return self.output_formatter([
                    {"opcode": "sample", "data": flush_data},
                    {"opcode": "flush", "data": None}])

            else:
                return self.output_formatter([
                    {"opcode": "flush", "data": None}])

    def _filter(self, input_):
        data = [numpy.float32(0.0)] * len(input_)

        for index, value in enumerate(input_):
            self._filter_buffer = numpy.insert(
                self._filter_buffer, 0, numpy.float32(value))
            self._filter_buffer = self._filter_buffer[0:-1]

            for tap, sample in zip(self.taps, self._filter_buffer):
                data[index] = data[index] + tap * sample

        return data
