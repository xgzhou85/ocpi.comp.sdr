.. moving_average_filter_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _moving_average_filter_b:


Moving average filter (``moving_average_filter_b``)
===================================================
Moving average for boolean values over a settable window.

Design
------
The input boolean stream is treated as a binary value, with the output set to one if there are more ones than zeros in the input window.

The mathematical representation of the implementation is given in :eq:`moving_average_filter_b-equation`.

.. math::
   :label: moving_average_filter_b-equation

   y[n] = \begin{cases}
            \hfil 1 & \hfil \displaystyle\sum_{m=0}^{M-1} x[n - M] > \frac{N}{2} \\
            \hfil 0 & \hfil \text{otherwise}
          \end{cases}

In :eq:`moving_average_filter_b-equation`:

 * :math:`x[n]` is the input values. In this expression the boolean value ``true`` is treated as :math:`1` and ``false`` treated as :math:`0`.

 * :math:`y[n]` is the output values.

 * :math:`M` is the size of the moving average window.

A block diagram representation of the implementation is given in :numref:`moving_average_filter_b-diagram`.

.. _moving_average_filter_b-diagram:

.. figure:: moving_average_filter_b.svg
   :alt: Block diagram outlining moving average filter implementation.
   :align: center

   Moving average filter implementation.

Interface
---------
.. literalinclude:: ../specs/moving_average_filter_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The moving average is only calculated on values in a sample opcode message.

When a message with a flush opcode is received the component will flush the internal buffer, which is  equivalent of inputting ``flush_length`` zeros. During this the component will apply backpressure until it has finished flushing its buffer. Afterwards the flush is completed the flush opcode message is passed to the output and backpressure removed.

All other opcode message have no effect on this component and are passed through.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   moving_average_size: Performance of the component is less ambiguous if this is set to an odd value.

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../moving_average_filter_b.hdl ../moving_average_filter_b.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Flush inserter primitive <flush_inserter-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``moving_average_filter_b`` are:

 * In the HDL implementation ``moving_average_size`` must be less than or equal to ``maximum_moving_average_size`` which is set to 63 by default.

Testing
-------
.. ocpi_documentation_test_result_summary::
