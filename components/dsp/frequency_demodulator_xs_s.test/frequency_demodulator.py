#!/usr/bin/env python3

# Python implementation of a frequency demodulator for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import math

import ocpi_testing


class FrequencyDemodulator(ocpi_testing.Implementation):
    def __init__(self):
        super().__init__()
        self._previous_sample = complex(0, 0)
        self._bit_width = 16
        self._max_value = 2**(self._bit_width - 1) - 1

    def reset(self):
        self._previous_sample = complex(0, 0)

    def sample(self, values):
        output_values = []
        for value in values:
            output_values.append(self._calculate_sample(value.real,
                                                        value.imag))
            self._previous_sample = value
        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])

    def _calculate_sample(self, value_i, value_q):
        current = complex(value_i, value_q)
        conjugate = complex(self._previous_sample.real,
                            - self._previous_sample.imag)
        diff = current * conjugate
        diff = complex(int(diff.real), int(diff.imag))

        return round(math.atan2(diff.imag, diff.real) / math.pi *
                     self._max_value)
