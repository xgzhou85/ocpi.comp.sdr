// FIR Filter Implementation
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_DSP_COMMON_FIR_FIR_CORE_HH_
#define COMPONENTS_DSP_COMMON_FIR_FIR_CORE_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstddef>

// LINT EXCEPTION: cpp_011: 3: New class definition allowed in commmon
// implementation
template <class TYPE>
class fir_core {
 public:
  fir_core(size_t length, const TYPE taps[]) { set_taps(length, taps); }

  ~fir_core() {
    if (m_history_real != NULL) delete m_history_real;
    if (m_history_imag != NULL) delete m_history_imag;
  }

  // Reset the histories
  void reset() {
    if (m_history_real != NULL) delete m_history_real;
    m_history_real = new TYPE[m_length];
    if (m_history_imag != NULL) delete m_history_imag;
    m_history_imag = new TYPE[m_length];
    for (size_t i = 0; i < m_length; i++) {
      m_history_real[i] = 0;
      m_history_imag[i] = 0;
    }
    m_history_pos = 0;
  }

  // Set the taps and reset to create correct length memory
  void set_taps(size_t length, const TYPE taps[]) {
    m_length = length;
    m_taps = taps;
    reset();
  }

  // If this is a non-complex filter, do real work
  void do_work(const TYPE input[], size_t samples, TYPE output[]) {
    // Loop over each sample
    for (size_t sample = 0; sample < samples; sample++) {
      // Assign the new sample to the history
      m_history_real[m_history_pos] = input[sample];
      // Reset the real accumulators
      output[sample] = 0;
      // Reset the tap position
      size_t tap = 0;
      // With the most recent first, go back through the history and do the
      // multiply accumulate operation
      for (int i = m_history_pos; i >= 0; i--) {
        output[sample] += m_taps[tap++] * m_history_real[i];
      }
      // Then do the same from the end back to the current history position
      for (size_t i = m_length - 1; i > m_history_pos; i--) {
        output[sample] += m_taps[tap++] * m_history_real[i];
      }
      // Increment the current position and wrap
      if (++m_history_pos >= m_length) m_history_pos = 0;
    }  // For loop over samples
  }    // do_work

  // If this is a complex filter, do complex work
  void do_work_complex(const TYPE input[], size_t samples, TYPE output[]) {
    // Loop over each sample
    for (size_t sample = 0; sample < (samples * 2); sample += 2) {
      // Assign the new sample to the history
      m_history_real[m_history_pos] = input[sample];
      m_history_imag[m_history_pos] = input[sample + 1];
      // Reset the real and imaginary accumulators
      output[sample] = 0;
      output[sample + 1] = 0;
      // Reset the tap position
      size_t tap = 0;
      // With the most recent first, go back through the history and do the
      // multiply accumulate operation
      for (int i = m_history_pos; i >= 0; i--) {
        output[sample] += m_taps[tap] * m_history_real[i];
        output[sample + 1] += m_taps[tap++] * m_history_imag[i];
      }
      // Then do the same from the end back to the current history position
      for (size_t i = m_length - 1; i > m_history_pos; i--) {
        output[sample] += m_taps[tap] * m_history_real[i];
        output[sample + 1] += m_taps[tap++] * m_history_imag[i];
      }
      // Increment the current position and wrap
      if (++m_history_pos >= m_length) m_history_pos = 0;
    }  // For loop over samples
  }    // do_work_complex

 private:
  size_t m_length;
  const TYPE *m_taps;
  TYPE *m_history_real = NULL;
  TYPE *m_history_imag = NULL;
  size_t m_history_pos;
};

#endif  // COMPONENTS_DSP_COMMON_FIR_FIR_CORE_HH_
