-- Delay module for Boolean Protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes stream data.
-- Delay_G should be set to the delay in clock cycles of the module that
-- processes stream data.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.moving_average_filter_b_worker_defs.all;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_delay;

entity boolean_protocol_delay is
  generic (
    DELAY_G              : integer   := 1;
    DATA_IN_WIDTH_G      : integer   := 8;
    PROCESSED_DATA_MUX_G : std_logic := '1'
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;  -- High when output is ready
    take_in             : in  std_logic;  -- High when data is taken from input
    input_in            : in  worker_input_in_t;   -- Input streaming interface
    -- Connect output from data processing module.
    processed_stream_in : in  std_logic_vector(DATA_IN_WIDTH_G - 1 downto 0);
    output_out          : out worker_output_out_t  -- Output streaming interface
    );
end boolean_protocol_delay;

architecture rtl of boolean_protocol_delay is

  constant OPCODE_WIDTH_C      : integer := 3;
  constant BYTE_ENABLE_WIDTH_C : integer := 1;

  function opcode_to_slv(inop : in bool_timed_sample_OpCode_t) return std_logic_vector is
  begin
    case inop is
      when bool_timed_sample_sample_op_e =>
        return "000";
      when bool_timed_sample_time_op_e =>
        return "001";
      when bool_timed_sample_sample_interval_op_e =>
        return "010";
      when bool_timed_sample_flush_op_e =>
        return "011";
      when bool_timed_sample_discontinuity_op_e =>
        return "100";
      when others =>
        return "101";
    end case;
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(OPCODE_WIDTH_C - 1 downto 0)) return bool_timed_sample_OpCode_t is
  begin
    case inslv is
      when "000" =>
        return bool_timed_sample_sample_op_e;
      when "001" =>
        return bool_timed_sample_time_op_e;
      when "010" =>
        return bool_timed_sample_sample_interval_op_e;
      when "011" =>
        return bool_timed_sample_flush_op_e;
      when "100" =>
        return bool_timed_sample_discontinuity_op_e;
      when others =>
        return bool_timed_sample_metadata_op_e;
    end case;
  end function;

  signal input_opcode  : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal output_opcode : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal output_data   : std_logic_vector(DATA_IN_WIDTH_G - 1 downto 0);

begin

  input_opcode <= opcode_to_slv(input_in.opcode);

  protocol_delay_i : protocol_interface_delay
    generic map (
      DELAY_G                 => DELAY_G,
      DATA_WIDTH_G            => DATA_IN_WIDTH_G,
      OPCODE_WIDTH_G          => OPCODE_WIDTH_C,
      BYTE_ENABLE_WIDTH_G     => BYTE_ENABLE_WIDTH_C,
      PROCESSED_DATA_MUX_G    => PROCESSED_DATA_MUX_G,
      PROCESSED_DATA_OPCODE_G => "000"
      )
    port map (
      clk                 => clk,
      reset               => reset,
      enable              => enable,
      take_in             => take_in,
      processed_stream_in => processed_stream_in,
      input_som           => input_in.som,
      input_eom           => input_in.eom,
      input_valid         => input_in.valid,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_opcode,
      input_data          => input_in.data,
      output_som          => output_out.som,
      output_eom          => output_out.eom,
      output_valid        => output_out.valid,
      output_give         => output_out.give,
      output_byte_enable  => output_out.byte_enable,
      output_opcode       => output_opcode,
      output_data         => output_data
      );

  output_out.data   <= output_data;
  output_out.opcode <= slv_to_opcode(output_opcode);

end rtl;
