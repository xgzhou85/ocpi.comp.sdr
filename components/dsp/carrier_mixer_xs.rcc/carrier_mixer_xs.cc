// RCC implementation of carrier_mixer_xs worker
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include "carrier_mixer_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Carrier_mixer_xsWorkerTypes;

class Carrier_mixer_xsWorker : public Carrier_mixer_xsWorkerBase {
  RCCBoolean instep_opcode_passthrough =
      CARRIER_MIXER_XS_INSTEP_OPCODE_PASSTHROUGH;

  // This component matches the behavior of another worker which uses the CORDIC
  // algorithm to calculate sine and cosine. The CORDIC algorithm scales the
  // output by a fixed constant. As such, we scale the amplitude of sine and
  // cosine to match that of the output from the worker using the CORDIC
  // algorithm.
  const double_t cordic_gain = 1.646760258;

  const uint8_t output_scaling = 15;

  double_t carrier_amplitude = 0.0;
  int32_t instep_phase = 0;

  size_t input_processed_samples = 0;
  size_t instep_processed_samples = 0;
  size_t output_processed_samples = 0;

  RCCResult start() {
    input_processed_samples = 0;
    instep_processed_samples = 0;
    output_processed_samples = 0;
    return RCC_OK;
  }

  // Notification that carrier_amplitude property has been written
  RCCResult carrier_amplitude_written() {
    this->carrier_amplitude = properties().carrier_amplitude;

    if (this->carrier_amplitude > 14060) {
      setError("carrier amplitude must be less than 14060");
      return RCC_FATAL;
    }

    this->carrier_amplitude = cordic_gain * this->carrier_amplitude;
    return RCC_OK;
  }

  Complex_short_timed_sampleSampleData carrier_mixer(
      const Complex_short_timed_sampleSampleData sample,
      const int32_t instep_sample) {
    RCCDouble carrier_wave_real =
        cos((2 * M_PI * this->instep_phase) / ((uint64_t)1 << 32));
    RCCDouble carrier_wave_imaginary =
        sin((2 * M_PI * this->instep_phase) / ((uint64_t)1 << 32));

    // Mix input with carrier wave
    int64_t mixed_output_real =
        static_cast<int64_t>(this->carrier_amplitude *
                             ((sample.real * carrier_wave_real) -
                              (sample.imaginary * carrier_wave_imaginary)));
    int64_t mixed_output_imaginary = static_cast<int64_t>(
        this->carrier_amplitude * ((sample.imaginary * carrier_wave_real) +
                                   (sample.real * carrier_wave_imaginary)));

    // Update phase for next loop
    this->instep_phase = this->instep_phase + instep_sample;

    // Scale output to short type range
    Complex_short_timed_sampleSampleData output_sample;
    output_sample.real =
        static_cast<int16_t>(mixed_output_real >> output_scaling);
    output_sample.imaginary =
        static_cast<int16_t>(mixed_output_imaginary >> output_scaling);
    return output_sample;
  }

  RCCResult run(bool) {
    // Discard non sample opcode on non-forwarding inputs
    if (input.opCode() != Complex_short_timed_sampleSample_OPERATION &&
        instep_opcode_passthrough) {
      input.advance();
      return RCC_OK;
    } else if (instep.opCode() != Long_timed_sampleSample_OPERATION &&
               !instep_opcode_passthrough) {
      instep.advance();
      return RCC_OK;
    }

    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION &&
        instep.opCode() == Long_timed_sampleSample_OPERATION) {
      const Complex_short_timed_sampleSampleData *input_data =
          input.sample().data().data();
      const int32_t *instep_data = instep.sample().data().data();
      Complex_short_timed_sampleSampleData *output_data =
          output.sample().data().data();
      // Advance the inputs to the next sample point to be handled
      input_data = &input_data[input_processed_samples];
      instep_data = &instep_data[instep_processed_samples];

      size_t input_length = input.sample().data().size();
      size_t instep_length = instep.sample().data().size();
      size_t input_unprocessed_samples = input_length - input_processed_samples;
      size_t instep_unprocessed_samples =
          instep_length - instep_processed_samples;
      size_t max_output_length =
          CARRIER_MIXER_XS_OCPI_MAX_BYTES_OUTPUT / sizeof(*output_data);

      // New output buffer is being used
      if (output_processed_samples == 0) {
        output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
        if (instep_opcode_passthrough) {
          output.sample().data().resize(
              std::min(instep_unprocessed_samples, max_output_length));
        } else {
          output.sample().data().resize(
              std::min(input_unprocessed_samples, max_output_length));
        }
      }

      size_t output_unprocessed_samples =
          output.sample().data().size() - output_processed_samples;
      output_data = &output_data[output_processed_samples];

      // Output buffer space is restricting total samples that can be processed
      if (output_unprocessed_samples <= input_unprocessed_samples &&
          output_unprocessed_samples <= instep_unprocessed_samples) {
        for (size_t i = 0; i < output_unprocessed_samples; i++) {
          *output_data = carrier_mixer(*input_data, *instep_data);
          output_data++;
          input_data++;
          instep_data++;
        }
        input_processed_samples += output_unprocessed_samples;
        instep_processed_samples += output_unprocessed_samples;
        output.advance();
        output_processed_samples = 0;
      } else {
        size_t avaliable_input_samples =
            std::min(input_unprocessed_samples, instep_unprocessed_samples);
        for (size_t i = 0; i < avaliable_input_samples; i++) {
          *output_data = carrier_mixer(*input_data, *instep_data);
          output_data++;
          input_data++;
          instep_data++;
        }
        input_processed_samples += avaliable_input_samples;
        instep_processed_samples += avaliable_input_samples;
        // Do not advance output as input data limited
        output_processed_samples += avaliable_input_samples;
      }
      if (input_processed_samples == input_length) {
        input.advance();
        input_processed_samples = 0;
      }
      if (instep_processed_samples == instep_length) {
        instep.advance();
        instep_processed_samples = 0;
      }
      return RCC_OK;
    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION ||
               instep.opCode() == Long_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
        output.time().fraction() = input.time().fraction();
        output.time().seconds() = input.time().seconds();
        input.advance();
      } else {
        output.time().fraction() = instep.time().fraction();
        output.time().seconds() = instep.time().seconds();
        instep.advance();
      }
      output.advance();
      return RCC_OK;
    } else if (input.opCode() ==
                   Complex_short_timed_sampleSample_interval_OPERATION ||
               instep.opCode() == Long_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      if (input.opCode() ==
          Complex_short_timed_sampleSample_interval_OPERATION) {
        output.sample_interval().fraction() =
            input.sample_interval().fraction();
        output.sample_interval().seconds() = input.sample_interval().seconds();
        input.advance();
      } else {
        output.sample_interval().fraction() =
            instep.sample_interval().fraction();
        output.sample_interval().seconds() = instep.sample_interval().seconds();
        instep.advance();
      }
      output.advance();
      return RCC_OK;
    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION ||
               instep.opCode() == Long_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
        input.advance();
      } else {
        instep.advance();
      }
      output.advance();
      return RCC_OK;
    } else if (input.opCode() ==
                   Complex_short_timed_sampleDiscontinuity_OPERATION ||
               instep.opCode() == Long_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      if (input.opCode() == Complex_short_timed_sampleDiscontinuity_OPERATION) {
        input.advance();
      } else {
        instep.advance();
      }
      output.advance();
      return RCC_OK;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION ||
               instep.opCode() == Long_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
        output.metadata().id() = input.metadata().id();
        output.metadata().value() = input.metadata().value();
        input.advance();
      } else {
        output.metadata().id() = instep.metadata().id();
        output.metadata().value() = instep.metadata().value();
        instep.advance();
      }
      output.advance();
      return RCC_OK;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

CARRIER_MIXER_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
CARRIER_MIXER_XS_END_INFO
