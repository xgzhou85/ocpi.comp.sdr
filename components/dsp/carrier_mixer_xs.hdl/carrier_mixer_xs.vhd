-- carrier_mixer_xs HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.cordic_dds;
library sdr_math;
use sdr_math.sdr_math.complex_multiplier;

architecture rtl of carrier_mixer_xs_worker is

  constant delay_dds_c        : integer := 15;  -- delay of cordic dds
  constant delay_multiplier_c : integer := 3;   -- delay complex multiplier
  -- Delay to align data
  constant delay_c            : integer := delay_dds_c + delay_multiplier_c;
  constant input_size_c       : integer := 16;  -- i & q data input widths

  signal input_take  : std_logic;       -- true when taking input data
  signal instep_take : std_logic;       -- true when taking instep data
  signal enable      : std_logic;       -- true when output can take data
  signal valid_data  : std_logic;       -- true when input data is valid

  signal dds_step    : std_logic_vector(31 downto 0);
  signal dds_sin_out : std_logic_vector(input_size_c - 1 downto 0);
  signal dds_cos_out : std_logic_vector(input_size_c - 1 downto 0);

  type data_in_t is array (delay_dds_c downto 0) of std_logic_vector(input_size_c - 1 downto 0);
  signal data_in_i_store   : data_in_t;
  signal data_in_q_store   : data_in_t;
  signal data_out_i        : std_logic_vector(input_size_c * 2 downto 0);
  signal data_out_q        : std_logic_vector(input_size_c * 2 downto 0);
  signal scaled_data_out_i : signed(15 downto 0);
  signal scaled_data_out_q : signed(15 downto 0);
  signal data_out          : std_logic_vector(input_size_c * 2 - 1 downto 0);
begin

  enable <= output_in.ready and ctl_in.is_operating;

  input_out.take  <= input_take;
  instep_out.take <= instep_take;

  -- Always take non stream opcodes from both inputs.
  -- Only take stream opcodes if both ports have a stream opcode.
  input_take_logic_p : process (enable, input_in, instep_in)
  begin
    -- INPUT 1
    -- If input and output are ready
    if (enable = '1' and input_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid
      if input_in.opcode /= complex_short_timed_sample_sample_op_e or input_in.valid = '0' then
        input_take <= '1';
      -- Only take a stream opcode if input 2 is ready, valid,
      -- and also a stream opcode
      elsif instep_in.ready = '1' and instep_in.valid = '1' and
        instep_in.opcode = long_timed_sample_sample_op_e then
        input_take <= '1';
      else
        input_take <= '0';
      end if;
    else
      input_take <= '0';
    end if;
    -- INPUT 2
    -- If input and output are ready
    if (enable = '1' and instep_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid
      if instep_in.opcode /= long_timed_sample_sample_op_e or instep_in.valid = '0' then
        instep_take <= '1';
      -- Only take a stream opcode if input 1 is ready, valid, and also a
      -- stream opcode.
      elsif input_in.ready = '1' and input_in.valid = '1' and
        input_in.opcode = complex_short_timed_sample_sample_op_e then
        instep_take <= '1';
      else
        instep_take <= '0';
      end if;
    else
      instep_take <= '0';
    end if;
  end process;

  valid_data <= '1' when input_take = '1' and instep_take = '1' and
                input_in.valid = '1' and instep_in.valid = '1' and
                input_in.opcode = complex_short_timed_sample_sample_op_e and
                instep_in.opcode = long_timed_sample_sample_op_e else '0';

  dds_step <= instep_in.data when valid_data = '1' else (others => '0');

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the cordic_dds module.
  input_opcode_passthrough_gen : if instep_opcode_passthrough = '0' generate
    -- Passthrough all non stream opcodes signals from the input port
    -- Discard all non-stream opcodes from the instep port
    interface_delay_i : entity work.complex_short_protocol_delay
      generic map (
        DELAY_G              => delay_c,
        DATA_IN_WIDTH_G      => input_in.data'length,
        PROCESSED_DATA_MUX_G => '1'
        )
      port map (
        clk                 => ctl_in.clk,
        reset               => ctl_in.reset,
        enable              => enable,
        take_in             => input_take,
        input_in            => input_in,
        processed_stream_in => data_out,
        output_out          => output_out
        );
  end generate;

  instep_opcode_passthrough_gen : if instep_opcode_passthrough = '1' generate
    -- Passthrough all non stream opcodes signals from the instep port
    -- Discard all non-stream opcodes from the input port
    interface_delay_i : entity work.long_to_complex_short_protocol_delay
      generic map (
        DELAY_G              => delay_c,
        DATA_IN_WIDTH_G      => instep_in.data'length,
        PROCESSED_DATA_MUX_G => '1'
        )
      port map (
        clk                 => ctl_in.clk,
        reset               => ctl_in.reset,
        enable              => enable,
        take_in             => instep_take,
        input_in            => instep_in,
        processed_stream_in => data_out,
        output_out          => output_out
        );
  end generate;

  -- DDS module
  dds_inst : cordic_dds
    generic map (
      output_size_g      => input_size_c,
      phase_accum_size_g => 32
      )
    port map (
      clk        => ctl_in.clk,
      rst        => ctl_in.reset,
      clk_en     => enable,
      step_size  => dds_step,
      gain_in    => std_logic_vector(props_in.carrier_amplitude),
      sine_out   => dds_sin_out,
      cosine_out => dds_cos_out
      );

  -- Breakout complex mixer input data into real and imag
  data_in_i_store <= data_in_i_store(data_in_i_store'high - 1 downto 0) &
                     input_in.data(15 downto 16 - input_size_c) when rising_edge(ctl_in.clk) and output_in.ready = '1';
  data_in_q_store <= data_in_q_store(data_in_q_store'high - 1 downto 0) &
                     input_in.data(31 downto 32 - input_size_c) when rising_edge(ctl_in.clk) and output_in.ready = '1';

  -- Complex mixer
  complex_multiplier_inst : complex_multiplier
    generic map (
      input_size_g  => input_size_c,
      output_size_g => (input_size_c * 2) + 1,
      bit_drop_g    => 0
      )
    port map (
      clk    => ctl_in.clk,
      rst    => ctl_in.reset,
      enable => enable,
      a_real => data_in_i_store(data_in_i_store'high),
      a_imag => data_in_q_store(data_in_q_store'high),
      b_real => dds_cos_out,
      b_imag => dds_sin_out,
      p_real => data_out_i,
      p_imag => data_out_q
      );

  -- Scale data by dividing by 2^15
  scaled_data_out_i <= signed(data_out_i(30 downto 15));
  scaled_data_out_q <= signed(data_out_q(30 downto 15));

  data_out <= (std_logic_vector(resize(scaled_data_out_q, output_out.data'length/2)) &
               std_logic_vector(resize(scaled_data_out_i, output_out.data'length/2)));

end rtl;
