// RCC implementation of moving_average_filter_b worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "moving_average_filter_b-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Moving_average_filter_bWorkerTypes;

class Moving_average_filter_bWorker : public Moving_average_filter_bWorkerBase {
  uint16_t moving_average_size = 0;
  uint8_t flush_length = 0;
  uint16_t threshold = 0;
  bool *data_buffer = NULL;
  uint16_t buffer_sum = 0;
  uint16_t buffer_index = 0;
  uint8_t data_flushed_flag = 0;  // Flags when flush data has been handled, and
                                  // flush opcode should be passed

  RCCResult start() {
    // Initialise a minimum sized buffer to be over-written by the first call of
    // moving_average_size_written().
    data_buffer = new bool[1];
    data_buffer[0] = false;
    moving_average_size = 1;
    moving_average_size_written();
    return RCC_OK;
  }

  RCCResult stop() {
    if (data_buffer != NULL) {
      delete[] data_buffer;
    }
    data_buffer = NULL;

    return RCC_OK;
  }

  RCCResult moving_average_size_written() {
    // Allocate a new buffer of the new moving average size in memory
    bool *new_buffer = new bool[properties().moving_average_size];

    buffer_sum = 0;

    // If the new moving average size is smaller then keep the 'newest' values
    if (moving_average_size > properties().moving_average_size) {
      for (uint16_t i = 0; i < properties().moving_average_size; i++) {
        new_buffer[i] = data_buffer[(buffer_index + moving_average_size -
                                     properties().moving_average_size + i) %
                                    moving_average_size];
        if (new_buffer[i]) {
          buffer_sum++;
        }
      }
    } else {
      for (uint16_t i = 0;
           i < (properties().moving_average_size - moving_average_size); i++) {
        new_buffer[i] = false;
      }

      for (uint16_t i =
               (properties().moving_average_size - moving_average_size);
           i < properties().moving_average_size; i++) {
        new_buffer[i] =
            data_buffer[(buffer_index + i - properties().moving_average_size +
                         moving_average_size) %
                        moving_average_size];
        if (new_buffer[i]) {
          buffer_sum++;
        }
      }
    }

    moving_average_size = properties().moving_average_size;
    threshold = uint16_t(moving_average_size / 2);
    buffer_index = 0;

    // Discard the old data buffer and assign the pointer location to the
    // data_buffer variable.
    if (data_buffer != NULL) {
      delete[] data_buffer;
    }
    data_buffer = new_buffer;
    new_buffer = NULL;

    return RCC_OK;
  }

  RCCResult flush_length_written() {
    flush_length = properties().flush_length;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const bool *inData =
          reinterpret_cast<const bool *>(input.sample().data().data());
      bool *outData = reinterpret_cast<bool *>(output.sample().data().data());

      output.setOpCode(Bool_timed_sampleSample_OPERATION);
      output.sample().data().resize(length);

      for (size_t i = 0; i < length; i++) {
        if (data_buffer[buffer_index]) {
          buffer_sum--;
        }
        data_buffer[buffer_index] = *inData;
        if (data_buffer[buffer_index]) {
          buffer_sum++;
        }
        buffer_index = (buffer_index + 1) % moving_average_size;

        *outData = (buffer_sum > threshold);

        inData++;
        outData++;
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Bool_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      if (data_flushed_flag == 0 && flush_length > 0) {
        // Pass through any data remaining in buffer
        output.setOpCode(Bool_timed_sampleSample_OPERATION);
        bool *outData = reinterpret_cast<bool *>(output.sample().data().data());
        output.setLength(flush_length * sizeof(bool));

        for (size_t i = 0; i < flush_length; i++) {
          if (data_buffer[buffer_index]) {
            buffer_sum--;
          }
          // Flush buffer with zeros
          data_buffer[buffer_index] = 0;
          buffer_index = (buffer_index + 1) % moving_average_size;
          *outData = (buffer_sum > threshold);
          outData++;
        }

        data_flushed_flag = 1;
        output.advance();  // Advance output only as no more input data is
                           // needed until after the flush opcode is sent.
        return RCC_OK;
      } else {
        data_flushed_flag = 0;
        // Pass through flush opcode
        output.setOpCode(Bool_timed_sampleFlush_OPERATION);
        return RCC_ADVANCE;
      }
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

MOVING_AVERAGE_FILTER_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
MOVING_AVERAGE_FILTER_B_END_INFO
