.. threshold_s_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: compare comparison


.. _threshold_s_b:


Threshold (``threshold_s_b``)
=============================
Output a zero or one depending on if the signal is greater or lower than the set threshold.

Design
------
Determine if an input value is over a set threshold, returning a zero or one value as determined by ``invert_output``.

The mathematical representation of the implementation is given in :eq:`threshold_s_b-equation`.

.. math::
   :label: threshold_s_b-equation

   y[n] = \begin{cases}
            1-i & x[n] \ge T \\
            i   & \text{otherwise}
          \end{cases}

In :eq:`threshold_s_b-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`i` is the ``invert_output`` property, considered as :math:`1` for ``True`` and :math:`0` for ``False``.

 * :math:`T` is the threshold value.

Interface
---------
.. literalinclude:: ../specs/threshold_s_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The threshold function is only applied to values in a sample opcode message.

All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../threshold_s_b.hdl ../threshold_s_b.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface delay (wide to narrow) primitive <wide_to_narrow_protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``threshold_s_b`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
