#!/usr/bin/env python3

# Runs on correct output for frequency_modulator_l_xs implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from frequency_modulator import FrequencyModulator


input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

# Initiate frequency modulator object
modulator_amplitude = int(os.environ.get("OCPI_TEST_modulator_amplitude"))
frequency_modulator_implementation = FrequencyModulator(modulator_amplitude)

test_id = ocpi_testing.get_test_case()

# Check the result
verifier = ocpi_testing.Verifier(frequency_modulator_implementation)
# Use a bounded comparison as modulator uses CORDIC algorithm to
# approximate sine and cosine.
verifier.set_port_types(
    ["long_timed_sample"], ["complex_short_timed_sample"], ["bounded"])
# Bounds empirically set to 15 to allow for CORDIC inaccuracies.
verifier.comparison[0].BOUND = 15
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
