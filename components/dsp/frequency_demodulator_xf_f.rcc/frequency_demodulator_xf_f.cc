// RCC worker implementation of frequency demodulation using floating point.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include "frequency_demodulator_xf_f-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Frequency_demodulator_xf_fWorkerTypes;

class Frequency_demodulator_xf_fWorker
    : public Frequency_demodulator_xf_fWorkerBase {
  Complex_float_timed_sampleSampleData lastSample;  // Store last sample

  RCCResult start() {
    this->lastSample.real = 0.0;
    this->lastSample.imaginary = 0.0;

    return RCC_OK;
  }

  RCCResult run(bool) {
    // Opcode IQ is data input from streaming interface stored in the inData
    // variable. Zero length messages are implicitly forwarded by resizing the
    // output to length and advancing the port.
    if (input.opCode() == Complex_float_timed_sampleSample_OPERATION) {
      size_t length =
          input.sample().data().size();  // Number of IQ structs in buffer
      const Complex_float_timed_sampleSampleData* inData =
          input.sample().data().data();  // Pointer to first input struct
      RCCFloat* outData =
          output.sample().data().data();  // Pointer to first output struct

      output.setOpCode(Float_timed_sampleSample_OPERATION);  // Set the opcode
      output.sample().data().resize(
          length);  // Set the output to the same number of structs as input

      // Loop over all structs in input buffer
      for (size_t i = 0; i < length; i++) {
        // conj(lastSample) x *inData = (ac - -bd) + i(ad + -bc)
        // a = real(lastSample)
        // b = imag(lastSample)
        // c = real(*inData)
        // d = imag(*inData)

        *outData = atan2(this->lastSample.real * (inData->imaginary) -
                             this->lastSample.imaginary * (inData->real),
                         this->lastSample.real * (inData->real) +
                             this->lastSample.imaginary * (inData->imaginary));

        // Store current sample in last sample buffer and increment input
        // pointer
        this->lastSample = *inData++;

        outData++;
      }

      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_float_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Float_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_float_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Float_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_float_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Float_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_float_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Float_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_float_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Float_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

FREQUENCY_DEMODULATOR_XF_F_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
FREQUENCY_DEMODULATOR_XF_F_END_INFO
