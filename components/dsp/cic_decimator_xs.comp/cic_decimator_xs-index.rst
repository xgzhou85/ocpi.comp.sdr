.. cic_decimator_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. meta::
   :keywords: filter down sample


.. _cic_decimator_xs:


CIC decimator (``cic_decimator_xs``)
====================================
CIC (cascading integrating comb) filter combined with decimator.

Design
------
CIC decimators are an efficient method of reducing the sample rate of an input signal by an integer factor. CIC decimators do not require any multiplication operations making them particularly suitable for compact FPGA designs.

In a CIC decimator the integrator stage is applied first, then the down sampling is applied before the comb stage is applied last. CIC filters have an inherent gain that typically needs to be removed; in this component this is done via a right shift operation allowing for power-of-2 gain compensation. A half-up rounding unit is used to minimise the amount of DC bias caused by the right shift operation..

A block diagram representation of the implementation is given in :numref:`cic_decimator_xs-diagram`.

.. _cic_decimator_xs-diagram:

.. figure:: cic_decimator_xs.svg
   :alt: Block diagram of CIC decimator implementation.
   :align: center

   Block diagram of CIC decimator implementation.


Frequency response
~~~~~~~~~~~~~~~~~~
The frequency response of a CIC decimator is given by in :eq:`cic_dec_frequency_response-equation`.

.. math::
   :label: cic_dec_frequency_response-equation

   H\left (\omega  \right ) = \left | \frac{sin\left ( \frac{\omega M}{2} \right )}{sin\left ( \frac{\omega }{2R} \right )} \right | ^{N}

In :eq:`cic_dec_frequency_response-equation`:

 * :math:`H[\omega]` is the narrowband frequency response.

 * :math:`N` is the order of the CIC filter (``cic_order``)

 * :math:`R` is the down sampling factor (``down_sample_factor``)

 * :math:`M` is the delay factor (``cic_delay``, this is typically only ever set to 1 or 2).

When down sampling a signal it is typically desirable to minimise the amplitude of any signals that will be above the Nyquist frequency of the lower sample rate as these signals will alias into the passband. The order and delay factor of the CIC sets the wideband frequency response of a CIC filter, with higher values giving more suppression of signals that will alias after the down sample operation.

The wideband frequency response (i.e. the response before the down sampling operation is performed) is shown in :numref:`cic_decimator_wide_frequency_response_m1-diagram` and :numref:`cic_decimator_wide_frequency_response_m2-diagram`.

.. _cic_decimator_wide_frequency_response_m1-diagram:

.. figure:: cic_decimator_wide_frequency_response_m1.svg
   :alt: Graph of wideband CIC frequency response
   :align: center

   Wideband CIC frequency response. :math:`M = 1`. :math:`R = 8`.

.. _cic_decimator_wide_frequency_response_m2-diagram:

.. figure:: cic_decimator_wide_frequency_response_m2.svg
   :alt: Graph of wideband CIC frequency response
   :align: center

   Wideband CIC frequency response. :math:`M = 2`. :math:`R = 8`.

Gain
~~~~
Increasing the order and differential delay does not come for free; the larger they are the greater the gain of the CIC. A larger gain means the width of internal registers used by the CIC decimator (set by the parameter ``cic_reg_size``) must be larger in order to store the calculated values. This in turn means the filter takes up more space on an FPGA.

The gain of a CIC decimator is given by :math:`G` in :eq:`cic_dec_gain-equation`

.. math::
   :label: cic_dec_gain-equation

   G = \left ( RM \right )^{N}

The minimum ``cic_reg_size`` in bits needed to correctly perform the CIC decimation is given in :eq:`cic_dec_gain_width-equation`. In this component ``inWidth`` is 16 bits.

.. math::
   :label: cic_dec_gain_width-equation

   \text{cic_reg_size} = \text{inWidth} + \left \lceil Nlog_{2}\left ( RM \right ) \right \rceil

Passband attenuation
~~~~~~~~~~~~~~~~~~~~
CIC decimators have attenuation in the passband. The larger the CIC order and delay factor the more attenuation in the passband. The narrowband frequency response of a CIC decimator is shown in :numref:`cic_decimator_narrow_frequency_response-diagram`.

.. _cic_decimator_narrow_frequency_response-diagram:

.. figure:: cic_decimator_narrow_frequency_response.svg
   :alt: Graph of narrowband CIC frequency response
   :align: center

   Narrowband (after decimation) CIC frequency response. :math:`R = 8`.

The narrowband frequency response of a CIC filter is same regardless of the ``down_sample_factor`` (:math:`R`). This is shown in :numref:`cic_decimator_narrow_freq_r-diagram`.

.. _cic_decimator_narrow_freq_r-diagram:

.. figure:: cic_decimator_narrow_frequency_response_r.svg
   :alt: Graph comparing narrowband CIC frequency response for different values of R
   :align: center

   Narrowband (after decimation) CIC frequency response. :math:`N = 3`. :math:`M = 2`.

Mathematical Representation
~~~~~~~~~~~~~~~~~~~~~~~~~~~

:eq:`cic_dec_integrate-equation` and :eq:`cic_dec_down_comb-equation` give a mathematical representation of the completed CIC filter implementation.

.. math::
   g[n] = \sum_{m=0}^{n}\left( \frac{\prod_{l=1}^{N-1} \left( n+N-m-l \right) }{(N-1)!} x[m] \right)
   :label: cic_dec_integrate-equation

.. math::
   h[n] = \sum_{i=0}^{N} (-1)^{i} \binom{N}{i} g[(n-iM)R]
   :label: cic_dec_down_comb-equation

In :eq:`cic_dec_integrate-equation` and :eq:`cic_dec_down_comb-equation`, :math:`h[n]` is the output values and :math:`x[n]` is the input data stream.

In :eq:`cic_dec_down_comb-equation` the notation :math:`\binom{n}{x}` is the binomial expansion, or more informally described as ":math:`n` choose :math:`x`".

After the CIC filter implementation using the above, an attenuation stage is applied to allow the option of cancelling out the inherent CIC filter gain, this attenuation is implemented as a right shift, which is equivalent to the expression in :eq:`cic_dec_atten-equation`.

.. math::
   y[n] = \frac{h[n]}{2^S}
   :label: cic_dec_atten-equation

In :eq:`cic_dec_atten-equation`, :math:`y[n]` is the final output of the component and :math:`S` is the scale output property.

Interface
---------
.. literalinclude:: ../specs/cic_decimator_xs-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Sample opcode messages are down sampled by the CIC decimator. The message boundaries of the input sample opcode message are preserved on the output sample opcode message. This means that output sample messages are smaller than the input sample messages by a factor of ``down_sample_factor``. Input sample opcode messages where all the samples are discarded due to the CIC decimator are output as zero length messages.

As this component is designed to down sample the input data stream, when a timestamp opcode message is received there is a change the sample that the timestamp relates to may not be output from the component. For this reason the timestamp opcode messages are not directly passed through, but instead stored and incremented (by the sample interval) for each valid sample opcode message that is input. The corrected timestamp is then inserted into the output stream directly before the next valid output message with the sample opcode. The result of this is that timestamp opcode messages are passed through the component as close as possible to in position to the relative input stream position, and the timestamp is corrected so that it always contains the time of the next valid message with a sample opcode.

Sample interval opcode message are passed through the component from input to output. The sample interval is stored in the component so that it can be used to increment the timestamp.

Flush opcode messages trigger the component to insert ``flush_length`` zero samples into the input of the CIC in order to flush out any historical data. On receipt of a flush opcode message, backpressure is applied to the input port while the samples are flushed. This will typically result in the output of a sample opcode message that is roughly :math:`\frac{\texttt{flush_length}}{\texttt{down_sample_factor}}` samples long. The flush opcode message is then passed from the input to the output.

All other opcodes are passed through the component unmodified and in the same position in the output stream as in the input stream.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   down_sample_factor: Must not be set to zero.
   scale_output: Must not be set greater than :math:`(\texttt{cic_reg_size} - 16`.

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

   cic_diff_delay: Must not be set to zero.

Implementations
---------------
.. ocpi_documentation_implementations:: ../cic_decimator_xs.hdl

CIC compensation filters
------------------------
Passband attenuation is typically unwanted and can be compensated for by following the CIC decimator with an FIR filter. The frequency response of an idea compensation filter is shown in :eq:`cic_dec_comp_freq_resp-equation`.

.. math::
   :label: cic_dec_comp_freq_resp-equation

   H( \omega ) = \begin{cases}
   \left ( RM \right )^{N}\left | \frac{sin\left ( \frac{\omega }{2R} \right )}{sin\left ( \frac{\omega M}{2} \right )} \right | ^{N} & \omega \leq \omega_{cutoff} \\
   0 & \omega >  \omega_{cutoff}
   \end{cases}

Wideband compensation
~~~~~~~~~~~~~~~~~~~~~
The passband frequency response of CIC filter with a compensation filter across the entire passband is shown in :numref:`cic_decimator_comp_freq_narrow-diagram`.

.. _cic_decimator_comp_freq_narrow-diagram:

.. figure:: cic_decimator_comp_freq_narrow.svg
   :alt: Narrowband frequency response of CIC with wideband compensation filter
   :align: center

   Narrowband frequency response of CIC with wideband compensation filter

Compensation across the entire passband is typically not recommended as it results in degraded rejection of aliasing signals compared to just a CIC decimator. This is shown in :numref:`cic_decimator_comp_freq_wide-diagram`.

.. _cic_decimator_comp_freq_wide-diagram:

.. figure:: cic_decimator_comp_freq_wide.svg
   :alt: Wideband frequency response of CIC with wideband compensation filter
   :align: center

   Wideband frequency response of CIC with wideband compensation filter

Narrowband Compensation
~~~~~~~~~~~~~~~~~~~~~~~
Narrowband compensation is when the CIC frequency response is only compensated for during part of the Nyquist bandwidth of the lower sample rate. Most systems use a narrowband compensation filter.

As a rule of thumb the cut-off frequency of the narrowband compensation filter should be :math:`F_{cutoff} \leq \frac{f_{s[low]}}{4M}`. If this condition is not met the performance of the CIC decimator, at some frequencies, will be negatively impacted by the compensation filter. This is shown in :numref:`cic_decimator_narrow_comp_narrow-diagram` and :numref:`cic_decimator_narrow_comp_wide-diagram`.

.. _cic_decimator_narrow_comp_narrow-diagram:

.. figure:: cic_decimator_narrow_comp_narrow.svg
   :alt: Narrowband Frequency Response of CIC with narrowband compensation filter
   :align: center

   Narrowband Frequency Response of CIC with narrowband compensation filter

.. _cic_decimator_narrow_comp_wide-diagram:

.. figure:: cic_decimator_narrow_comp_wide.svg
   :alt: Wideband Frequency Response of CIC with narrowband compensation filter
   :align: center

   Wideband Frequency Response of CIC with narrowband compensation filter

Generating Filter Taps
~~~~~~~~~~~~~~~~~~~~~~
The following python code can be used to generate the taps for an FIR compensation filter and to plot the system's frequency response.

.. code-block:: python

   from scipy.signal import firwin2
   from scipy.signal import freqz
   import numpy as np
   import matplotlib.pyplot as plt
   np.seterr(divide='ignore', invalid='ignore');
    
   # cutOff is the cut off freq as a fraction of the lower sample rate
   # i.e 0.5 = Nyquist frequency
   def getFIRCompensationFilter(R,M,N,cutOff,numTaps,calcRes=1024):
       w = np.arange(calcRes) * np.pi/(calcRes - 1)
       Hcomp = lambda w : ((M*R)**N)*(np.abs((np.sin(w/(2.*R))) /
                           (np.sin((w*M)/2.)) ) **N)
       cicCompResponse = np.array(list(map(Hcomp, w)))
       # Set DC response to 1 as it is calculated as 'nan' by Hcomp
       cicCompResponse[0] = 1
       # Set stopband response to 0
       cicCompResponse[int(calcRes*cutOff*2):] = 0
       normFreq = np.arange(calcRes) / (calcRes - 1)
       taps = firwin2(numTaps, normFreq, cicCompResponse)
       return taps

   def plotFIRCompFilter(R,M,N,taps,wideband=False):
       if wideband: # Interpolate FIR filter to higher sample rate
           interp = np.zeros(len(taps)*R)
           interp[::R] = taps
           freqs,compResponse = freqz(interp)
           w = np.arange(len(freqs)) * np.pi/len(freqs) * R
       else:
           freqs,compResponse = freqz(taps)
           w = np.arange(len(freqs)) * np.pi/len(freqs)
       Hcic = lambda w : (1/((M*R)**N))*np.abs( (np.sin((w*M)/2.)) / (np.sin(w/(2.*R))) )**N
       cicMagResponse = np.array(list(map(Hcic, w)))
       combinedResponse = cicMagResponse * compResponse
       plt.plot(freqs/(2*np.pi),20*np.log10(abs(cicMagResponse)), label="CIC Filter")
       plt.plot(freqs/(2*np.pi),20*np.log10(abs(compResponse)), label="Compensation Filter")
       plt.plot(freqs/(2*np.pi),20*np.log10(abs(combinedResponse)), label="Combined Response")
       plt.grid(); plt.legend();
       axes = plt.gca(); axes.set_ylim([-200,25])
       plt.show()

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`CIC decimator primitive <cic_dec-primitive>`

 * :ref:`Rounding (half-up) primitive <rounding_halfup-primitive>`

 * :ref:`Down-sample protocol interface delay primitive <downsample_protocol_interface_delay-primitive>`

 * :ref:`Timestamp recovery primitive <timestamp_recovery-primitive>`

 * :ref:`Flush inserter primitive <flush_inserter-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``cic_decimator_xs`` are:

 * After a timestamp opcode message is received any zero length messages with the sample opcode are discarded until the next valid message with a sample opcode is received. Under all other conditions zero length sample opcode messages are passed directly through.

Testing
-------
.. ocpi_documentation_test_result_summary::
