.. cic_interpolator_xs HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _cic_interpolator_xs-HDL-worker:


``cic_interpolator_xs`` HDL worker
==================================

Detail
------
The worker implements the CIC interpolator primitive, with two instances declared one for the real data values and the second for the imaginary data values.

While the CIC filter stages are completed within the child primitive, the attenuation before the final output is done in the worker.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
