-- Delay module for CIC Interpolator
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes stream data.
-- Delay_G should be set to the delay in clock cycles of the module that
-- processes stream data.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.cic_interpolator_xs_worker_defs.all;

entity cic_interpolator_protocol_delay is
  generic (
    STAGE1_DELAY_G : integer := 1;
    STAGE2_DELAY_G : integer := 1

    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;  -- High when output is ready
    take_in             : in  std_logic;  -- High when data is taken from input
    input_in            : in  worker_input_in_t;   -- Input streaming interface
    up_sample_factor    : in  unsigned(15 downto 0);
    -- Connect output from data processing module
    processed_stream_in : in  std_logic_vector(31 downto 0);
    output_out          : out worker_output_out_t  -- Output streaming interface
    );
end cic_interpolator_protocol_delay;

architecture rtl of cic_interpolator_protocol_delay is
  -- Input register types
  type stage1_input_in_array_t is array (STAGE1_DELAY_G - 1 downto 0) of worker_input_in_t;
  type stage2_input_in_array_t is array (STAGE2_DELAY_G - 1 downto 0) of worker_input_in_t;
  -- Interface delay registers
  signal stage1_take_store     : std_logic_vector(STAGE1_DELAY_G - 1 downto 0);
  signal stage1_input_register : stage1_input_in_array_t;
  signal stage1_take           : std_logic;
  signal stage1_output         : worker_input_in_t;
  signal stage2_input          : worker_input_in_t;
  signal stage2_take_input     : std_logic;
  signal stage2_take_store     : std_logic_vector(STAGE2_DELAY_G - 1 downto 0);
  signal stage2_input_register : stage2_input_in_array_t;
  -- Up-sample state machine signal
  signal input_freeze          : std_logic;
  type state_t is (go_s, up_sample_s);
  signal current_state         : state_t;
  signal counter               : unsigned(15 downto 0);
  signal eom_r                 : std_logic;

begin

  ------------------------------------------------------------------------------
  -- Stage 1 Interface delay
  ------------------------------------------------------------------------------
  -- Add delay to align data with respective flow control signals
  stage1_delay_pipeline_1_gen : if STAGE1_DELAY_G = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          stage1_take_store <= (others => '0');
        -- other registers don't need to be reset as gated by
        -- stage1_take_store.
        elsif(enable = '1' and input_freeze = '0') then
          stage1_take_store(0)     <= take_in;
          stage1_input_register(0) <= input_in;
        end if;
      end if;
    end process;
  end generate;

  stage1_delay_pipeline_2_plus_gen : if STAGE1_DELAY_G > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          stage1_take_store <= (others => '0');
        -- other registers don't need to be reset as gated by
        -- stage1_take_store
        elsif(enable = '1' and input_freeze = '0') then
          stage1_take_store     <= stage1_take_store(stage1_take_store'length - 2 downto 0) & take_in;
          stage1_input_register <= stage1_input_register(stage1_input_register'length - 2 downto 0) & input_in;
        end if;
      end if;
    end process;
  end generate;

  stage1_take   <= stage1_take_store(stage1_take_store'high);
  stage1_output <= stage1_input_register(stage1_input_register'high);

  ------------------------------------------------------------------------------
  -- Up-sample stage
  ------------------------------------------------------------------------------
  -- Pauses the stage 1 input register while up-sampled samples are being
  -- generated.
  interpolate_stage_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        current_state <= go_s;
      elsif (enable = '1') then
        case current_state is
          when go_s =>
            if stage1_take = '1' and stage1_output.valid = '1' and up_sample_factor /= 1 and
              stage1_output.opcode = complex_short_timed_sample_sample_op_e then
              current_state <= up_sample_s;
              counter       <= up_sample_factor - 2;
              eom_r         <= stage1_output.eom;
            end if;
          when up_sample_s =>
            counter <= counter - 1;
            if counter = 0 then
              current_state <= go_s;
            end if;
        end case;
      end if;
    end if;
  end process;

  input_freeze <= '1' when current_state = up_sample_s else '0';

  -- Inject the interface control signals for the up sampled data
  stage1_output_p : process(current_state, counter, stage1_output,
                            stage1_take, up_sample_factor, eom_r)
  begin
    -- Data is always passed through as we only care about the contents when
    -- the opcode is not a stream opcode.
    stage2_input.data <= stage1_output.data;
    if current_state = go_s then
      stage2_take_input        <= stage1_take;
      stage2_input.valid       <= stage1_output.valid;
      stage2_input.som         <= stage1_output.som;
      stage2_input.byte_enable <= stage1_output.byte_enable;
      stage2_input.opcode      <= stage1_output.opcode;
      -- Suppress EOM flag when interpolating as it will be output at the
      -- end of the interpolated samples.
      if stage1_take = '1' and stage1_output.valid = '1' and up_sample_factor /= 1 and
        stage1_output.opcode = complex_short_timed_sample_sample_op_e then
        stage2_input.eom <= '0';
      else
        stage2_input.eom <= stage1_output.eom;
      end if;
    else
      -- During interpolation most of the interface control signals are fixed
      stage2_take_input        <= '1';
      stage2_input.valid       <= '1';
      stage2_input.som         <= '0';
      stage2_input.byte_enable <= (others => '1');
      stage2_input.opcode      <= complex_short_timed_sample_sample_op_e;
      -- On the last sample of the interpolation output the EOM flag if the
      -- original up sampled sample had it set.
      if counter = 0 then
        stage2_input.eom <= eom_r;
      else
        stage2_input.eom <= '0';
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Stage 2 interface delay
  ------------------------------------------------------------------------------
  -- Add further delay to align data with respective flow control signals
  stage2_delay_pipeline_1_gen : if STAGE2_DELAY_G = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          stage2_take_store <= (others => '0');
        -- Other registers don't need to be reset as gated by
        -- stage2_take_store
        elsif(enable = '1') then
          stage2_take_store(0)     <= stage2_take_input;
          stage2_input_register(0) <= stage2_input;
        end if;
      end if;
    end process;
  end generate;

  stage2_delay_pipeline_2_plus_gen : if STAGE2_DELAY_G > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          stage2_take_store <= (others => '0');
        -- Other registers don't need to be reset as gated by
        -- stage2_take_store
        elsif(enable = '1') then
          stage2_take_store     <= stage2_take_store(stage2_take_store'length - 2 downto 0) & stage2_take_input;
          stage2_input_register <= stage2_input_register(stage2_input_register'length - 2 downto 0) & stage2_input;
        end if;
      end if;
    end process;
  end generate;

  -- When the delayed interface opcode is the stream opcode and the data is
  -- valid, output the processed stream data rather than the delayed data.
  output_out.data <=
    std_logic_vector(processed_stream_in) when
    stage2_input_register(stage2_input_register'high).valid = '1' and
    stage2_input_register(stage2_input_register'high).opcode = complex_short_timed_sample_sample_op_e
    else stage2_input_register(stage2_input_register'high).data;

  -- Streaming interface output
  output_out.give        <= stage2_take_store(stage2_take_store'high) and enable;
  output_out.valid       <= stage2_input_register(stage2_input_register'high).valid;
  output_out.som         <= stage2_input_register(stage2_input_register'high).som;
  output_out.eom         <= stage2_input_register(stage2_input_register'high).eom;
  output_out.byte_enable <= stage2_input_register(stage2_input_register'high).byte_enable;
  output_out.opcode      <= stage2_input_register(stage2_input_register'high).opcode;

end rtl;
