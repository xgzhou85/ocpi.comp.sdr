-- HDL Implementation of a threshold component.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of threshold_s_b_worker is

  constant delay_c : integer := 1;

  signal enable        : std_logic;
  signal output_ready  : std_logic;
  signal take          : std_logic;
  signal input_hold    : std_logic;
  signal threshold_out : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  ------------------------------------------------------------------------------
  -- Interface signals
  ------------------------------------------------------------------------------

  -- Take input when both input and output are ready
  take           <= ctl_in.is_operating and input_in.ready and output_in.ready and not input_hold;
  input_out.take <= take;

  output_ready <= ctl_in.is_operating and output_in.ready;
  -- Only enable processing when the output is ready and input_hold is low.
  -- Input hold is high when non-stream opcodes with valid data are being
  -- output from the component, as this takes two clock cycles per input due to
  -- this difference in input and output width.
  enable       <= output_ready and not input_hold;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the threshold operation.
  interface_delay_i : entity work.short_to_bool_protocol_delay
    generic map (
      DELAY_G          => delay_c,
      DATA_IN_WIDTH_G  => input_in.data'length,
      DATA_OUT_WIDTH_G => output_out.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_ready,
      take_in             => take,
      input_in            => input_in,
      processed_stream_in => threshold_out,
      output_out          => output_out,
      input_hold_out      => input_hold
      );

  ------------------------------------------------------------------------------
  -- Threshold Logic
  ------------------------------------------------------------------------------
  -- Test if the input is less than the threshold
  threshold_process : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if enable = '1' then
        if (signed(input_in.data) < props_in.threshold) then
          if (props_in.invert_output = '1') then
            threshold_out <= X"01";
          else
            threshold_out <= X"00";
          end if;
        else
          if (props_in.invert_output = '1') then
            threshold_out <= X"00";
          else
            threshold_out <= X"01";
          end if;
        end if;
      end if;
    end if;
  end process;

end rtl;
