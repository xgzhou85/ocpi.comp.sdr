#!/usr/bin/env python3

# Python implementation of CIC filter, with decimator.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import numpy as np
import decimal

import ocpi_testing

# Comb stage


class Comb:
    def __init__(self, delay):
        self._delay = delay
        self.reset()

    def reset(self):
        self._delay_buffer = [0] * (self._delay)

    def call(self, value):
        self._delay_buffer.append(value)
        return value - self._delay_buffer.pop(0)

# Integrator stage


class Integrator:
    def __init__(self):
        self.reset()

    def reset(self):
        self._previous_value = 0

    def call(self, value):
        self._previous_value = value + self._previous_value
        return self._previous_value

# Decimate stage


class Decimate:
    def __init__(self, down_sample_factor):
        self._down_sample_factor = down_sample_factor
        self.reset()

    def reset(self):
        self._down_sample_count = 0

    def call(self, value):
        if self._down_sample_count == 0:
            self._down_sample_count = self._down_sample_factor - 1
            return value
        else:
            self._down_sample_count = self._down_sample_count - 1
            return None


class CicDecimatorSingleChannel():
    def __init__(self, cic_order, delay_factor, down_sample_factor,
                 output_scale_factor, cic_reg_size):
        self._cic_order = cic_order
        self._delay_factor = delay_factor
        self._down_sample_factor = down_sample_factor
        self._output_scale_factor = output_scale_factor
        self._cic_reg_size = cic_reg_size

        self._max_internal_value = 2**(cic_reg_size - 1) - 1
        self._min_internal_value = -1 * (2**(cic_reg_size - 1))

        self._integrators = [Integrator() for n in range(cic_order)]
        self._combs = [Comb(delay_factor) for n in range(cic_order)]
        self._decimate = Decimate(down_sample_factor)

    def reset(self):
        for n in range(0, self._cic_order):
            self._combs[n].reset()
            self._integrators[n].reset()
        self._decimate.reset()

    def sample(self, values):
        output_values = []
        for value in values:
            # Apply each integrator stage
            for n in range(self._cic_order):
                value = self._integrators[n].call(value)
                # Match implementation bit width
                while value < self._min_internal_value:
                    value = value + (2**self._cic_reg_size)
                while value > self._max_internal_value:
                    value = value - (2**self._cic_reg_size)

            # Decimate the data
            value = self._decimate.call(value)

            # Apply each comb stage - value can be None after decimator
            if value is not None:
                for n in range(self._cic_order):
                    value = self._combs[n].call(value)
                    # Match implementation bit width
                    while value < self._min_internal_value:
                        value = value + (2**self._cic_reg_size)
                    while value > self._max_internal_value:
                        value = value - (2**self._cic_reg_size)

                # Do the output scaling
                # Don't halfup round
                if self._output_scale_factor == 0:
                    value = np.int16(value)
                    output_values.append(value)
                else:
                    # Round halfup
                    value = np.right_shift(
                        int(value), self._output_scale_factor - 1) + 1
                    value = np.int16(np.right_shift(value, 1))
                    output_values.append(value)

        return output_values


class CicDecimator(ocpi_testing.Implementation):
    def __init__(self, cic_order, delay_factor, down_sample_factor,
                 output_scale_factor, flush_length, cic_reg_size):
        super().__init__(cic_order=cic_order,
                         delay_factor=delay_factor,
                         down_sample_factor=down_sample_factor,
                         output_scale_factor=output_scale_factor,
                         flush_length=flush_length,
                         cic_reg_size=cic_reg_size)

        self._max_message_samples = 4096
        self._sample_counter = 0
        self._sample_interval = 0
        self._timestamp = None

        self._cic_real = CicDecimatorSingleChannel(
            self.cic_order, self.delay_factor, self.down_sample_factor,
            self.output_scale_factor, self.cic_reg_size)
        self._cic_imag = CicDecimatorSingleChannel(
            self.cic_order, self.delay_factor, self.down_sample_factor,
            self.output_scale_factor, self.cic_reg_size)

    def reset(self):
        self._cic_real.reset()
        self._cic_imag.reset()
        self._sample_counter = 0
        self._sample_interval = 0
        self._timestamp = None

    def sample(self, values):
        input_data = np.array(values)
        output_real = self._cic_real.sample(np.real(input_data))
        output_imag = self._cic_imag.sample(np.imag(input_data))
        output = []
        if len(output_real):
            output = list(np.vectorize(complex)(output_real, output_imag))

        messages = [{"opcode": "sample", "data": output}]
        # Insert timestamp if available
        if self._timestamp is not None:
            # Update timestamp if not enough stream data is received to
            # append a timestamp
            if self._sample_counter + len(values) < self.down_sample_factor:
                self._timestamp += (self._sample_interval *
                                    decimal.Decimal(len(values)))
            else:
                if self._sample_counter:
                    self._timestamp += self._sample_interval * \
                        decimal.Decimal((self.down_sample_factor -
                                         self._sample_counter))
                    self._timestamp %= decimal.Decimal(2**63)
                messages[:0] = [
                    {"opcode": "time", "data": self._timestamp}]
                self._timestamp = None
        self._sample_counter += len(values)
        self._sample_counter %= self.down_sample_factor

        # Suppress length 1 messages that are decimated away.
        if len(np.real(input_data)) == 1 and len(output_real) == 0:
            return self.output_formatter([])
        else:
            return self.output_formatter(messages)

    def flush(self, *inputs):
        if self.flush_length > 0:
            remaining_flush_samples = self.flush_length
            messages = []
            # Run stream method on flush samples. Split input message over
            # multiple messages if required.
            while remaining_flush_samples > 0:
                if remaining_flush_samples > self._max_message_samples:
                    messages += self.sample(
                        [0] * self._max_message_samples).output
                    remaining_flush_samples -= self._max_message_samples
                else:
                    messages += self.sample([0]*remaining_flush_samples).output
                    remaining_flush_samples = 0

            messages.append({"opcode": "flush", "data": None})
            return self.output_formatter(messages)
        else:
            return self.output_formatter([{"opcode": "flush", "data": None}])

    def sample_interval(self, value):
        self._sample_interval = decimal.Decimal(value)
        return self.output_formatter(
            [{"opcode": "sample_interval", "data": value}])

    def time(self, value):
        self._timestamp = decimal.Decimal(value)
        return self.output_formatter([])
