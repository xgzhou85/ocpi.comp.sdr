#!/usr/bin/env python3

# Generates the input binary file for cic_interpolator_xs testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import random
import os

import ocpi_protocols
import ocpi_testing


class CicDecimatorGenerator(ocpi_testing.generator.ComplexShortGenerator):
    # This test checks timestamps are advanced correctly
    def custom(self, seed, subcase):
        if subcase == "timestamp":
            # Back up original message length
            original_message_length = self.SAMPLE_DATA_LENGTH
            random.seed(seed)
            # Set sample interval to a non-zero value
            messages = [{"opcode": "sample_interval",
                         "data": random.randint(1, 100000000)}]
            for val in range(10):
                self.SAMPLE_DATA_LENGTH = random.randint(
                    1, original_message_length)
                messages += [
                    {"opcode": "time",
                     "data": random.randint(1, 100000000)}]
                messages += self.typical(seed, subcase)

            # Reset SAMPLE_DATA_LENGTH back to its original value
            self.SAMPLE_DATA_LENGTH = original_message_length
            return messages
        else:
            raise ValueError(f"Unexpected subcase of {subcase} for custom()")


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
cic_order = int(os.environ.get("OCPI_TEST_cic_order"))
delay_factor = int(os.environ.get("OCPI_TEST_cic_diff_delay"))
down_sample_factor = int(os.environ.get("OCPI_TEST_down_sample_factor"))
output_scale_factor = int(os.environ.get("OCPI_TEST_scale_output"))
flush_length = int(os.environ.get("OCPI_TEST_flush_length"))
cic_reg_size = int(os.environ.get("OCPI_TEST_cic_reg_size"))

seed = ocpi_testing.get_test_seed(arguments.case, subcase, cic_order,
                                  delay_factor, down_sample_factor,
                                  output_scale_factor, flush_length,
                                  cic_reg_size)

generator = CicDecimatorGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path,
                                      "complex_short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
