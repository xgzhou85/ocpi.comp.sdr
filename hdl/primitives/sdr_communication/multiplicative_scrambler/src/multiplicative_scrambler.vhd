-- HDL implementation of a multiplicative self-synchronising scrambler.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Scrambler - multiplicative (self-synchronising) implementation to
-- scramble the input serial data stream.
-- polynomial_g sets the polynomial N.B. "11101" is 1 + z^-1 + z^-2 + z^-4
-- seed sets the initial value
-- data_valid_in clocks in input data on a rising clock edge when high
-- data_in is the serial input data
-- data_valid_out is high when the output data is valid
-- data_out is the scrambled data
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity multiplicative_scrambler is
  generic (
    polynomial_g : std_logic_vector := "11101"
    );
  port (
    clk            : in  std_logic;
    rst            : in  std_logic;
    seed           : in  std_logic_vector(polynomial_g'length - 2 downto 0);
    data_valid_in  : in  std_logic;
    data_in        : in  std_logic;
    data_valid_out : out std_logic;
    data_out       : out std_logic
    );
end multiplicative_scrambler;

architecture rtl of multiplicative_scrambler is

  signal lfsr_q         : std_logic_vector(polynomial_g'length - 2 downto 0);
  signal lfsr_c         : std_logic_vector(polynomial_g'length - 2 downto 0);
  signal scrambled_data : std_logic;

begin

  -- Shift logic for LFSR
  lfsr_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        lfsr_q <= seed;
      else
        if data_valid_in = '1' then
          lfsr_q(polynomial_g'length - 2) <= scrambled_data;
          for i in 0 to polynomial_g'length - 3 loop
            lfsr_q(i) <= lfsr_q(i+1);
          end loop;
        end if;
      end if;
    end if;
  end process;

  -- Feedback XOR logic
  lfsr_c(polynomial_g'length - 2) <= lfsr_q(polynomial_g'length - 2) when
                                     polynomial_g(polynomial_g'length - 2) = '1' else '0';

  feedback_g : for i in 0 to polynomial_g'length - 3 generate
    lfsr_c(i) <= lfsr_q(i) xor lfsr_c(i+1) when polynomial_g(i) = '1' else
                 lfsr_c(i+1);
  end generate;

  -- XOR input data with feedback logic value
  scrambled_data <= data_in xor lfsr_c(0);

  -- Output data
  data_valid_out <= data_valid_in;
  data_out       <= scrambled_data;

end rtl;
