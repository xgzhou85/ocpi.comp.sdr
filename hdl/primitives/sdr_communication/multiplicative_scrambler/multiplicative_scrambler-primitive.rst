.. multiplicative_scrambler documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _multiplicative_scrambler-primitive:


Multiplicative scrambler (``multiplicative_scrambler``)
=======================================================
Multiplicative (self-synchronising) scrambler.

Design
------
A scrambler (also referred to as a randomiser) is a device that manipulates a data stream before transmitting. The scrambled data stream can be reversed by a descrambler at the receiver end to recover the original data stream. The purpose of scrambling is to reduce the length of consecutive zeros or ones in a transmitted data stream, which facilitates accurate timing recovery at the receiver. Their designs are usually based on linear feedback shift registers (LFSRs) due to their good statistical properties and ease of implementation in hardware. Note that there are still sequences of ones and zeros that can yield all ones or zeros, or other undesirable periodic output data. This primitive implements a multiplicative (self-synchronising) scrambler, also known as a feed-through. :numref:`multiplicative_scrambler-diagram` shows an example multiplicative (self-synchronising) scrambler implementation.

.. _multiplicative_scrambler-diagram:

.. figure:: multiplicative_scrambler.svg
   :alt: Example multiplicative scrambler implementation.
   :align: center

   Example implementation of multiplicative (self-synchronising) scrambler. With a polynomial of :math:`x^4 + x^2 + x^1 + 1`. :math:`x[n]` are the input values. :math:`y[n]` are the output values.

Implementation
--------------
The polynomial determines the number of shift registers and XOR gates in the LFSR implementation. The polynomial is specified at build time using the generic ``polynomial_g``.

The initial state of the LFSRs can be set during run time using the ``seed`` input. When the reset signal, ``rst``, is set high the LFSRs becomes the value of ``seed`` on the next rising clock edge.

This implementation can generate a new output every clock cycle. There is a one clock cycle latency before the next value to XOR the input data with is available after a valid input.

Interface
---------

Generics
~~~~~~~~

 * ``polynomial_g`` (``std_logic_vector``): Polynomial value. E.g. "11101" implements :math:`1 + x^1 + x^2 + x^4`.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``seed`` (``std_logic_vector``, ``polynomial_g'length - 1`` bits), in: Seed. Initial LFSR values.

 * ``data_valid_in`` (``std_logic``), in: Valid Input. High when data on ``data_in`` port is available.

 * ``data_in`` (``std_logic``), in: Data input. Binary input data.

 * ``data_valid_out`` (``std_logic``), in: Valid Output. High when data on ``data_out`` port is available.

 * ``data_out`` (``std_logic``), in: Data Output. Binary scrambled output data.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``multiplicative_scrambler`` are:

 * None.
