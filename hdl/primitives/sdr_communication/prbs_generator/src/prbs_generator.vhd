-- HDL Implementation of a PRBS generator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Pseudo-random Bit Sequence Generator
-- Generates a pseudo-random sequence of bits from a generator polynomial.
-- Uses a Fibonacci style Linear Feedback Shift Register (LFSR).
-- Output can be inverted and an initial seed for the LFSR can be set.
-- Generator polynomial is set at compile time.
-- initial_i seed is set at runtime when reset = '0'
-- invert_i is set at runtime and can be changed at anytime.
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

entity prbs_generator is

  generic (
    LFSR_WIDTH_G : positive := 31;
    LFSR_TAP_1_G : natural  := 28;
    LFSR_TAP_2_G : natural  := 0;
    LFSR_TAP_3_G : natural  := 0;
    LFSR_TAP_4_G : natural  := 0;
    LFSR_TAP_5_G : natural  := 0
    );
  port (
    clk        : in  std_logic;
    reset      : in  std_logic;
    -- PRBS gen output enabled when clk_en='1', else disabled.
    clk_en     : in  std_logic;
    -- Set to 0 for normal output or 1 to invert output.
    invert_i   : in  std_logic;
    -- initial_i must not be all 0's if invert_i = '0'
    -- initial_i must not be all 1's if invert_i = '1'
    initial_i  : in  std_logic_vector(LFSR_WIDTH_G - 1 downto 0);
    data_o     : out std_logic_vector(LFSR_WIDTH_G - 1 downto 0);
    data_msb_o : out std_logic
    );

end prbs_generator;

architecture behavioral of prbs_generator is

  -- Create types for defining tap array
  -- Many max length sequences can be done with a single tap but some sequences
  -- in range n=3 to n=168 require up to 5 taps (See Xilinx XAPP210 for details)
  constant MAX_TAPS_C   : positive                                := 5;
  type tap_position_t is array(0 to MAX_TAPS_C - 1) of natural;
  constant PRBS_TAPS_C  : tap_position_t                          := (LFSR_TAP_1_G, LFSR_TAP_2_G, LFSR_TAP_3_G, LFSR_TAP_4_G, LFSR_TAP_5_G);
  signal feedback_bit   : std_logic;
  -- Use 1 indexed so that the polynomial taps can be applied directly to array
  -- rather than subtracting one from each tap.
  signal register_value : std_logic_vector(LFSR_WIDTH_G downto 1) := (others => '0');

begin

  ------------------------------------------------------------------------------
  -- Linear Feedback Shift Register
  ------------------------------------------------------------------------------
  -- Implements a Fibonacci style LFSR
  lfsr_advance_p : process (clk)
  begin
    if rising_edge(clk) then
      -- Set LFSR initial value
      if reset = '1' then
        register_value <= initial_i;
      elsif clk_en = '1' then
        if invert_i = '0' then
          -- XOR feedback when invert_i == '0'
          register_value <= register_value(register_value'left - 1 downto 1) & feedback_bit;
        else
          -- XNOR feedback when invert_i == '1'
          register_value <= register_value(register_value'left - 1 downto 1) & (not feedback_bit);
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Generate feedback bit for LFSR
  ------------------------------------------------------------------------------
  lfsr_feedback_p : process(register_value)
    variable feedback_xor_var : std_logic;
  begin
    -- Take MSB of LFSR
    feedback_xor_var := register_value(register_value'left);
    -- Perform XOR of MSB and all taps
    for tap in 0 to (MAX_TAPS_C - 1) loop
      if PRBS_TAPS_C(tap) /= 0 then
        feedback_xor_var := feedback_xor_var xor register_value(PRBS_TAPS_C(tap));
      end if;
    end loop;
    feedback_bit <= feedback_xor_var;
  end process;

  ------------------------------------------------------------------------------
  -- Data Output
  ------------------------------------------------------------------------------
  -- Output full LFSR register
  data_o     <= register_value;
  -- Output just MSB of LFSR
  data_msb_o <= register_value(register_value'left);

end behavioral;
