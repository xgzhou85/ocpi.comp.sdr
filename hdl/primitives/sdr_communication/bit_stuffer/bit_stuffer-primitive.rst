.. bit_stuffer documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _bit_stuffer-primitive:


Bit stuffer (``bit_stuffer``)
=============================
Zero insertion bit stuffer.

Design
------
Bit stuffing is the insertion of non-information bits into binary data. It has various purposes, such as to alter the bit rate of a bit stream, or to fill up a frame, or to aid receiver synchronisation. Typically it is used to limit the number of consecutive bits of the same value in the data to be transmitted. This allows for certain sequences of ones and zeros to be reserved for frame delimiting by ensuring the data itself can never be the same as the beginning or end flags.

An example block diagram representation of the implementation is given in :numref:`bit_stuffer-diagram`.

.. _bit_stuffer-diagram:

.. figure:: bit_stuffer.svg
   :alt: Example of bit stuffer arrangement of registers data flow.
   :align: center

   Example implementation of bit stuffer when matching four repeated ones.

Implementation
--------------
The number of consecutively received ones to insert a zero after is set by ``consecutive_ones_g`` at build time. When the input buffer contains all ones the ``all_ones`` binary output is high. When a one is present on the input and the input buffer contains all ones, ``ready`` switches low on the next rising clock edge. ``ready`` remains low until the next output sample whilst a zero is inserted.

The reset signal, ``rst`` empties the internal buffer of all previously received 1s on the next rising clock edge when high.

This implementation can generate a new output every clock cycle without any clock cycle latency.

Interface
---------

Generics
~~~~~~~~

 * ``consecutive_ones_g`` (``integer``): Number of consecutive ones to insert a zero after.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Clock Enable. Enables all processes on a rising clock edge.

 * ``ready`` (``std_logic``), out: Input Ready. High when input is ready for data.

 * ``all_ones`` (``std_logic``), out: High when the internal input buffer contains all 1s.

 * ``data_valid_in`` (``std_logic``), in: Valid Input. High when data on ``data_in`` port is available.

 * ``data_in`` (``std_logic``), in: Data input. Binary input data.

 * ``data_valid_out`` (``std_logic``), in: Valid Output. High when data on ``data_out`` port is available.

 * ``data_out`` (``std_logic``), in: Data Output. Binary bit stuffed output data.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``bit_stuffer`` are:

 * ``consecutive_ones_g`` cannot be set to zero
