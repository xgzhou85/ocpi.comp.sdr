-- HDL implementation of a bit stuffer.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Bit Stuffer - bit stuffer to insert a 0 after a number of consecutive 1s
-- consecutive_ones_g is the number of consecutive 1s to insert a zero after
-- rst empties the input buffer of previously received 1s when high
-- enable enables all processes on a rising clock edge
-- ready indicates when the input is ready for data
-- all_ones is true when another input one would cause a zero insertion
-- data_valid_in clocks in input data on a rising clock edge when high
-- data_in is the input data
-- data_valid_out is high when the output data is valid
-- data_out is the bit stuffed data
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity bit_stuffer is
  generic (
    consecutive_ones_g : integer := 5
    );
  port (
    clk            : in  std_logic;
    rst            : in  std_logic;
    enable         : in  std_logic;
    ready          : out std_logic;
    all_ones       : out std_logic;
    data_valid_in  : in  std_logic;
    data_in        : in  std_logic;
    data_valid_out : out std_logic;
    data_out       : out std_logic
    );
end bit_stuffer;

architecture rtl of bit_stuffer is

  constant ones_c : std_logic_vector(consecutive_ones_g - 1 downto 0) :=
    (others => '1');

  signal data         : std_logic;
  signal data_valid   : std_logic;
  signal ones_full    : std_logic;
  signal insert_zero  : std_logic;
  signal input_ready  : std_logic;
  signal input_buffer : std_logic_vector(consecutive_ones_g - 1 downto 0);

begin

  -- Data signals
  data       <= data_in and input_ready;
  data_valid <= data_valid_in or not input_ready;

  -- Check that the number of consecutive ones is greater than 1 at build time
  consecutive_ones_2_gen : if consecutive_ones_g > 1 generate

    -- Input buffer
    input_buffer_p : process(clk)
    begin
      if rising_edge(clk) then
        if rst = '1' then
          input_buffer <= (others => '0');
        elsif enable = '1' and data_valid = '1' then
          input_buffer(0) <= data;
          input_buffer(input_buffer'length - 1 downto 1) <=
            input_buffer(input_buffer'length - 2 downto 0);
        end if;
      end if;
    end process;

    -- Signal when consecutive_ones_g - 1 consecutive 1s have been received and
    -- the input buffer is full
    ones_full <= '1' when input_buffer(consecutive_ones_g - 2 downto 0) =
                 ones_c(consecutive_ones_g - 2 downto 0) else '0';

    -- Signal when about to insert a zero due to the input buffer being full of
    -- 1s and another 1 available on the input
    insert_zero <= ones_full and data_valid_in and data_in;

  end generate;

  -- Insert a zero after every one if the number of consecutive ones is set to
  -- 1 at build time
  consecutive_ones_1_gen : if consecutive_ones_g = 1 generate
    insert_zero <= data_valid_in and data_in;
  end generate;

  -- Implement bit stuffing
  bit_stuffer_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        input_ready <= '1';
      elsif enable = '1' then
        -- Signal the input is not ready for more data on the next output
        -- sample because a zero is being inserted then
        if insert_zero = '1' then
          input_ready <= '0';
        else
          input_ready <= '1';
        end if;
      end if;
    end if;
  end process;

  -- Output data
  all_ones       <= ones_full;
  ready          <= input_ready;
  data_out       <= data;
  data_valid_out <= enable and data_valid;

end rtl;
