.. crc_serial documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _crc_serial-primitive:


Serial CRC generator (``crc_serial``)
=====================================
Cyclic Redundancy Check (CRC) generator.

Design
------
A CRC is an error-detecting algorithm commonly used to detect errors during the exchange of data.

The CRC value is the remainder of a polynomial division algorithm on the input data stream. The CRC calculation (modulo 2 divisions) can be easily performed by logical combinations of shift registers and XOR gates.

The linear feedback shift register (LFSR) is the most common approach for a serial CRC generator implementation. The specific LFSR type this design uses is called Galois.

:numref:`crc_serial-diagram` shows an example serial CRC generator with a polynomial of :math:`x^5 + x^2 + 1` (0x5) as is used for the USB 2.0 protocol.

.. _crc_serial-diagram:

.. figure:: crc_serial.svg
   :alt: Example serial CRC implementation.
   :align: center

   Example serial CRC generator. With polynomial :math:`x^5 + x^2 + 1`. :math:`x[n]` are the input values. :math:`y[n]` are the output values.

Implementation
--------------
The polynomial sets the divisor of the polynomial long division algorithm. The order of the polynomial sets the number of shift registers required to implement the LFSR. The polynomial is defined at build time using the generic ``polynomial_g``.

The initial CRC output can be set during run time using the ``seed`` input. When the reset signal, ``rst``, is set high the CRC output becomes the value of ``seed`` on the next rising clock edge.

Depending on the application the CRC result may need to be reflected. Generic ``output_reflect_g`` provides the option to enable or disable this at build time.

Some applications require a final XOR on the CRC result. The final XOR value can be set during run time using the ``final_xor`` input.

This implementation can generate a new output every clock cycle. There is a one clock cycle latency before the CRC result is available after a valid input. A clock enable input, ``clk_en``, is provided to control when an input is valid and therefore shifted into the LFSR. The CRC generator state will not advance when the clock enable is low.

Interface
---------

Generics
~~~~~~~~

 * ``polynomial_g`` (``std_logic_vector``): Polynomial value.

 * ``output_reflect_g`` (``std_logic``): Enable / disable output CRC reflection.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``clk_en`` (``std_logic``), in: Enable. Enables / disables data input when high.

 * ``data_in`` (``std_logic``), in: Data input. Binary input data.

 * ``seed`` (``std_logic_vector``, ``polynomial_g'length`` bits), in: Seed. Initial CRC value.

 * ``final_xor``  (``std_logic_vector``, ``polynomial_g'length`` bits), in: Final XOR. Value to XOR CRC result with.

 * ``crc_out`` (``std_logic_vector``, ``polynomial_g'length`` bits), out: CRC. Available after one clock cycle delay.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``crc_serial`` are:

 * Although less resources are utilised than ``crc_parallel``'s design the throughput is less as it only allows the CRC calculation of one data bit every clock cycle.
