-- Signed array package.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

package fir_signed_array_pkg is

  constant pkg_coefficient_width : integer := 16;

  type fir_signed_array_15 is array (0 to 14) of signed(pkg_coefficient_width - 1 downto 0);
  type fir_signed_array_16 is array (0 to 15) of signed(pkg_coefficient_width - 1 downto 0);
  type fir_signed_array_31 is array (0 to 30) of signed(pkg_coefficient_width - 1 downto 0);
  type fir_signed_array_32 is array (0 to 31) of signed(pkg_coefficient_width - 1 downto 0);
  type fir_signed_array_63 is array (0 to 62) of signed(pkg_coefficient_width - 1 downto 0);
  type fir_signed_array_64 is array (0 to 63) of signed(pkg_coefficient_width - 1 downto 0);
  type fir_signed_array_127 is array (0 to 126) of signed(pkg_coefficient_width - 1 downto 0);
  type fir_signed_array_128 is array (0 to 126) of signed(pkg_coefficient_width - 1 downto 0);
  type fir_signed_array_255 is array (0 to 254) of signed(pkg_coefficient_width - 1 downto 0);
  type fir_signed_array_256 is array (0 to 255) of signed(pkg_coefficient_width - 1 downto 0);

  type fir_signed_array is array (natural range <>) of signed(15 downto 0);

end package fir_signed_array_pkg;
