-- HDL Implementation of a CORDIC based sine and cosine calculation
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;


-- Calculates the Sine and Cosine of the angle_in input. Uses a pipelined
-- implementation of the CORDIC algorithm.
entity cordic_sin_cos is

  generic (
    -- Sets the width of the angle_in input. (Input size must match CORDIC
    -- lookup table width)
    input_size_g  : integer := 32;
    -- Sets the width of the sine_out and cosine_out outputs. (Max. 32 bits,
    -- or LUT_depth_c + 1)
    output_size_g : integer := 16
    );

  port (
    clk        : in  std_logic;
    rst        : in  std_logic;
    clk_en     : in  std_logic;
    -- The angle for which the Sine and Cosine should be calculated. 0 = 0 deg
    -- and (2^32 - 1) = 359.999... deg. (Unsigned)
    angle_in   : in  std_logic_vector(input_size_g - 1 downto 0);
    -- Sets the scaling of the sine/cosine outputs.
    -- Set to floor(desired_output_scale/1.646760258).
    gain_in    : in  std_logic_vector(output_size_g - 1 downto 0);
    sine_out   : out std_logic_vector(output_size_g - 1 downto 0);
    cosine_out : out std_logic_vector(output_size_g - 1 downto 0)
    );
end cordic_sin_cos;


architecture behavioral of cordic_sin_cos is
  -- Define Cordic look up table (LUT)
  -- Sets size of lookup table. Limits max output_size_g setting.
  constant LUT_depth_c : integer := 31;
  -- Sets width of lookup table. Must be equal to input_size_g.
  constant LUT_width_c : integer := 32;

  type LUT_t is array (0 to LUT_depth_c - 1) of signed(LUT_width_c - 1 downto 0);

  -- CORDIC Look up table.
  -- Generated using: round(ATAN(2^-i) * ((2^LUT_width_c)/2pi)),
  -- i = 0,1,...,(LUT\_depth\_c - 1) (Assuming atan output is radians)
  constant cordic_lut_c : LUT_t := (
    X"20000000", X"12E4051E", X"09FB385B", X"051111D4", X"028B0D43", X"0145D7E1",
    X"00A2F61E", X"00517C55", X"0028BE53", X"00145F2F", X"000A2F98", X"000517CC",
    X"00028BE6", X"000145F3", X"0000A2FA", X"0000517D", X"000028BE", X"0000145F",
    X"00000A30", X"00000518", X"0000028C", X"00000146", X"000000A3", X"00000051",
    X"00000029", X"00000014", X"0000000A", X"00000005", X"00000003", X"00000001",
    X"00000000"
    );

  signal quadrant : std_logic_vector(1 downto 0);
  signal gain     : std_logic_vector(output_size_g downto 0);

  -- Stores initial values for x, y, and z
  signal initial_x : signed(output_size_g downto 0);
  signal initial_y : signed(output_size_g downto 0);
  signal initial_z : signed(input_size_g - 1 downto 0);

  -- Stores x and y values for each pipeline stage of the cordic algorithm
  -- X and Y must be one bit wider than the output size due to adder carry bits
  type pipeline_array_t is array (0 to output_size_g - 1)
    of signed(output_size_g downto 0);

  signal X : pipeline_array_t := (others => (others => '0'));
  signal Y : pipeline_array_t := (others => (others => '0'));

  -- Stores z values for each pipeline stage of the cordic algorithm.
  -- Z must be as wide as the input angle
  type pipeline_array_z_t is array (0 to output_size_g - 1)
    of signed(input_size_g - 1 downto 0);

  signal Z : pipeline_array_z_t := (others => (others => '0'));

begin

  -- Top two bits of angle_in signal indicate its quadrant
  quadrant <= angle_in(input_size_g - 1 downto input_size_g - 2);

  -- Expand gain_in by one bit while preserving the sign bit
  gain <= ('1' & gain_in) when gain_in(output_size_g - 1) = '1'
          else ('0' & gain_in);

  -- This cordic implementation only works between -90 and +90. This process
  -- rotates the input by +/- 90 degrees so that -90 >= Z(0) <= +90
  angle_input : process(quadrant, gain, angle_in)
  begin

    case quadrant is

      when "00" =>
        initial_x <= signed(gain);
        initial_y <= (others => '0');
        initial_z <= signed(angle_in);
      when "01" =>
        initial_x <= (others => '0');
        initial_y <= signed(gain);
        initial_z <= signed(("00" & angle_in(input_size_g-3 downto 0)));
      when "10" =>
        initial_x <= (others => '0');
        initial_y <= -(signed(gain));
        initial_z <= signed(("11" & angle_in(input_size_g-3 downto 0)));
      when "11" =>
        initial_x <= signed(gain);
        initial_y <= (others => '0');
        initial_z <= signed(angle_in);
      when others =>
        initial_x <= (others => '0');
        initial_y <= (others => '0');
        initial_z <= (others => '0');

    end case;

  end process angle_input;

  -- This process generates the pipelined stages of the CORDIC algorithm.
  -- See the Microchip Application note AN1061 for more details.
  pipelined_cordic : process(clk, rst)
  begin
    if rising_edge(clk) then

      if (rst = '1') then

        -- Clear the whole pipeline on reset
        for gen_var in 0 to (output_size_g - 1) loop
          X(gen_var) <= (others => '0');
          Y(gen_var) <= (others => '0');
          Z(gen_var) <= (others => '0');
        end loop;

      elsif (clk_en = '1') then

        -- Register initial values
        X(0) <= initial_x;
        Y(0) <= initial_y;
        Z(0) <= initial_z;

        -- Perform the CORDIC algorithm. Save the result for each stage in
        -- the next stage of the pipeline.
        generate_pipeline : for gen_var in 0 to output_size_g - 2 loop
          if (Z(gen_var) < 0) then
            X(gen_var + 1) <= X(gen_var) + (shift_right((Y(gen_var)), gen_var));
            Y(gen_var + 1) <= Y(gen_var) - (shift_right((X(gen_var)), gen_var));
            Z(gen_var + 1) <= Z(gen_var) + cordic_lut_c(gen_var);
          else
            X(gen_var + 1) <= X(gen_var) - (shift_right((Y(gen_var)), gen_var));
            Y(gen_var + 1) <= Y(gen_var) + (shift_right((X(gen_var)), gen_var));
            Z(gen_var + 1) <= Z(gen_var) - cordic_lut_c(gen_var);
          end if;
        end loop generate_pipeline;

      end if;
    end if;

  end process pipelined_cordic;

  -- Assign output as last stage of the CORDIC pipeline.
  -- As long as gain is set to ensure full scale range of output width (or less)
  -- then MSBit can be discarded to obtain the desired output width
  sine_out   <= std_logic_vector(Y(output_size_g - 1)(output_size_g - 1 downto 0));
  cosine_out <= std_logic_vector(X(output_size_g - 1)(output_size_g - 1 downto 0));

end behavioral;
