-- HDL Implementation of an FIR filter
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.fir_signed_array_pkg.all;

entity fir_filter is
  generic (
    data_in_width_g   : integer := 16;
    num_multipliers_g : integer := 16;
    num_taps_g        : integer := 2
    );
  port (
    clk            : in  std_logic;
    rst            : in  std_logic;
    enable         : in  std_logic;
    data_in_valid  : in  std_logic;
    take           : out std_logic;
    data_in        : in  signed(data_in_width_g - 1 downto 0);
    taps_in        : in  fir_signed_array(0 to num_taps_g - 1);
    data_out       : out signed(data_in_width_g + pkg_coefficient_width + integer(ceil(log2(real(num_taps_g)))) - 1 downto 0);
    data_valid_out : out std_logic
    );
end fir_filter;

architecture behavioural of fir_filter is

  -- Calculates the final output size of the FIR filter.
  -- When adding N numbers inputWidthA + inputWidthB + log2(N) gives the number
  -- of extra bits required to store the result.
  constant adder_tree_size_c : integer := data_in_width_g + pkg_coefficient_width + integer(ceil(log2(real(num_taps_g))));

  ------------------------------------------------------------------------------
  -- Operation 1 - Multiply Accumulate Signals
  ------------------------------------------------------------------------------
  -- Given the number of multiplier units available, calculate the number of
  -- clock cycles required to perform multiply accumulate (MAC) operation on all
  -- taps and inputs.
  constant stages_c : integer := integer(ceil(real(num_taps_g) / real(num_multipliers_g)));

  -- Calculate the size needed for each of the MAC units. When adding N numbers
  -- inputWidthA + inputWidthB +  log2(N) gives the number of extra bits
  -- required to store the result.
  constant accumulator_size_c : integer := data_in_width_g + pkg_coefficient_width + integer(ceil(log2(real(stages_c))));

  -- Calculate the size of the buffer that is the smallest multiple of
  -- num_multipliers_g that is larger than or equal to num_taps_g.
  constant buffer_size_c : integer := stages_c * num_multipliers_g;

  -- Array to store delayed input data.
  type delay_array_t is array(natural range <>) of signed(data_in_width_g - 1 downto 0);
  signal delay_pipeline : delay_array_t(0 to buffer_size_c - 1) := (others => (others => '0'));

  -- Array to store taps.
  type taps_array_t is array(natural range <>) of signed(pkg_coefficient_width - 1 downto 0);
  signal taps_pipeline : taps_array_t(0 to buffer_size_c - 1) := (others => (others => '0'));

  -- Array to store output of multiplier units
  type multiplier_result_array_t is array(natural range <>) of signed(data_in_width_g + pkg_coefficient_width - 1 downto 0);
  signal multiplier_result : multiplier_result_array_t(0 to num_multipliers_g - 1) := (others => (others => '0'));

  -- Array to store accumulator results
  type accumulator_array_t is array(natural range <>) of signed(accumulator_size_c - 1 downto 0);
  signal accumulator : accumulator_array_t(0 to num_multipliers_g - 1) := (others => (others => '0'));

  -- Points to the current delayed input value for each stage of the FIR
  -- operation.
  signal current_delay : delay_array_t(0 to num_multipliers_g - 1) := (others => (others => '0'));

  -- Points to the current tap value for each stage of the FIR operation.
  signal current_tap : taps_array_t(0 to num_multipliers_g - 1) := (others => (others => '0'));

  -- Indicates a stage 0 input into the MAC units, so that the accumulators
  -- can be reest at this point.
  signal clear_accumulator   : std_logic;
  signal clear_accumulator_r : std_logic;

  -- Indicates that the MAC operation is done. Used to trigger the serial
  -- adder process.
  signal mac_ready : std_logic;

  -- Keeps track of the current stage of the serial adder process
  signal stage_counter : integer range 0 to stages_c - 1 := (stages_c - 1);

  ------------------------------------------------------------------------------
  -- Operation 2 - Serialised Adder Signals
  ------------------------------------------------------------------------------
  -- Work out how many adders are required to add up the accumulator for each
  -- multiplier within stages_c clock cycles.
  constant num_serial_adders_c : integer := integer(ceil(real(num_multipliers_g) / real(stages_c)));

  -- Given the number of adders, work out the actual number of clock cycles
  -- required.
  constant serial_adder_stages_c : integer := integer(ceil(real(num_multipliers_g) / real(num_serial_adders_c)));

  -- Calculate length of buffer to hold the expanded accumulator results.
  constant serial_adder_buffer_size_c : integer := serial_adder_stages_c * num_serial_adders_c;

  -- Buffer to hold input and results of serial adders.
  type serial_adder_array_t is array(natural range <>) of signed(adder_tree_size_c - 1 downto 0);
  signal serial_adder_buffer : serial_adder_array_t(0 to serial_adder_buffer_size_c - 1) := (others => (others => '0'));

  -- Keeps track of current stage within the serial adder
  signal adder_stage_counter : integer range 0 to serial_adder_stages_c - 1 := 0;

  -- Points to the current input value for each stage of the serialised
  -- addition operation.
  signal current_adder_input : serial_adder_array_t(0 to num_serial_adders_c - 1) := (others => (others => '0'));

  ------------------------------------------------------------------------------
  -- Operation 3 - Parallel Adder Tree Signals
  ------------------------------------------------------------------------------
  -- To add N numbers together using two input adders takes log2(N) stages.
  constant adder_tree_stages_c : integer := integer(ceil(log2(real(num_serial_adders_c))));

  -- Buffer size is the closest power-of-2 that is equal or above the
  -- num_serial_adders_c.
  constant adder_tree_buffer_size_c : integer := 2**adder_tree_stages_c;

  -- Create array wide enough to hold the widest part of the adder tree.
  type adder_tree_array_t is array(0 to adder_tree_buffer_size_c - 1) of signed(adder_tree_size_c - 1 downto 0);
  -- Create array of arrays to form the adder tree. Each stage of the adder
  -- tree will be of fixed length, but only half the number of registers that
  -- were present in the previous stage will be used.
  -- We rely on synthesis to identify and remove these unused registers.
  -- E.g. To add 8 numbers the following register usage pattern would occur
  -- where X indicates an unused register.
  -- Stage 0: 01234567
  -- Stage 1: 0123XXXX
  -- Stage 2: 01XXXXXX
  -- Stage 3: 0XXXXXXX
  type adder_tree_array_array_t is array(0 to adder_tree_stages_c) of adder_tree_array_t;
  signal adder_tree_buffer : adder_tree_array_array_t := (others => (others => (others => '0')));

  ------------------------------------------------------------------------------
  -- Control Signals
  ------------------------------------------------------------------------------
  -- Total input to output delay is:
  -- stages_c
  -- + 2 (for the MAC delay)
  -- + serial_adder_stages_c (for the serial adder)
  -- + adder_tree_stages_c   (for the parallel adder)
  signal data_valid_delay : std_logic_vector(stages_c + 2 + serial_adder_stages_c + adder_tree_stages_c - 1 downto 0);

  -- High when both fir_ready and data_in_valid and high
  signal data_taken : std_logic;

  -- High when the FIR implementation is able to take data
  signal fir_ready : std_logic;

begin
  ------------------------------------------------------------------------------
  -- Data input
  ------------------------------------------------------------------------------
  -- Data taken when FIR is ready for more data, and the input data is valid.
  data_taken <= '1' when fir_ready = '1' and data_in_valid = '1' else '0';

  -- Delay the input valid signal in order to generate an output valid signal.
  valid_pipeline_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        data_valid_delay <= (others => '0');
      elsif enable = '1' then
        data_valid_delay <= data_valid_delay(data_valid_delay'high - 1 downto 0) & data_taken;
      end if;
    end if;
  end process;

  -- Assign taps to taps_pipeline.
  -- When num_multipliers_g is an integer factor of num_taps_g, then taps_in
  -- and taps_pipeline are identical. However, when num_multipliers_g is not
  -- an integer factor of num_taps_g, taps_pipeline is bigger than taps_in
  -- by num_taps_g % num_multipliers_g. The additional values are fixed at a
  -- value of zero. This simplifies the logic as it means that
  -- num_multipliers_length is always an integer factor of the length of
  -- taps_pipeline.
  taps_load_p : process(taps_in)
  begin
    for i in 0 to (num_taps_g - 1) loop
      taps_pipeline(i) <= taps_in(i);
    end loop;
  end process;

  -- Input data delay shift register
  delay_pipeline_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        delay_pipeline <= (others => (others => '0'));
      else
        if fir_ready = '1' and enable = '1' and data_in_valid = '1' then
          delay_pipeline(0) <= data_in;
          for i in 1 to (num_taps_g - 1) loop
            delay_pipeline(i) <= delay_pipeline(i - 1);
          end loop;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Operation 1 - Multiply Accumulate Signals
  ------------------------------------------------------------------------------
  -- Multiply each tap with a delayed version of the input signal.

  -- Generates a mux for each multipler to select the input taps and
  -- delayed value.
  current_stage_mux_p : process(stage_counter, delay_pipeline, taps_pipeline)
  begin
    for i in 0 to (num_multipliers_g - 1) loop
      current_tap(i)   <= taps_pipeline (stage_counter + (stages_c * i));
      current_delay(i) <= delay_pipeline(stage_counter + (stages_c * i));
    end loop;
  end process;

  -- Performs the MAC operation
  multiply_accumulate_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        clear_accumulator_r <= '0';
      else
        if enable = '1' then
          clear_accumulator_r <= clear_accumulator;
          for i in 0 to (num_multipliers_g - 1) loop
            multiplier_result(i) <= current_tap(i) * current_delay(i);
            if clear_accumulator_r = '1' then
              accumulator(i) <= to_signed(0, accumulator_size_c) + multiplier_result(i);
            else
              accumulator(i) <= accumulator(i) + multiplier_result(i);
            end if;
          end loop;
        end if;
      end if;
    end if;
  end process;

  -- Step through each stage once per clock cycle
  -- Used to control multiplexers for MAC inputs
  stage_counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        stage_counter <= stages_c - 1;
      elsif enable = '1' then
        if stage_counter = (stages_c - 1) then
          if data_in_valid = '1' then
            stage_counter <= 0;
          end if;
        else
          stage_counter <= stage_counter + 1;
        end if;
      end if;
    end if;
  end process;

  -- Control signals
  mac_ready         <= data_valid_delay(stages_c + 1);
  clear_accumulator <= '1' when stage_counter = 0              else '0';
  fir_ready         <= '1' when stage_counter = (stages_c - 1) else '0';
  take              <= fir_ready;

  ------------------------------------------------------------------------------
  -- Operation 2 - Serialised adder
  ------------------------------------------------------------------------------
  -- The result of each MAC unit needs to be added up.
  -- When there are multiple clock cycles between the output of the MAC units
  -- the addition can be done serially.

  -- If the addition can be done over multiple clock cycles, then generate the
  -- serialised adder unit.
  serial_adder_gen : if serial_adder_stages_c > 1 generate
    serial_adder_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          if mac_ready = '1' then
            -- Register all the accumulator output data
            for i in 0 to (num_multipliers_g - 1) loop
              serial_adder_buffer(i) <= resize(accumulator(i), adder_tree_size_c);
            end loop;
          else
            -- Create num_serial_adders_c serial adders
            for i in 0 to (num_serial_adders_c - 1) loop
              serial_adder_buffer(i*serial_adder_stages_c) <= serial_adder_buffer(i*serial_adder_stages_c) + current_adder_input(i);
            end loop;
          end if;
        end if;
      end if;
    end process;

    -- Serial adder stage counter
    -- Used to select which inputs are fed into the serial adder.
    serial_adder_stage_counter_p : process(clk)
    begin
      if rising_edge(clk) then
        if rst = '1' then
          adder_stage_counter <= 0;
        elsif enable = '1' then
          if adder_stage_counter = (serial_adder_stages_c - 1) then
            adder_stage_counter <= 0;
          else
            -- Only start counter once mac is ready
            if (mac_ready = '1' and adder_stage_counter = 0) or
              adder_stage_counter /= 0 then
              adder_stage_counter <= adder_stage_counter + 1;
            end if;
          end if;
        end if;
      end if;
    end process;

    -- Select the correct input for each serial adder based on the current
    -- adder stage.
    current_adder_stage_mux_p : process(adder_stage_counter, serial_adder_buffer)
    begin
      for i in 0 to (num_serial_adders_c - 1) loop
        current_adder_input(i) <= serial_adder_buffer(adder_stage_counter + (serial_adder_stages_c * i));
      end loop;
    end process;
  end generate;

  -- If there is only a single clock cycle between MAC outputs, then the
  -- the addition of the MAC outputs must be done in a parallel pipelined
  -- manner. In this process the accumulator outputs are just registered.
  no_serial_adder_gen : if serial_adder_stages_c = 1 generate
    -- Store the accumulator data
    no_serial_adder_p : process(clk)
    begin
      if rising_edge(clk) then
        if enable = '1' then
          if mac_ready = '1' then
            for i in 0 to (num_multipliers_g - 1) loop
              serial_adder_buffer(i) <= resize(accumulator(i), adder_tree_size_c);
            end loop;
          end if;
        end if;
      end if;
    end process;
  end generate;

  ------------------------------------------------------------------------------
  -- Operation 3 - Parallel Adder Tree Signals
  ------------------------------------------------------------------------------
  -- Creates a fully pipelined adder to add an array of numbers.

  -- Connect the first stage of the adder tree to outputs of the serialised
  -- adders.
  adder_tree_input_p : process(serial_adder_buffer)
  begin
    for i in 0 to (num_serial_adders_c - 1) loop
      adder_tree_buffer(0)(i) <= serial_adder_buffer(i*serial_adder_stages_c);
    end loop;
  end process;

  -- If there is more than one serial adder unit, generate the pipelined adder
  -- tree.
  adder_tree_gen : if adder_tree_stages_c > 0 generate
    adder_tree_stage_gen : for stage_gen in 1 to (adder_tree_stages_c) generate
      adder_tree_p : process(clk)
      begin
        if rising_edge(clk) then
          if enable = '1' then
            for i in 0 to ((2**((adder_tree_stages_c - (stage_gen-1)) - 1)) - 1) loop
              adder_tree_buffer(stage_gen)(i) <= adder_tree_buffer(stage_gen-1)(2*i) + adder_tree_buffer(stage_gen-1)((2*i) + 1);
            end loop;
          end if;
        end if;
      end process;
    end generate;
  end generate;

  -- Output signals
  data_valid_out <= data_valid_delay(data_valid_delay'high);
  data_out       <= adder_tree_buffer(adder_tree_stages_c)(0);

end behavioural;
