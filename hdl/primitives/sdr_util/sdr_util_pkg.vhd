-- Utilities Primitive Package
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This package enables VHDL code to instantiate all entities and modules in
-- this library.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

package sdr_util is

  component counter_updown
  generic (
    counter_size_g : integer := 8
  );
  port (
    clk                 : in std_logic;
    reset               : in std_logic;
    enable              : in std_logic;
    direction           : in std_logic;
    load_trig           : in std_logic;
    load_value          : in unsigned(counter_size_g - 1 downto 0);
    length              : in unsigned(counter_size_g - 1 downto 0);
    count_out           : out unsigned(counter_size_g - 1 downto 0)
  );
  end component;

end package sdr_util;
