.. complex_multiplier documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _complex_multiplier-primitive:


Complex multiplier (``complex_multiplier``)
===========================================
Multiplies together two complex values.

Design
------
The mathematical representation of the implementation is given in :eq:`complex_multiplier-equation`.

.. math::
   :label: complex_multiplier-equation

   z[n] = x[n] * y[n]

In :eq:`complex_multiplier-equation`:

 * :math:`x[n]` and :math:`y[n]` are the input values.

 * :math:`z[n]` is the output values.

A block diagram representation of the implementation is given in :numref:`complex_multiplier-diagram`.

.. _complex_multiplier-diagram:

.. figure:: complex_multiplier.svg
   :alt: Block diagram of complex multiplier implementation.
   :align: center

   Complex multiplier implementation.

Implementation
--------------
The complex multiplication is done as four multiplications, as show in :eq:`complex_multiplication-equation`.

.. math::
   :label: complex_multiplication-equation

   z[n] = \left( \operatorname{Re}(x[n])\operatorname{Re}(y[n]) - \operatorname{Im}(x[n])\operatorname{Im}(y[n]) ) + \\
          i( \operatorname{Re}(x[n])\operatorname{Im}(y[n]) + \operatorname{Im}(x[n]) \operatorname{Re}(y[n]) \right)

This primitive is fully pipelined with one cycle latency.

The output can be optionally right shifted by setting the ``bit_drop_g`` generic. This is useful when it is desired that the inputs and outputs are the same width.

Interface
---------

Generics
~~~~~~~~

 * ``input_size_g`` (``integer``): Input bit width of the complex multiplier.

 * ``output_size_g`` (``integer``): Output bit width of the complex multiplier.

 * ``bit_drop_g`` (``integer``): Number of places to right shift the result of the multiplier by before connecting it to the output port.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. Primitive enabled when high.

 * ``a_real`` (``std_logic_vector``, ``input_size_g`` bits), in: :math:`\operatorname{Re}(a)` input, signed.

 * ``a_imag`` (``std_logic_vector``, ``input_size_g`` bits), in: :math:`\operatorname{Im}(a)` input, signed.

 * ``b_real`` (``std_logic_vector``, ``input_size_g`` bits), in: :math:`\operatorname{Re}(b)` input, signed.

 * ``b_imag`` (``std_logic_vector``, ``input_size_g`` bits), in: :math:`\operatorname{Im}(b)` input, signed.

 * ``p_real`` (``std_logic_vector``, ``output_size_g`` bits), out: :math:`\operatorname{Re}(y)` output, signed.

 * ``p_imag`` (``std_logic_vector``, ``output_size_g`` bits), out: :math:`\operatorname{Im}(y)` output, signed.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``complex_multiplier`` are:

 * None.
