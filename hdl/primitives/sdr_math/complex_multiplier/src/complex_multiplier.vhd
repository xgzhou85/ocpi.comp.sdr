-- HDL Implementation of a complex multiplier.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

-------------------------------------------------------------------------------
-- Signed two input Complex Multiplier
-- The module is fully pipelined with one cycle latency.
-- Function: p_real+j(p_imag) = [a_real+j(a_imag)]*[b_real+j(b_imag)]
-------------------------------------------------------------------------------
entity complex_multiplier is
  generic (
    input_size_g  : integer := 12;
    output_size_g : integer := 26;
    bit_drop_g    : integer := 0        -- Number of output bits to drop
    );
  port (
    clk    : in  std_logic;
    rst    : in  std_logic;
    enable : in  std_logic;
    a_real : in  std_logic_vector(input_size_g - 1 downto 0);   -- (Signed)
    a_imag : in  std_logic_vector(input_size_g - 1 downto 0);   -- (Signed)
    b_real : in  std_logic_vector(input_size_g - 1 downto 0);   -- (Signed)
    b_imag : in  std_logic_vector(input_size_g - 1 downto 0);   -- (Signed)
    p_real : out std_logic_vector(output_size_g - 1 downto 0);  -- (Signed)
    p_imag : out std_logic_vector(output_size_g - 1 downto 0)   -- (Signed)
    );
end complex_multiplier;

architecture rtl of complex_multiplier is

  -----------------------------------------------------------------------------
  -- Internal signals
  -----------------------------------------------------------------------------
  signal a_imag_x_b_imag : signed((input_size_g * 2) - 1 downto 0);
  signal a_real_x_b_real : signed((input_size_g * 2) - 1 downto 0);
  signal a_real_x_b_imag : signed((input_size_g * 2) - 1 downto 0);
  signal a_imag_x_b_real : signed((input_size_g * 2) - 1 downto 0);

  signal imag_output : signed((input_size_g * 2) downto 0);
  signal real_output : signed((input_size_g * 2) downto 0);

  signal real_output_slv : std_logic_vector((input_size_g * 2) downto 0);
  signal imag_output_slv : std_logic_vector((input_size_g * 2) downto 0);

begin

  -----------------------------------------------------------------------------
  -- Complex Multiplier
  -----------------------------------------------------------------------------
  -- Performs: (a + bi)*(c + di) = (ac - bd) + (ad + bc)i
  complex_multi_p : process(clk)
  begin

    if rising_edge(clk) then
      if rst = '1' then
        a_imag_x_b_imag <= (others => '0');
        a_real_x_b_real <= (others => '0');
        a_real_x_b_imag <= (others => '0');
        a_imag_x_b_real <= (others => '0');
        real_output     <= (others => '0');
        imag_output     <= (others => '0');
      else
        if enable = '1' then

          -- ac
          a_real_x_b_real <= signed(a_real) * signed(b_real);
          -- bd
          a_imag_x_b_imag <= signed(a_imag) * signed(b_imag);
          -- ad
          a_real_x_b_imag <= signed(a_real) * signed(b_imag);
          -- bc
          a_imag_x_b_real <= signed(a_imag) * signed(b_real);

          -- ac - bd
          real_output <= resize(a_real_x_b_real, real_output'length) -
                         resize(a_imag_x_b_imag, real_output'length);
          -- ad + bc
          imag_output <= resize(a_real_x_b_imag, imag_output'length) +
                         resize(a_imag_x_b_real, imag_output'length);

        end if;
      end if;
    end if;

  end process;

  -----------------------------------------------------------------------------
  -- Resize output
  -----------------------------------------------------------------------------
  real_output_slv <= std_logic_vector(real_output);
  imag_output_slv <= std_logic_vector(imag_output);

  -- Remove bit_drop_g LSBs in order to scale result to fit in output_size_g.
  -- Equivalent to a divide by 2^bit_drop_g
  p_real <= real_output_slv(output_size_g + bit_drop_g - 1 downto bit_drop_g);
  p_imag <= imag_output_slv(output_size_g + bit_drop_g - 1 downto bit_drop_g);

end rtl;
