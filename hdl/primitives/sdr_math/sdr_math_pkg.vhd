-- Math Primitive Package
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This package enables VHDL code to instantiate all entities and modules
-- in this library.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

package sdr_math is

  component complex_multiplier
    generic (
      input_size_g : integer := 12;
      output_size_g: integer := 26;
      bit_drop_g   : integer := 0
    );
    port (
      clk     : in std_logic;
      rst     : in std_logic;
      enable  : in std_logic;
      a_real  : in std_logic_vector(input_size_g - 1 downto 0);
      a_imag  : in std_logic_vector(input_size_g - 1 downto 0);
      b_real  : in std_logic_vector(input_size_g - 1 downto 0);
      b_imag  : in std_logic_vector(input_size_g - 1 downto 0);
      p_real  : out std_logic_vector(output_size_g - 1 downto 0);
      p_imag  : out std_logic_vector(output_size_g - 1 downto 0)
    );
  end component;

  component exponentiation is
    generic (
      input_size_g  : integer := 32;
      output_size_g : integer := 32;
      base_g        : real    := 10.0
    );
    port (
      clk            : in  std_logic;
      rst            : in  std_logic;
      clk_en         : in  std_logic;
      data_valid_in  : in  std_logic;
      data_in        : in  unsigned(input_size_g - 1 downto 0);
      data_valid_out : out std_logic;
      data_out       : out unsigned(output_size_g - 1 downto 0)
    );
  end component;

  component logarithm is
    generic (
      input_size_g  : integer := 16;
      output_size_g : integer := 16;
      base_g        : real    := 10.0
    );
    port (
      clk            : in  std_logic;
      rst            : in  std_logic;
      clk_en         : in  std_logic;
      data_valid_in  : in  std_logic;
      data_in        : in  unsigned(input_size_g - 1 downto 0);
      data_valid_out : out std_logic;
      data_out       : out signed(output_size_g - 1 downto 0)
    );
  end component;

end package sdr_math;
