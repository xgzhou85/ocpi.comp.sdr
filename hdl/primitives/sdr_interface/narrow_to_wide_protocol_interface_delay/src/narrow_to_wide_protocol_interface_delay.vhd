-- Narrow to wide protocol interface delay primitive.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity narrow_to_wide_protocol_interface_delay is
  generic (
    DELAY_G                 : integer          := 1;   -- Must not be 0
    DATA_IN_WIDTH_G         : integer          := 8;
    DATA_OUT_WIDTH_G        : integer          := 16;  -- Must be a multiple of
                                                       -- DATA_in_WIDTH_G.
    OPCODE_WIDTH_G          : integer          := 3;
    BYTE_ENABLE_WIDTH_G     : integer          := 1;
    PROCESSED_DATA_OPCODE_G : std_logic_vector := "000"
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;  -- High when output is ready
    take_in             : in  std_logic;  -- High when data is taken from input
    processed_stream_in : in  std_logic_vector(DATA_OUT_WIDTH_G - 1 downto 0);
    -- Input interface signals
    input_som           : in  std_logic;
    input_eom           : in  std_logic;
    input_valid         : in  std_logic;
    input_byte_enable   : in  std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
    input_opcode        : in  std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
    input_data          : in  std_logic_vector(DATA_IN_WIDTH_G - 1 downto 0);
    -- Output interface signals
    output_som          : out std_logic;
    output_eom          : out std_logic;
    output_valid        : out std_logic;
    output_give         : out std_logic;
    output_byte_enable  : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
    output_opcode       : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
    output_data         : out std_logic_vector(DATA_OUT_WIDTH_G - 1 downto 0)
    );
end narrow_to_wide_protocol_interface_delay;

architecture rtl of narrow_to_wide_protocol_interface_delay is

  constant inputs_per_output_c : integer := DATA_OUT_WIDTH_G / DATA_IN_WIDTH_G;

  type state_t is (passthrough_message_s, join_message_s);

  type byte_enable_array_t is array (DELAY_G - 1 downto 0) of std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
  type opcode_array_t is array (DELAY_G - 1 downto 0) of std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
  type data_array_t is array (DELAY_G - 1 downto 0) of std_logic_vector(DATA_IN_WIDTH_G - 1 downto 0);

  -- Interface delay registers
  signal input_register_take        : std_logic_vector(DELAY_G - 1 downto 0);
  signal input_register_som         : std_logic_vector(DELAY_G - 1 downto 0);
  signal input_register_eom         : std_logic_vector(DELAY_G - 1 downto 0);
  signal input_register_valid       : std_logic_vector(DELAY_G - 1 downto 0);
  signal input_register_byte_enable : byte_enable_array_t;
  signal input_register_opcode      : opcode_array_t;
  signal input_register_data        : data_array_t;

  -- Interface delay registers
  signal shifted_data             : std_logic_vector(DATA_OUT_WIDTH_G - DATA_IN_WIDTH_G - 1 downto 0);
  signal som, som_r               : std_logic;
  signal eom, eom_r               : std_logic;
  -- Control signals
  signal other_data_message_valid : std_logic;
  signal input_word_counter       : unsigned(2 downto 0);
  signal current_state            : state_t;

begin

  -- High when valid non-stream data is detected
  other_data_message_valid <= '1' when
                              input_register_valid(input_register_valid'high) = '1' and
                              input_register_opcode(input_register_opcode'high) /= PROCESSED_DATA_OPCODE_G
                              else '0';

  -- When joining input words, SOM/EOM are set if any of the input words
  -- present in the output word are SOM/EOM
  som <= som_r or input_register_som(input_register_som'high);
  eom <= eom_r or input_register_eom(input_register_eom'high);

  -- Add delay to align data with respective flow control signals
  delay_pipeline_1_gen : if DELAY_G = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          input_register_take <= (others => '0');
        -- Other registers don't need to be reset as gated by
        -- input_register_take.
        elsif(enable = '1') then
          input_register_take(0)        <= take_in;
          input_register_som(0)         <= input_som;
          input_register_eom(0)         <= input_eom;
          input_register_valid(0)       <= input_valid;
          input_register_byte_enable(0) <= input_byte_enable;
          input_register_opcode(0)      <= input_opcode;
          input_register_data(0)        <= input_data;
        end if;
      end if;
    end process;
  end generate;

  delay_pipeline_2_plus_gen : if DELAY_G > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          input_register_take <= (others => '0');
        -- Other registers don't need to be reset as gated by
        -- input_register_take
        elsif(enable = '1') then
          input_register_take        <= input_register_take(DELAY_G - 2 downto 0) & take_in;
          input_register_som         <= input_register_som(DELAY_G - 2 downto 0) & input_som;
          input_register_eom         <= input_register_eom(DELAY_G - 2 downto 0) & input_eom;
          input_register_valid       <= input_register_valid(DELAY_G - 2 downto 0) & input_valid;
          input_register_byte_enable <= input_register_byte_enable(DELAY_G - 2 downto 0) & input_byte_enable;
          input_register_opcode      <= input_register_opcode(DELAY_G - 2 downto 0) & input_opcode;
          input_register_data        <= input_register_data(DELAY_G - 2 downto 0) & input_data;
        end if;
      end if;
    end process;
  end generate;

  interface_state_machine_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        current_state <= passthrough_message_s;
      elsif (enable = '1' and input_register_take(input_register_take'high) = '1' and other_data_message_valid = '1') then
        -- Shift input data into data register. LSBs arrive over input
        -- interface first.
        shifted_data <= input_register_data(input_register_data'high) & shifted_data(shifted_data'high downto DATA_IN_WIDTH_G);
        -- State machine
        case current_state is
          when passthrough_message_s =>
            current_state      <= join_message_s;
            som_r              <= input_register_som(input_register_som'high);
            eom_r              <= input_register_eom(input_register_eom'high);
            input_word_counter <= to_unsigned(inputs_per_output_c - 2, input_word_counter'length);
          when join_message_s =>
            -- When all input words are received, send message and go back to
            -- passthrough state
            if input_word_counter = 0 then
              current_state <= passthrough_message_s;
            end if;
            -- som/eom are an OR of som_r/eom_r and the som/eom from input
            som_r              <= som;
            eom_r              <= eom;
            -- Update number of remaining shifts
            input_word_counter <= input_word_counter - 1;
        end case;
      end if;
    end if;
  end process;

  output_mux_p : process(current_state, input_register_valid, input_register_som,
                         input_register_eom, input_register_byte_enable,
                         input_register_opcode, input_register_take,
                         input_register_data, enable, processed_stream_in,
                         input_word_counter, som, eom, shifted_data,
                         other_data_message_valid)
  begin
    -- Always just pass through the opcode
    output_opcode <= input_register_opcode(input_register_opcode'high);
    -- All other signals depend on current_state
    if (current_state = passthrough_message_s) then
      -- Don't assert give if a valid non-stream message is received as this
      -- will be output on a future clock cycle once the message has been
      -- reconstructed.
      output_give        <= input_register_take(input_register_take'high) and enable and not other_data_message_valid;
      -- In passthrough condition, pass all signals through directly
      output_valid       <= input_register_valid(input_register_valid'high);
      output_som         <= input_register_som(input_register_som'high);
      output_eom         <= input_register_eom(input_register_eom'high);
      output_byte_enable <= input_register_byte_enable(input_register_byte_enable'high);
      -- In this state always output the processed data as non-stream
      -- opcode data will never be passed through directly.
      output_data        <= processed_stream_in;
    else
      -- Give when all input words have been received
      if (input_word_counter = 0 and input_register_take(input_register_take'high) = '1' and
          other_data_message_valid = '1' and enable = '1') then
        output_give <= '1';
      else
        output_give <= '0';
      end if;
      -- Message is always valid as only valid messages will cause this state
      -- to be entered
      output_valid       <= '1';
      output_byte_enable <= (others => '1');
      output_som         <= som;
      output_eom         <= eom;
      output_data        <= input_register_data(input_register_data'high) & shifted_data;
    end if;
  end process;

end rtl;
