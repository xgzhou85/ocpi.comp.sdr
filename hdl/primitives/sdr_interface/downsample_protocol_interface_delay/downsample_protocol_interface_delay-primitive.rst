.. downsample_protocol_interface_delay documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _downsample_protocol_interface_delay-primitive:


Protocol interface delay (Down-sample) (``downsample_protocol_interface_delay``)
================================================================================
Delays OpenCPI interface signals to align them with a processed data signal. Handles timestamp correction to maintain timestamp alignment with down-sampled data.

Design
------
When data processing is performed on a stream of input data it typically results in a valid output one or more clock cycles after the input. This primitive delays OpenCPI interface signals in order to align them with a processed data stream. When the input opcode is equal to ``PROCESSED_DATA_OPCODE_G`` the processed data stream is output instead of the the delayed input data.

This primitive differs from the :ref:`protocol interface delay primitive <protocol_interface_delay-primitive>` as it also handles realignment of timestamp messages when the sample the timestamp is referring to is removed due to a down-sampling operation.

Implementation
--------------
A shift register of length ``DELAY_G`` is constructed for each of the interface signals (``SOM`` (start of message), ``EOM`` (end of message), ``valid``, ``byte_enable``, ``opcode`` and ``data``). A shift register of length ``DELAY_G`` is also constructed for the interface ``take`` signal to keep track of when data was actually taken from the input interface.

The ``DELAY_G`` value is set to the same length as the delay present between the input signal and the processed data. As such, at the end of the shift registers, the interface signals are correctly aligned with the processed data. A multiplexer is used to select either the processed data signal or the delayed input signal. Processed data is selected when the delayed ``opcode`` signal is equal to ``PROCESSED_DATA_OPCODE_G`` and the delayed ``valid`` signal is ``1``. The output ``give`` signal is driven whenever the delayed version of the interface ``take`` signal is ``1`` and the output interface is ready to accept data.

If the output interface is not ready to accept data then the delay shift registers are not advanced. This means for every input signal an output signal is generated after a delay of ``DELAY_G`` clock cycles where the output is ready to accept data.

Timestamp messages are suppressed until the next data sample so that it can be determined if the timestamp related to a sample that is removed due to the down-sampling operation. If the sample is not removed, the timestamp is passed on unmodified. If the sample is removed the timestamp is incremented by the sample interval for each new data sample into the module (that is not output due to the down-sampling operation). When a data sample is output from the module the adjusted timestamp is inserted just before the data sample.


Interface
---------

Generics
~~~~~~~~

 * ``DELAY_G`` (``integer``): Sets the length of the interface delay (in clock cycles) to be added.

 * ``DATA_WIDTH_G`` (``integer``): Sets the width of the interface data signal.

 * ``OPCODE_WIDTH_G`` (``integer``): Sets the width of the interface opcode signal.

 * ``BYTE_ENABLE_WIDTH_G`` (``integer``): Sets the width of the interface byte enable signal.

 * ``PROCESSED_DATA_OPCODE_G`` (``std_logic_vector``): Sets the value of the opcode for which the ``processed_stream_in`` signal should be used for the output data signal instead of the delayed input data signal.

 * ``TIME_OPCODE_G`` (``std_logic_vector``): Sets the value of the opcode which indicates that the message contains a timestamp

 * ``SAMPLE_INTERVAL_OPCODE_G`` (``std_logic_vector``): Sets the value of the opcode which indicates that the message contains the sample interval.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. Primitive enabled when high.

 * ``take_in`` (``std_logic``), in: High when data is taken from the input interface.

 * ``input_hold_out`` (``std_logic``), out: When high data processing should be stopped and data should not be taken from the input interface. This indicates that the primitive is generating data, i.e. an adjusted timestamp.

 * ``processed_stream_in`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), in: The processed data stream. i.e. the data this primitive is aligning the interface signals with.

 * ``processed_mask_in`` (``std_logic``), in: Set to '1' when the processed data stream is discarded due to the down-sampling operation.

 * ``input_som`` (``std_logic``), in: SOM signal from OpenCPI interface.

 * ``input_eom`` (``std_logic``), in: EOM signal from OpenCPI interface.

 * ``input_valid`` (``std_logic``), in: Valid signal from OpenCPI interface.

 * ``input_byte_enable`` (``std_logic_vector``, ``BYTE_ENABLE_WIDTH_G`` bits), in: Byte enable signal from OpenCPI interface.

 * ``input_opcode`` (``std_logic_vector``, ``OPCODE_WIDTH_G`` bits), in: Opcode signal from OpenCPI interface.

 * ``input_data`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), in: Data signal from OpenCPI interface.

 * ``output_som`` (``std_logic``), out: SOM signal on output OpenCPI interface.

 * ``output_eom`` (``std_logic``), out: EOM signal on output OpenCPI interface.

 * ``output_valid`` (``std_logic``), out: Valid signal on output OpenCPI interface.

 * ``output_give`` (``std_logic``), out: Give signal on output OpenCPI interface.

 * ``output_byte_enable`` (``std_logic_vector``, ``BYTE_ENABLE_WIDTH_G`` bits), out: Byte enable signal on output OpenCPI interface.

 * ``output_opcode`` (``std_logic_vector``, ``OPCODE_WIDTH_G`` bits), out: Opcode signal on output OpenCPI interface.

 * ``output_data`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), out: Data signal on output OpenCPI interface.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Timestamp recovery primitive <timestamp_recovery-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``downsample_protocol_interface_delay`` are:

 * ``DELAY_G`` must be greater than zero.
