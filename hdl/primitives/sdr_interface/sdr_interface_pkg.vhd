-- Interface Primitive Package
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This package enables VHDL code to instantiate all entities and modules in
-- this library.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

package sdr_interface is

  component timestamp_recovery
    generic (
      DATA_IN_WIDTH_G : integer := 8;
      ADDER_WIDTH_G   : integer := 32
      );
    port (
      clk                          : in  std_logic;
      reset                        : in  std_logic;
      enable                       : in  std_logic;
      timestamp_valid_in           : in  std_logic;
      sample_interval_valid_in     : in  std_logic;
      data_valid_in                : in  std_logic;
      data_in                      : in  std_logic_vector(DATA_IN_WIDTH_G - 1 downto 0);
      timestamp_units_out          : out std_logic_vector(31 downto 0);
      timestamp_fraction_out       : out std_logic_vector(63 downto 0);
      sample_interval_units_out    : out std_logic_vector(31 downto 0);
      sample_interval_fraction_out : out std_logic_vector(63 downto 0)
      );
  end component;

  component protocol_interface_delay is
    generic (
      DELAY_G                 : integer          := 1;
      DATA_WIDTH_G            : integer          := 8;
      OPCODE_WIDTH_G          : integer          := 3;
      BYTE_ENABLE_WIDTH_G     : integer          := 1;
      PROCESSED_DATA_MUX_G    : std_logic        := '1';
      PROCESSED_DATA_OPCODE_G : std_logic_vector := "000"
      );
    port (
      clk                 : in  std_logic;
      reset               : in  std_logic;
      enable              : in  std_logic;
      take_in             : in  std_logic;
      processed_stream_in : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
      input_som           : in  std_logic;
      input_eom           : in  std_logic;
      input_valid         : in  std_logic;
      input_byte_enable   : in  std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      input_opcode        : in  std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      input_data          : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
      output_som          : out std_logic;
      output_eom          : out std_logic;
      output_valid        : out std_logic;
      output_give         : out std_logic;
      output_byte_enable  : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      output_opcode       : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      output_data         : out std_logic_vector(DATA_WIDTH_G - 1 downto 0)
      );
  end component;

  component wide_to_narrow_protocol_interface_delay is
    generic (
      DELAY_G                 : integer          := 1;
      DATA_IN_WIDTH_G         : integer          := 16;
      DATA_OUT_WIDTH_G        : integer          := 8;
      OPCODE_WIDTH_G          : integer          := 3;
      BYTE_ENABLE_WIDTH_G     : integer          := 1;
      PROCESSED_DATA_OPCODE_G : std_logic_vector := "000"
      );
    port (
      clk                 : in  std_logic;
      reset               : in  std_logic;
      enable              : in  std_logic;
      take_in             : in  std_logic;
      input_hold_out      : out std_logic;
      processed_stream_in : in  std_logic_vector(DATA_OUT_WIDTH_G - 1 downto 0);
      input_som           : in  std_logic;
      input_eom           : in  std_logic;
      input_valid         : in  std_logic;
      input_byte_enable   : in  std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      input_opcode        : in  std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      input_data          : in  std_logic_vector(DATA_IN_WIDTH_G - 1 downto 0);
      output_som          : out std_logic;
      output_eom          : out std_logic;
      output_valid        : out std_logic;
      output_give         : out std_logic;
      output_byte_enable  : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      output_opcode       : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      output_data         : out std_logic_vector(DATA_OUT_WIDTH_G - 1 downto 0)
      );
  end component;

  component narrow_to_wide_protocol_interface_delay is
    generic (
      DELAY_G                 : integer          := 1;
      DATA_IN_WIDTH_G         : integer          := 8;
      DATA_OUT_WIDTH_G        : integer          := 16;
      OPCODE_WIDTH_G          : integer          := 3;
      BYTE_ENABLE_WIDTH_G     : integer          := 1;
      PROCESSED_DATA_OPCODE_G : std_logic_vector := "000"
      );
    port (
      clk                 : in  std_logic;
      reset               : in  std_logic;
      enable              : in  std_logic;
      take_in             : in  std_logic;
      processed_stream_in : in  std_logic_vector(DATA_OUT_WIDTH_G - 1 downto 0);
      input_som           : in  std_logic;
      input_eom           : in  std_logic;
      input_valid         : in  std_logic;
      input_byte_enable   : in  std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      input_opcode        : in  std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      input_data          : in  std_logic_vector(DATA_IN_WIDTH_G - 1 downto 0);
      output_som          : out std_logic;
      output_eom          : out std_logic;
      output_valid        : out std_logic;
      output_give         : out std_logic;
      output_byte_enable  : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      output_opcode       : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      output_data         : out std_logic_vector(DATA_OUT_WIDTH_G - 1 downto 0)
      );
  end component;

  component downsample_protocol_interface_delay is
    generic (
      DELAY_G                  : integer          := 1;
      DATA_WIDTH_G             : integer          := 8;
      OPCODE_WIDTH_G           : integer          := 3;
      BYTE_ENABLE_WIDTH_G      : integer          := 1;
      PROCESSED_DATA_OPCODE_G  : std_logic_vector := "000";
      TIME_OPCODE_G            : std_logic_vector := "001";
      SAMPLE_INTERVAL_OPCODE_G : std_logic_vector := "010"
      );
    port (
      clk                 : in  std_logic;
      reset               : in  std_logic;
      enable              : in  std_logic;
      take_in             : in  std_logic;
      input_hold_out      : out std_logic;
      processed_stream_in : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
      processed_mask_in   : in  std_logic;
      input_som           : in  std_logic;
      input_eom           : in  std_logic;
      input_valid         : in  std_logic;
      input_byte_enable   : in  std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      input_opcode        : in  std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      input_data          : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
      output_som          : out std_logic;
      output_eom          : out std_logic;
      output_valid        : out std_logic;
      output_give         : out std_logic;
      output_byte_enable  : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      output_opcode       : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      output_data         : out std_logic_vector(DATA_WIDTH_G - 1 downto 0)
      );
  end component;

  component protocol_interface_generator is
    generic (
      DELAY_G                 : integer          := 1;
      DATA_WIDTH_G            : integer          := 8;
      OPCODE_WIDTH_G          : integer          := 3;
      BYTE_ENABLE_WIDTH_G     : integer          := 1;
      PROCESSED_DATA_OPCODE_G : std_logic_vector := "000";
      DISCONTINUITY_OPCODE_G  : std_logic_vector := "100"
      );
    port (
      clk                   : in  std_logic;
      reset                 : in  std_logic;
      output_ready          : in  std_logic;
      discontinuity_trigger : in  std_logic;
      generator_enable      : in  std_logic;
      generator_reset       : in  std_logic;
      processed_stream_in   : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
      message_length        : in  unsigned(15 downto 0);
      output_hold           : out std_logic;
      output_som            : out std_logic;
      output_eom            : out std_logic;
      output_valid          : out std_logic;
      output_give           : out std_logic;
      output_byte_enable    : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      output_opcode         : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      output_data           : out std_logic_vector(DATA_WIDTH_G - 1 downto 0)
      );
  end component;

  component flush_inserter is
    generic (
      DATA_WIDTH_G         : integer          := 8;
      OPCODE_WIDTH_G       : integer          := 3;
      BYTE_ENABLE_WIDTH_G  : integer          := 1;
      MAX_MESSAGE_LENGTH_G : integer          := 2048;
      FLUSH_OPCODE_G       : std_logic_vector := "011";
      DATA_OPCODE_G        : std_logic_vector := "000"
      );
    port (
      clk                : in  std_logic;
      reset              : in  std_logic;
      flush_length       : in  unsigned;
      take_in            : in  std_logic;
      take_out           : out std_logic;
      input_som          : in  std_logic;
      input_eom          : in  std_logic;
      input_valid        : in  std_logic;
      input_byte_enable  : in  std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      input_opcode       : in  std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      input_data         : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
      input_ready        : in  std_logic;
      output_som         : out std_logic;
      output_eom         : out std_logic;
      output_valid       : out std_logic;
      output_byte_enable : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      output_opcode      : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      output_data        : out std_logic_vector(DATA_WIDTH_G - 1 downto 0);
      output_ready       : out std_logic
      );
  end component;

  component message_length_limiter is
    generic (
      DATA_WIDTH_G         : integer          := 8;
      OPCODE_WIDTH_G       : integer          := 3;
      BYTE_ENABLE_WIDTH_G  : integer          := 1;
      MAX_MESSAGE_LENGTH_G : integer          := 2048;
      LIMIT_OPCODE_G       : std_logic_vector := "000"
      );
    port (
      clk                        : in  std_logic;
      reset                      : in  std_logic;
      enable                     : in  std_logic;
      output_som                 : in  std_logic;
      output_eom                 : in  std_logic;
      output_valid               : in  std_logic;
      output_give                : in  std_logic;
      output_byte_enable         : in  std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      output_opcode              : in  std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      output_data                : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
      output_limited_som         : out std_logic;
      output_limited_eom         : out std_logic;
      output_limited_valid       : out std_logic;
      output_limited_give        : out std_logic;
      output_limited_byte_enable : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
      output_limited_opcode      : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
      output_limited_data        : out std_logic_vector(DATA_WIDTH_G - 1 downto 0)
      );
  end component;

end package sdr_interface;
